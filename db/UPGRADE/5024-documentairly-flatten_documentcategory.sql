BEGIN;

ALTER TABLE file_metadata ADD COLUMN document_category VARCHAR(150);
-- ALTER TABLE file_metadata DROP COLUMN document_category_parent;
-- ALTER TABLE file_metadata DROP COLUMN document_category_child;

COMMIT;
