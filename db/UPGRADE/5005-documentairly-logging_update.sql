BEGIN;

CREATE INDEX ON logging USING btree (component) WHERE component IS NOT NULL;
CREATE INDEX ON logging USING btree (component_id) WHERE component_id IS NOT NULL;

COMMIT;
