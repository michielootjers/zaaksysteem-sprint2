BEGIN;

alter table zaaktype_sjablonen add bibliotheek_kenmerken_id integer;


ALTER TABLE ONLY zaaktype_sjablonen
    ADD CONSTRAINT zaaktype_sjablonen_bibliotheek_kenmerken_id_fkey FOREIGN KEY (bibliotheek_kenmerken_id) REFERENCES bibliotheek_kenmerken(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;
    


update zaaktype_relatie set eigenaar_type = 'aanvrager' where eigenaar_type is null;    
alter table zaaktype_relatie alter eigenaar_type set not null;
alter table zaaktype_relatie alter eigenaar_type set default 'aanvrager';

alter table zaaktype_relatie add required varchar(12);

alter table zaaktype_kenmerken add referential varchar(12);

COMMIT;