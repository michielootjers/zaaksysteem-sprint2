BEGIN;

DROP TABLE checklist_antwoord;
DROP TABLE checklist_vraag;

-- This sets the highest phase of a case type as the required completion-state for subcases when no required state has been set yet due to legacy reasons. (You probably need to read that a few times to get it, sorry.)
UPDATE
  zaaktype_relatie
SET
  required = sq.count FROM (
      SELECT
        zs.zaaktype_node_id, count(distinct zs.id)
      FROM
        zaaktype_status zs,
        zaaktype_relatie zr
     WHERE
        zs.zaaktype_node_id = zr.zaaktype_node_id
    GROUP BY zs.zaaktype_node_id
  ) AS sq
WHERE
  zaaktype_relatie.zaaktype_node_id = sq.zaaktype_node_id
  AND relatie_type = 'deelzaak'
  AND (required IS NULL or required = '');

UPDATE
  zaaktype_resultaten
SET
  selectielijst = (
    SELECT
      def.selectielijst
    FROM
      zaaktype_definitie def
      INNER JOIN zaaktype_node ON zaaktype_node.zaaktype_definitie_id = def.id
    WHERE zaaktype_node.id = zaaktype_resultaten.zaaktype_node_id
  );

COMMIT;
