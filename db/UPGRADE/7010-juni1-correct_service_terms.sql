-- MAKE SURE THIS ONE RUNS ONLY ONCE...

BEGIN;

update zaaktype_definitie set afhandeltermijn=servicenorm, servicenorm=afhandeltermijn, afhandeltermijn_type=servicenorm_type, servicenorm_type=afhandeltermijn_type  where servicenorm != afhandeltermijn OR servicenorm_type != afhandeltermijn_type;

COMMIT;
