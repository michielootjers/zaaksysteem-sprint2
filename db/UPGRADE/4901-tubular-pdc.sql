ALTER TABLE kennisbank_producten ADD COLUMN omschrijving_internal text;
ALTER TABLE kennisbank_producten ADD COLUMN voorwaarden_internal text;
ALTER TABLE kennisbank_producten ADD COLUMN aanpak_internal text;
ALTER TABLE kennisbank_producten ADD COLUMN kosten_internal text;
ALTER TABLE kennisbank_producten ADD COLUMN external boolean;
ALTER TABLE kennisbank_producten ADD COLUMN internal boolean;
ALTER TABLE kennisbank_producten ADD COLUMN publication character varying(32);
ALTER TABLE kennisbank_producten ADD COLUMN coupling character varying(32);

