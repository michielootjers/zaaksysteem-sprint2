BEGIN;

ALTER TABLE zaak_meta ADD COLUMN stalled_since timestamp without time zone;

COMMIT;
