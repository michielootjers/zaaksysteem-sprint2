-- Column: contact_info_intake

-- ALTER TABLE zaaktype_node DROP COLUMN contact_info_intake;

ALTER TABLE zaaktype_node ADD COLUMN contact_info_intake boolean;
ALTER TABLE zaaktype_node ALTER COLUMN contact_info_intake SET DEFAULT true;

