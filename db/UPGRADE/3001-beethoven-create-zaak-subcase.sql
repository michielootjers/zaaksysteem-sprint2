BEGIN;

DROP TABLE IF EXISTS zaak_subcase;
CREATE TABLE zaak_subcase (
    id serial,
    zaak_id integer not null,
    zaaktype_relatie_id integer not null,
    relation_zaak_id integer,
    required integer,
    relatie_type text,
    eigenaar_type text,
    kopieren_kenmerken integer,
    start_delay character varying(255),
    delay_type character varying(255),
    ou_id integer,
    role_id integer
);



ALTER TABLE ONLY zaak_subcase
    ADD CONSTRAINT zaak_subcase_zaak_id_fkey FOREIGN KEY (zaak_id) REFERENCES zaak(id);

ALTER TABLE ONLY zaak_subcase
    ADD CONSTRAINT zaak_subcase_relation_zaak_id_fkey FOREIGN KEY (relation_zaak_id) REFERENCES zaak(id);

ALTER TABLE ONLY zaak_subcase
    ADD CONSTRAINT zaak_subcase_zaaktype_relatie_id_fkey FOREIGN KEY (zaaktype_relatie_id) REFERENCES zaaktype_relatie(id);

ALTER TABLE ONLY zaak_subcase ADD CONSTRAINT zaak_subcase_pkey PRIMARY KEY (id);
COMMIT;



ALTER TABLE zaaktype_kenmerken ADD publish_public INTEGER;
