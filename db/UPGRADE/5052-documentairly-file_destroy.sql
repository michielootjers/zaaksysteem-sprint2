BEGIN;

ALTER TABLE file ADD COLUMN destroyed boolean default 'f';

COMMIT;
