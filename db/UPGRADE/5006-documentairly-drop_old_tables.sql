BEGIN;

DROP TABLE documenten;
DROP TABLE documents_mail;
DROP TABLE documents;
DROP TABLE filestore_old;
DROP TABLE dropped_documents;

COMMIT;
