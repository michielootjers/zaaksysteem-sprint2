-- Table: checklist

-- DROP TABLE checklist;

CREATE TABLE checklist
(
  id serial NOT NULL,
  case_id integer NOT NULL,
  case_milestone integer,
  CONSTRAINT checklist_pkey PRIMARY KEY (id ),
  CONSTRAINT checklist_case_id_fkey FOREIGN KEY (case_id)
      REFERENCES zaak (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

-- Table: checklist_item

-- DROP TABLE checklist_item;

CREATE TABLE checklist_item
(
  id serial NOT NULL,
  checklist_id integer NOT NULL,
  label text,
  state boolean NOT NULL DEFAULT false,
  sequence integer,
  user_defined boolean NOT NULL DEFAULT true,
  deprecated_answer character varying(8),
  CONSTRAINT checklist_item_pkey PRIMARY KEY (id ),
  CONSTRAINT checklist_item_checklist_id_fkey FOREIGN KEY (checklist_id)
      REFERENCES checklist (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);


-- Table: zaaktype_status_checklist_item

-- DROP TABLE zaaktype_status_checklist_item;

CREATE TABLE zaaktype_status_checklist_item
(
  id serial NOT NULL,
  casetype_status_id integer NOT NULL,
  label text,
  sequence integer,
  CONSTRAINT zaaktype_checklist_item_pkey PRIMARY KEY (id ),
  CONSTRAINT zaaktype_status_checklist_item_zaaktype_status_id_fkey FOREIGN KEY (casetype_status_id)
      REFERENCES zaaktype_status (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

