ALTER TABLE natuurlijk_persoon ADD COLUMN system_of_record character varying(32);
ALTER TABLE natuurlijk_persoon ADD COLUMN system_of_record_id integer;

ALTER TABLE bedrijf ADD COLUMN system_of_record character varying(32);
ALTER TABLE bedrijf ADD COLUMN system_of_record_id integer;
