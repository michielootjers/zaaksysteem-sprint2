BEGIN;

ALTER TABLE contactmoment_email ADD COLUMN body TEXT NOT NULL;
ALTER TABLE contactmoment_email ADD COLUMN subject VARCHAR(150) NOT NULL;
ALTER TABLE contactmoment_email ADD COLUMN recipient VARCHAR(150) NOT NULL;
ALTER TABLE contactmoment_email DROP COLUMN body_preview;

COMMIT;
