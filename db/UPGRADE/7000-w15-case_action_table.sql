-- Table: case_action

-- DROP TABLE case_action;

CREATE TABLE case_action
(
  id SERIAL,
  case_id integer NOT NULL,
  casetype_status_id integer,
  type character varying(64),
  label character varying(255),
  automatic boolean,
  data text,
  CONSTRAINT case_actions_pkey PRIMARY KEY (id),
  CONSTRAINT case_actions_case_id_fkey FOREIGN KEY (case_id)
      REFERENCES zaak (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT case_actions_casetype_status_id_fkey FOREIGN KEY (casetype_status_id)
      REFERENCES zaaktype_status (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

