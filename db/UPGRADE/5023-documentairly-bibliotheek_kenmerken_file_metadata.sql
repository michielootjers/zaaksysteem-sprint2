BEGIN;

ALTER TABLE bibliotheek_kenmerken ADD COLUMN file_metadata_id INTEGER REFERENCES file_metadata(id);

COMMIT;
