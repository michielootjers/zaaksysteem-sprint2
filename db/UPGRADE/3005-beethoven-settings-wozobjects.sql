begin;
create table woz_objects (
	id serial,
	owner varchar(255),
	object_data text
);

create table settings (
	id serial,
	key varchar(255),
	value text
);


ALTER TABLE ONLY settings
    ADD CONSTRAINT settings_pkey PRIMARY KEY (id);

commit;
