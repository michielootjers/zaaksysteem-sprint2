begin;

alter table zaak_subcase drop zaaktype_relatie_id;
alter table zaak_subcase drop relatie_type;
alter table zaak_subcase drop eigenaar_type;
alter table zaak_subcase drop kopieren_kenmerken;
alter table zaak_subcase drop start_delay;
alter table zaak_subcase drop delay_type;
alter table zaak_subcase drop ou_id;
alter table zaak_subcase drop role_id;

commit;
