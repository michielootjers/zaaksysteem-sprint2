--
-- PostgreSQL database dump
--

SET SESSION AUTHORIZATION zaaksysteem;

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

--
-- Name: zaaksysteem_bag_types; Type: TYPE; Schema: public; Owner: zaakadmins
--

CREATE TYPE zaaksysteem_bag_types AS ENUM (
    'nummeraanduiding',
    'verblijfsobject',
    'pand',
    'openbareruimte'
);


ALTER TYPE public.zaaksysteem_bag_types OWNER TO zaakadmins;

--
-- Name: zaaksysteem_status; Type: TYPE; Schema: public; Owner: zaakadmins
--

CREATE TYPE zaaksysteem_status AS ENUM (
    'new',
    'open',
    'resolved',
    'stalled',
    'deleted',
    'overdragen'
);


ALTER TYPE public.zaaksysteem_status OWNER TO zaakadmins;

--
-- Name: zaaksysteem_trigger; Type: TYPE; Schema: public; Owner: zaakadmins
--

CREATE TYPE zaaksysteem_trigger AS ENUM (
    'extern',
    'intern'
);


ALTER TYPE public.zaaksysteem_trigger OWNER TO zaakadmins;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: adres; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE adres (
    id integer NOT NULL,
    straatnaam character varying(80),
    huisnummer smallint,
    huisletter character(1),
    huisnummertoevoeging character varying(4),
    nadere_aanduiding character varying(35),
    postcode character varying(6),
    woonplaats character varying(75),
    gemeentedeel character varying(75),
    functie_adres character(1),
    datum_aanvang_bewoning date,
    woonplaats_id character varying(32),
    gemeente_code smallint,
    hash character varying(32),
    import_datum timestamp(6) without time zone,
    deleted_on timestamp(6) without time zone
);


ALTER TABLE public.adres OWNER TO zaakadmins;

--
-- Name: adres_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE adres_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.adres_id_seq OWNER TO zaakadmins;

--
-- Name: adres_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zaakadmins
--

ALTER SEQUENCE adres_id_seq OWNED BY adres.id;


--
-- Name: bag_ligplaats; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE bag_ligplaats (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14) NOT NULL,
    einddatum character varying(14),
    officieel character varying(1) NOT NULL,
    status character varying(80) NOT NULL,
    hoofdadres character varying(16) NOT NULL,
    inonderzoek character varying(1) NOT NULL,
    documentdatum character varying(14) NOT NULL,
    documentnummer character varying(20) NOT NULL,
    correctie character varying(1) NOT NULL
);


ALTER TABLE public.bag_ligplaats OWNER TO zaakadmins;

--
-- Name: TABLE bag_ligplaats; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON TABLE bag_ligplaats IS '55 : een ligplaats is een formeel door de gemeenteraad als zodanig aangewezen plaats in het water, al dan niet aangevuld met een op de oever aanwezig terrein of een gedeelte daarvan, dat bestemd is voor het permanent afmeren van een voor woon-, bedrijfsmatige- of recreatieve doeleinden geschikt vaartuig.';


--
-- Name: COLUMN bag_ligplaats.identificatie; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_ligplaats.identificatie IS '58.01 : de unieke aanduiding van een ligplaats.';


--
-- Name: COLUMN bag_ligplaats.begindatum; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_ligplaats.begindatum IS '58.91 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een ligplaats een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_ligplaats.einddatum; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_ligplaats.einddatum IS '58.92 : de einddatum van een periode waarin er geen wijzigingen hebben plaatsgevonden in de gegevens die worden bijgehouden over een ligplaats.';


--
-- Name: COLUMN bag_ligplaats.officieel; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_ligplaats.officieel IS '58.02 : een aanduiding waarmee kan worden aangegeven dat een object in de registratie is opgenomen als gevolg van een feitelijke constatering, zonder dat er op het moment van opname sprake is van een formele grondslag voor deze opname.';


--
-- Name: COLUMN bag_ligplaats.status; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_ligplaats.status IS '58.03 : de fase van de levenscyclus van een ligplaats, waarin de betreffende ligplaats zich bevindt.';


--
-- Name: COLUMN bag_ligplaats.hoofdadres; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_ligplaats.hoofdadres IS '58:10 : de identificatiecode nummeraanduiding waaronder het hoofdadres van een ligplaats, dat in het kader van de basis gebouwen registratie als zodanig is aangemerkt, is opgenomen in de basis registratie adressen.';


--
-- Name: COLUMN bag_ligplaats.inonderzoek; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_ligplaats.inonderzoek IS '58.93 : een aanduiding waarmee wordt aangegeven dat een onderzoek wordt uitgevoerd naar de juistheid van een of meerdere gegevens van het betreffende object.';


--
-- Name: COLUMN bag_ligplaats.documentdatum; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_ligplaats.documentdatum IS '58.97 : de datum waarop het brondocument is vastgesteld, op basis waarvan een opname, mutatie of een in de historie plaatsen van gegevens ten aanzien van een ligplaats heeft plaatsgevonden.';


--
-- Name: COLUMN bag_ligplaats.documentnummer; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_ligplaats.documentnummer IS '58.98 : de unieke aanduiding van het brondocument op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een ligplaats heeft plaatsgevonden, binnen een gemeente.';


--
-- Name: COLUMN bag_ligplaats.correctie; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_ligplaats.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_ligplaats_nevenadres; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE bag_ligplaats_nevenadres (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14) NOT NULL,
    nevenadres character varying(16) NOT NULL,
    correctie character varying(1) NOT NULL
);


ALTER TABLE public.bag_ligplaats_nevenadres OWNER TO zaakadmins;

--
-- Name: TABLE bag_ligplaats_nevenadres; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON TABLE bag_ligplaats_nevenadres IS 'koppeltabel voor nevenadressen bij ligplaats';


--
-- Name: COLUMN bag_ligplaats_nevenadres.identificatie; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_ligplaats_nevenadres.identificatie IS '58.01 : de unieke aanduiding van een ligplaats.';


--
-- Name: COLUMN bag_ligplaats_nevenadres.begindatum; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_ligplaats_nevenadres.begindatum IS '58.91 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een ligplaats een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_ligplaats_nevenadres.nevenadres; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_ligplaats_nevenadres.nevenadres IS '58.11 : de identificatiecodes nummeraanduiding waaronder nevenadressen van een ligplaats, die in het kader van de basis gebouwen registratie als zodanig zijn aangemerkt, zijn opgenomen in de basis registratie adressen.';


--
-- Name: COLUMN bag_ligplaats_nevenadres.correctie; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_ligplaats_nevenadres.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_nummeraanduiding; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE bag_nummeraanduiding (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14) NOT NULL,
    einddatum character varying(14),
    huisnummer integer NOT NULL,
    officieel character varying(1) NOT NULL,
    huisletter character varying(1),
    huisnummertoevoeging character varying(4),
    postcode character varying(6),
    woonplaats character varying(4),
    inonderzoek character varying(1) NOT NULL,
    openbareruimte character varying(16) NOT NULL,
    type character varying(20) NOT NULL,
    documentdatum character varying(14) NOT NULL,
    documentnummer character varying(20) NOT NULL,
    status character varying(80) NOT NULL,
    correctie character varying(1) NOT NULL
);


ALTER TABLE public.bag_nummeraanduiding OWNER TO zaakadmins;

--
-- Name: TABLE bag_nummeraanduiding; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON TABLE bag_nummeraanduiding IS '11.2 : een nummeraanduiding is een door de gemeenteraad als zodanig toegekende aanduiding van een adresseerbaar object.';


--
-- Name: COLUMN bag_nummeraanduiding.identificatie; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_nummeraanduiding.identificatie IS '11.02 : de unieke aanduiding van een nummeraanduiding.';


--
-- Name: COLUMN bag_nummeraanduiding.begindatum; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_nummeraanduiding.begindatum IS '11.62 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een nummeraanduiding een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_nummeraanduiding.einddatum; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_nummeraanduiding.einddatum IS '11.63 : de einddatum van een periode waarin er geen wijzigingen hebben plaatsgevonden in de gegevens die worden bijgehouden over een nummeraanduiding.';


--
-- Name: COLUMN bag_nummeraanduiding.huisnummer; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_nummeraanduiding.huisnummer IS '11.20 : een door of namens het gemeentebestuur ten aanzien van een adresseerbaar object toegekende nummering.';


--
-- Name: COLUMN bag_nummeraanduiding.officieel; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_nummeraanduiding.officieel IS '11.21 : een aanduiding waarmee kan worden aangegeven dat een object in de registratie is opgenomen als gevolg van een feitelijke constatering, zonder dat er op het moment van opname sprake is van een formele grondslag voor deze opname.';


--
-- Name: COLUMN bag_nummeraanduiding.huisletter; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_nummeraanduiding.huisletter IS '11.30 : een door of namens het gemeentebestuur ten aanzien van  een adresseerbaar object toegekende toevoeging aan een huisnummer in de vorm van een alfanumeriek teken.';


--
-- Name: COLUMN bag_nummeraanduiding.huisnummertoevoeging; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_nummeraanduiding.huisnummertoevoeging IS '11.40 : een door of namens het gemeentebestuur ten aanzien van  een adresseerbaar object toegekende nadere toevoeging aan een huisnummer of een combinatie van huisnummer en huisletter.';


--
-- Name: COLUMN bag_nummeraanduiding.postcode; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_nummeraanduiding.postcode IS '11.60 : de door tnt post vastgestelde code behorende bij een bepaalde combinatie van een straatnaam en een huisnummer.';


--
-- Name: COLUMN bag_nummeraanduiding.woonplaats; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_nummeraanduiding.woonplaats IS '11.61 : unieke aanduiding van de woonplaats waarbinnen het object waaraan de nummeraanduiding is toegekend is gelegen.';


--
-- Name: COLUMN bag_nummeraanduiding.inonderzoek; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_nummeraanduiding.inonderzoek IS '11.64 : een aanduiding waarmee wordt aangegeven dat een onderzoek wordt uitgevoerd naar de juistheid van een of meerdere gegevens van het betreffende object.';


--
-- Name: COLUMN bag_nummeraanduiding.openbareruimte; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_nummeraanduiding.openbareruimte IS '11.65 : de unieke aanduiding van een openbare ruimte waaraan een adresseerbaar object is gelegen.';


--
-- Name: COLUMN bag_nummeraanduiding.type; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_nummeraanduiding.type IS '11.66 : de aard van een als zodanig benoemde nummeraanduiding.';


--
-- Name: COLUMN bag_nummeraanduiding.documentdatum; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_nummeraanduiding.documentdatum IS '11.67 : de datum waarop het brondocument is vastgesteld, op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een nummeraanduiding heeft plaatsgevonden.';


--
-- Name: COLUMN bag_nummeraanduiding.documentnummer; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_nummeraanduiding.documentnummer IS '11.68 : de unieke aanduiding van het brondocument op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een nummeraanduiding heeft plaatsgevonden, binnen een gemeente.';


--
-- Name: COLUMN bag_nummeraanduiding.status; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_nummeraanduiding.status IS '11.69 : de fase van de levenscyclus van een nummeraanduiding, waarin de betreffende nummeraanduiding zich bevindt.';


--
-- Name: COLUMN bag_nummeraanduiding.correctie; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_nummeraanduiding.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_openbareruimte; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE bag_openbareruimte (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14) NOT NULL,
    einddatum character varying(14),
    naam character varying(80) NOT NULL,
    officieel character varying(1) NOT NULL,
    woonplaats character varying(4),
    type character varying(40) NOT NULL,
    inonderzoek character varying(1) NOT NULL,
    documentdatum character varying(14) NOT NULL,
    documentnummer character varying(20) NOT NULL,
    status character varying(80) NOT NULL,
    correctie character varying(1) NOT NULL
);


ALTER TABLE public.bag_openbareruimte OWNER TO zaakadmins;

--
-- Name: TABLE bag_openbareruimte; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON TABLE bag_openbareruimte IS '11.1 : een openbare ruimte is een door de gemeenteraad als zodanig aangewezen benaming van een binnen een woonplaats gelegen buitenruimte.';


--
-- Name: COLUMN bag_openbareruimte.identificatie; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_openbareruimte.identificatie IS '11.01 : de unieke aanduiding van een openbare ruimte.';


--
-- Name: COLUMN bag_openbareruimte.begindatum; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_openbareruimte.begindatum IS '11.12 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een openbare ruimte een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_openbareruimte.einddatum; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_openbareruimte.einddatum IS '11.13 : de einddatum van een periode waarin er geen wijzigingen hebben plaatsgevonden in de gegevens die worden bijgehouden over een openbare ruimte.';


--
-- Name: COLUMN bag_openbareruimte.naam; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_openbareruimte.naam IS '11.10 : een naam die aan een openbare ruimte is toegekend in een daartoe strekkend formeel gemeentelijk besluit.';


--
-- Name: COLUMN bag_openbareruimte.officieel; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_openbareruimte.officieel IS '11.11 : een aanduiding waarmee kan worden aangegeven dat een object in de registratie is opgenomen als gevolg van een feitelijke constatering, zonder dat er op het moment van opname sprake is van een formele grondslag voor deze opname.';


--
-- Name: COLUMN bag_openbareruimte.woonplaats; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_openbareruimte.woonplaats IS '11.15 : unieke aanduiding van de woonplaats waarbinnen een openbare ruimte is gelegen.';


--
-- Name: COLUMN bag_openbareruimte.type; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_openbareruimte.type IS '11.16 : de aard van de als zodanig benoemde openbare ruimte.';


--
-- Name: COLUMN bag_openbareruimte.inonderzoek; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_openbareruimte.inonderzoek IS '11.14 : een aanduiding waarmee wordt aangegeven dat een onderzoek wordt uitgevoerd naar de juistheid van een of meerdere gegevens van het betreffende object.';


--
-- Name: COLUMN bag_openbareruimte.documentdatum; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_openbareruimte.documentdatum IS '11.17 : de datum waarop het brondocument is vastgesteld, op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een openbare ruimte heeft plaatsgevonden.';


--
-- Name: COLUMN bag_openbareruimte.documentnummer; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_openbareruimte.documentnummer IS '11.18 : de unieke aanduiding van het brondocument op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een openbare ruimte heeft plaatsgevonden, binnen een gemeente.';


--
-- Name: COLUMN bag_openbareruimte.status; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_openbareruimte.status IS '11.19 : de fase van de levenscyclus van een openbare ruimte, waarin de betreffende openbare ruimte zich bevindt.';


--
-- Name: COLUMN bag_openbareruimte.correctie; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_openbareruimte.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_pand; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE bag_pand (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14) NOT NULL,
    einddatum character varying(14),
    officieel character varying(1) NOT NULL,
    bouwjaar integer NOT NULL,
    status character varying(80) NOT NULL,
    inonderzoek character varying(1) NOT NULL,
    documentdatum character varying(14) NOT NULL,
    documentnummer character varying(20) NOT NULL,
    correctie character varying(1) NOT NULL
);


ALTER TABLE public.bag_pand OWNER TO zaakadmins;

--
-- Name: TABLE bag_pand; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON TABLE bag_pand IS '55 : een pand is de kleinste, bij de totstandkoming functioneel en bouwkundig constructief zelfstandige eenheid, die direct en duurzaam met de aarde is verbonden.';


--
-- Name: COLUMN bag_pand.identificatie; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_pand.identificatie IS '55.01 : de unieke aanduiding van een pand';


--
-- Name: COLUMN bag_pand.begindatum; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_pand.begindatum IS '55.91 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een pand een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_pand.einddatum; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_pand.einddatum IS '55.92 : de einddatum van een periode waarin er geen wijzigingen hebben plaatsgevonden in de gegevens die worden bijgehouden over een pand.';


--
-- Name: COLUMN bag_pand.officieel; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_pand.officieel IS '55.02 : een aanduiding waarmee kan worden aangegeven dat een object in de registratie is opgenomen als gevolg van een feitelijke constatering, zonder dat er op het moment van opname sprake is van een formele grondslag voor deze opname';


--
-- Name: COLUMN bag_pand.bouwjaar; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_pand.bouwjaar IS '55.30 : de aanduiding van het jaar waarin een pand oorspronkelijk als bouwkundig gereed is opgeleverd.';


--
-- Name: COLUMN bag_pand.status; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_pand.status IS '55.31 : de fase van de levenscyclus van een pand, waarin het betreffende pand zich bevindt.';


--
-- Name: COLUMN bag_pand.inonderzoek; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_pand.inonderzoek IS '55.93 : een aanduiding waarmee wordt aangegeven dat een onderzoek wordt uitgevoerd naar de juistheid van een of meerdere gegevens van het betreffende object.';


--
-- Name: COLUMN bag_pand.documentdatum; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_pand.documentdatum IS '55.97 : de datum waarop het brondocument is vastgesteld, op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een pand heeft plaatsgevonden.';


--
-- Name: COLUMN bag_pand.documentnummer; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_pand.documentnummer IS '55.98 : de unieke aanduiding van het brondocument op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een pand heeft plaatsgevonden, binnen een gemeente.';


--
-- Name: COLUMN bag_pand.correctie; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_pand.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_standplaats; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE bag_standplaats (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14) NOT NULL,
    einddatum character varying(14),
    officieel character varying(1) NOT NULL,
    status character varying(80) NOT NULL,
    hoofdadres character varying(16) NOT NULL,
    inonderzoek character varying(1) NOT NULL,
    documentdatum character varying(14) NOT NULL,
    documentnummer character varying(20) NOT NULL,
    correctie character varying(1) NOT NULL
);


ALTER TABLE public.bag_standplaats OWNER TO zaakadmins;

--
-- Name: TABLE bag_standplaats; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON TABLE bag_standplaats IS '57 : een standplaats is een formeel door de gemeenteraad als zodanig aangewezen terrein of een gedeelte daarvan, dat bestemd is voor het permanent plaatsen van een niet direct en duurzaam met de aarde verbonden en voor woon -, bedrijfsmatige - of recreatieve doeleinden geschikte ruimte.';


--
-- Name: COLUMN bag_standplaats.identificatie; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_standplaats.identificatie IS '57.01 : de unieke aanduiding van een standplaats.';


--
-- Name: COLUMN bag_standplaats.begindatum; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_standplaats.begindatum IS '57.91 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een standplaats een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_standplaats.einddatum; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_standplaats.einddatum IS '57.92 : de einddatum van een periode waarin er geen wijzigingen hebben plaatsgevonden in de gegevens die worden bijgehouden over een standplaats.';


--
-- Name: COLUMN bag_standplaats.officieel; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_standplaats.officieel IS '57.02 : een aanduiding waarmee kan worden aangegeven dat een object in de registratie is opgenomen als gevolg van een feitelijke constatering, zonder dat er op het moment van opname sprake is van een formele grondslag voor deze opname.';


--
-- Name: COLUMN bag_standplaats.status; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_standplaats.status IS '57.03 : de fase van de levenscyclus van een standplaats, waarin de betreffende standplaats zich bevindt.';


--
-- Name: COLUMN bag_standplaats.hoofdadres; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_standplaats.hoofdadres IS '57:10 : de identificatiecode nummeraanduiding waaronder het hoofdadres van een standplaats, dat in het kader van de basis gebouwen registratie als zodanig is aangemerkt, is opgenomen in de basis registratie adressen.';


--
-- Name: COLUMN bag_standplaats.inonderzoek; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_standplaats.inonderzoek IS '57.93 : een aanduiding waarmee wordt aangegeven dat een onderzoek wordt uitgevoerd naar de juistheid van een of meerdere gegevens van het betreffende object.';


--
-- Name: COLUMN bag_standplaats.documentdatum; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_standplaats.documentdatum IS '57.97 : de datum waarop het brondocument is vastgesteld, op basis waarvan een opname, mutatie of een in de historie plaatsen van gegevens ten aanzien van een standplaats heeft plaatsgevonden.';


--
-- Name: COLUMN bag_standplaats.documentnummer; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_standplaats.documentnummer IS '57.98 : de unieke aanduiding van het brondocument op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een standplaats heeft plaatsgevonden, binnen een gemeente.';


--
-- Name: COLUMN bag_standplaats.correctie; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_standplaats.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_verblijfsobject; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE bag_verblijfsobject (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14) NOT NULL,
    einddatum character varying(14),
    officieel character varying(1) NOT NULL,
    hoofdadres character varying(16) NOT NULL,
    oppervlakte integer NOT NULL,
    status character varying(80) NOT NULL,
    inonderzoek character varying(1) NOT NULL,
    documentdatum character varying(14) NOT NULL,
    documentnummer character varying(20) NOT NULL,
    correctie character varying(1) NOT NULL
);


ALTER TABLE public.bag_verblijfsobject OWNER TO zaakadmins;

--
-- Name: TABLE bag_verblijfsobject; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON TABLE bag_verblijfsobject IS '56 : een verblijfsobject is de kleinste binnen een of meerdere panden gelegen en voor woon -, bedrijfsmatige - of recreatieve doeleinden geschikte eenheid van gebruik, die ontsloten wordt via een eigen toegang vanaf de openbare weg, een erf of een gedeelde verkeersruimte en die onderwerp kan zijn van rechtshandelingen.';


--
-- Name: COLUMN bag_verblijfsobject.identificatie; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_verblijfsobject.identificatie IS '56.01 : de unieke aanduiding van een verblijfsobject';


--
-- Name: COLUMN bag_verblijfsobject.begindatum; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_verblijfsobject.begindatum IS '56.91 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een verblijfsobject een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_verblijfsobject.einddatum; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_verblijfsobject.einddatum IS '56.92 : de einddatum van een periode waarin er geen wijzigingen hebben plaatsgevonden in de gegevens die worden bijgehouden over een verblijfsobject.';


--
-- Name: COLUMN bag_verblijfsobject.officieel; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_verblijfsobject.officieel IS '56.02 : een aanduiding waarmee kan worden aangegeven dat een object in de registratie is opgenomen als gevolg van een feitelijke constatering, zonder dat er op het moment van opname sprake is van een formele grondslag voor deze opname';


--
-- Name: COLUMN bag_verblijfsobject.hoofdadres; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_verblijfsobject.hoofdadres IS '56:10 : de identificatiecode nummeraanduiding waaronder het hoofdadres van een verblijfsobject, dat in het kader van de basis gebouwen registratie als zodanig is aangemerkt, is opgenomen in de basis registratie adressen.';


--
-- Name: COLUMN bag_verblijfsobject.oppervlakte; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_verblijfsobject.oppervlakte IS '56.31 : de gebruiksoppervlakte van een verblijfsobject in gehele vierkante meters.';


--
-- Name: COLUMN bag_verblijfsobject.status; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_verblijfsobject.status IS '56.32 : de fase van de levenscyclus van een verblijfsobject, waarin het betreffende verblijfsobject zich bevindt.';


--
-- Name: COLUMN bag_verblijfsobject.inonderzoek; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_verblijfsobject.inonderzoek IS '56.93 : een aanduiding waarmee wordt aangegeven dat een onderzoek wordt uitgevoerd naar de juistheid van een of meerdere gegevens van het betreffende object.';


--
-- Name: COLUMN bag_verblijfsobject.documentdatum; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_verblijfsobject.documentdatum IS '56.97 : de datum waarop het brondocument is vastgesteld, op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een verblijfsobject heeft plaatsgevonden.';


--
-- Name: COLUMN bag_verblijfsobject.documentnummer; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_verblijfsobject.documentnummer IS '56.98 : de unieke aanduiding van het brondocument op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een verblijfsobject heeft plaatsgevonden, binnen een gemeente.';


--
-- Name: COLUMN bag_verblijfsobject.correctie; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_verblijfsobject.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_verblijfsobject_gebruiksdoel; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE bag_verblijfsobject_gebruiksdoel (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14) NOT NULL,
    gebruiksdoel character varying(80) NOT NULL,
    correctie character varying(1) NOT NULL
);


ALTER TABLE public.bag_verblijfsobject_gebruiksdoel OWNER TO zaakadmins;

--
-- Name: TABLE bag_verblijfsobject_gebruiksdoel; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON TABLE bag_verblijfsobject_gebruiksdoel IS 'koppeltabel voor gebruiksdoelen bij verblijfsobject';


--
-- Name: COLUMN bag_verblijfsobject_gebruiksdoel.identificatie; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_verblijfsobject_gebruiksdoel.identificatie IS '56.01 : de unieke aanduiding van een verblijfsobject';


--
-- Name: COLUMN bag_verblijfsobject_gebruiksdoel.begindatum; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_verblijfsobject_gebruiksdoel.begindatum IS '56.91 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een verblijfsobject een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_verblijfsobject_gebruiksdoel.gebruiksdoel; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_verblijfsobject_gebruiksdoel.gebruiksdoel IS '56.30 : een categorisering van de gebruiksdoelen van het betreffende verblijfsobject, zoals dit  formeel door de overheid als zodanig is toegestaan.';


--
-- Name: COLUMN bag_verblijfsobject_gebruiksdoel.correctie; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_verblijfsobject_gebruiksdoel.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_verblijfsobject_pand; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE bag_verblijfsobject_pand (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14) NOT NULL,
    pand character varying(16) NOT NULL,
    correctie character varying(1) NOT NULL
);


ALTER TABLE public.bag_verblijfsobject_pand OWNER TO zaakadmins;

--
-- Name: TABLE bag_verblijfsobject_pand; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON TABLE bag_verblijfsobject_pand IS 'koppeltabel voor panden bij verblijfsobject';


--
-- Name: COLUMN bag_verblijfsobject_pand.identificatie; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_verblijfsobject_pand.identificatie IS '56.01 : de unieke aanduiding van een verblijfsobject';


--
-- Name: COLUMN bag_verblijfsobject_pand.begindatum; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_verblijfsobject_pand.begindatum IS '56.91 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een verblijfsobject een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_verblijfsobject_pand.pand; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_verblijfsobject_pand.pand IS '56.90 : de unieke aanduidingen van de panden waarvan het verblijfsobject onderdeel uitmaakt.';


--
-- Name: COLUMN bag_verblijfsobject_pand.correctie; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_verblijfsobject_pand.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_woonplaats; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE bag_woonplaats (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14) NOT NULL,
    einddatum character varying(14),
    officieel character varying(1) NOT NULL,
    naam character varying(80) NOT NULL,
    status character varying(80) NOT NULL,
    inonderzoek character varying(1) NOT NULL,
    documentdatum character varying(14) NOT NULL,
    documentnummer character varying(20) NOT NULL,
    correctie character varying(1) NOT NULL
);


ALTER TABLE public.bag_woonplaats OWNER TO zaakadmins;

--
-- Name: TABLE bag_woonplaats; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON TABLE bag_woonplaats IS '11.7 : een woonplaats is een door de gemeenteraad als zodanig aangewezen gedeelte van het gemeentelijk grondgebied.';


--
-- Name: COLUMN bag_woonplaats.identificatie; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_woonplaats.identificatie IS '11.03 : de landelijk unieke aanduiding van een woonplaats, zoals vastgesteld door de beheerder van de landelijke tabel voor woonplaatsen.';


--
-- Name: COLUMN bag_woonplaats.begindatum; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_woonplaats.begindatum IS '11.73 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een woonplaats een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_woonplaats.einddatum; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_woonplaats.einddatum IS '11.74 : de einddatum van een periode waarin er geen wijzigingen hebben plaatsgevonden in de gegevens die worden bijgehouden over een woonplaats.';


--
-- Name: COLUMN bag_woonplaats.officieel; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_woonplaats.officieel IS '11.72 : een aanduiding waarmee kan worden aangegeven dat een object in de registratie is opgenomen als gevolg van een feitelijke constatering, zonder dat er op het moment van opname sprake is van een formele grondslag voor deze opname.';


--
-- Name: COLUMN bag_woonplaats.naam; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_woonplaats.naam IS '11.70 : de benaming van een door het gemeentebestuur aangewezen woonplaats.';


--
-- Name: COLUMN bag_woonplaats.status; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_woonplaats.status IS '11.79 : de fase van de levenscyclus van een woonplaats, waarin de betreffende woonplaats zich bevindt.';


--
-- Name: COLUMN bag_woonplaats.inonderzoek; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_woonplaats.inonderzoek IS '11.75 : een aanduiding waarmee wordt aangegeven dat een onderzoek wordt uitgevoerd naar de juistheid van een of meerdere gegevens van het betreffende object.';


--
-- Name: COLUMN bag_woonplaats.documentdatum; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_woonplaats.documentdatum IS '11.77 : de datum waarop het brondocument is vastgesteld, op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een woonplaats heeft plaatsgevonden.';


--
-- Name: COLUMN bag_woonplaats.documentnummer; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_woonplaats.documentnummer IS '11.78 : de unieke aanduiding van het brondocument op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een woonplaats heeft plaatsgevonden, binnen een gemeente.';


--
-- Name: COLUMN bag_woonplaats.correctie; Type: COMMENT; Schema: public; Owner: zaakadmins
--

COMMENT ON COLUMN bag_woonplaats.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: searchable; Type: TABLE; Schema: public; Owner: michiel; Tablespace: 
--

CREATE TABLE searchable (
    search_index tsvector,
    search_term text,
    object_type character varying(100),
    searchable_id integer NOT NULL
);


ALTER TABLE public.searchable OWNER TO michiel;

--
-- Name: searchable_searchable_id_seq; Type: SEQUENCE; Schema: public; Owner: michiel
--

CREATE SEQUENCE searchable_searchable_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.searchable_searchable_id_seq OWNER TO michiel;

--
-- Name: searchable_searchable_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: michiel
--

ALTER SEQUENCE searchable_searchable_id_seq OWNED BY searchable.searchable_id;


--
-- Name: bedrijf; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE bedrijf (
    search_index tsvector,
    search_term text,
    object_type character varying(100) DEFAULT 'bedrijf'::character varying,
    id integer NOT NULL,
    dossiernummer character varying(8),
    subdossiernummer character varying(4),
    hoofdvestiging_dossiernummer character varying(8),
    hoofdvestiging_subdossiernummer character varying(4),
    vorig_dossiernummer character varying(8),
    vorig_subdossiernummer character varying(4),
    handelsnaam character varying(45),
    rechtsvorm smallint,
    kamernummer smallint,
    faillisement smallint,
    surseance smallint,
    telefoonnummer character varying(10),
    email character varying(128),
    vestiging_adres character varying(30),
    vestiging_straatnaam character varying(25),
    vestiging_huisnummer character varying(6),
    vestiging_huisnummertoevoeging character varying(12),
    vestiging_postcodewoonplaats character varying(30),
    vestiging_postcode character varying(6),
    vestiging_woonplaats character varying(24),
    correspondentie_adres character varying(30),
    correspondentie_straatnaam character varying(25),
    correspondentie_huisnummer character varying(6),
    correspondentie_huisnummertoevoeging character varying(12),
    correspondentie_postcodewoonplaats character varying(30),
    correspondentie_postcode character varying(6),
    correspondentie_woonplaats character varying(24),
    hoofdactiviteitencode integer,
    nevenactiviteitencode1 integer,
    nevenactiviteitencode2 integer,
    werkzamepersonen integer,
    contact_naam character varying(64),
    contact_aanspreektitel character varying(45),
    contact_voorletters character varying(19),
    contact_voorvoegsel character varying(8),
    contact_geslachtsnaam character varying(95),
    contact_geslachtsaanduiding character varying(1),
    authenticated smallint,
    authenticatedby text,
    fulldossiernummer text,
    import_datum timestamp(6) without time zone,
    deleted_on timestamp(6) without time zone,
    verblijfsobject_id character varying(16)
)
INHERITS (searchable);


ALTER TABLE public.bedrijf OWNER TO zaakadmins;

--
-- Name: bedrijf_authenticatie; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE bedrijf_authenticatie (
    id integer NOT NULL,
    gegevens_magazijn_id integer,
    login integer,
    password character varying(255),
    created timestamp without time zone,
    last_modified timestamp without time zone
);


ALTER TABLE public.bedrijf_authenticatie OWNER TO zaakadmins;

--
-- Name: bedrijf_authenticatie_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE bedrijf_authenticatie_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.bedrijf_authenticatie_id_seq OWNER TO zaakadmins;

--
-- Name: bedrijf_authenticatie_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zaakadmins
--

ALTER SEQUENCE bedrijf_authenticatie_id_seq OWNED BY bedrijf_authenticatie.id;


--
-- Name: bedrijf_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE bedrijf_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.bedrijf_id_seq OWNER TO zaakadmins;

--
-- Name: bedrijf_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zaakadmins
--

ALTER SEQUENCE bedrijf_id_seq OWNED BY bedrijf.id;


--
-- Name: beheer_import; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE beheer_import (
    id integer NOT NULL,
    importtype character varying(256),
    succesvol integer,
    finished timestamp without time zone,
    import_create integer,
    import_update integer,
    error integer,
    error_message text,
    entries integer,
    created timestamp without time zone,
    last_modified timestamp without time zone
);


ALTER TABLE public.beheer_import OWNER TO zaakadmins;

--
-- Name: beheer_import_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE beheer_import_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.beheer_import_id_seq OWNER TO zaakadmins;

--
-- Name: beheer_import_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zaakadmins
--

ALTER SEQUENCE beheer_import_id_seq OWNED BY beheer_import.id;


--
-- Name: beheer_import_log; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE beheer_import_log (
    id integer NOT NULL,
    import_id integer,
    old_data text,
    new_data text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    kolom text,
    identifier text,
    action character varying(255)
);


ALTER TABLE public.beheer_import_log OWNER TO zaakadmins;

--
-- Name: beheer_import_log_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE beheer_import_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.beheer_import_log_id_seq OWNER TO zaakadmins;

--
-- Name: beheer_import_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zaakadmins
--

ALTER SEQUENCE beheer_import_log_id_seq OWNED BY beheer_import_log.id;


--
-- Name: beheer_plugins; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE beheer_plugins (
    id integer NOT NULL,
    label text,
    naam text,
    help text,
    versie text,
    actief integer,
    last_modified timestamp without time zone
);


ALTER TABLE public.beheer_plugins OWNER TO zaakadmins;

--
-- Name: beheer_plugins_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE beheer_plugins_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.beheer_plugins_id_seq OWNER TO zaakadmins;

--
-- Name: beheer_plugins_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zaakadmins
--

ALTER SEQUENCE beheer_plugins_id_seq OWNED BY beheer_plugins.id;


--
-- Name: betrokkene_notes; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE betrokkene_notes (
    id integer NOT NULL,
    betrokkene_exid integer,
    betrokkene_type text,
    betrokkene_from text,
    ntype text,
    subject text,
    message text,
    created timestamp without time zone,
    last_modified timestamp without time zone
);


ALTER TABLE public.betrokkene_notes OWNER TO zaakadmins;

--
-- Name: betrokkene_notes_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE betrokkene_notes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.betrokkene_notes_id_seq OWNER TO zaakadmins;

--
-- Name: betrokkene_notes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zaakadmins
--

ALTER SEQUENCE betrokkene_notes_id_seq OWNED BY betrokkene_notes.id;


--
-- Name: betrokkenen; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE betrokkenen (
    id integer NOT NULL,
    btype integer,
    gm_natuurlijk_persoon_id integer,
    naam text
);


ALTER TABLE public.betrokkenen OWNER TO zaakadmins;

--
-- Name: betrokkenen_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE betrokkenen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.betrokkenen_id_seq OWNER TO zaakadmins;

--
-- Name: betrokkenen_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zaakadmins
--

ALTER SEQUENCE betrokkenen_id_seq OWNED BY betrokkenen.id;


--
-- Name: bibliotheek_categorie; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE bibliotheek_categorie (
    search_index tsvector,
    search_term text,
    object_type character varying(100) DEFAULT 'bibliotheek_categorie'::character varying,
    id integer NOT NULL,
    naam character varying(256),
    label text,
    description text,
    help text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    system integer,
    pid integer
)
INHERITS (searchable);


ALTER TABLE public.bibliotheek_categorie OWNER TO zaakadmins;

--
-- Name: bibliotheek_categorie_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE bibliotheek_categorie_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.bibliotheek_categorie_id_seq OWNER TO zaakadmins;

--
-- Name: bibliotheek_categorie_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zaakadmins
--

ALTER SEQUENCE bibliotheek_categorie_id_seq OWNED BY bibliotheek_categorie.id;


--
-- Name: bibliotheek_kenmerken; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE bibliotheek_kenmerken (
    search_index tsvector,
    search_term text,
    object_type character varying(100) DEFAULT 'bibliotheek_kenmerken'::character varying,
    id integer NOT NULL,
    naam character varying(256),
    value_type text,
    value_default text,
    label text,
    description text,
    help text,
    magic_string text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    bibliotheek_categorie_id integer,
    document_categorie text,
    system integer,
    type_multiple integer,
    deleted timestamp without time zone
)
INHERITS (searchable);


ALTER TABLE public.bibliotheek_kenmerken OWNER TO zaakadmins;

--
-- Name: bibliotheek_kenmerken_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE bibliotheek_kenmerken_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.bibliotheek_kenmerken_id_seq OWNER TO zaakadmins;

--
-- Name: bibliotheek_kenmerken_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zaakadmins
--

ALTER SEQUENCE bibliotheek_kenmerken_id_seq OWNED BY bibliotheek_kenmerken.id;


--
-- Name: bibliotheek_kenmerken_values; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE bibliotheek_kenmerken_values (
    id integer NOT NULL,
    bibliotheek_kenmerken_id integer,
    value text
);


ALTER TABLE public.bibliotheek_kenmerken_values OWNER TO zaakadmins;

--
-- Name: bibliotheek_kenmerken_values_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE bibliotheek_kenmerken_values_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.bibliotheek_kenmerken_values_id_seq OWNER TO zaakadmins;

--
-- Name: bibliotheek_kenmerken_values_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zaakadmins
--

ALTER SEQUENCE bibliotheek_kenmerken_values_id_seq OWNED BY bibliotheek_kenmerken_values.id;


--
-- Name: bibliotheek_sjablonen; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE bibliotheek_sjablonen (
    search_index tsvector,
    search_term text,
    object_type character varying(100) DEFAULT 'bibliotheek_sjablonen'::character varying,
    id integer NOT NULL,
    bibliotheek_categorie_id integer,
    naam character varying(256),
    label text,
    description text,
    help text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    filestore_id integer,
    deleted timestamp without time zone
)
INHERITS (searchable);


ALTER TABLE public.bibliotheek_sjablonen OWNER TO zaakadmins;

--
-- Name: bibliotheek_sjablonen_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE bibliotheek_sjablonen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.bibliotheek_sjablonen_id_seq OWNER TO zaakadmins;

--
-- Name: bibliotheek_sjablonen_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zaakadmins
--

ALTER SEQUENCE bibliotheek_sjablonen_id_seq OWNED BY bibliotheek_sjablonen.id;


--
-- Name: bibliotheek_sjablonen_magic_string; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE bibliotheek_sjablonen_magic_string (
    id integer NOT NULL,
    bibliotheek_sjablonen_id integer,
    value text
);


ALTER TABLE public.bibliotheek_sjablonen_magic_string OWNER TO zaakadmins;

--
-- Name: bibliotheek_sjablonen_magic_string_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE bibliotheek_sjablonen_magic_string_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.bibliotheek_sjablonen_magic_string_id_seq OWNER TO zaakadmins;

--
-- Name: bibliotheek_sjablonen_magic_string_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zaakadmins
--

ALTER SEQUENCE bibliotheek_sjablonen_magic_string_id_seq OWNED BY bibliotheek_sjablonen_magic_string.id;


--
-- Name: checklist_antwoord_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE checklist_antwoord_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.checklist_antwoord_id_seq OWNER TO zaakadmins;

--
-- Name: checklist_antwoord; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE checklist_antwoord (
    zaak_id integer NOT NULL,
    mogelijkheid_id integer,
    antwoord text,
    vraag_id integer,
    id integer DEFAULT nextval('checklist_antwoord_id_seq'::regclass) NOT NULL
);


ALTER TABLE public.checklist_antwoord OWNER TO zaakadmins;

--
-- Name: checklist_vraag; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE checklist_vraag (
    id integer NOT NULL,
    nr integer,
    vraag text,
    vraagtype text,
    zaaktype_node_id integer,
    zaaktype_status_id integer
);


ALTER TABLE public.checklist_vraag OWNER TO zaakadmins;

--
-- Name: checklist_vraag_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE checklist_vraag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.checklist_vraag_id_seq OWNER TO zaakadmins;

--
-- Name: checklist_vraag_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zaakadmins
--

ALTER SEQUENCE checklist_vraag_id_seq OWNED BY checklist_vraag.id;


--
-- Name: contact_data; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE contact_data (
    id integer NOT NULL,
    gegevens_magazijn_id integer,
    betrokkene_type integer,
    mobiel character varying(255),
    telefoonnummer character varying(255),
    email character varying(255),
    created timestamp without time zone,
    last_modified timestamp without time zone
);


ALTER TABLE public.contact_data OWNER TO zaakadmins;

--
-- Name: contact_data_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE contact_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.contact_data_id_seq OWNER TO zaakadmins;

--
-- Name: contact_data_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zaakadmins
--

ALTER SEQUENCE contact_data_id_seq OWNED BY contact_data.id;


--
-- Name: documenten; Type: TABLE; Schema: public; Owner: michiel; Tablespace: 
--

CREATE TABLE documenten (
    id integer NOT NULL,
    folder boolean,
    folder_id integer,
    zaak_id integer,
    betrokkene_dsn text,
    milestone integer,
    naam text,
    zaaktype_kenmerken_id integer,
    document_type text,
    verplicht boolean,
    pip boolean,
    versie integer,
    store_type text,
    filestore_id integer,
    notitie_id integer,
    queue boolean,
    ontvangstdatum timestamp without time zone,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    deleted timestamp without time zone,
    job_id integer
);


ALTER TABLE public.documenten OWNER TO michiel;

--
-- Name: documenten_id_seq; Type: SEQUENCE; Schema: public; Owner: michiel
--

CREATE SEQUENCE documenten_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.documenten_id_seq OWNER TO michiel;

--
-- Name: documenten_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: michiel
--

ALTER SEQUENCE documenten_id_seq OWNED BY documenten.id;


--
-- Name: documents; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE documents (
    search_index tsvector,
    search_term text,
    object_type character varying(100) DEFAULT 'documents'::character varying,
    id integer NOT NULL,
    pid integer,
    zaak_id integer,
    betrokkene text,
    description text,
    filename text,
    filesize integer,
    mimetype text,
    documenttype text,
    category text,
    status integer,
    post_registratie character varying(255),
    verplicht integer,
    catalogus integer,
    zaakstatus integer,
    betrokkene_id text,
    ontvangstdatum timestamp without time zone,
    dagtekeningdatum timestamp without time zone,
    versie integer,
    help text,
    pip boolean,
    private boolean,
    md5 text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    deleted_on timestamp without time zone,
    option_order integer,
    zaaktype_kenmerken_id integer,
    queue integer
)
INHERITS (searchable);


ALTER TABLE public.documents OWNER TO zaakadmins;

--
-- Name: documents_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE documents_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.documents_id_seq OWNER TO zaakadmins;

--
-- Name: documents_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zaakadmins
--

ALTER SEQUENCE documents_id_seq OWNED BY documents.id;


--
-- Name: documents_mail; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE documents_mail (
    id integer NOT NULL,
    document_id integer,
    rcpt text,
    message text,
    subject text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    option_order integer
);


ALTER TABLE public.documents_mail OWNER TO zaakadmins;

--
-- Name: documents_mail_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE documents_mail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.documents_mail_id_seq OWNER TO zaakadmins;

--
-- Name: documents_mail_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zaakadmins
--

ALTER SEQUENCE documents_mail_id_seq OWNED BY documents_mail.id;


--
-- Name: dropped_documents; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE dropped_documents (
    id integer NOT NULL,
    description text,
    filename text,
    filesize integer,
    mimetype text,
    betrokkene_id text,
    load_time timestamp without time zone,
    created timestamp without time zone,
    last_modified timestamp without time zone
);


ALTER TABLE public.dropped_documents OWNER TO zaakadmins;

--
-- Name: dropped_documents_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE dropped_documents_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.dropped_documents_id_seq OWNER TO zaakadmins;

--
-- Name: dropped_documents_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zaakadmins
--

ALTER SEQUENCE dropped_documents_id_seq OWNED BY dropped_documents.id;


--
-- Name: filestore; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE filestore (
    id integer NOT NULL,
    filename character varying(256),
    mimetype character varying(256),
    label text,
    description text,
    help text,
    md5sum text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    filesize integer
);


ALTER TABLE public.filestore OWNER TO zaakadmins;

--
-- Name: filestore_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE filestore_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.filestore_id_seq OWNER TO zaakadmins;

--
-- Name: filestore_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zaakadmins
--

ALTER SEQUENCE filestore_id_seq OWNED BY filestore.id;


--
-- Name: gm_adres; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE gm_adres (
    id integer NOT NULL,
    straatnaam character varying(80),
    huisnummer smallint,
    huisletter character(1),
    huisnummertoevoeging character varying(4),
    nadere_aanduiding character varying(35),
    postcode character varying(6),
    woonplaats character varying(75),
    gemeentedeel character varying(75),
    functie_adres character(1),
    datum_aanvang_bewoning date,
    woonplaats_id character varying(32),
    gemeente_code smallint,
    hash character varying(32),
    import_datum timestamp(6) without time zone
);


ALTER TABLE public.gm_adres OWNER TO zaakadmins;

--
-- Name: gm_adres_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE gm_adres_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.gm_adres_id_seq OWNER TO zaakadmins;

--
-- Name: gm_adres_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zaakadmins
--

ALTER SEQUENCE gm_adres_id_seq OWNED BY gm_adres.id;


--
-- Name: gm_bedrijf; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE gm_bedrijf (
    id integer NOT NULL,
    gegevens_magazijn_id integer,
    dossiernummer character varying(8),
    subdossiernummer character varying(4),
    hoofdvestiging_dossiernummer character varying(8),
    hoofdvestiging_subdossiernummer character varying(4),
    vorig_dossiernummer character varying(8),
    vorig_subdossiernummer character varying(4),
    handelsnaam character varying(45),
    rechtsvorm smallint,
    kamernummer smallint,
    faillisement smallint,
    surseance smallint,
    telefoonnummer character varying(15),
    email character varying(128),
    vestiging_adres character varying(30),
    vestiging_straatnaam character varying(25),
    vestiging_huisnummer character varying(6),
    vestiging_huisnummertoevoeging character varying(12),
    vestiging_postcodewoonplaats character varying(30),
    vestiging_postcode character varying(6),
    vestiging_woonplaats character varying(24),
    correspondentie_adres character varying(30),
    correspondentie_straatnaam character varying(25),
    correspondentie_huisnummer character varying(6),
    correspondentie_huisnummertoevoeging character varying(12),
    correspondentie_postcodewoonplaats character varying(30),
    correspondentie_postcode character varying(6),
    correspondentie_woonplaats character varying(24),
    hoofdactiviteitencode integer,
    nevenactiviteitencode1 integer,
    nevenactiviteitencode2 integer,
    werkzamepersonen integer,
    contact_naam character varying(64),
    contact_aanspreektitel character varying(45),
    contact_voorletters character varying(19),
    contact_voorvoegsel character varying(8),
    contact_geslachtsnaam character varying(95),
    contact_geslachtsaanduiding character varying(1),
    authenticated smallint,
    authenticatedby text,
    import_datum timestamp(6) without time zone,
    verblijfsobject_id character varying(16)
);


ALTER TABLE public.gm_bedrijf OWNER TO zaakadmins;

--
-- Name: gm_bedrijf_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE gm_bedrijf_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.gm_bedrijf_id_seq OWNER TO zaakadmins;

--
-- Name: gm_bedrijf_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zaakadmins
--

ALTER SEQUENCE gm_bedrijf_id_seq OWNED BY gm_bedrijf.id;


--
-- Name: gm_natuurlijk_persoon; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE gm_natuurlijk_persoon (
    id integer NOT NULL,
    gegevens_magazijn_id integer,
    betrokkene_type integer,
    burgerservicenummer character varying(9),
    a_nummer character varying(10),
    voorletters character varying(10),
    voornamen character varying(200),
    geslachtsnaam character varying(200),
    voorvoegsel character varying(50),
    geslachtsaanduiding character varying(3),
    nationaliteitscode1 smallint,
    nationaliteitscode2 smallint,
    nationaliteitscode3 smallint,
    geboortegemeente character varying(75),
    geboorteplaats character varying(75),
    geboortegemeente_omschrijving character varying(150),
    geboorteregio character varying(150),
    geboorteland character varying(75),
    geboortedatum timestamp without time zone,
    aanhef_aanschrijving character varying(10),
    voorletters_aanschrijving character varying(20),
    voornamen_aanschrijving character varying(200),
    naam_aanschrijving character varying(200),
    voorvoegsel_aanschrijving character varying(50),
    burgerlijke_staat character(1),
    indicatie_gezag character varying(2),
    indicatie_curatele character(1),
    indicatie_geheim character(1),
    aanduiding_verblijfsrecht smallint,
    datum_aanvang_verblijfsrecht date,
    datum_einde_verblijfsrecht date,
    aanduiding_soort_vreemdeling character varying(10),
    land_vanwaar_ingeschreven smallint,
    land_waarnaar_vertrokken smallint,
    adres_buitenland1 character varying(35),
    adres_buitenland2 character varying(35),
    adres_buitenland3 character varying(35),
    nnp_ts character varying(32),
    hash character varying(32),
    import_datum timestamp(6) without time zone,
    adres_id integer,
    authenticatedby text,
    authenticated smallint,
    datum_overlijden timestamp(6) without time zone,
    verblijfsobject_id character varying(16),
    aanduiding_naamgebruik character varying(1),
    onderzoek_persoon boolean,
    onderzoek_persoon_ingang timestamp without time zone,
    onderzoek_persoon_einde timestamp without time zone,
    onderzoek_persoon_onjuist character varying(1),
    onderzoek_huwelijk boolean,
    onderzoek_huwelijk_ingang timestamp without time zone,
    onderzoek_huwelijk_einde timestamp without time zone,
    onderzoek_huwelijk_onjuist character varying(1),
    onderzoek_overlijden boolean,
    onderzoek_overlijden_ingang timestamp without time zone,
    onderzoek_overlijden_einde timestamp without time zone,
    onderzoek_overlijden_onjuist character varying(1),
    onderzoek_verblijfplaats boolean,
    onderzoek_verblijfplaats_ingang timestamp without time zone,
    onderzoek_verblijfplaats_einde timestamp without time zone,
    onderzoek_verblijfplaats_onjuist character varying(1),
    partner_a_nummer character varying(50),
    partner_burgerservicenummer character varying(50),
    partner_voorvoegsel character varying(50),
    partner_geslachtsnaam character varying(50),
    datum_huwelijk timestamp without time zone,
    datum_huwelijk_ontbinding timestamp without time zone
);


ALTER TABLE public.gm_natuurlijk_persoon OWNER TO zaakadmins;

--
-- Name: gm_natuurlijk_persoon_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE gm_natuurlijk_persoon_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.gm_natuurlijk_persoon_id_seq OWNER TO zaakadmins;

--
-- Name: gm_natuurlijk_persoon_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zaakadmins
--

ALTER SEQUENCE gm_natuurlijk_persoon_id_seq OWNED BY gm_natuurlijk_persoon.id;


--
-- Name: jobs; Type: TABLE; Schema: public; Owner: michiel; Tablespace: 
--

CREATE TABLE jobs (
    id integer NOT NULL,
    bericht text,
    task_context text,
    task text,
    raw_input text,
    raw_result text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    deleted timestamp without time zone
);


ALTER TABLE public.jobs OWNER TO michiel;

--
-- Name: jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: michiel
--

CREATE SEQUENCE jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.jobs_id_seq OWNER TO michiel;

--
-- Name: jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: michiel
--

ALTER SEQUENCE jobs_id_seq OWNED BY jobs.id;


--
-- Name: kennisbank_producten; Type: TABLE; Schema: public; Owner: michiel; Tablespace: 
--

CREATE TABLE kennisbank_producten (
    search_index tsvector,
    search_term text,
    object_type character varying(100) DEFAULT 'kennisbank_producten'::character varying,
    id integer NOT NULL,
    pid integer,
    bibliotheek_categorie_id integer,
    naam text,
    omschrijving text,
    voorwaarden text,
    aanpak text,
    kosten text,
    versie integer,
    author text,
    commit_message text,
    active boolean,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    deleted timestamp without time zone
)
INHERITS (searchable);


ALTER TABLE public.kennisbank_producten OWNER TO michiel;

--
-- Name: kennisbank_producten_id_seq; Type: SEQUENCE; Schema: public; Owner: michiel
--

CREATE SEQUENCE kennisbank_producten_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.kennisbank_producten_id_seq OWNER TO michiel;

--
-- Name: kennisbank_producten_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: michiel
--

ALTER SEQUENCE kennisbank_producten_id_seq OWNED BY kennisbank_producten.id;


--
-- Name: kennisbank_relaties; Type: TABLE; Schema: public; Owner: michiel; Tablespace: 
--

CREATE TABLE kennisbank_relaties (
    id integer NOT NULL,
    kennisbank_producten_id integer,
    kennisbank_vragen_id integer,
    zaaktype_id integer
);


ALTER TABLE public.kennisbank_relaties OWNER TO michiel;

--
-- Name: kennisbank_relaties_id_seq; Type: SEQUENCE; Schema: public; Owner: michiel
--

CREATE SEQUENCE kennisbank_relaties_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.kennisbank_relaties_id_seq OWNER TO michiel;

--
-- Name: kennisbank_relaties_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: michiel
--

ALTER SEQUENCE kennisbank_relaties_id_seq OWNED BY kennisbank_relaties.id;


--
-- Name: kennisbank_vragen; Type: TABLE; Schema: public; Owner: michiel; Tablespace: 
--

CREATE TABLE kennisbank_vragen (
    search_index tsvector,
    search_term text,
    object_type character varying(100) DEFAULT 'kennisbank_vragen'::character varying,
    id integer NOT NULL,
    pid integer,
    bibliotheek_categorie_id integer,
    naam text,
    vraag text,
    antwoord text,
    versie integer,
    author text,
    commit_message text,
    active boolean,
    last_viewed timestamp without time zone,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    deleted timestamp without time zone
)
INHERITS (searchable);


ALTER TABLE public.kennisbank_vragen OWNER TO michiel;

--
-- Name: kennisbank_vragen_id_seq; Type: SEQUENCE; Schema: public; Owner: michiel
--

CREATE SEQUENCE kennisbank_vragen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.kennisbank_vragen_id_seq OWNER TO michiel;

--
-- Name: kennisbank_vragen_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: michiel
--

ALTER SEQUENCE kennisbank_vragen_id_seq OWNED BY kennisbank_vragen.id;


--
-- Name: logging; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE logging (
    id integer NOT NULL,
    loglevel character varying(32),
    zaak_id integer,
    betrokkene_id character varying(128),
    aanvrager_id character varying(128),
    is_bericht integer,
    component character varying(64),
    component_id integer,
    seen integer,
    onderwerp character varying(255),
    bericht text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    deleted_on timestamp without time zone
);


ALTER TABLE public.logging OWNER TO zaakadmins;

--
-- Name: logging_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE logging_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.logging_id_seq OWNER TO zaakadmins;

--
-- Name: logging_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zaakadmins
--

ALTER SEQUENCE logging_id_seq OWNED BY logging.id;


--
-- Name: natuurlijk_persoon; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE natuurlijk_persoon (
    search_index tsvector,
    search_term text,
    object_type character varying(100) DEFAULT 'natuurlijk_persoon'::character varying,
    id integer NOT NULL,
    burgerservicenummer character varying(9),
    a_nummer character varying(10),
    voorletters character varying(10),
    voornamen character varying(200),
    geslachtsnaam character varying(200),
    voorvoegsel character varying(50),
    geslachtsaanduiding character varying(3),
    nationaliteitscode1 smallint,
    nationaliteitscode2 smallint,
    nationaliteitscode3 smallint,
    geboortegemeente character varying(75),
    geboorteplaats character varying(75),
    geboortegemeente_omschrijving character varying(150),
    geboorteregio character varying(150),
    geboorteland character varying(75),
    geboortedatum timestamp without time zone,
    aanhef_aanschrijving character varying(10),
    voorletters_aanschrijving character varying(20),
    voornamen_aanschrijving character varying(200),
    naam_aanschrijving character varying(200),
    voorvoegsel_aanschrijving character varying(50),
    burgerlijke_staat character(1),
    indicatie_gezag character varying(2),
    indicatie_curatele character(1),
    indicatie_geheim character(1),
    aanduiding_verblijfsrecht smallint,
    datum_aanvang_verblijfsrecht date,
    datum_einde_verblijfsrecht date,
    aanduiding_soort_vreemdeling character varying(10),
    land_vanwaar_ingeschreven smallint,
    land_waarnaar_vertrokken smallint,
    adres_buitenland1 character varying(35),
    adres_buitenland2 character varying(35),
    adres_buitenland3 character varying(35),
    nnp_ts character varying(32),
    hash character varying(32),
    import_datum timestamp(6) without time zone,
    adres_id integer,
    email character varying(32),
    telefoon character varying(32),
    authenticated boolean,
    authenticatedby text,
    deleted_on timestamp(6) without time zone,
    verblijfsobject_id character varying(16),
    datum_overlijden timestamp without time zone,
    aanduiding_naamgebruik character varying(1),
    onderzoek_persoon boolean,
    onderzoek_persoon_ingang timestamp without time zone,
    onderzoek_persoon_einde timestamp without time zone,
    onderzoek_persoon_onjuist boolean,
    onderzoek_huwelijk boolean,
    onderzoek_huwelijk_ingang timestamp without time zone,
    onderzoek_huwelijk_einde timestamp without time zone,
    onderzoek_huwelijk_onjuist boolean,
    onderzoek_overlijden boolean,
    onderzoek_overlijden_ingang timestamp without time zone,
    onderzoek_overlijden_einde timestamp without time zone,
    onderzoek_overlijden_onjuist boolean,
    onderzoek_verblijfplaats boolean,
    onderzoek_verblijfplaats_ingang timestamp without time zone,
    onderzoek_verblijfplaats_einde timestamp without time zone,
    onderzoek_verblijfplaats_onjuist boolean,
    partner_a_nummer character varying(50),
    partner_burgerservicenummer character varying(50),
    partner_voorvoegsel character varying(50),
    partner_geslachtsnaam character varying(50),
    datum_huwelijk timestamp without time zone,
    datum_huwelijk_ontbinding timestamp without time zone
)
INHERITS (searchable);


ALTER TABLE public.natuurlijk_persoon OWNER TO zaakadmins;

--
-- Name: natuurlijk_persoon_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE natuurlijk_persoon_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.natuurlijk_persoon_id_seq OWNER TO zaakadmins;

--
-- Name: natuurlijk_persoon_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zaakadmins
--

ALTER SEQUENCE natuurlijk_persoon_id_seq OWNED BY natuurlijk_persoon.id;


--
-- Name: notitie; Type: TABLE; Schema: public; Owner: michiel; Tablespace: 
--

CREATE TABLE notitie (
    id integer NOT NULL,
    onderwerp text,
    bericht text,
    contactkanaal text,
    md5 text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    deleted timestamp without time zone
);


ALTER TABLE public.notitie OWNER TO michiel;

--
-- Name: notitie_id_seq; Type: SEQUENCE; Schema: public; Owner: michiel
--

CREATE SEQUENCE notitie_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.notitie_id_seq OWNER TO michiel;

--
-- Name: notitie_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: michiel
--

ALTER SEQUENCE notitie_id_seq OWNED BY notitie.id;


--
-- Name: parkeergebied; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE parkeergebied (
    id integer NOT NULL,
    bag_hoofdadres bigint,
    postcode character varying(6) NOT NULL,
    straatnaam character varying(255) NOT NULL,
    huisnummer integer,
    huisletter character varying(1),
    huisnummertoevoeging character varying(4),
    parkeergebied_id integer,
    parkeergebied character varying(255),
    created timestamp without time zone,
    last_modified timestamp without time zone
);


ALTER TABLE public.parkeergebied OWNER TO zaakadmins;

--
-- Name: parkeergebied_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE parkeergebied_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.parkeergebied_id_seq OWNER TO zaakadmins;

--
-- Name: parkeergebied_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zaakadmins
--

ALTER SEQUENCE parkeergebied_id_seq OWNED BY parkeergebied.id;


--
-- Name: parkeergebied_kosten; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE parkeergebied_kosten (
    id integer NOT NULL,
    betrokkene_type character varying(128),
    parkeergebied character varying(255),
    parkeergebied_id integer,
    aanvraag_soort integer,
    geldigheid integer,
    prijs real,
    created timestamp without time zone,
    last_modified timestamp without time zone
);


ALTER TABLE public.parkeergebied_kosten OWNER TO zaakadmins;

--
-- Name: parkeergebied_kosten_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE parkeergebied_kosten_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.parkeergebied_kosten_id_seq OWNER TO zaakadmins;

--
-- Name: parkeergebied_kosten_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zaakadmins
--

ALTER SEQUENCE parkeergebied_kosten_id_seq OWNED BY parkeergebied_kosten.id;


--
-- Name: sbus_logging; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE sbus_logging (
    id integer NOT NULL,
    sbus_traffic_id integer,
    pid integer,
    mutatie_type text,
    object text,
    params text,
    kerngegeven text,
    label text,
    changes text,
    error boolean,
    error_message text,
    created timestamp without time zone,
    modified timestamp without time zone
);


ALTER TABLE public.sbus_logging OWNER TO zaakadmins;

--
-- Name: sbus_logging_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE sbus_logging_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.sbus_logging_id_seq OWNER TO zaakadmins;

--
-- Name: sbus_logging_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zaakadmins
--

ALTER SEQUENCE sbus_logging_id_seq OWNED BY sbus_logging.id;


--
-- Name: sbus_traffic; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE sbus_traffic (
    id integer NOT NULL,
    sbus_type text,
    object text,
    operation text,
    input text,
    input_raw text,
    output text,
    output_raw text,
    error boolean,
    error_message text,
    created timestamp without time zone,
    modified timestamp without time zone
);


ALTER TABLE public.sbus_traffic OWNER TO zaakadmins;

--
-- Name: sbus_traffic_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE sbus_traffic_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.sbus_traffic_id_seq OWNER TO zaakadmins;

--
-- Name: sbus_traffic_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zaakadmins
--

ALTER SEQUENCE sbus_traffic_id_seq OWNED BY sbus_traffic.id;


--
-- Name: search_query_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE search_query_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.search_query_id_seq OWNER TO zaakadmins;

--
-- Name: search_query; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE search_query (
    id integer DEFAULT nextval('search_query_id_seq'::regclass) NOT NULL,
    settings text,
    ldap_id integer,
    name character varying(256),
    sort_index integer
);


ALTER TABLE public.search_query OWNER TO zaakadmins;

--
-- Name: search_query_delen_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE search_query_delen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.search_query_delen_id_seq OWNER TO zaakadmins;

--
-- Name: search_query_delen; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE search_query_delen (
    id integer DEFAULT nextval('search_query_delen_id_seq'::regclass) NOT NULL,
    search_query_id integer,
    ou_id integer,
    role_id integer
);


ALTER TABLE public.search_query_delen OWNER TO zaakadmins;

--
-- Name: seen; Type: TABLE; Schema: public; Owner: michiel; Tablespace: 
--

CREATE TABLE seen (
    id integer NOT NULL,
    component text,
    kennisbank_producten_id integer,
    kennisbank_vragen_id integer,
    betrokkene_dsn text,
    created timestamp without time zone
);


ALTER TABLE public.seen OWNER TO michiel;

--
-- Name: seen_id_seq; Type: SEQUENCE; Schema: public; Owner: michiel
--

CREATE SEQUENCE seen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.seen_id_seq OWNER TO michiel;

--
-- Name: seen_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: michiel
--

ALTER SEQUENCE seen_id_seq OWNED BY seen.id;


--
-- Name: user_app_lock; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE user_app_lock (
    type character(40) NOT NULL,
    type_id character(20) NOT NULL,
    create_unixtime integer NOT NULL,
    session_id character(40) NOT NULL,
    uidnumber integer NOT NULL
);


ALTER TABLE public.user_app_lock OWNER TO zaakadmins;

--
-- Name: zaak_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE zaak_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.zaak_id_seq OWNER TO zaakadmins;

--
-- Name: zaak; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE zaak (
    search_index tsvector,
    search_term text,
    object_type character varying(100) DEFAULT 'zaak'::character varying,
    id integer DEFAULT nextval('zaak_id_seq'::regclass) NOT NULL,
    pid integer,
    relates_to integer,
    zaaktype_id integer NOT NULL,
    zaaktype_node_id integer NOT NULL,
    status zaaksysteem_status NOT NULL,
    milestone integer NOT NULL,
    contactkanaal character varying(128) NOT NULL,
    aanvraag_trigger zaaksysteem_trigger NOT NULL,
    onderwerp character varying(256),
    resultaat text,
    besluit text,
    coordinator integer,
    behandelaar integer,
    aanvrager integer NOT NULL,
    route_ou integer,
    route_role integer,
    locatie_zaak integer,
    locatie_correspondentie integer,
    streefafhandeldatum timestamp(6) without time zone,
    registratiedatum timestamp(6) without time zone NOT NULL,
    afhandeldatum timestamp(6) without time zone,
    vernietigingsdatum timestamp(6) without time zone,
    created timestamp(6) without time zone NOT NULL,
    last_modified timestamp(6) without time zone NOT NULL,
    deleted timestamp(6) without time zone,
    vervolg_van integer,
    aanvrager_gm_id integer,
    behandelaar_gm_id integer,
    coordinator_gm_id integer
)
INHERITS (searchable);


ALTER TABLE public.zaak OWNER TO zaakadmins;

--
-- Name: zaak_bag_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE zaak_bag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.zaak_bag_id_seq OWNER TO zaakadmins;

--
-- Name: zaak_bag; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE zaak_bag (
    id integer DEFAULT nextval('zaak_bag_id_seq'::regclass) NOT NULL,
    pid integer,
    zaak_id integer,
    bag_type zaaksysteem_bag_types,
    bag_id character varying(255),
    bag_verblijfsobject_id character varying(255),
    bag_openbareruimte_id character varying(255),
    bag_nummeraanduiding_id character varying(255),
    bag_pand_id character varying(255),
    bag_standplaats_id character varying(255),
    bag_ligplaats_id character varying(255)
);


ALTER TABLE public.zaak_bag OWNER TO zaakadmins;

--
-- Name: zaak_betrokkenen_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE zaak_betrokkenen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.zaak_betrokkenen_id_seq OWNER TO zaakadmins;

--
-- Name: zaak_betrokkenen; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE zaak_betrokkenen (
    id integer DEFAULT nextval('zaak_betrokkenen_id_seq'::regclass) NOT NULL,
    zaak_id integer,
    betrokkene_type character varying(128),
    betrokkene_id integer,
    gegevens_magazijn_id integer,
    verificatie character varying(128),
    naam character varying(255),
    rol text,
    magic_string_prefix text,
    deleted timestamp without time zone
);


ALTER TABLE public.zaak_betrokkenen OWNER TO zaakadmins;

--
-- Name: zaak_kenmerk; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE zaak_kenmerk (
    zaak_id integer NOT NULL,
    bibliotheek_kenmerken_id integer NOT NULL,
    value text NOT NULL,
    id integer NOT NULL
);


ALTER TABLE public.zaak_kenmerk OWNER TO zaakadmins;

--
-- Name: zaak_kenmerk_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE zaak_kenmerk_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.zaak_kenmerk_id_seq OWNER TO zaakadmins;

--
-- Name: zaak_kenmerk_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zaakadmins
--

ALTER SEQUENCE zaak_kenmerk_id_seq OWNED BY zaak_kenmerk.id;


--
-- Name: zaak_kenmerken_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE zaak_kenmerken_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.zaak_kenmerken_id_seq OWNER TO zaakadmins;

--
-- Name: zaak_kenmerken; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE zaak_kenmerken (
    id integer DEFAULT nextval('zaak_kenmerken_id_seq'::regclass) NOT NULL,
    zaak_id integer NOT NULL,
    bibliotheek_kenmerken_id integer NOT NULL,
    value_type character varying(128),
    naam character varying(128),
    multiple boolean
);


ALTER TABLE public.zaak_kenmerken OWNER TO zaakadmins;

--
-- Name: zaak_kenmerken_values_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE zaak_kenmerken_values_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.zaak_kenmerken_values_id_seq OWNER TO zaakadmins;

--
-- Name: zaak_kenmerken_values; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE zaak_kenmerken_values (
    id integer DEFAULT nextval('zaak_kenmerken_values_id_seq'::regclass) NOT NULL,
    zaak_kenmerken_id integer NOT NULL,
    bibliotheek_kenmerken_id integer NOT NULL,
    value text,
    zaak_bag_id integer
);


ALTER TABLE public.zaak_kenmerken_values OWNER TO zaakadmins;

--
-- Name: zaak_meta_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE zaak_meta_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.zaak_meta_id_seq OWNER TO zaakadmins;

--
-- Name: zaak_meta; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE zaak_meta (
    id integer DEFAULT nextval('zaak_meta_id_seq'::regclass) NOT NULL,
    zaak_id integer,
    verlenging character varying(255),
    opschorten character varying(255),
    deel character varying(255),
    gerelateerd character varying(255),
    vervolg character varying(255),
    afhandeling character varying(255)
);


ALTER TABLE public.zaak_meta OWNER TO zaakadmins;

--
-- Name: zaak_onafgerond; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE zaak_onafgerond (
    zaaktype_id integer NOT NULL,
    betrokkene character(50) NOT NULL,
    json_string text NOT NULL,
    afronden boolean,
    create_unixtime integer
);


ALTER TABLE public.zaak_onafgerond OWNER TO zaakadmins;

--
-- Name: zaaktype; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE zaaktype (
    search_index tsvector,
    search_term text,
    object_type character varying(100) DEFAULT 'zaaktype'::character varying,
    id integer NOT NULL,
    zaaktype_node_id integer,
    version integer,
    active integer,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    deleted timestamp without time zone,
    bibliotheek_categorie_id integer
)
INHERITS (searchable);


ALTER TABLE public.zaaktype OWNER TO zaakadmins;

--
-- Name: zaaktype_authorisation; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE zaaktype_authorisation (
    id integer NOT NULL,
    zaaktype_node_id integer,
    recht text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    deleted timestamp without time zone,
    role_id integer,
    ou_id integer,
    zaaktype_id integer
);


ALTER TABLE public.zaaktype_authorisation OWNER TO zaakadmins;

--
-- Name: zaaktype_authorisation_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE zaaktype_authorisation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.zaaktype_authorisation_id_seq OWNER TO zaakadmins;

--
-- Name: zaaktype_authorisation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zaakadmins
--

ALTER SEQUENCE zaaktype_authorisation_id_seq OWNED BY zaaktype_authorisation.id;


--
-- Name: zaaktype_betrokkenen; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE zaaktype_betrokkenen (
    id integer NOT NULL,
    zaaktype_node_id integer,
    betrokkene_type text,
    created timestamp without time zone,
    last_modified timestamp without time zone
);


ALTER TABLE public.zaaktype_betrokkenen OWNER TO zaakadmins;

--
-- Name: zaaktype_betrokkenen_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE zaaktype_betrokkenen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.zaaktype_betrokkenen_id_seq OWNER TO zaakadmins;

--
-- Name: zaaktype_betrokkenen_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zaakadmins
--

ALTER SEQUENCE zaaktype_betrokkenen_id_seq OWNED BY zaaktype_betrokkenen.id;


--
-- Name: zaaktype_definitie; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE zaaktype_definitie (
    id integer NOT NULL,
    openbaarheid character varying(255),
    handelingsinitiator character varying(255),
    grondslag character varying(255),
    procesbeschrijving character varying(255),
    afhandeltermijn character varying(255),
    afhandeltermijn_type character varying(255),
    iv3_categorie character varying(255),
    besluittype character varying(255),
    selectielijst character varying(255),
    servicenorm character varying(255),
    servicenorm_type character varying(255),
    pdc_voorwaarden text,
    pdc_description text,
    pdc_meenemen text,
    pdc_tarief text,
    omschrijving_upl character varying(255),
    aard character varying(255),
    extra_informatie character varying(255),
    custom_webform character varying(255)
);


ALTER TABLE public.zaaktype_definitie OWNER TO zaakadmins;

--
-- Name: zaaktype_definitie_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE zaaktype_definitie_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.zaaktype_definitie_id_seq OWNER TO zaakadmins;

--
-- Name: zaaktype_definitie_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zaakadmins
--

ALTER SEQUENCE zaaktype_definitie_id_seq OWNED BY zaaktype_definitie.id;


--
-- Name: zaaktype_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE zaaktype_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.zaaktype_id_seq OWNER TO zaakadmins;

--
-- Name: zaaktype_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zaakadmins
--

ALTER SEQUENCE zaaktype_id_seq OWNED BY zaaktype.id;


--
-- Name: zaaktype_kenmerken; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE zaaktype_kenmerken (
    id integer NOT NULL,
    bibliotheek_kenmerken_id integer,
    value_mandatory integer,
    label text,
    help text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    zaaktype_node_id integer,
    zaak_status_id integer,
    pip integer,
    zaakinformatie_view integer DEFAULT 1,
    bag_zaakadres integer,
    is_group integer,
    besluit integer,
    date_fromcurrentdate integer DEFAULT 0,
    value_default text,
    pip_can_change boolean
);


ALTER TABLE public.zaaktype_kenmerken OWNER TO zaakadmins;

--
-- Name: zaaktype_kenmerken_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE zaaktype_kenmerken_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.zaaktype_kenmerken_id_seq OWNER TO zaakadmins;

--
-- Name: zaaktype_kenmerken_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zaakadmins
--

ALTER SEQUENCE zaaktype_kenmerken_id_seq OWNED BY zaaktype_kenmerken.id;


--
-- Name: zaaktype_node; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE zaaktype_node (
    id integer NOT NULL,
    zaaktype_id integer,
    zaaktype_rt_queue text,
    code text,
    trigger text,
    titel character varying(128),
    version integer,
    active integer,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    deleted timestamp without time zone,
    webform_toegang integer,
    webform_authenticatie text,
    adres_relatie text,
    aanvrager_hergebruik integer,
    automatisch_aanvragen integer,
    automatisch_behandelen integer,
    toewijzing_zaakintake integer,
    toelichting character varying(128),
    online_betaling integer,
    zaaktype_definitie_id integer,
    adres_andere_locatie integer,
    adres_aanvrager integer,
    bedrijfid_wijzigen integer,
    zaaktype_vertrouwelijk integer,
    zaaktype_trefwoorden text,
    zaaktype_omschrijving text,
    extra_relaties_in_aanvraag boolean
);


ALTER TABLE public.zaaktype_node OWNER TO zaakadmins;

--
-- Name: zaaktype_node_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE zaaktype_node_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.zaaktype_node_id_seq OWNER TO zaakadmins;

--
-- Name: zaaktype_node_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zaakadmins
--

ALTER SEQUENCE zaaktype_node_id_seq OWNED BY zaaktype_node.id;


--
-- Name: zaaktype_notificatie; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE zaaktype_notificatie (
    id integer NOT NULL,
    zaaktype_node_id integer,
    zaak_status_id integer,
    label text,
    rcpt text,
    onderwerp text,
    bericht text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    intern_block integer,
    email character varying(128)
);


ALTER TABLE public.zaaktype_notificatie OWNER TO zaakadmins;

--
-- Name: zaaktype_notificatie_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE zaaktype_notificatie_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.zaaktype_notificatie_id_seq OWNER TO zaakadmins;

--
-- Name: zaaktype_notificatie_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zaakadmins
--

ALTER SEQUENCE zaaktype_notificatie_id_seq OWNED BY zaaktype_notificatie.id;


--
-- Name: zaaktype_regel_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE zaaktype_regel_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.zaaktype_regel_id_seq OWNER TO zaakadmins;

--
-- Name: zaaktype_regel; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE zaaktype_regel (
    id integer DEFAULT nextval('zaaktype_regel_id_seq'::regclass) NOT NULL,
    zaaktype_node_id integer,
    zaak_status_id integer,
    naam text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    settings text
);


ALTER TABLE public.zaaktype_regel OWNER TO zaakadmins;

--
-- Name: zaaktype_relatie; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE zaaktype_relatie (
    id integer NOT NULL,
    zaaktype_node_id integer,
    relatie_zaaktype_id integer,
    zaaktype_status_id integer,
    relatie_type text,
    eigenaar_type text,
    start_delay character varying(255),
    created timestamp without time zone,
    last_modified timestamp without time zone,
    status integer,
    kopieren_kenmerken integer,
    delay_type character varying(255),
    ou_id integer,
    role_id integer,
    automatisch_behandelen boolean
);


ALTER TABLE public.zaaktype_relatie OWNER TO zaakadmins;

--
-- Name: zaaktype_relatie_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE zaaktype_relatie_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.zaaktype_relatie_id_seq OWNER TO zaakadmins;

--
-- Name: zaaktype_relatie_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zaakadmins
--

ALTER SEQUENCE zaaktype_relatie_id_seq OWNED BY zaaktype_relatie.id;


--
-- Name: zaaktype_resultaten; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE zaaktype_resultaten (
    id integer NOT NULL,
    zaaktype_node_id integer,
    zaaktype_status_id integer,
    resultaat text,
    ingang text,
    bewaartermijn integer,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    dossiertype character varying(50)
);


ALTER TABLE public.zaaktype_resultaten OWNER TO zaakadmins;

--
-- Name: zaaktype_resultaten_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE zaaktype_resultaten_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.zaaktype_resultaten_id_seq OWNER TO zaakadmins;

--
-- Name: zaaktype_resultaten_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zaakadmins
--

ALTER SEQUENCE zaaktype_resultaten_id_seq OWNED BY zaaktype_resultaten.id;


--
-- Name: zaaktype_sjablonen; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE zaaktype_sjablonen (
    id integer NOT NULL,
    zaaktype_node_id integer,
    bibliotheek_sjablonen_id integer,
    help text,
    zaak_status_id integer,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    automatisch_genereren integer
);


ALTER TABLE public.zaaktype_sjablonen OWNER TO zaakadmins;

--
-- Name: zaaktype_sjablonen_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE zaaktype_sjablonen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.zaaktype_sjablonen_id_seq OWNER TO zaakadmins;

--
-- Name: zaaktype_sjablonen_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zaakadmins
--

ALTER SEQUENCE zaaktype_sjablonen_id_seq OWNED BY zaaktype_sjablonen.id;


--
-- Name: zaaktype_status; Type: TABLE; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE TABLE zaaktype_status (
    id integer NOT NULL,
    zaaktype_node_id integer,
    status integer,
    status_type text,
    naam text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    ou_id integer,
    role_id integer,
    checklist integer,
    fase character varying(255),
    role_set integer
);


ALTER TABLE public.zaaktype_status OWNER TO zaakadmins;

--
-- Name: zaaktype_status_id_seq; Type: SEQUENCE; Schema: public; Owner: zaakadmins
--

CREATE SEQUENCE zaaktype_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.zaaktype_status_id_seq OWNER TO zaakadmins;

--
-- Name: zaaktype_status_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zaakadmins
--

ALTER SEQUENCE zaaktype_status_id_seq OWNED BY zaaktype_status.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zaakadmins
--

ALTER TABLE adres ALTER COLUMN id SET DEFAULT nextval('adres_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zaakadmins
--

ALTER TABLE bedrijf ALTER COLUMN id SET DEFAULT nextval('bedrijf_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zaakadmins
--

ALTER TABLE bedrijf_authenticatie ALTER COLUMN id SET DEFAULT nextval('bedrijf_authenticatie_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zaakadmins
--

ALTER TABLE beheer_import ALTER COLUMN id SET DEFAULT nextval('beheer_import_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zaakadmins
--

ALTER TABLE beheer_import_log ALTER COLUMN id SET DEFAULT nextval('beheer_import_log_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zaakadmins
--

ALTER TABLE beheer_plugins ALTER COLUMN id SET DEFAULT nextval('beheer_plugins_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zaakadmins
--

ALTER TABLE betrokkene_notes ALTER COLUMN id SET DEFAULT nextval('betrokkene_notes_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zaakadmins
--

ALTER TABLE betrokkenen ALTER COLUMN id SET DEFAULT nextval('betrokkenen_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zaakadmins
--

ALTER TABLE bibliotheek_categorie ALTER COLUMN id SET DEFAULT nextval('bibliotheek_categorie_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zaakadmins
--

ALTER TABLE bibliotheek_kenmerken ALTER COLUMN id SET DEFAULT nextval('bibliotheek_kenmerken_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zaakadmins
--

ALTER TABLE bibliotheek_kenmerken_values ALTER COLUMN id SET DEFAULT nextval('bibliotheek_kenmerken_values_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zaakadmins
--

ALTER TABLE bibliotheek_sjablonen ALTER COLUMN id SET DEFAULT nextval('bibliotheek_sjablonen_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zaakadmins
--

ALTER TABLE bibliotheek_sjablonen_magic_string ALTER COLUMN id SET DEFAULT nextval('bibliotheek_sjablonen_magic_string_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zaakadmins
--

ALTER TABLE checklist_vraag ALTER COLUMN id SET DEFAULT nextval('checklist_vraag_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zaakadmins
--

ALTER TABLE contact_data ALTER COLUMN id SET DEFAULT nextval('contact_data_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: michiel
--

ALTER TABLE documenten ALTER COLUMN id SET DEFAULT nextval('documenten_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zaakadmins
--

ALTER TABLE documents ALTER COLUMN id SET DEFAULT nextval('documents_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zaakadmins
--

ALTER TABLE documents_mail ALTER COLUMN id SET DEFAULT nextval('documents_mail_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zaakadmins
--

ALTER TABLE dropped_documents ALTER COLUMN id SET DEFAULT nextval('dropped_documents_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zaakadmins
--

ALTER TABLE filestore ALTER COLUMN id SET DEFAULT nextval('filestore_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zaakadmins
--

ALTER TABLE gm_adres ALTER COLUMN id SET DEFAULT nextval('gm_adres_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zaakadmins
--

ALTER TABLE gm_bedrijf ALTER COLUMN id SET DEFAULT nextval('gm_bedrijf_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zaakadmins
--

ALTER TABLE gm_natuurlijk_persoon ALTER COLUMN id SET DEFAULT nextval('gm_natuurlijk_persoon_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: michiel
--

ALTER TABLE jobs ALTER COLUMN id SET DEFAULT nextval('jobs_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: michiel
--

ALTER TABLE kennisbank_producten ALTER COLUMN id SET DEFAULT nextval('kennisbank_producten_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: michiel
--

ALTER TABLE kennisbank_relaties ALTER COLUMN id SET DEFAULT nextval('kennisbank_relaties_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: michiel
--

ALTER TABLE kennisbank_vragen ALTER COLUMN id SET DEFAULT nextval('kennisbank_vragen_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zaakadmins
--

ALTER TABLE logging ALTER COLUMN id SET DEFAULT nextval('logging_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zaakadmins
--

ALTER TABLE natuurlijk_persoon ALTER COLUMN id SET DEFAULT nextval('natuurlijk_persoon_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: michiel
--

ALTER TABLE notitie ALTER COLUMN id SET DEFAULT nextval('notitie_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zaakadmins
--

ALTER TABLE parkeergebied ALTER COLUMN id SET DEFAULT nextval('parkeergebied_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zaakadmins
--

ALTER TABLE parkeergebied_kosten ALTER COLUMN id SET DEFAULT nextval('parkeergebied_kosten_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zaakadmins
--

ALTER TABLE sbus_logging ALTER COLUMN id SET DEFAULT nextval('sbus_logging_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zaakadmins
--

ALTER TABLE sbus_traffic ALTER COLUMN id SET DEFAULT nextval('sbus_traffic_id_seq'::regclass);


--
-- Name: searchable_id; Type: DEFAULT; Schema: public; Owner: michiel
--

ALTER TABLE searchable ALTER COLUMN searchable_id SET DEFAULT nextval('searchable_searchable_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: michiel
--

ALTER TABLE seen ALTER COLUMN id SET DEFAULT nextval('seen_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zaakadmins
--

ALTER TABLE zaak_kenmerk ALTER COLUMN id SET DEFAULT nextval('zaak_kenmerk_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zaakadmins
--

ALTER TABLE zaaktype ALTER COLUMN id SET DEFAULT nextval('zaaktype_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zaakadmins
--

ALTER TABLE zaaktype_authorisation ALTER COLUMN id SET DEFAULT nextval('zaaktype_authorisation_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zaakadmins
--

ALTER TABLE zaaktype_betrokkenen ALTER COLUMN id SET DEFAULT nextval('zaaktype_betrokkenen_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zaakadmins
--

ALTER TABLE zaaktype_definitie ALTER COLUMN id SET DEFAULT nextval('zaaktype_definitie_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zaakadmins
--

ALTER TABLE zaaktype_kenmerken ALTER COLUMN id SET DEFAULT nextval('zaaktype_kenmerken_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zaakadmins
--

ALTER TABLE zaaktype_node ALTER COLUMN id SET DEFAULT nextval('zaaktype_node_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zaakadmins
--

ALTER TABLE zaaktype_notificatie ALTER COLUMN id SET DEFAULT nextval('zaaktype_notificatie_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zaakadmins
--

ALTER TABLE zaaktype_relatie ALTER COLUMN id SET DEFAULT nextval('zaaktype_relatie_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zaakadmins
--

ALTER TABLE zaaktype_resultaten ALTER COLUMN id SET DEFAULT nextval('zaaktype_resultaten_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zaakadmins
--

ALTER TABLE zaaktype_sjablonen ALTER COLUMN id SET DEFAULT nextval('zaaktype_sjablonen_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zaakadmins
--

ALTER TABLE zaaktype_status ALTER COLUMN id SET DEFAULT nextval('zaaktype_status_id_seq'::regclass);


--
-- Name: adres_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY adres
    ADD CONSTRAINT adres_pkey PRIMARY KEY (id);


--
-- Name: bedrijf_authenticatie_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY bedrijf_authenticatie
    ADD CONSTRAINT bedrijf_authenticatie_pkey PRIMARY KEY (id);


--
-- Name: bedrijf_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY bedrijf
    ADD CONSTRAINT bedrijf_pkey PRIMARY KEY (id);


--
-- Name: beheer_import_log_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY beheer_import_log
    ADD CONSTRAINT beheer_import_log_pkey PRIMARY KEY (id);


--
-- Name: beheer_import_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY beheer_import
    ADD CONSTRAINT beheer_import_pkey PRIMARY KEY (id);


--
-- Name: beheer_plugins_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY beheer_plugins
    ADD CONSTRAINT beheer_plugins_pkey PRIMARY KEY (id);


--
-- Name: betrokkene_notes_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY betrokkene_notes
    ADD CONSTRAINT betrokkene_notes_pkey PRIMARY KEY (id);


--
-- Name: betrokkenen_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY betrokkenen
    ADD CONSTRAINT betrokkenen_pkey PRIMARY KEY (id);


--
-- Name: bibliotheek_categorie_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY bibliotheek_categorie
    ADD CONSTRAINT bibliotheek_categorie_pkey PRIMARY KEY (id);


--
-- Name: bibliotheek_kenmerken_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY bibliotheek_kenmerken
    ADD CONSTRAINT bibliotheek_kenmerken_pkey PRIMARY KEY (id);


--
-- Name: bibliotheek_kenmerken_values_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY bibliotheek_kenmerken_values
    ADD CONSTRAINT bibliotheek_kenmerken_values_pkey PRIMARY KEY (id);


--
-- Name: bibliotheek_sjablonen_magic_string_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY bibliotheek_sjablonen_magic_string
    ADD CONSTRAINT bibliotheek_sjablonen_magic_string_pkey PRIMARY KEY (id);


--
-- Name: bibliotheek_sjablonen_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY bibliotheek_sjablonen
    ADD CONSTRAINT bibliotheek_sjablonen_pkey PRIMARY KEY (id);


--
-- Name: checklist_antwoord_id_key; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY checklist_antwoord
    ADD CONSTRAINT checklist_antwoord_id_key UNIQUE (id);


--
-- Name: checklist_antwoord_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY checklist_antwoord
    ADD CONSTRAINT checklist_antwoord_pkey PRIMARY KEY (id);


--
-- Name: checklist_vraag_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY checklist_vraag
    ADD CONSTRAINT checklist_vraag_pkey PRIMARY KEY (id);


--
-- Name: contact_data_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY contact_data
    ADD CONSTRAINT contact_data_pkey PRIMARY KEY (id);


--
-- Name: documenten_pkey; Type: CONSTRAINT; Schema: public; Owner: michiel; Tablespace: 
--

ALTER TABLE ONLY documenten
    ADD CONSTRAINT documenten_pkey PRIMARY KEY (id);


--
-- Name: documents_mail_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY documents_mail
    ADD CONSTRAINT documents_mail_pkey PRIMARY KEY (id);


--
-- Name: documents_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY documents
    ADD CONSTRAINT documents_pkey PRIMARY KEY (id);


--
-- Name: dropped_documents_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY dropped_documents
    ADD CONSTRAINT dropped_documents_pkey PRIMARY KEY (id);


--
-- Name: filestore_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY filestore
    ADD CONSTRAINT filestore_pkey PRIMARY KEY (id);


--
-- Name: gm_adres_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY gm_adres
    ADD CONSTRAINT gm_adres_pkey PRIMARY KEY (id);


--
-- Name: gm_bedrijf_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY gm_bedrijf
    ADD CONSTRAINT gm_bedrijf_pkey PRIMARY KEY (id);


--
-- Name: gm_natuurlijk_persoon_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY gm_natuurlijk_persoon
    ADD CONSTRAINT gm_natuurlijk_persoon_pkey PRIMARY KEY (id);


--
-- Name: jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: michiel; Tablespace: 
--

ALTER TABLE ONLY jobs
    ADD CONSTRAINT jobs_pkey PRIMARY KEY (id);


--
-- Name: kennisbank_producten_pkey; Type: CONSTRAINT; Schema: public; Owner: michiel; Tablespace: 
--

ALTER TABLE ONLY kennisbank_producten
    ADD CONSTRAINT kennisbank_producten_pkey PRIMARY KEY (id);


--
-- Name: kennisbank_relaties_pkey; Type: CONSTRAINT; Schema: public; Owner: michiel; Tablespace: 
--

ALTER TABLE ONLY kennisbank_relaties
    ADD CONSTRAINT kennisbank_relaties_pkey PRIMARY KEY (id);


--
-- Name: kennisbank_vragen_pkey; Type: CONSTRAINT; Schema: public; Owner: michiel; Tablespace: 
--

ALTER TABLE ONLY kennisbank_vragen
    ADD CONSTRAINT kennisbank_vragen_pkey PRIMARY KEY (id);


--
-- Name: logging_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY logging
    ADD CONSTRAINT logging_pkey PRIMARY KEY (id);


--
-- Name: natuurlijk_persoon_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY natuurlijk_persoon
    ADD CONSTRAINT natuurlijk_persoon_pkey PRIMARY KEY (id);


--
-- Name: notitie_pkey; Type: CONSTRAINT; Schema: public; Owner: michiel; Tablespace: 
--

ALTER TABLE ONLY notitie
    ADD CONSTRAINT notitie_pkey PRIMARY KEY (id);


--
-- Name: parkeergebied_kosten_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY parkeergebied_kosten
    ADD CONSTRAINT parkeergebied_kosten_pkey PRIMARY KEY (id);


--
-- Name: parkeergebied_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY parkeergebied
    ADD CONSTRAINT parkeergebied_pkey PRIMARY KEY (id);


--
-- Name: pk_ligplaats; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY bag_ligplaats
    ADD CONSTRAINT pk_ligplaats PRIMARY KEY (identificatie, begindatum, correctie);


--
-- Name: pk_ligplaats_nevenadres; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY bag_ligplaats_nevenadres
    ADD CONSTRAINT pk_ligplaats_nevenadres PRIMARY KEY (identificatie, begindatum, correctie, nevenadres);


--
-- Name: pk_nummeraanduiding; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY bag_nummeraanduiding
    ADD CONSTRAINT pk_nummeraanduiding PRIMARY KEY (identificatie, begindatum, correctie);


--
-- Name: pk_openbareruimte; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY bag_openbareruimte
    ADD CONSTRAINT pk_openbareruimte PRIMARY KEY (identificatie, begindatum, correctie);


--
-- Name: pk_pand; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY bag_pand
    ADD CONSTRAINT pk_pand PRIMARY KEY (identificatie, begindatum, correctie);


--
-- Name: pk_standplaats; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY bag_standplaats
    ADD CONSTRAINT pk_standplaats PRIMARY KEY (identificatie, begindatum, correctie);


--
-- Name: pk_verblijfsobject; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY bag_verblijfsobject
    ADD CONSTRAINT pk_verblijfsobject PRIMARY KEY (identificatie, begindatum, correctie);


--
-- Name: pk_verblijfsobject_gebrdoel; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY bag_verblijfsobject_gebruiksdoel
    ADD CONSTRAINT pk_verblijfsobject_gebrdoel PRIMARY KEY (identificatie, begindatum, correctie, gebruiksdoel);


--
-- Name: pk_verblijfsobject_pand; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY bag_verblijfsobject_pand
    ADD CONSTRAINT pk_verblijfsobject_pand PRIMARY KEY (identificatie, begindatum, correctie, pand);


--
-- Name: pk_woonplaats; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY bag_woonplaats
    ADD CONSTRAINT pk_woonplaats PRIMARY KEY (identificatie, begindatum, correctie);


--
-- Name: sbus_logging_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY sbus_logging
    ADD CONSTRAINT sbus_logging_pkey PRIMARY KEY (id);


--
-- Name: sbus_traffic_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY sbus_traffic
    ADD CONSTRAINT sbus_traffic_pkey PRIMARY KEY (id);


--
-- Name: search_query_delen_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY search_query_delen
    ADD CONSTRAINT search_query_delen_pkey PRIMARY KEY (id);


--
-- Name: search_query_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY search_query
    ADD CONSTRAINT search_query_pkey PRIMARY KEY (id);


--
-- Name: seen_pkey; Type: CONSTRAINT; Schema: public; Owner: michiel; Tablespace: 
--

ALTER TABLE ONLY seen
    ADD CONSTRAINT seen_pkey PRIMARY KEY (id);


--
-- Name: user_app_lock_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY user_app_lock
    ADD CONSTRAINT user_app_lock_pkey PRIMARY KEY (uidnumber, type, type_id);


--
-- Name: zaak_bag_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY zaak_bag
    ADD CONSTRAINT zaak_bag_pkey PRIMARY KEY (id);


--
-- Name: zaak_betrokkenen_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY zaak_betrokkenen
    ADD CONSTRAINT zaak_betrokkenen_pkey PRIMARY KEY (id);


--
-- Name: zaak_kenmerk_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY zaak_kenmerk
    ADD CONSTRAINT zaak_kenmerk_pkey PRIMARY KEY (id);


--
-- Name: zaak_kenmerken_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY zaak_kenmerken
    ADD CONSTRAINT zaak_kenmerken_pkey PRIMARY KEY (id);


--
-- Name: zaak_kenmerken_values_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY zaak_kenmerken_values
    ADD CONSTRAINT zaak_kenmerken_values_pkey PRIMARY KEY (id);


--
-- Name: zaak_meta_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY zaak_meta
    ADD CONSTRAINT zaak_meta_pkey PRIMARY KEY (id);


--
-- Name: zaak_onafgerond_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY zaak_onafgerond
    ADD CONSTRAINT zaak_onafgerond_pkey PRIMARY KEY (zaaktype_id, betrokkene);


--
-- Name: zaak_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY zaak
    ADD CONSTRAINT zaak_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_authorisation_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY zaaktype_authorisation
    ADD CONSTRAINT zaaktype_authorisation_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_betrokkenen_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY zaaktype_betrokkenen
    ADD CONSTRAINT zaaktype_betrokkenen_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_definitie_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY zaaktype_definitie
    ADD CONSTRAINT zaaktype_definitie_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_kenmerken_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY zaaktype_kenmerken
    ADD CONSTRAINT zaaktype_kenmerken_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_node_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY zaaktype_node
    ADD CONSTRAINT zaaktype_node_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_notificatie_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY zaaktype_notificatie
    ADD CONSTRAINT zaaktype_notificatie_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY zaaktype
    ADD CONSTRAINT zaaktype_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_regel_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY zaaktype_regel
    ADD CONSTRAINT zaaktype_regel_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_relatie_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY zaaktype_relatie
    ADD CONSTRAINT zaaktype_relatie_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_resultaten_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY zaaktype_resultaten
    ADD CONSTRAINT zaaktype_resultaten_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_sjablonen_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY zaaktype_sjablonen
    ADD CONSTRAINT zaaktype_sjablonen_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_status_pkey; Type: CONSTRAINT; Schema: public; Owner: zaakadmins; Tablespace: 
--

ALTER TABLE ONLY zaaktype_status
    ADD CONSTRAINT zaaktype_status_pkey PRIMARY KEY (id);


--
-- Name: beheer_import_log_idx_import_id; Type: INDEX; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE INDEX beheer_import_log_idx_import_id ON beheer_import_log USING btree (import_id);


--
-- Name: bibliotheek_kenmerken_values_idx_bibliotheek_kenmerken_id; Type: INDEX; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE INDEX bibliotheek_kenmerken_values_idx_bibliotheek_kenmerken_id ON bibliotheek_kenmerken_values USING btree (bibliotheek_kenmerken_id);


--
-- Name: documents_idx_pid; Type: INDEX; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE INDEX documents_idx_pid ON documents USING btree (pid);


--
-- Name: documents_mail_idx_document_id; Type: INDEX; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE INDEX documents_mail_idx_document_id ON documents_mail USING btree (document_id);


--
-- Name: gm_natuurlijk_persoon_idx_adres_id; Type: INDEX; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE INDEX gm_natuurlijk_persoon_idx_adres_id ON gm_natuurlijk_persoon USING btree (adres_id);


--
-- Name: natuurlijk_persoon_burgerservicenummer; Type: INDEX; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE INDEX natuurlijk_persoon_burgerservicenummer ON natuurlijk_persoon USING btree (burgerservicenummer);


--
-- Name: natuurlijk_persoon_idx_adres_id; Type: INDEX; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE INDEX natuurlijk_persoon_idx_adres_id ON natuurlijk_persoon USING btree (adres_id);


--
-- Name: zaak_betrokkenen_gegevens_magazijn_index; Type: INDEX; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE INDEX zaak_betrokkenen_gegevens_magazijn_index ON zaak_betrokkenen USING btree (gegevens_magazijn_id);


--
-- Name: zaaktype_authorisation_idx_zaaktype_node_id; Type: INDEX; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE INDEX zaaktype_authorisation_idx_zaaktype_node_id ON zaaktype_authorisation USING btree (zaaktype_node_id);


--
-- Name: zaaktype_betrokkenen_idx_zaaktype_node_id; Type: INDEX; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE INDEX zaaktype_betrokkenen_idx_zaaktype_node_id ON zaaktype_betrokkenen USING btree (zaaktype_node_id);


--
-- Name: zaaktype_idx_zaaktype_node_id; Type: INDEX; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE INDEX zaaktype_idx_zaaktype_node_id ON zaaktype USING btree (zaaktype_node_id);


--
-- Name: zaaktype_node_idx_zaaktype_id; Type: INDEX; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE INDEX zaaktype_node_idx_zaaktype_id ON zaaktype_node USING btree (zaaktype_id);


--
-- Name: zaaktype_relatie_idx_relatie_zaaktype_id; Type: INDEX; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE INDEX zaaktype_relatie_idx_relatie_zaaktype_id ON zaaktype_relatie USING btree (relatie_zaaktype_id);


--
-- Name: zaaktype_relatie_idx_zaaktype_node_id; Type: INDEX; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE INDEX zaaktype_relatie_idx_zaaktype_node_id ON zaaktype_relatie USING btree (zaaktype_node_id);


--
-- Name: zaaktype_relatie_idx_zaaktype_status_id; Type: INDEX; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE INDEX zaaktype_relatie_idx_zaaktype_status_id ON zaaktype_relatie USING btree (zaaktype_status_id);


--
-- Name: zaaktype_resultaten_idx_zaaktype_node_id; Type: INDEX; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE INDEX zaaktype_resultaten_idx_zaaktype_node_id ON zaaktype_resultaten USING btree (zaaktype_node_id);


--
-- Name: zaaktype_resultaten_idx_zaaktype_status_id; Type: INDEX; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE INDEX zaaktype_resultaten_idx_zaaktype_status_id ON zaaktype_resultaten USING btree (zaaktype_status_id);


--
-- Name: zaaktype_status_idx_zaaktype_node_id; Type: INDEX; Schema: public; Owner: zaakadmins; Tablespace: 
--

CREATE INDEX zaaktype_status_idx_zaaktype_node_id ON zaaktype_status USING btree (zaaktype_node_id);


--
-- Name: beheer_import_log_import_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY beheer_import_log
    ADD CONSTRAINT beheer_import_log_import_id_fkey FOREIGN KEY (import_id) REFERENCES beheer_import(id);


--
-- Name: bibliotheek_categorie_pid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY bibliotheek_categorie
    ADD CONSTRAINT bibliotheek_categorie_pid_fkey FOREIGN KEY (pid) REFERENCES bibliotheek_categorie(id);


--
-- Name: bibliotheek_kenmerken_bibliotheek_categorie_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY bibliotheek_kenmerken
    ADD CONSTRAINT bibliotheek_kenmerken_bibliotheek_categorie_id_fkey FOREIGN KEY (bibliotheek_categorie_id) REFERENCES bibliotheek_categorie(id);


--
-- Name: bibliotheek_kenmerken_values_bibliotheek_kenmerken_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY bibliotheek_kenmerken_values
    ADD CONSTRAINT bibliotheek_kenmerken_values_bibliotheek_kenmerken_id_fkey FOREIGN KEY (bibliotheek_kenmerken_id) REFERENCES bibliotheek_kenmerken(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: bibliotheek_sjablonen_bibliotheek_categorie_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY bibliotheek_sjablonen
    ADD CONSTRAINT bibliotheek_sjablonen_bibliotheek_categorie_id_fkey FOREIGN KEY (bibliotheek_categorie_id) REFERENCES bibliotheek_categorie(id);


--
-- Name: bibliotheek_sjablonen_filestore_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY bibliotheek_sjablonen
    ADD CONSTRAINT bibliotheek_sjablonen_filestore_id_fkey FOREIGN KEY (filestore_id) REFERENCES filestore(id);


--
-- Name: bibliotheek_sjablonen_magic_strin_bibliotheek_sjablonen_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY bibliotheek_sjablonen_magic_string
    ADD CONSTRAINT bibliotheek_sjablonen_magic_strin_bibliotheek_sjablonen_id_fkey FOREIGN KEY (bibliotheek_sjablonen_id) REFERENCES bibliotheek_sjablonen(id);


--
-- Name: checklist_vraag_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY checklist_vraag
    ADD CONSTRAINT checklist_vraag_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES zaaktype_node(id) DEFERRABLE;


--
-- Name: checklist_vraag_zaaktype_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY checklist_vraag
    ADD CONSTRAINT checklist_vraag_zaaktype_status_id_fkey FOREIGN KEY (zaaktype_status_id) REFERENCES zaaktype_status(id) DEFERRABLE;


--
-- Name: documenten_filestore_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: michiel
--

ALTER TABLE ONLY documenten
    ADD CONSTRAINT documenten_filestore_id_fkey FOREIGN KEY (filestore_id) REFERENCES filestore(id);


--
-- Name: documenten_folder_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: michiel
--

ALTER TABLE ONLY documenten
    ADD CONSTRAINT documenten_folder_id_fkey FOREIGN KEY (folder_id) REFERENCES documenten(id);


--
-- Name: documenten_job_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: michiel
--

ALTER TABLE ONLY documenten
    ADD CONSTRAINT documenten_job_id_fkey FOREIGN KEY (job_id) REFERENCES jobs(id);


--
-- Name: documenten_notitie_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: michiel
--

ALTER TABLE ONLY documenten
    ADD CONSTRAINT documenten_notitie_id_fkey FOREIGN KEY (notitie_id) REFERENCES notitie(id);


--
-- Name: documenten_zaak_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: michiel
--

ALTER TABLE ONLY documenten
    ADD CONSTRAINT documenten_zaak_id_fkey FOREIGN KEY (zaak_id) REFERENCES zaak(id);


--
-- Name: documenten_zaaktype_kenmerken_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: michiel
--

ALTER TABLE ONLY documenten
    ADD CONSTRAINT documenten_zaaktype_kenmerken_id_fkey FOREIGN KEY (zaaktype_kenmerken_id) REFERENCES zaaktype_kenmerken(id);


--
-- Name: documents_mail_document_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY documents_mail
    ADD CONSTRAINT documents_mail_document_id_fkey FOREIGN KEY (document_id) REFERENCES documents(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: documents_pid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY documents
    ADD CONSTRAINT documents_pid_fkey FOREIGN KEY (pid) REFERENCES documents(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: gm_natuurlijk_persoon_adres_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY gm_natuurlijk_persoon
    ADD CONSTRAINT gm_natuurlijk_persoon_adres_id_fkey FOREIGN KEY (adres_id) REFERENCES gm_adres(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: kennisbank_producten_bibliotheek_categorie_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: michiel
--

ALTER TABLE ONLY kennisbank_producten
    ADD CONSTRAINT kennisbank_producten_bibliotheek_categorie_id_fkey FOREIGN KEY (bibliotheek_categorie_id) REFERENCES bibliotheek_categorie(id);


--
-- Name: kennisbank_producten_pid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: michiel
--

ALTER TABLE ONLY kennisbank_producten
    ADD CONSTRAINT kennisbank_producten_pid_fkey FOREIGN KEY (pid) REFERENCES kennisbank_producten(id);


--
-- Name: kennisbank_relaties_kennisbank_producten_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: michiel
--

ALTER TABLE ONLY kennisbank_relaties
    ADD CONSTRAINT kennisbank_relaties_kennisbank_producten_id_fkey FOREIGN KEY (kennisbank_producten_id) REFERENCES kennisbank_producten(id);


--
-- Name: kennisbank_relaties_kennisbank_vragen_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: michiel
--

ALTER TABLE ONLY kennisbank_relaties
    ADD CONSTRAINT kennisbank_relaties_kennisbank_vragen_id_fkey FOREIGN KEY (kennisbank_vragen_id) REFERENCES kennisbank_vragen(id);


--
-- Name: kennisbank_relaties_zaaktype_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: michiel
--

ALTER TABLE ONLY kennisbank_relaties
    ADD CONSTRAINT kennisbank_relaties_zaaktype_id_fkey FOREIGN KEY (zaaktype_id) REFERENCES zaaktype(id);


--
-- Name: kennisbank_vragen_bibliotheek_categorie_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: michiel
--

ALTER TABLE ONLY kennisbank_vragen
    ADD CONSTRAINT kennisbank_vragen_bibliotheek_categorie_id_fkey FOREIGN KEY (bibliotheek_categorie_id) REFERENCES bibliotheek_categorie(id);


--
-- Name: kennisbank_vragen_pid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: michiel
--

ALTER TABLE ONLY kennisbank_vragen
    ADD CONSTRAINT kennisbank_vragen_pid_fkey FOREIGN KEY (pid) REFERENCES kennisbank_vragen(id);


--
-- Name: natuurlijk_persoon_adres_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY natuurlijk_persoon
    ADD CONSTRAINT natuurlijk_persoon_adres_id_fkey FOREIGN KEY (adres_id) REFERENCES adres(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: sbus_logging_pid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY sbus_logging
    ADD CONSTRAINT sbus_logging_pid_fkey FOREIGN KEY (pid) REFERENCES sbus_logging(id);


--
-- Name: sbus_logging_sbus_traffic_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY sbus_logging
    ADD CONSTRAINT sbus_logging_sbus_traffic_id_fkey FOREIGN KEY (sbus_traffic_id) REFERENCES sbus_traffic(id);


--
-- Name: search_query_delen_search_query_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY search_query_delen
    ADD CONSTRAINT search_query_delen_search_query_id_fkey FOREIGN KEY (search_query_id) REFERENCES search_query(id);


--
-- Name: seen_kennisbank_producten_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: michiel
--

ALTER TABLE ONLY seen
    ADD CONSTRAINT seen_kennisbank_producten_id_fkey FOREIGN KEY (kennisbank_producten_id) REFERENCES kennisbank_producten(id);


--
-- Name: seen_kennisbank_vragen_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: michiel
--

ALTER TABLE ONLY seen
    ADD CONSTRAINT seen_kennisbank_vragen_id_fkey FOREIGN KEY (kennisbank_vragen_id) REFERENCES kennisbank_vragen(id);


--
-- Name: zaak_aanvrager_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY zaak
    ADD CONSTRAINT zaak_aanvrager_fkey FOREIGN KEY (aanvrager) REFERENCES zaak_betrokkenen(id);


--
-- Name: zaak_bag_pid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY zaak_bag
    ADD CONSTRAINT zaak_bag_pid_fkey FOREIGN KEY (pid) REFERENCES zaak_bag(id);


--
-- Name: zaak_bag_zaak_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY zaak_bag
    ADD CONSTRAINT zaak_bag_zaak_id_fkey FOREIGN KEY (zaak_id) REFERENCES zaak(id);


--
-- Name: zaak_behandelaar_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY zaak
    ADD CONSTRAINT zaak_behandelaar_fkey FOREIGN KEY (behandelaar) REFERENCES zaak_betrokkenen(id);


--
-- Name: zaak_betrokkenen_zaak_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY zaak_betrokkenen
    ADD CONSTRAINT zaak_betrokkenen_zaak_id_fkey FOREIGN KEY (zaak_id) REFERENCES zaak(id);


--
-- Name: zaak_coordinator_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY zaak
    ADD CONSTRAINT zaak_coordinator_fkey FOREIGN KEY (coordinator) REFERENCES zaak_betrokkenen(id);


--
-- Name: zaak_kenmerk_bibliotheek_kenmerken_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY zaak_kenmerk
    ADD CONSTRAINT zaak_kenmerk_bibliotheek_kenmerken_id_fkey FOREIGN KEY (bibliotheek_kenmerken_id) REFERENCES bibliotheek_kenmerken(id);


--
-- Name: zaak_kenmerk_zaak_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY zaak_kenmerk
    ADD CONSTRAINT zaak_kenmerk_zaak_id_fkey FOREIGN KEY (zaak_id) REFERENCES zaak(id);


--
-- Name: zaak_kenmerken_bibliotheek_kenmerken_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY zaak_kenmerken
    ADD CONSTRAINT zaak_kenmerken_bibliotheek_kenmerken_id_fkey FOREIGN KEY (bibliotheek_kenmerken_id) REFERENCES bibliotheek_kenmerken(id);


--
-- Name: zaak_kenmerken_values_bibliotheek_kenmerken_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY zaak_kenmerken_values
    ADD CONSTRAINT zaak_kenmerken_values_bibliotheek_kenmerken_id_fkey FOREIGN KEY (bibliotheek_kenmerken_id) REFERENCES bibliotheek_kenmerken(id);


--
-- Name: zaak_kenmerken_values_zaak_bag_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY zaak_kenmerken_values
    ADD CONSTRAINT zaak_kenmerken_values_zaak_bag_id_fkey FOREIGN KEY (zaak_bag_id) REFERENCES zaak_bag(id);


--
-- Name: zaak_kenmerken_values_zaak_kenmerken_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY zaak_kenmerken_values
    ADD CONSTRAINT zaak_kenmerken_values_zaak_kenmerken_id_fkey FOREIGN KEY (zaak_kenmerken_id) REFERENCES zaak_kenmerken(id);


--
-- Name: zaak_kenmerken_zaak_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY zaak_kenmerken
    ADD CONSTRAINT zaak_kenmerken_zaak_id_fkey FOREIGN KEY (zaak_id) REFERENCES zaak(id);


--
-- Name: zaak_locatie_correspondentie_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY zaak
    ADD CONSTRAINT zaak_locatie_correspondentie_fkey FOREIGN KEY (locatie_correspondentie) REFERENCES zaak_bag(id);


--
-- Name: zaak_locatie_zaak_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY zaak
    ADD CONSTRAINT zaak_locatie_zaak_fkey FOREIGN KEY (locatie_zaak) REFERENCES zaak_bag(id);


--
-- Name: zaak_meta_zaak_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY zaak_meta
    ADD CONSTRAINT zaak_meta_zaak_id_fkey FOREIGN KEY (zaak_id) REFERENCES zaak(id);


--
-- Name: zaak_zaaktype_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY zaak
    ADD CONSTRAINT zaak_zaaktype_id_fkey FOREIGN KEY (zaaktype_id) REFERENCES zaaktype(id);


--
-- Name: zaak_zaaktype_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY zaak_onafgerond
    ADD CONSTRAINT zaak_zaaktype_id_fkey FOREIGN KEY (zaaktype_id) REFERENCES zaaktype(id);


--
-- Name: zaak_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY zaak
    ADD CONSTRAINT zaak_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES zaaktype_node(id);


--
-- Name: zaaktype_authorisation_zaaktype_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY zaaktype_authorisation
    ADD CONSTRAINT zaaktype_authorisation_zaaktype_id_fkey FOREIGN KEY (zaaktype_id) REFERENCES zaaktype(id);


--
-- Name: zaaktype_authorisation_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY zaaktype_authorisation
    ADD CONSTRAINT zaaktype_authorisation_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES zaaktype_node(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype_betrokkenen_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY zaaktype_betrokkenen
    ADD CONSTRAINT zaaktype_betrokkenen_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES zaaktype_node(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype_bibliotheek_categorie_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY zaaktype
    ADD CONSTRAINT zaaktype_bibliotheek_categorie_id_fkey FOREIGN KEY (bibliotheek_categorie_id) REFERENCES bibliotheek_categorie(id) DEFERRABLE;


--
-- Name: zaaktype_kenmerken_bibliotheek_kenmerken_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY zaaktype_kenmerken
    ADD CONSTRAINT zaaktype_kenmerken_bibliotheek_kenmerken_id_fkey FOREIGN KEY (bibliotheek_kenmerken_id) REFERENCES bibliotheek_kenmerken(id);


--
-- Name: zaaktype_kenmerken_zaak_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY zaaktype_kenmerken
    ADD CONSTRAINT zaaktype_kenmerken_zaak_status_id_fkey FOREIGN KEY (zaak_status_id) REFERENCES zaaktype_status(id);


--
-- Name: zaaktype_kenmerken_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY zaaktype_kenmerken
    ADD CONSTRAINT zaaktype_kenmerken_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES zaaktype_node(id);


--
-- Name: zaaktype_node_zaaktype_definitie_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY zaaktype_node
    ADD CONSTRAINT zaaktype_node_zaaktype_definitie_id_fkey FOREIGN KEY (zaaktype_definitie_id) REFERENCES zaaktype_definitie(id) DEFERRABLE;


--
-- Name: zaaktype_node_zaaktype_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY zaaktype_node
    ADD CONSTRAINT zaaktype_node_zaaktype_id_fkey FOREIGN KEY (zaaktype_id) REFERENCES zaaktype(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype_notificatie_zaak_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY zaaktype_notificatie
    ADD CONSTRAINT zaaktype_notificatie_zaak_status_id_fkey FOREIGN KEY (zaak_status_id) REFERENCES zaaktype_status(id);


--
-- Name: zaaktype_notificatie_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY zaaktype_notificatie
    ADD CONSTRAINT zaaktype_notificatie_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES zaaktype_node(id);


--
-- Name: zaaktype_regel_zaak_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY zaaktype_regel
    ADD CONSTRAINT zaaktype_regel_zaak_status_id_fkey FOREIGN KEY (zaak_status_id) REFERENCES zaaktype_status(id);


--
-- Name: zaaktype_regel_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY zaaktype_regel
    ADD CONSTRAINT zaaktype_regel_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES zaaktype_node(id);


--
-- Name: zaaktype_relatie_relatie_zaaktype_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY zaaktype_relatie
    ADD CONSTRAINT zaaktype_relatie_relatie_zaaktype_id_fkey FOREIGN KEY (relatie_zaaktype_id) REFERENCES zaaktype(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype_relatie_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY zaaktype_relatie
    ADD CONSTRAINT zaaktype_relatie_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES zaaktype_node(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype_relatie_zaaktype_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY zaaktype_relatie
    ADD CONSTRAINT zaaktype_relatie_zaaktype_status_id_fkey FOREIGN KEY (zaaktype_status_id) REFERENCES zaaktype_status(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype_resultaten_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY zaaktype_resultaten
    ADD CONSTRAINT zaaktype_resultaten_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES zaaktype_node(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype_resultaten_zaaktype_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY zaaktype_resultaten
    ADD CONSTRAINT zaaktype_resultaten_zaaktype_status_id_fkey FOREIGN KEY (zaaktype_status_id) REFERENCES zaaktype_status(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype_sjablonen_bibliotheek_sjablonen_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY zaaktype_sjablonen
    ADD CONSTRAINT zaaktype_sjablonen_bibliotheek_sjablonen_id_fkey FOREIGN KEY (bibliotheek_sjablonen_id) REFERENCES bibliotheek_sjablonen(id);


--
-- Name: zaaktype_sjablonen_zaak_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY zaaktype_sjablonen
    ADD CONSTRAINT zaaktype_sjablonen_zaak_status_id_fkey FOREIGN KEY (zaak_status_id) REFERENCES zaaktype_status(id);


--
-- Name: zaaktype_sjablonen_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY zaaktype_sjablonen
    ADD CONSTRAINT zaaktype_sjablonen_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES zaaktype_node(id);


--
-- Name: zaaktype_status_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY zaaktype_status
    ADD CONSTRAINT zaaktype_status_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES zaaktype_node(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zaakadmins
--

ALTER TABLE ONLY zaaktype
    ADD CONSTRAINT zaaktype_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES zaaktype_node(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

