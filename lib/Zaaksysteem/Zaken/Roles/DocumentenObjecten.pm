package Zaaksysteem::Zaken::Roles::DocumentenObjecten;

use Moose::Role;
use Data::Dumper;
use Clone qw/clone/;

#
# new sleeker version. avoid excessive rule execution
#
sub is_documents_complete {
    my ($self) = @_;

    my $goto_status = (
        $self->volgende_fase
            ? $self->volgende_fase->status
            : $self->huidige_fase->status
        );

    my $statusses = $self->zaaktype_node_id->zaaktype_statussen->search({
        status => { '<=' => $goto_status}
    });

    my $required_documents = $self->zaaktype_node_id->zaaktype_kenmerken({
        'zaak_status_id'                         => { '-in' => $statusses->get_column('id')->as_query },
        'bibliotheek_kenmerken_id.value_type'    => 'file',
        'value_mandatory'                        => 1,
    },
    {
        'join'  => 'bibliotheek_kenmerken_id'
    });

    my $given_kenmerken ||= $self->field_values();

    my $rules_result;
    my $required_fields = $self->visible_fields({
        mandatory       => 1,
        field_values    => clone($given_kenmerken),
        result          => $rules_result,
        include_documents => 1,
    });


    while (my $required_document = $required_documents->next) {
        next unless $required_fields->{
            $required_document->bibliotheek_kenmerken_id->id
        };

        if (
            !$self  ->files
                    ->search_by_case_type_document_id($required_document->id)
                    ->count
        ) {
            return 0;
        }
    }

    return 1;
}



sub is_documenten_compleet {
    my ($self, $args) = @_;

    ### Check if every kenmerk is filled in this fase and the fase before
    my $goto_status = (
        $self->volgende_fase
            ? $self->volgende_fase->status
            : $self->huidige_fase->status
        );

    my $statusses = $self->zaaktype_node_id->zaaktype_statussen->search(
        {
            status  => { '<=' => $goto_status }
        },
        {
            order_by    => { '-asc' => 'id' }
        }
    );

    my $incomplete;
    while (my $status = $statusses->next) {
        unless (
            $self->_check_phase_documents({ 
                status => $status->status
            })
        ) {
            $incomplete = $status->status
        }
    }

    if ($incomplete) {
        return {
            phase   => $incomplete,
            result  => 0,
        };
    } else {
        return {
            result  => 1,
        };

    }
}



sub _check_phase_documents {
    my ($self, $args) = @_;


    my $fasen   = $self->zaaktype_node_id
        ->zaaktype_statussen
        ->search({
            status => $args->{status},
        });

    my $fase    = $fasen->first or die('Er is geen volgende fase');

    my $documenten  = $fase->zaaktype_kenmerken->search(
            {
                'bibliotheek_kenmerken_id.value_type'   => 'file',
                'me.value_mandatory'                    => 1,
            },
            {
                'join'  => 'bibliotheek_kenmerken_id'
            }
        );

    my $error = 0;

    ### Get list of phase_fields
    my $required_fields;
    {
        my $given_kenmerken ||= $self->field_values({ fase => $fase->status });

        my $kenmerken       = { %{ $given_kenmerken } };

        my $rules_result;
        $required_fields = $self->visible_fields({
            phase           => $fase,
            mandatory       => 1,
            field_values    => $given_kenmerken,
            result          => $rules_result,
            include_documents => 1,
        });
    }


    while (my $document = $documenten->next) {
        next unless (
            defined(
                $required_fields->{ $document->bibliotheek_kenmerken_id->id }
            ) &&
            $required_fields->{ $document->bibliotheek_kenmerken_id->id }
        );

        if (!$self->documents->search({
                'zaaktype_kenmerken_id' => $document->id,
                'deleted_on'            => undef
            })->count
        ) {
            $error = 1;
        }
    }

    return 1 unless $error;
    return 0;
}

sub is_document_queue_empty {
    my $self        = shift;

    return (
        $self->files->search(
            {
                accepted        => 0,
                date_deleted => undef,
                destroyed => { '!=' => 1 }
            }
        )->count
    ) ? undef : 1;
}


around 'can_volgende_fase' => sub {
    my $orig    = shift;
    my $self    = shift;

    my $advance_result = $self->$orig(@_);

    
#    my $result = $self->is_documenten_compleet;
#    if ($result && $result->{result} == 1 &&

    if(
        $self->is_documents_complete &&
        $self->is_document_queue_empty
    ) {
        $advance_result->documents_complete(1);
    }

    return $advance_result;
};


sub _publishable_fields {
    my ($self) = @_;

    return $self->zaaktype_id->zaaktype_node_id->zaaktype_kenmerkens->search({
        'publish_public' => 1,
        'bibliotheek_kenmerken_id.value_type' => 'file',
    }, {
        join => ['bibliotheek_kenmerken_id'],
    });
}


sub publishable_documents {
    my ($self) = @_;
    
    my $documents = $self->documents->search({
        zaaktype_kenmerken_id => { 
            '-in' => $self->_publishable_fields->get_column('id')->as_query 
        }
    }, {
        order_by => 'id',
    });

    return $documents;
}

sub non_publishable_documents {
    my ($self) = @_;

    my $documents = $self->documents->search({
        zaaktype_kenmerken_id => { 
            '-not_in' => $self->_publishable_fields->get_column('id')->as_query 
        }
    }, {
        order_by => 'id',
    });

    return $documents;
}


1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

