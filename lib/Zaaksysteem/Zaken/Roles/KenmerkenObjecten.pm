package Zaaksysteem::Zaken::Roles::KenmerkenObjecten;

use Moose::Role;
use Data::Dumper;
use Zaaksysteem::Constants;
use Zaaksysteem::Zaken::AdvanceResult;



{
=head1

field_values
get values of zaak_kenmerken (case fields)

bibliotheek_kenmerken_id:  limit the results on a single field. 

=cut

    Params::Profile->register_profile(
        method  => 'field_values',
        profile => {
            optional => [ qw/
                bibliotheek_kenmerken_id
                fase
            / ],
        }
    );
    sub field_values {
        my $self = shift;
        my $params = shift || {};

        return $self->result_source->resultset->{attrs}->{cache}->get({
            key => 'field_values'
        }) if (
            $self->result_source->resultset->{attrs}->{cache}->get({
                key         => 'field_values',
                params      => { %$params, zaak_id => $self->id }
            })
        );

		$params ||= {};
        my $dv = Params::Profile->check(params  => $params);
        die "invalid options for field_values" unless $dv->success;

        my $where = {};
        if($dv->valid('bibliotheek_kenmerken_id')) {
            $where->{bibliotheek_kenmerken_id} = $dv->valid('bibliotheek_kenmerken_id');
        }
 
        my $options = {
            prefetch        => [ 
                'bibliotheek_kenmerken_id',
            ],
            order_by        => { '-asc'   => 'me.id' },
        };

        if($params->{fase}) {
            $where->{bibliotheek_kenmerken_id} = $self->_get_phase_field_ids({
                fase => $params->{fase},
            });    
        }

        my $kenmerken           = $self->zaak_kenmerken->search($where, $options);
        my $zaaktype_kenmerken  = $self
                                ->zaaktype_node_id
                                ->zaaktype_kenmerken->search(
                                    {
                                        bibliotheek_kenmerken_id => {
                                            in => $kenmerken->get_column('bibliotheek_kenmerken_id')->as_query
                                        },
                                    },
                                    {
                                        prefetch                 => ['bibliotheek_kenmerken_id']
                                    }
                                );

        ### Sort kenmerken by bibliotheek_kenmerken_id
        my %kenmerk_rows_by_bib_id;
        while (my $kenmerk = $kenmerken->next) {
            next unless $kenmerk->bibliotheek_kenmerken_id;

            if ($kenmerk_rows_by_bib_id{$kenmerk->bibliotheek_kenmerken_id->id}) {
                push(
                    @{ $kenmerk_rows_by_bib_id{$kenmerk->bibliotheek_kenmerken_id->id} },
                    $kenmerk
                );
            } else {
                $kenmerk_rows_by_bib_id{$kenmerk->bibliotheek_kenmerken_id->id} = [$kenmerk];
            }
        }
      
        my $kenmerk_values  = {};
        my $veldopties      = ZAAKSYSTEEM_CONSTANTS->{veld_opties};

        my %prevent_collisions;

        while (my $zaaktype_kenmerk = $zaaktype_kenmerken->next) {
            my $field_object                = $zaaktype_kenmerk->bibliotheek_kenmerken_id or die "need object";
            my $bibliotheek_kenmerken_id    = $field_object->id;
            my $kenmerken                   = $kenmerk_rows_by_bib_id{$bibliotheek_kenmerken_id};

            next if($prevent_collisions{ $bibliotheek_kenmerken_id });
            $prevent_collisions{ $bibliotheek_kenmerken_id }++;

            # if the field is referential, it is taken from the parent.
            # potential recursion hazard - keep track of visited cases.
            
            # TODO performance hazard - rewrite this sub:
            # - first get zaaktype_kenmerken
            # - limit on phase
            # - use output to get values

            # TODO make bibliotheek_kenmerken_id unique per zaaktype_node_id, or allow multiple instances

            if($zaaktype_kenmerk && $zaaktype_kenmerk->referential && $self->pid) {
                my $case_id = $self->id;

                die "recursion hazard, quitting" if exists 
                    $params->{recursion_hazard}->{ $case_id };

                my $recursion_protection = $params->{recursion_protection} || {};
                $recursion_protection->{ $case_id } = 1;

                my $pid_field_values = $self->pid->field_values({
                    bibliotheek_kenmerken_id    => $bibliotheek_kenmerken_id,
                    recursion_protection        => $recursion_protection
                });

                $kenmerk_values->{
                    $bibliotheek_kenmerken_id
                } = $pid_field_values->{
                    $bibliotheek_kenmerken_id
                };
                next;
            }

            for my $kenmerk (@{ $kenmerken }) {
                next unless(length $kenmerk->value);

                if (
                    $kenmerk->bibliotheek_kenmerken_id->type_multiple ||
                    $veldopties->{
                        $kenmerk->bibliotheek_kenmerken_id->value_type
                    }->{multiple} ||
                    $kenmerk_values->{$bibliotheek_kenmerken_id}
                ) {
                    if (
                        $kenmerk_values->{$bibliotheek_kenmerken_id} &&
                        !UNIVERSAL::isa(
                            $kenmerk_values->{$bibliotheek_kenmerken_id},
                            'ARRAY'
                        )
                    ) {
                        $kenmerk_values->{$bibliotheek_kenmerken_id} = [
                            $kenmerk_values->{$bibliotheek_kenmerken_id}
                        ];
                    } else {
                        $kenmerk_values->{$bibliotheek_kenmerken_id} ||= [];
                    }

                    push(
                        @{ $kenmerk_values->{$bibliotheek_kenmerken_id} },
                        $kenmerk->value
                    );
                } else {
                    $kenmerk_values->{$bibliotheek_kenmerken_id} = $kenmerk->value;
                }
            }
        }

        return $self->result_source->resultset->{attrs}->{cache}->set({
            key         => 'field_values',
            value       => $kenmerk_values,
            params      => { %$params, zaak_id => $self->id }
        });
    }
}


#
# to limit the retrieve value to only the values of a given phase, an in-query
# is generated that select that ids of the fields for the phase.
#
sub _get_phase_field_ids {
    my ($self, $params) = @_;

    my $first_status       = $self->zaaktype_node_id
        ->zaaktype_statussen
        ->search(
            {
                status  => $params->{fase},
            }
        )->first;

    my $zaaktype_kenmerken_rs = $self->zaaktype_node_id
        ->zaaktype_kenmerken
        ->search(
            {
                zaak_status_id  => $first_status->id,
            },
            {
                prefetch    => ['bibliotheek_kenmerken_id', 'zaak_status_id'],
                order_by    => 'me.id'
            }
        );

    return { 
        -in => $zaaktype_kenmerken_rs->get_column('bibliotheek_kenmerken_id')->as_query 
    };
}



around 'can_volgende_fase' => sub {
    my $orig    = shift;
    my $self    = shift;
    
    my $advance_result = $self->$orig(@_);

    ### Check if every kenmerk is filled in this fase an d the fase before
    my $goto_status = (
        $self->volgende_fase
            ? $self->volgende_fase->status
            : $self->huidige_fase->status
        );

    my $statusses = $self->zaaktype_node_id->zaaktype_statussen->search(
        {
            status  => { '<=' => $goto_status }
        },
        {
            order_by    => { '-asc' => 'id' }
        }
    );

    my $incomplete;
    while (my $status = $statusses->next) {
        my $rules_result = $self->phase_fields_complete({
            phase => $status
        });

        $incomplete++ if ($rules_result->{required});
    }

    $advance_result->fields_complete(1) unless $incomplete;

    return $advance_result;
};


1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

