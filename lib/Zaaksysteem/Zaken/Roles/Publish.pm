package Zaaksysteem::Zaken::Roles::Publish;

use Moose::Role;
use Hash::Merge::Simple qw( clone_merge );
use Data::Dumper;
$Data::Dumper::Sortkeys = 1;

#use XML::Simple;
use Archive::Zip;
use File::stat;
#use XML::Dumper;
use XML::Simple qw(:strict);
use Encode;
use Params::Profile;
use Template;
use Zaaksysteem::Constants;


=head1 METHODS

=head2 publish

Return value: file reference

    my $file_reference = $zaak->publish();

=cut

{
    Params::Profile->register_profile(
        'method'    => 'export_vergaderingen_ede',
        'profile'   => {
            required    => [ qw/root_dir publish_dir published_file_ids published_related_ids/ ],
            msgs        => PARAMS_PROFILE_DEFAULT_MSGS,
        }
    );

    sub export_vergaderingen_ede {
        my $self        = shift;
        my $opts        = shift;

        my $dv          = Params::Profile->check(params => $opts);

        die('Parameters incorrect:' . Dumper($opts)) unless $dv->success;
        my $args = $dv->valid();

        my $publish_dir = $dv->valid('publish_dir');
        my $root_dir    = $dv->valid('root_dir');

        my $logging = '';

        # create publish dir
        unless(-d $publish_dir) {
            mkdir $publish_dir
                or die "could not create $publish_dir: $!\n";
            $logging .= "Created export directory: $publish_dir.\n";
        }

        my $published_related_ids = $dv->valid('published_related_ids');
        foreach my $relation_view ($self->zaak_relaties) {
            next unless $relation_view->type eq 'related';

            my $related_case = $relation_view->case;

            next unless $published_related_ids->{ $related_case->id };

            $logging .= "Related case: " . $related_case->id . "\n";
        }

        my $published_file_ids = $dv->valid('published_file_ids');
        $logging .= $self->_export_files_as_pdf({
            published_file_ids => $published_file_ids,
            publish_dir => $dv->valid('publish_dir'),
        });

        foreach my $publish ( 0, 1 ) {
            my $xml_output = $self->publish_tt({
                publish     => $publish,
                root_dir    => $root_dir,
                published_file_ids => $published_file_ids,
                published_related_ids => $published_related_ids
            });
    
            # write case xml to disk
            my $number = sprintf("%024d", $self->id * 10 + $publish);
            my $filename = $publish_dir . $number . ".xml";
            $logging .= "Writing to $filename.\n";

            open FILE, ">$filename"
                or die "could not write to $filename: $!";    
            print FILE $xml_output;
            close FILE;
        }
        return $logging;
    }
}


sub _export_files_as_pdf {
    my ($self, $options) = @_;

    my $published_file_ids  = $options->{published_file_ids}    or die "need published_file_ids";
    my $publish_dir         = $options->{publish_dir}           or die "need publish_dir";

    my $file_model = $self->result_source->schema->resultset('File');

    my $logging = '';
    foreach my $published_file_id (keys %$published_file_ids) {
        my $file = $file_model->find($published_file_id);
        die "File $published_file_id not found, reload page." unless $file;

        my $pdf_filename = $file->filestore_id->uuid . '.pdf';
        my $pdf_filepath = $publish_dir . $pdf_filename;

       $logging .= "Generating PDF for published file $published_file_id: " . $file->name . " to '$pdf_filepath'.\n";

        my $convert_filepath = $file->filestore_id->convert({
            target_format   => 'pdf',
            target_file     => $pdf_filepath,
        });

        # this is the case with pdf - already a file on disk. we can just use that one.
        # to include it in the export however, it need to go in to the temporary
        # upload directory.
        if($convert_filepath ne $pdf_filepath) {
            system("cp $convert_filepath $pdf_filepath");
        }
    }
    return $logging;
}        


sub value_by_magic_string {
    my ($self, $opts) = @_;
    
    my $magic_string = $opts->{magic_string} or die "need magic string";
    $magic_string    = lc($magic_string);

    my $required     = $opts->{required};

    my $CACHE_KEY    = '__values_by_magic_string_cached';

    unless($self->{$CACHE_KEY}) {

        my $field_values = $self->field_values;

        my $fields = $self->zaaktype_node_id->zaaktype_kenmerken->search({
            bibliotheek_kenmerken_id    => { -not => undef }
        });

        unless($opts->{ plain }) {
            $fields = $fields->search({ publish_public => 1 });
        }
    
        my $map = {}; 
          
        while(my $row = $fields->next()) {
            next unless $row->bibliotheek_kenmerken_id;

            my $bibliotheek_kenmerken_id    = $row->bibliotheek_kenmerken_id->id;
            my $magic_string                = $row->bibliotheek_kenmerken_id->magic_string;

            my $value = $field_values->{$bibliotheek_kenmerken_id};

            if($row->bibliotheek_kenmerken_id->value_type eq 'file') {
                my $file = $self->files->search({case_type_document_id => $row->id})->first;
                next unless $file;
                $value = $file->id;
            } elsif(ref $value && ref $value eq 'ARRAY') {
                $value = join ",", @$value;
            }

            $map->{$magic_string} = $value || '';
                        
        }

        $self->{$CACHE_KEY} = $map;
    }

    if(exists $self->{$CACHE_KEY}->{$magic_string}) {
        return $self->{$CACHE_KEY}->{$magic_string};
    }

    # else    
    my $warning = "Het veld '$magic_string' is niet gedefinieerd voor zaak " .
        $self->id . 
        ". Controleer het zaaktype. Klopt de magic string, en staat 'Publieke website' aangevinkt?\n";

    if($required) {
        die $warning;
    }
    #warn $warning;
    return '';
}



sub publish_tt {
    my ($self, $opts) = @_;
    
    my $root_dir = $opts->{root_dir} or die "need root_dir";

    my $tt = Template->new({
        INTERPOLATE => 1,
        ABSOLUTE    => 1,
    }) or die "$Template::ERROR";

    my $stash = {
        zaak => $self,
        c => $opts->{ c },
        published_file_ids => $opts->{ published_file_ids },
        published_related_ids => $opts->{ published_related_ids }
    };

    my $tijdstip = $self->value_by_magic_string({magic_string => 'vergader_tijdstip', required => 1});
    
    die "Tijdstip in zaak " . $self->id . " in incorrect format: $tijdstip. Formaat moet HH::MM zijn.\n"
        unless $tijdstip && $tijdstip =~ m|^[0-9]{2}\:[0-9]{2}$|; 
    

    $stash->{publish} = 1 if $opts->{publish};

    my $output;

    $tt->process(
        $root_dir . '/tpl/zaak_v1/nl_NL/plugins/vergaderingen_ede/publish.xml.tt', 
        $stash,
        \$output
    ) or die $tt->error();

    return $output;    
}

1;
