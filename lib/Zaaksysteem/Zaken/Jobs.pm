package Zaaksysteem::Zaken::Jobs;

use Moose;

use Data::Dumper;

use Zaaksysteem::Constants;
use Zaaksysteem::Profiles;
use Params::Profile;

has 'zaak'  => (
    is  => 'rw'
);

=head1 METHODS

=head2 update_kenmerk(\%options)

Return value: $true_or_false

 $zaak->update_kenmerk(
    {
        betrokkene      => $betrokkene_object,
        kenmerk_id      => 23,
        kenmerk_value   => 'Gewijzigd naar pietje',
    }
 );

B<Options>

=over 4

=item betrokkene_id

required

Betrokkene identifier which triggered this call

=item kenmerk_id

Kenmerk to change

=item kenmerk_value

Value for kenmerk

=back

=cut

{

    Params::Profile->register_profile(
        'method'    => 'register_update_kenmerk',
        'profile'   => PROFILE_JOB_UPDATE_KENMERK
    );

    sub register_update_kenmerk {
        my $self        = shift;
        my $opts        = shift;

        my $dv          = Params::Profile->check(params => $opts);

        die('Parameters incorrect:' . Dumper([$dv->invalid, $dv->missing]))
            unless $dv->success;

        my $betrokkene = $dv->valid('betrokkene');

        ### Retrieve human readable informatino
        my ($bericht, $naam, $kenmerk);
        {
            ### Retrieve kenmerk information
            $kenmerk = $self->zaak
                ->zaaktype_node_id
                ->zaaktype_kenmerken
                ->search(
                    {
                        bibliotheek_kenmerken_id => $dv->valid('kenmerk_id')
                    },
                    {
                        rows    => 1,
                    }
                )->first;

                die(
                    'Cannot find kenmerk with id: '
                    . $dv->valid('kenmerk_id')
                ) unless $kenmerk;

                die(
                    'Kenmerk kan niet worden gewijzigd via PIP'
                ) unless $kenmerk->pip_can_change;

                die(
                    'Kenmerk kan niet worden gewijzigd door deze persoon'
                ) unless (
                    $self->zaak->aanvrager_object->betrokkene_identifier eq
                        $betrokkene->betrokkene_identifier
                );

                $naam       = $betrokkene->naam
                    . ' wenst een kenmerk te wijzigen';

                $bericht    = "Beste behandelaar,\n"
                    ."\n"
                    ."Aanvrager van deze zaak, " . $betrokkene->naam . ","
                    ." wenst de volgende wijziging uit te voeren aan de zaak:"
                    ."\n\n"
                    ."Huidig kenmerk:\n"
                    . $kenmerk->label . "\n\n"
                    ."Wijzigen naar:\n"
                    .$dv->valid('kenmerk_value');

                if ($dv->valid('toelichting')) {
                    $bericht .= "\n\nToelichting:\n"
                               .  ALLOW_NO_HTML->(
                                   $dv->valid('toelichting')
                               );
                }
        }

        return $self->zaak
            ->result_source
            ->schema
            ->resultset('Jobs')
            ->create_job({
                bericht             => $bericht,
                naam                => $naam,
                betrokkene          => $betrokkene,
                betrokkene_dsn      => $betrokkene->betrokkene_identifier,
                zaak_id             => $self->zaak->id,
                contactkanaal       => 'webform',
                task                => 'update_kenmerk',
                task_context        => 'zaak',
                queue               => 1,
                pip                 => $kenmerk->pip,
                opts                => {
                    kenmerk_id      => $opts->{kenmerk_id},
                    kenmerk_value   => $opts->{kenmerk_value},
                    zaak_id         => $self->zaak->id,
                    betrokkene      => $betrokkene->betrokkene_identifier,
                },
            });
    }

    Params::Profile->register_profile(
        'method'    => 'update_kenmerk',
        'profile'   => PROFILE_JOB_UPDATE_KENMERK
    );

    sub update_kenmerk {
        my $self        = shift;
        my $opts        = shift;


        my $dv          = Params::Profile->check(params => $opts);

        my $valid = $dv->valid;
        warn ('CREATE JOB: ' . Dumper($valid));

        die('Parameters incorrect:' . Dumper($opts)) unless $dv->success;

        if (
            $self->zaak->zaak_kenmerken->replace_kenmerk({
                values                      => $valid->{'kenmerk_value'},
                bibliotheek_kenmerken_id    => $valid->{'kenmerk_id'},
                zaak_id                     => $self->zaak->id,
            })
        ) {
            ### Success;
            return 1;
        }

        return;
    }
}


1;
