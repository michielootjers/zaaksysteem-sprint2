package Zaaksysteem::Cache;

use Moose;

use Storable     qw( freeze thaw );
use Digest::MD5  qw( md5_base64 );

use Data::Dumper;
use Data::FormValidator;

has 'storage' => (
    is =>'ro',
);


sub BUILD {
    my $self        = shift;

    die('Missing required storage object') unless $self->storage;
}

=head1 NAME

Zaaksysteem::Cache - Zaaksysteem cache object

=head1 SYNOPSIS

    my $cache = Zaaksysteem::Cache->new(
        storage => $c->stash->{__zs_cache}
    );

    $cache->set({
        key         => 'hello',
        value       => 'world',
        params      => { withthese => 'params'}
    });

    print $cache->get({
        key     => 'hello',
    });

    # Returns: world

    print $cache->get({
        key     => 'hello',
        params  => { withthese => 'params'}
    });

    # Returns: world

    print $cache->get({
        key     => 'hello',
        params  => { withthese => 'invalidparams'}
    });

    # Returns: undef

    $cache->clear();

    # Cache cleared

=head1 DESCRIPTION

Caching object with the added ability to store information dependent
on given parameters.

=head1 METHODS

=head2 $cache->set(\%OPTIONS)

    $cache->set(
        key         => 'say_hoi',
        value       => 'hoi',
        params      => { onlywhenthisparam => 'isset' }
        callback    => sub {
            my ($cache, $id, $zaak) = @_;

            return 1 if $zaak->last_modified < DateTime->now()->subtract(seconds => 1);
        }
    )

Sets the the given content in cache object $IDENTIFIER. Optional $PARAMETERS
can be given, to make sure you can only retrieve this object when the
parameters match.

=cut

sub set {
    my ($self, $options) = @_;

    my $dv = Data::FormValidator->check(
        $options,
        {
            required => [qw/key value/],
            optional => [qw/callback params/],
        }
    );

    die('Missing params: ' . Dumper($dv)) unless $dv->success;

    my $identifier = $options->{key};
    my $content    = $options->{value};
    my $parameters = $options->{params};
    my $callback   = $options->{callback};

    $self->storage->{$identifier} = {
        content => $content,
    };

    if ($parameters) {
        $self->storage->{$identifier}->{hash} = $self->_create_hash($parameters),
    }

    if ($callback) {
        $self->storage->{$identifier}->{callback} = $callback,
    }

    return $content;
}

=head2 $cache->get(\%OPTIONS)

    $cache->get(
        key             => 'say_hoi',
        params          => { onlywhenthisparam => 'isset' }
        callback_args   => [$case],
    );

Gets the content from cache object identifier by $IDENTIFIER. Optional
$PARAMETERS can be given, to make sure you only retrieve this object
when the parameters match.

=cut

sub get {
    my ($self, $options) = @_;

    my $dv = Data::FormValidator->check(
        $options,
        {
            required => [qw/key/],
            optional => [qw/callback_args params/],
        }
    );

    die('Missing params: ' . Dumper($dv)) unless $dv->success;

    my $identifier = $options->{key};
    my $content    = $options->{value};
    my $parameters = $options->{params};
    my $callbackargs = $options->{callback_args} || [];

    return unless (
        exists($self->storage->{$identifier}) &&
        UNIVERSAL::isa($self->storage->{$identifier}, 'HASH')
    );

    my $cacheobj    = $self->storage->{$identifier};

    if ($parameters) {
        return unless (
            $cacheobj->{hash} eq
                $self->_create_hash($parameters)
        );
    }

    if ($cacheobj->{callback}) {
        return unless $cacheobj->{callback}($self, $identifier, @{ $callbackargs });
    }

    return $self->storage->{$identifier}->{content};
}

=head2 $cache->clear([$IDENTIFIER]);

Clears the cache for identifier (key), $IDENTIFIER. When no argument
is given, it will clear the complete cache.

=cut

sub clear {
    my $self        = shift;
    my $identifier  = shift;

    if ($identifier) {
        delete($self->storage->{$identifier})
            if exists($self->storage->{$identifier});

        return 1;
    }

    delete $self->storage->{$_} for keys %{ $self->storage };

    return 1;
}


=head2 PRIVATE METHODS

=head2 $cache->_create_hash($opts)

Created a unique identifier of the given parameters

=cut

sub _create_hash {
    my $self            = shift;
    my $options         = shift;

    return unless $options;

    my $serialized      = freeze($options);

    return md5_base64($serialized);
}


1;

__END__

=head1 EXAMPLES

See L<SYNOPSIS>

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Rudolf Leermakers

Marco Baan

Dario Gieselaar

Nick Diel

Laura van der Kaaij

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut
