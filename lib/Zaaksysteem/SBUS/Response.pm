package Zaaksysteem::SBUS::Response;

use Moose::Role;

use Zaaksysteem::SBUS::Constants;

use Zaaksysteem::SBUS::Objecten::R03;
use Zaaksysteem::SBUS::Objecten::R02;
use Zaaksysteem::SBUS::Objecten::ADR;
use Zaaksysteem::SBUS::Objecten::PRS;
use Zaaksysteem::SBUS::Objecten::NNP;
use Zaaksysteem::SBUS::Objecten::VBO;

use Zaaksysteem::SBUS::Types::StUF;

use Data::Dumper;


Params::Profile->register_profile(
    method  => 'response',
    profile => {
        required        => [qw/
            sbus_type
        /],
        optional        => [qw/
            object
            input
            input_raw
            operation
        /],
        require_some    => {
            raw_or_full => [1, 'input','input_raw'],
        },
        constraint_methods  => {
            'object'    => sub {
                my ($dfv, $val)     = @_;

                my $SBUS_OBJECTS = SBUS_OBJECTS;
                if (grep { $_ eq $val } @{ $SBUS_OBJECTS }) {
                    return 1;
                }

                return;
            },
            'sbus_type'    => sub {
                my ($dfv, $val)     = @_;

                my $SBUS_TYPES = SBUS_TYPES;
                if (grep { $_ eq $val } @{ $SBUS_TYPES }) {
                    return 1;
                }

                return;
            }
        }
    }
);

sub response {
    my ($self, $raw_params) = @_;
    my $response;

    ### VALIDATION
    my $params;
    {
        my $dv = Params::Profile->check(
            params  => $raw_params,
        );

        die('Invalid call to response: ' . Dumper($dv))
            unless $dv->success;

        $params = $dv->valid;
    }

    my $to  = $self->_register_traffic($params);

    return $self->_response(
        $params,
        $to
    );
}

sub _response {
    my ($self, $params, $to)    = @_;
    my $response;

    my $typeobject;
    eval {
        $self->log->info(
            'ServiceBus request dispatching to TYPE: '
            . $params->{sbus_type}
        );

        my $typeclass   = 'Zaaksysteem::SBUS::Types::' . $params->{sbus_type};

        $typeobject  = $typeclass->new(
            schema  => $self->schema,
            log     => $self->log,
        );

        $typeobject->config($self->config);

        $params     = $typeobject->_prepare_input_params($params);

        my $package = 'Zaaksysteem::SBUS::Objecten::'
            . $params->{object};

        my $object  = $package->new(
            schema  => $self->schema,
            log     => $self->log,
            config  => $self->config
        );

        $response   = $object->handle_response(
            $params,
            {
                traffic_object  => $to,
            }
        );
    };

    if ($@) {
        $to->error(1);
        $to->error_message('SBUS Failure: ' . $@);
        $self->log->error($to->error_message);

        die($to->error_message) if $self->die_on_error;

        if ($typeobject) {
            $response   = $typeobject->generate_error(
                {
                    traffic_object  => $to,
                },
                $params
            );
        }
    }

    $to->update;

    return $response;
}

sub response_from_id {
    my ($self, $response_id) = @_;

    my $to      = $self->schema->resultset('SbusTraffic')->find(
        $response_id
    );

    my $input   = $self->_deserialize($to->input);

    $self->log->info('ServiceBus request by id: ' . $response_id);

    return $self->_response(
        {
            input       => $input,
            object      => $to->object,
            sbus_type   => $to->sbus_type,
            input_raw   => $to->input_raw,
            operation   => $to->operation,
        },
        $to
    );
}

sub compile_response {
    my ($self, $params) = @_;

    my $typeclass   = 'Zaaksysteem::SBUS::Types::' . $params->{sbus_type};

    my $typeobject  = $typeclass->new(
        schema  => $self->schema,
        log     => $self->log,
    );

    $typeobject->config($self->config);

    return $typeobject->compile_response(
        $params
    );
}

1;
