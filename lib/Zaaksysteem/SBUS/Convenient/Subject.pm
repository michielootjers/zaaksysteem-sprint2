package Zaaksysteem::SBUS::Convenient::Subject;

use Moose::Role;
use Zaaksysteem::Constants;

use constant STUF_MAP => {
    'PRS'   => {
        'a-nummer'                  => 'a_nummer',
        'bsn-nummer'                => 'burgerservicenummer',
        voorletters                 => 'voorletters',
        voornamen                   => 'voornamen',
        voorvoegselGeslachtsnaam    => 'voorvoegsel',
        geslachtsnaam               => 'geslachtsnaam',
        geslachtsaanduiding         => 'geslachtsaanduiding',
        geboortedatum               => 'geboortedatum',
        datumOverlijden             => 'datum_overlijden',
        indicatieGeheim             => 'indicatie_geheim',
        burgerlijkeStaat            => 'burgerlijke_staat',
        aanduidingNaamgebruik       => 'aanduiding_naamgebruik',
        sleutelGegevensbeheer       => 'system_of_record_id',
    },
    'ADR'   => {
        straatnaam                  => 'straatnaam',
        huisnummer                  => 'huisnummer',
        huisletter                  => 'huisletter',
        huisnummertoevoeging        => 'huisnummertoevoeging',
        aanduidingBijHuisnummer     => 'nadere_aanduiding',
        gemeentecode                => 'gemeente_code',
        postcode                    => 'postcode',
        woonplaatsnaam              => 'woonplaats',
    }
};

=head2 $sbus->subject_search(\%SEARCH_PARAMS)

Return value: \@LIST_OF_SUBJECTS

    my $subjects    = $sbus->subject_search(
        {
            burgerservicenummer => $bsn,
        }
    );

Returns a list of subjects from the makelaar. Only StUF supported

=cut

sub _subject_search_proper_params {
    my ($self, $params) = @_;

    die('subject_search requires a Parameter HASHREF as first parameter')
        unless ($params && UNIVERSAL::isa($params, 'HASH'));

    ### Prepare params
    my %search_params;
    {
        my %swapped_stuf_prs = map(
            { STUF_MAP->{PRS}->{ $_ } => $_ }
            keys %{ STUF_MAP()->{PRS} }
        );

        for my $key (%{ $params }) {
            next unless defined($swapped_stuf_prs{ $key });

            $search_params{ $swapped_stuf_prs{ $key } } = $params->{ $key };
        }
    }

    return \%search_params;
}

sub subject_search {
    my ($self, $params) = @_;

    my $search_params   = $self->_subject_search_proper_params($params);

    my $stuf_message    = $self->request(
        {
            operation   => 'search',
            sbus_type   => 'StUF',
            object      => 'PRS',
            input       => $search_params
        }
    );

    $stuf_message = $stuf_message->{synchroonAntwoord} if (
        defined($stuf_message->{synchroonAntwoord}));

    return unless (
        $stuf_message &&
        $stuf_message->{body} &&
        $stuf_message->{body}->{PRS}
    );

    my @subjects;
    for my $subject_row (@{ $stuf_message->{body}->{PRS} }) {


        my $subject = {};

        $subject->{ STUF_MAP->{ PRS }->{ $_ } } = $self->_parse_stuf_value(
            $subject_row->{ $_ }
        ) for keys %{ STUF_MAP->{PRS} };

        if ($subject_row->{ PRSADRVBL } && $subject_row->{ PRSADRVBL }->[0]) {
            $subject_row = $subject_row->{ PRSADRVBL }->[0]->{ADR};

            $subject->{ STUF_MAP->{ ADR }->{ $_ } } = $self->_parse_stuf_value(
                $subject_row->{$_ }
            ) for keys %{ STUF_MAP->{ADR} };
        }

        ### Local subject security
        ### Marco: warnings break the testsuite (and are generally a bad habit.)
        # warn(Dumper($subject));
        # warn(
        #     'GEMCODE:' . $subject->{gemeente_code}
        #     . ' CONFCODE' . $self->config->{gemeentecode}

        # );
        if (
            exists($self->config->{gemeentecode}) &&
            $subject->{gemeente_code} != $self->config->{gemeentecode}
        ) {
            warn($subject->{gemeente_code} . ':' . $self->config->{gemeentecode});
            next;
        }

        push(@subjects, $subject);
    }

    return \@subjects;
}

=head2 $sbus->subject_activate(\%SUBJECT_PARAMS)

Return value: $BOOL_TRUE_OR_FALSE

    my $is_activated    = $sbus->subject_activate(
        {
            burgerservicenummer => $bsn,
        }
    );

Activate a subject on a makelaar by sending an afnemerIndicatie. Please look at
L<subject_import_and_activate> for more information about first importing and
then activating in one call.

Currently, the only parameter which is supported is 'burgerservicenummer'

=cut

Params::Profile->register_profile(
    method  => 'subject_activate',
    profile => {
        required    => [qw/system_of_record_id/],
    }
);

sub subject_activate {
    my ($self, $raw_params) = @_;
    my ($params);

    {
        my $dv  = Params::Profile->check(params => $raw_params);

        unless ($dv->success) {
            die(
                "Missing parameters:\n"
                ."missing: " . join(',', $dv->missing) . "\n"
                ."invalid: " . join(',', $dv->invalid) . "\n"
            );
        }

        $params = $dv->valid;
    }

    my $stuf_message    = $self->request(
        {
            operation   => 'kennisgeving',
            sbus_type   => 'StUF',
            object      => 'PRS',
            input       => {
                'sleutelGegevensbeheer' => $params->{
                    'system_of_record_id'
                },
                'afnemerIndicatie'  => 'I',
            },
        }
    );

    if (defined($stuf_message->{bevestiging})) {
        $stuf_message   = $stuf_message->{bevestiging};
    }

    use Data::Dumper;
    $self->log->debug('ACTIVATE!' . Dumper($stuf_message));

    if (
        $stuf_message &&
        $stuf_message->{stuurgegevens} &&
        $stuf_message->{stuurgegevens}->{bevestiging} &&
        $stuf_message->{stuurgegevens}->{bevestiging}
            ->{crossRefNummer}
    ) {
        return 1;
    }

    return;
}

=head2 $sbus->subject_import_and_activate(\%SUBJECT_PARAMS)

Return value: $BOOL_TRUE_OR_FALSE

    my $is_activated    = $sbus->subject_import_and_activate(
        {
            burgerservicenummer => $bsn,
        }
    );

Import the given burgerservicenummer from makelaar, and let the makelaar know we would like to follow this subject.

=cut

Params::Profile->register_profile(
    method  => 'subject_import_and_activate',
    profile => __PACKAGE__ . '::subject_activate'
);

sub subject_import_and_activate {
    my $self            = shift;
    my ($raw_params)    = @_;
    my ($params);

    {
        my $dv  = Params::Profile->check(params => $raw_params);

        unless ($dv->success) {
            die(
                "Missing parameters:\n"
                ."missing: " . join(',', $dv->missing) . "\n"
                ."invalid: " . join(',', $dv->invalid) . "\n"
            );
        }

        $params = $dv->valid;
    }

    my $subjects;

    unless(
        ($subjects = $self->subject_search(@_)) &&
        scalar(@{ $subjects }) == 1
    ) {
        die(
            'Did not retrieve exactly one row for importing and activating subject'
        );
    }

    my $subject = shift(@{ $subjects });

    ### IMPORT SUBJECT
    if (my $subject_from_db = $self->subject_import($subject)) {
        ### ACTIVATE SUBJECT
        if ( $self->subject_activate(@_)) {
            $self->schema->resultset('Logging')->trigger('subject/stufimport', {
                component => LOGGING_COMPONENT_BETROKKENE,
                component_id => $subject_from_db->id,
                data => {
                    subject_id          => 'betrokkene-natuurlijk_persoon-' .
                        $subject_from_db->id,
                    system_of_record_id => $subject->{system_of_record_id},
                    description => 'PRS: ' . $subject_from_db->geslachtsnaam,
                }
            });

            return $subject_from_db;
        }
    }

    return;
}

sub subject_import {
    my ($self, $subject) = @_;

    die('No burgerservicenummer given') unless (
        defined($subject->{system_of_record_id}) &&
        $subject->{system_of_record_id}
    );

    ### Make sure this subject is not already authenticated
    $self->log->debug('NP: ' .
        $self->schema->resultset('NatuurlijkPersoon')->search(
            {
                system_of_record_id => $subject->{system_of_record_id},
                authenticated       => { '!=' => undef },
                deleted_on          => undef,
            }
        )->count
    );

    if (
        $self->schema->resultset('NatuurlijkPersoon')->search(
            {
                system_of_record_id => $subject->{'system_of_record_id'},
                authenticated       => { '!=' => undef },
                deleted_on          => undef,
            }
        )->count
    ) {
        die('Subject already imported');
    }

    if (
        $self->schema->resultset('NatuurlijkPersoon')->import_entry(
            {
                log         => $self->log,
                create      => $subject,
            }
        )
    ) {
        return $self->schema->resultset('NatuurlijkPersoon')->search(
            {
                system_of_record_id => $subject->{'system_of_record_id'},
                authenticated       => { '!=' => undef },
                deleted_on          => undef,
            }
        )->first;
    }

    return;
}

=head2 $sbus->subject_deactivate(\%SUBJECT_PARAMS)

Return value: $BOOL_TRUE_OR_FALSE

    my $is_deactivated    = $sbus->subject_deactivate(
        {
            burgerservicenummer => $bsn,
        }
    );

Deactivate a subject from makelaar by sending an indicatorAfnemerIndicatie=0.
Please look at L<subject_remove_and_deactivate> for more information about first
removing and then deactivating a subject in one call

=cut

Params::Profile->register_profile(
    method  => 'subject_deactivate',
    profile => __PACKAGE__ . '::subject_activate'
);

sub subject_deactivate {
    my ($self, $raw_params) = @_;
    my ($params);

    {
        my $dv  = Params::Profile->check(params => $raw_params);

        unless ($dv->success) {
            die(
                "Missing parameters:\n"
                ."missing: " . join(',', $dv->missing) . "\n"
                ."invalid: " . join(',', $dv->invalid) . "\n"
            );
        }

        $params = $dv->valid;
    }

    my $stuf_message    = $self->request(
        {
            operation   => 'search',
            sbus_type   => 'StUF',
            object      => 'PRS',
            input       => {
                'bsn-nummer'        => $params->{burgerservicenummer},
                'afnemerIndicatie'  => 0,
            },
        }
    );

    if (defined($stuf_message->{bevestiging})) {
        $stuf_message   = $stuf_message->{bevestiging};
    }

    if (
        $stuf_message &&
        $stuf_message &&
        $stuf_message->{stuurgegevens} &&
        $stuf_message->{stuurgegevens}->{bevestiging} &&
        $stuf_message->{stuurgegevens}->{bevestiging}
            ->{crossRefNummer}
    ) {

        return 1;
    }

    return;
}

=head2 $sbus->subject_remove_and_deactivate(\%SUBJECT_PARAMS)

Return value: $BOOL_TRUE_OR_FALSE

    my $is_deactivated    = $sbus->subject_remove_and_deactivate(
        {
            burgerservicenummer => $bsn,
        }
    );

Remove subject from zaaksysteem (setting deleted=NOW()) and deactivate a
subject from makelaar by sending an indicatorAfnemerIndicatie=0.

=cut

Params::Profile->register_profile(
    method  => 'subject_remove_and_deactivate',
    profile => __PACKAGE__ . '::subject_activate'
);

sub subject_remove_and_deactivate {
    my $self            = shift;
    my ($raw_params)    = @_;
    my ($params);

    {
        my $dv  = Params::Profile->check(params => $raw_params);

        unless ($dv->success) {
            die(
                "Missing parameters:\n"
                ."missing: " . join(',', $dv->missing) . "\n"
                ."invalid: " . join(',', $dv->invalid) . "\n"
            );
        }

        $params = $dv->valid;
    }

    my $record      = $self
                    ->schema
                    ->resultset('NatuurlijkPersoon')
                    ->search(
                        {
                            burgerservicenummer => $params->{burgerservicenummer},
                            authenticated       => { '!=' => undef },
                            deleted_on          => undef,
                        }
                    )->first;

    unless ($record) {
        die('Record not found in database, cannot remove');
    }

    if ($self->subject_deactivate(@_)) {
        $record->deleted_on(DateTime->now());
        return $record->update;
    }

    return;
}

sub _parse_stuf_value {
    my $self    = shift;
    my $value   = shift;

    if (UNIVERSAL::isa($value, 'HASH') && defined($value->{_})) {
        $value = $value->{_};
    }

    if ($value eq 'NIL' || $value eq 'NIL:geenWaarde') {
        $value = '';
    }

    return $value;
}

1;
