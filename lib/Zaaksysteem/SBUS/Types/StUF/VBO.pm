package Zaaksysteem::SBUS::Types::StUF::VBO;

use Data::Dumper;
use Moose;

use Zaaksysteem::SBUS::Constants;

extends 'Zaaksysteem::SBUS::Types::StUF::GenericAdapter';

sub _stuf_to_params {
    my ($self, $vbo_xml, $stuf_options) = @_;

    my $params = $self->_convert_stuf_to_hash(
        $vbo_xml,
        {
            brutoVloerOppervlak => 'verblijfsobject_oppervlakte',
        }
    );

    return $params;
}

sub _stuf_relaties {
    my ($self, $vbo_xml, $stuf_options, $create_options) = @_;

    my $address = $self->handle_adr_relations($vbo_xml, 'VBO');

    if ($vbo_xml->{extraElementen}) {
        my $extra = $self->_convert_stuf_extra_elementen_to_hash(
            $vbo_xml->{extraElementen}
        );

        my $mapping = STUF_VBO_MAPPING_EXTRA;

        for my $key (keys %{ $mapping }) {
            next unless defined($extra->{ $key });

            $create_options->{
                $mapping->{ $key }
            } = $extra->{ $key };
        }
    }

    $create_options->{ $_ } = $address->{ $_ }
        for keys %{ $address };

    if (
        defined($create_options->{verblijfsobject_gebruiksdoel}) &&
        $create_options->{verblijfsobject_gebruiksdoel}
    ) {
        $create_options->{verblijfsobject_gebruiksdoel} = [
            $create_options->{verblijfsobject_gebruiksdoel}
        ];
    }

    return $create_options;
}


1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

