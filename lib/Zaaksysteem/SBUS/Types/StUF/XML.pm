package Zaaksysteem::SBUS::Types::StUF::XML;

use Moose;
use Data::Dumper;
use XML::LibXML;
use XML::Compile::Util;

use XML::Compile::Translate::Reader;

use constant WRITER_CONFIG  => {
    'hooks' => [
        {
            before => sub {
                my ($doc, $value, $path) = @_;

                if (my ($novalue) = $value =~ /NIL:(.*)/) {
                    my ($tag)       = $path =~ /\/([\w\d_-]*)$/;

                    my $value    = XML::LibXML::Element->new( 'BG:' . $tag );
                    $value->setAttribute('xsi:nil', 'true');
                    $value->setAttribute('StUF:noValue', $novalue);

                    return $value;
                }

                return $value;
            }
        },
    ],
    #prefixes        => {
    #    'http://www.egem.nl/StUF/StUF0204'  => 'STUF',
    #    'http://www.egem.nl/StUF/sector/bg/0204'  => 'BG'
    #}
};

use constant READER_CONFIG  => {
    sloppy_integers => 1,
    interpret_nillable_as_optional => 1,
    check_values    => 0,
    'hooks' => [
        {
            before => sub {
                my ($node, $value, $path) = @_;

                ### Stuf patch, we just don't want any historic events,
                ### for people moving out. Our library simply cannot handle
                ### this.
                ### This routine will remove the second
                ### PRSADR(INS,VBL,ETC) from the xml

                if ($node->localName =~ /^PRS$/) {
                    my @children = $node->childNodes();
                    my $found = {};
                    for my $child (@children) {
                        if ($child->localName && $child->localName =~ /^PRSADR\w{3}$/) {
                            if ($found->{$child->nodeName}) {
                                $node->removeChild($child);
                            }

                            $found->{$child->nodeName} = 1;
                        }
                    }
                }

                ### Vicrea Patch, add nillable when we get a noValue
                ### attribute
                if (
                    $node->hasAttributeNS(
                        'http://www.egem.nl/StUF/StUF0204',
                        'noValue'
                    ) &&
                    !$node->hasAttributeNS(
                        'http://www.w3.org/2001/XMLSchema-instance',
                        'nil'
                    )
                ) {
                    $node->setAttributeNS(
                        'http://www.w3.org/2001/XMLSchema-instance',
                        'nil',
                        'true'
                    );
                }

                return $node;
            },
            after => sub {
                my ($node, $value, $path) = @_;

                if (
                    $node->hasAttributeNS(
                        'http://www.egem.nl/StUF/StUF0204',
                        'sleutelGegevensbeheer'
                    )
                    && $node->localName =~ /^NNP|PRS$/
                ) {
                    $value->{
                        'sleutelGegevensbeheer'
                    } = $node->getAttributeNS(
                        'http://www.egem.nl/StUF/StUF0204',
                        'sleutelGegevensbeheer'
                    );
                }

                if (
                    $node->hasAttributeNS(
                        'http://www.egem.nl/StUF/StUF0204',
                        'sleutelVerzendend'
                    )
                    && $node->localName =~ /^NNP|PRS$/
                ) {
                    $value->{
                        'sleutelVerzendend'
                    } = $node->getAttributeNS(
                        'http://www.egem.nl/StUF/StUF0204',
                        'sleutelVerzendend'
                    );
                }


                if (
                    $value &&
                    $value eq 'NIL' ||
                    $node->hasAttributeNS(
                        'http://www.egem.nl/StUF/StUF0204',
                        'noValue'
                    )
                ) {
                    if (
                        $node->getAttributeNS(
                            'http://www.egem.nl/StUF/StUF0204',
                            'noValue'
                        ) ne 'geenWaarde'
                    ) {
                        return 'NIL:geenWaarde'
                    }

                    return 'NIL';
                }

                return $value;
            }
        }
    ]
};

has 'xml_compile_config'    => (
    is      => 'ro',
    default => sub {
        return {
            'READER'    => READER_CONFIG,
            'WRITER'    => WRITER_CONFIG,
        };
    }
);

has 'xml_definitions'   => (
    'is'    => 'ro',
    lazy    => 1,
    default => sub {
        my $self    = shift;

        return [
            $self->home . '/share/wsdl/stuf/0204/stuf0204.xsd',
            $self->home . '/share/wsdl/stuf/bg0204/bgstuf0204.xsd',
            $self->home . '/share/wsdl/stuf/bg0204/bg0204.xsd',
        ];
    }
);

has [qw/perlin xmlin home/] => (
    is  => 'rw',
);

has 'schema'    => (
    is      => 'rw',
    lazy    => 1,
    default => sub {
        my $self    = shift;

        my $schema  = XML::Compile::Schema->new($self->xml_definitions);

        return $schema;
    }
);

sub _get_body_as_element {
    my $self        = shift;
    my $dom         = XML::LibXML->load_xml(string => $self->xmlin);

    ### Search for namespaces in the sector model, e.g: skip transmissionError
    ###   '{http://www.egem.nl/StUF/StUF0204}transmissionError',
    ###   '{http://www.egem.nl/StUF/sector/bg/0204}asynchroonAntwoordBericht',
    ###   '{http://www.egem.nl/StUF/sector/bg/0204}kennisgevingsBericht',
    ###   '{http://www.egem.nl/StUF/sector/bg/0204}synchroonAntwoordBericht',
    ###   '{http://www.egem.nl/StUF/sector/bg/0204}vraagBericht'

    for my $element ($self->schema->elements) {
        my ($ns, $tag)  = $element =~ /^{(.*?)}(.*)$/;
        next unless $ns =~ /sector/;

        my @elements    = $dom->getElementsByTagNameNS($ns, $tag);

        return shift(@elements) if scalar(@elements);
    }

    ### ALTERNATIVE WAY, But NEEDS Soap envelope

#    my @envelopes   = $dom->getElementsByLocalName('Envelope');
#
#    ### Check for valid SOAP envelope
#    my ($envelope)      = grep {
#        $_->namespaceURI =~ /soap-envelope/ ||
#        $_->namespaceURI =~ /soap\/envelope/
#    } @envelopes;
#
#    die('Cannot find SOAP Envelope in given STRING or TEXT or XML')
#        unless $envelope;
#
#    my ($body)      = $envelope->getElementsByTagNameNS(
#        $envelope->namespaceURI,
#        'Body'
#    );
#
#    die('INVALID soap message, cannot find Body in SOAP Envelope')
#        unless $envelope;
#
#    my ($stufxml)   = grep { $_->nodeType == 1 } $body->childNodes;
#
#    return $stufxml;
}

sub transform_to_perl {
    my $self    = shift;

    die('xmlin needs to be set to be able to transform xml')
        unless $self->xmlin;

    my $stufxml = $self->_get_body_as_element();

    my $elem    = pack_type $stufxml->namespaceURI, $stufxml->localName;

    my $reader  = $self->schema->compile(
        READER => $elem,
        %{ READER_CONFIG() }
    );

    # {kennisgeving} => {}
    return {
        name    => $stufxml->localName,
        content => $reader->($stufxml)
    };
}

sub transform_to_xml {
    my $self    = shift;

    die('xmlin needs to be set to be able to transform perl data')
        unless $self->perlin;

    my ($stufcall, $perldata)           = (
        keys %{ $self->perlin },
        values( %{ $self->perlin } )
    );

    my $writer      = $self->schema->compile(
        WRITER => $stufcall,
        %{ WRITER_CONFIG() }
    );

    my $doc     = XML::LibXML::Document->new('1.0', 'UTF-8');

    my $xmlelem = $writer->($doc, $perldata);

    return $xmlelem;
}

1;
