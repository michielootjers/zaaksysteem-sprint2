=head1 NAME

Zaaksysteem::Manual::API - Zaaksysteem API description

=head1 SYNOPSIS

 # Will login and ask for a password. After entering the password and
 # authenticating, the location below will be called as a json request.

 # Creating an entry (create)
 dev-bin/zs_request -u admin -p 'http://zs.url/helloworld/create'

 # Listing entries (search)
 dev-bin/zs_request -u admin -p 'http://zs.url/helloworld'

 # Viewing a single entry (read)
 dev-bin/zs_request -u admin -p 'http://zs.url/helloworld/1'

 # Updating a single entry (update)
 dev-bin/zs_request -u admin -p 'http://zs.url/helloworld/1/update'

 # Deleting a single entry (delete)
 dev-bin/zs_request -u admin -p 'http://zs.url/helloworld/1/delete'

 # A simpler procedure is by using the internal json generator
 https://test.zaaksysteem.nl/dev/json?trigger=event/list

=head1 DESCRIPTION

When developing the Zaaksysteem API, please make sure your calls react in a
certain way. Below a description of the API.

=head1 JSON

Every response should be in proper JSON. Requesting an API call is simple.
Just call the apropriate URL of the API call.

However, sometimes it is necessary to call the API with variables. Our
implementation accepts different form of parameter passing.

=over 4

=item B<Query Parameters>

Using query parameters, e.g by calling

 http://zs.url/helloworld/create?world=MiddleEarth

=item B<POST Parameters>

By using the POST parameters in postdata

=item B<JSON Data in POST Parameters>

And of course, our favourite: a json call in our body. This way you can, aside
from passing key/value pairs, pass complete JSON objects to our backend.

    {
        question: 'Which world is more awesome?'
        world: 'MiddleEarth',
        beerbrewers: [
            'heineken',
            'hertog',
            'amstel',
        ]
    }

=back

=head1 POST/GET

Make sure you use the proper action for your calls. When querying our API,
make you set your action to GET.

But when making alterations in Zaaksysteem, use the POST action.

B<Best Practice>: Make use of POST parameters or JSON data when making
alterations in Zaaksysteem. This way your changes won't pop up in proxy or
other server logs. GET.

But when making alterations in Zaaksysteem, use the POST action.

=head1 ERROR HANDLING

When we have to trigger an error in our API call, we do this by setting the
proper HTTP Response Code. Below is an example of different kind of errors.

  Everything OK:
  201   - CREATE/UPDATE/DELETE CALL SUCCESS

  Client Errors:
  400   - Bad Request - We do not know what you are talking about
  401   - Unauthorized - You are not logged in.
  403   - Forbidden - You do not have the proper permissions
  404   - Not Found - The API call you requested could not be found
  405   - Method Not Allowed - Use of wrong request method for this URL

  Server Errors:
  500   - Internet Server Error - There is an error. Error can be found in JSON message (except of
  course when all hell broke loose)

When explaining the error, for example when an internal server error (500)
occured, we will respond in a simple JSON response. Example below

    {
        rows: 1,
        num_rows: 1,
        at: undef,
        comment: undef,
        result: [
            {
                data: {
                    invalid: [
                        "street",
                        "zipcode"
                    ],
                    missing: [
                        "lastname"
                    ],
                },
                type: "form/validation",
                messages: [
                    'Could not find the subject you requested'
                ],
                stacktrace: []
            }
        ],
        next: "http://zs.url/helloworld?page=5&rows=25",
        prev: "http://zs.url/helloworld?page=3&rows=25",
        status_code: "500"
   }


Normally, the only thing you would need is the first value from C<messages>.

=head1 CRUD

Our api is inspired on the CRUD method. This means that most of our JSON calls
have standard read, write, update and delete methods.

There is a Test controller which you can use for inspiration when developing a
JSON call. C<Controller/HelloWorld.pm>

=head2 basic response

When no error occured, we will send a standardized JSON response. Even when the
call is supposed to give ONE entry back, we will always assume every call is
able to respond with multiple entries.

Example:

    {
        rows: 25,
        num_rows: 1022,
        at: undef,
        comment: 'Informative comment about call',
        result: [
            {
                question: 'Which world is more awesome?'
                world: 'MiddleEarth',
                beerbrewers: [
                    'heineken',
                    'hertog',
                    'amstel',
                ],
                created: "2013-01-16T08:19Z"
            },
            {
                question: 'Which world is ours?'
                world: 'Earth',
                beerbrewers: null,
                created: "2013-01-16T08:10Z"
            }
        ],
        next: "http://zs.url/helloworld?page=5&rows=25",
        prev: "http://zs.url/helloworld?page=3&rows=25",
        status_code: "200"
   }

B<Components>

=over 4

=item result

The result will B<always> be in the form of an array and will contain 0 or more
entries.

=item pagination

There are 5 parameters in the basic response which define the pagination of
the API.

B<at>: Defines the position of the entry where we at

B<rows>: The number of rows on this page

B<rows_total>: Total number of rows

B<next>: The API link for the next page of this call

B<prev>: The API link for the previous page of this call

=item sorting

TODO

=back

=head2 search

When doing a search, make sure you use QUERY parameters for filtering the
search. Else we won't be able to give back a correct next/pevious link

=head2 create

A response code of 201 will tell you your create call is a succes

=head2 update

A response code of 201 will tell you your update call is a succes

=head2 read

A response code of 200 will tell you your read is fine.

=head1 SERIALIZATION

=head2 Date/time

DateTime objects are in the form of the ISO8601 and are ALWAYS in UTC. Please
use the proper locale for converting it to your local timezone.

example:

    2013-01-16T08:19Z

=head2 undef vs 0 vs ""

We use the proper JSON implementation for sorting out the differences, but in
short:

    {
        not_married_anymore: null,
        does_not_have_a_lastname_suffix: '',
        the_amount_of_cash_on_his_bankaccount: 0,
    }

When you do not want to update a value...please leave it out of the request

=head2 true/false

We use the strings "true" or "false" to determine boolean values...just as it
should:

    {
        she_is_a_lovely_girl: true,
        never_drinks_alcohol: false,
    }

=head1 ENCODING

All calls should be in proper UTF-8 encoding

=head1 ERROR CODES

Following is a partial listing of error codes that can be found in a JSON error
response body.

=over 4

=item /file/file_create_sjabloon/sjabloon_not_found

=item /file/file_create_sjabloon/case_not_found

=item /file/update/file_not_found

=item /file/download/file_not_found

=item /file/download_as_pdf/file_not_found

=item /file/verify/file_not_found

=item /file/get_thumbnail/file_not_found

=item /file/update_file/file_not_found

=item /file/version_info/file_not_found

=item /file/file_create/invalid_parameters

Parameters met verkeerde inhoud

=item /file/file_create/missing_parameters

Ontbrekende parameters

=item /file/file_create/case_type_document_in_use

Document al gezet bij andere file

=item /file/update_properties/missing_parameters

=item /file/update_properties/invalid_parameters

=item /file/update_properties/cannot_alter_deleted

=item /file/update_properties/rejection_reason_required

Bij weigeren file

=item /file/update_properties/cannot_accept_unassigned

Document zonder C<case_id> gezet kun je niet accepteren

=item /file/update_properties/case_already_defined

C<case_id> veranderen met een ander C<case_id> mag niet

=item /file/update_metadata/invalid_options

=item /file/update_file/invalid_options

=item /file/update_file/not_last_version

file die je probeert te updaten is niet de laatste versie

=item /file/update_existing/invalid_options

=item /file/update_existing/existing_already_accepted

Bestaande file is al geaccepteerd (mag alleen met documenten uit de queue)

=item /file/update_existing/existing_newer_version_available

Bestaande file is niet de laatste versie in zijn reeks

=item /filestore/filestore_create/invalid_parameters

=item /filestore/filestore_create/missing_parameters

=item /filestore/assert_allowed_filetype/no_extension

gegeven file heeft geen extensie

=item /filestore/assert_allowed_filetype/extension_not_allowed

=item /filestore/create_thumbnail/missing_parameters

=item /filestore/create_thumbnail/invalid_parameters

=item /filestore/create_thumbnail/imagemagick_error

=item /directory/directory_create/directory_exists

=item /directory/update_properties/directory_exists

=item /directory/delete/not_empty

=back

=head1 EXAMPLES

See L<SYNOPSIS>

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem>


=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Rudolf Leermakers

Marco Baan

Dario Gieselaar

Nick Diel

Laura van der Kaaij

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

