package Zaaksysteem::ZAPI::Response::ResultSet;

use Moose::Role;

sub from_resultset {
    my $self        = shift;
    my $resultset   = shift;

    die('Not a valid ResultSet: ' . ref($resultset))
        unless UNIVERSAL::isa($resultset, 'DBIx::Class::ResultSet');

    $self->_input_type('resultset');

    if ($self->page_current) {
        $resultset = $resultset->search(
            {},
            {
                page    => $self->page_current
            }
        );
    }

    $self->_generate_paging_attributes(
        $resultset->pager,
    );

    $self->result([ $resultset->all ]);

    $self->_validate_response;

    return $self->response;
}

around from_unknown => sub {
    my $orig        = shift;
    my $self        = shift;
    my ($data)      = @_;

    if (
        UNIVERSAL::isa($data, 'DBIx::Class::ResultSet')
    ) {
        $self->from_resultset(@_);
    }

    $self->$orig( @_ );
};

1;
