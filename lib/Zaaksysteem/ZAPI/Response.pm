package Zaaksysteem::ZAPI::Response;

use Moose;
use URI;
use URI::QueryParam;

use Storable 'dclone';

with qw/
    Zaaksysteem::ZAPI::Response::ResultSet
    Zaaksysteem::ZAPI::Response::Row
    Zaaksysteem::ZAPI::Response::Array
/;

use constant    DEFAULT_PAGE_SIZE   => 25;

use constant    ZAPI_RESPONSE_KEYS  => [
    qw/
        at

        next
        prev

        rows
        num_rows

        comment
        result

        status_code
    /
];

use constant    ZAPI_RESPONSE_REQUIRED  => [ qw/rows num_rows result/ ];

has [@{ ZAPI_RESPONSE_KEYS() }] => (
    is      => 'rw'
);

has [qw/_input_object _input_type uri_prefix/] => (
    is      => 'rw',
);

has 'page_size'                 => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        return DEFAULT_PAGE_SIZE;
    }
);

has 'page_current'                 => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        return 1;
    }
);

around BUILDARGS    => sub {
    my $orig            = shift;
    my $class           = shift;
    my (%opts)          = @_;

    for my $key (keys %opts) {
        my $method  = 'from_' . $key;

        next unless $class->can($method);

        return $class->$orig(
            _input_type     => $key,
            _input_object   => $opts{ $key },
            @_
        );
    }

    return $class->$orig(
        @_
    );
};

sub BUILD {
    my $self        = shift;

    if ($self->_input_type && $self->_input_object) {
        $self->_load_response(
            $self->_input_type   => $self->_input_object
        );
    }
}

sub _load_response {
    my $self        = shift;
    my (%opts)      = @_;

    for my $key (keys %opts) {
        my $method  = 'from_' . $key;
        next unless $self->can($method);

        $self->$method      ($opts{ $key });
    }
}

sub from_unknown { }

sub _generate_paging_attributes {
    my $self        = shift;
    my $pager       = shift;

    die('Not a valid Data::Page object: ' . ref($pager))
        unless UNIVERSAL::isa($pager, 'Data::Page');

    ### Not yet implemented
    $self->at           (undef);

    $self->rows         ($pager->entries_on_this_page);
    $self->num_rows     ($pager->total_entries);

    $self->_generate_paging_url($pager);
}

sub _generate_paging_url {
    my $self        = shift;
    my $pager       = shift;

    # No urls for single response
    return if ($self->rows < 2);

    die('No URI prefix given') unless $self->uri_prefix;

    my $next        = dclone($self->uri_prefix);
    my $prev        = dclone($self->uri_prefix);

    ### Remove old attributes
    $next->query_param_delete($_) for qw/next prev/;
    $prev->query_param_delete($_) for qw/next prev/;

    $next->query_param(
        'next'  => $pager->next_page
    ) if $pager->next_page;

    $prev->query_param(
        'prev'  => $pager->previous_page
    ) if $pager->previous_page;

    $self->next         ($next->as_string);
    $self->prev         ($prev->as_string);
}

sub _validate_response {
    my $self        = shift;

    if (
        ($self->result && scalar( @{ $self->result }) > 0) &&
        (my @filled_attrs = grep { $self->$_ } @{ ZAPI_RESPONSE_REQUIRED() })
            != scalar( @{ ZAPI_RESPONSE_REQUIRED() })
    ) {
        die('Missing required attributes: ' . join(',',
                grep { !$self->$_ } @{ ZAPI_RESPONSE_REQUIRED() }
            )
        );
    }

    die('Invalid format for result attribute') unless UNIVERSAL::isa(
        $self->result,
        'ARRAY'
    );
}

sub response {
    my $self        = shift;

    $self->_load_response(@_);

    my $rep_object  = {
        map { $_ => $self->$_ }
            @{ ZAPI_RESPONSE_KEYS() }
    };

    if (!$rep_object->{status_code}) {
        $rep_object->{status_code}  = '200';
    }

    $self->_validate_response;

    return $rep_object;
}

1;

=head1 NAME

Zaaksysteem::ZAPI::Response - Generates a correct ZAPI Reponse

=head1 SYNOPSIS

    my $response    = Zaaksysteem::ZAPI::Response->new(
        resultset       => $c->model('DB::Zaak')->search(),
        uri_prefix      => $c->uri_for('/zaak'),
        page_size    => $c->req->params->{next} || 1,
    )

    $c->res->body($response->response);


=head1 DESCRIPTION

To make sure every response is valid, this object validates the input
parameters and implements a correct return HASHREF.

=head1 INPUT STRUCTURES

You can use different input structures for generating a correct Response
object.

=head2 RESULTSET

Providing a DBIx::Class::ResultSet will generate the necessary next pages, row
definitions etc.

    my $response    = Zaaksysteem::ZAPI::Response->new(
        resultset       => $c->model('DB::Zaak')->search(),
        uri_prefix      => $c->uri_for('/zaak'),
        page_size    => $c->req->params->{next} || 1,
    )

=head2 ARRAY

Providing an ARRAY REF with values.
    
    my $response    = Zaaksysteem::ZAPI::Response->new(
        array           => [(1..125)],
        uri_prefix      => $c->uri_for('/zaak'),
        page_size    => $c->req->params->{next} || 1,
    )

=head2 UNKNOWN

When you do not know beforehand what kind of object this is, please use the
unknown attribute. It will guess for ARRAY of ResultSet.

    my $response    = Zaaksysteem::ZAPI::Response->new(
        unknown         => 
        uri_prefix      => $c->uri_for('/zaak'),
        page_size    => $c->req->params->{next},
    )

=head1 PUBLIC ATTRIBUTES

=head2 $resp->comment

Comment for this reply

=head2 $resp->uri_prefix

Will use this URI object for defining a correct next and prev url for the
pagination

=head2 $resp->page_current

Defines the current_page for this request, when not already set in ResultSet
(mainly for the ARRAY function)

=head2 $resp->page_size

Defines the number of rows per page, when not already set in ResultSet
(mainly for the ARRAY function)

=head1 PUBLIC READ ONLY ATTRIBUTES

=head2 $resp->rows

Number of rows on the current page

=head2 $resp->num_rows

Total number of rows on this ARRAY or ResultSet

=head2 $resp->next

The next page, made from the given L<uri_prefix>

=head2 $resp->prev

The prev page, made from the given L<uri_prefix>

=head2 $resp->result

An ARRAY ref with the results


=head1 EXAMPLES

B<Example output>

 {
   'at' => undef,
   'comment' => undef,
   'next' => bless( do{\(my $o = 'http://localhost/index?kenmerk_id_1=25&kenmerk_id_2=33&next=3')}, 'URI::http' ),
   'num_rows' => 75,
   'prev' => bless( do{\(my $o = 'http://localhost/index?kenmerk_id_1=25&kenmerk_id_2=33&prev=1')}, 'URI::http' ),
   'result' => [
     26,
     27,
     28,
     29,
     30,
     31,
     32,
     33,
     34,
     35,
     36,
     37,
     38,
     39,
     40,
     41,
     42,
     43,
     44,
     45,
     46,
     47,
     48,
     49,
     50
   ],
   'rows' => 25
 }


See L<SYNOPSIS>

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Rudolf Leermakers

Marco Baan

Dario Gieselaar

Nick Diel

Laura van der Kaaij

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.
