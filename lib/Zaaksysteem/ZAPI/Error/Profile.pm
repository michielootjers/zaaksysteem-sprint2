package Zaaksysteem::ZAPI::Error::Profile;

use Moose::Role;

use Zaaksysteem::Constants qw/PARAMS_PROFILE_DEFAULT_MSGS/;

use constant ZAPI_PARAM_CAT     => 'ZAPIProfile';

sub BUILD {
    my $self            = shift;

    $self->_generate_profile_data;
}

sub _load_profile_error {
    my $self            = shift;
    my $error           = shift;

    if (
        !UNIVERSAL::isa($error, 'Data::FormValidator::Results')
    ) {
        die('Not a valid Data::FormValidator::Results object given');
    }

    $error->{profile}{msgs} = PARAMS_PROFILE_DEFAULT_MSGS;

    my (@rv, @message);
    for my $param (
        $error->missing,
        $error->invalid,
    ) {
        my $result      = 'invalid';
        $result         = 'missing' if $error->missing($param);

        push(@rv,
            {
                parameter   => $param,
                result      => $result,
                message     => $error->msgs->{$param},
            }
        );

        push(
            @message,
            'Invalid field "' . $param . '": ' .
                $error->msgs->{$param}
        );
    }

    $self->messages(\@message);
    $self->data(\@rv);
    $self->type('params/profile');
}

### When this is a data::formvalidator object, make sure we TO_JSON it.
sub _generate_profile_data {
    my $self    = shift;

    return unless (
        $self->type eq ZAPI_PARAM_CAT ||
        $self->type eq 'params/profile'
    );

    return unless (
        UNIVERSAL::isa($self->data, 'Data::FormValidator::Results')
    );

    $self->_load_profile_error($self->data);
}

before '_validate_response' => sub {
    my $self    = shift;

    $self->_generate_profile_data;
};

1;
