package Zaaksysteem::General::ZAPIController;

use Moose;
use namespace::autoclean;

BEGIN {extends 'Catalyst::Controller::ActionRole'; }

__PACKAGE__->config(
    action_roles    => ['ZAPI', 'Profile'],
    dv_profiles     => {},
);

sub vprofile {
    my ($self, $c)          = @_;

    my $caller  = $self->path_prefix . '.' . [ caller(1) ]->[3];

    return unless (
        defined( $c->stash->{_zapi} ) &&
        defined( $c->stash->{_zapi}->{dv_result} ) &&
        defined( $c->stash->{_zapi}->{dv_result}->{$caller})
    );

    return $c->stash->{_zapi}->{dv_result}->{$caller};
}

sub help : Local('help') : ZAPIMan { }

1;

