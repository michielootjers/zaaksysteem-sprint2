package Zaaksysteem::Users;

use strict;
use warnings;

use Params::Profile;
use Data::Dumper;
use Zaaksysteem::Constants;

use Net::LDAP;

use Moose;
use namespace::autoclean;

use constant    DEFAULT_IMPORT_OU   => 'Import';
use constant    INTERNAL_USERS      => [qw/admin/];
use constant    OBJECT_CLASSES      => [qw/
    inetOrgPerson
    person
    posixAccount
    shadowAccount
    top
/];

use constant    OBJECT_CLASSES_POSIXGROUP   => [qw/
    posixGroup
    top
/];

use constant    OBJECT_CLASSES_OU   => [qw/
    organizationalUnit
    top
/];

use constant    LDAP_COLUMNS    => [qw/
    cn
    sn
    displayName
    givenName
    mail
    telephoneNumber

    homeDirectory
    userPassword
    uid
    uidNumber
    gidNumber
    loginShell
    initials
/];

use constant    LDAP_TRANSLATE_DEPARTMENTS  => {
    'Directie'                  => 'Directieteam',
    'Sociale zaken'             => 'Sociale_Zaken',
    'SoZa'                      => 'Sociale_Zaken',
    'Vergunningen & Handhaving' => 'V&H',
    'Ruimtelijke Inrichting'    => 'RI',
    'Ruimte'                    => 'RI',
    'Sociale Zaken'             => 'Sociale_Zaken',
    'Burgemeester & Wethouders' => 'College',
    'Soziale Zaken'             => 'Sociale_Zaken',
    'Planvoorbereiding en Groenbeheer'  => 'Planvoorbereiding en Groenbeheer',
    'Facilitaire zaken, Receptie en ICT beheer' => 'Facilitaire zaken, Receptie en ICT beheer',
    'DIV en communicatie en WenB werken'   => 'DIV en communicatie en WenB werken',
};


has [qw/customer config prod log ldap _ldaph uidnumber/] => (
    'is'    => 'rw',
);

has 'components'            => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        return [qw/sync_users sync_password/];
    }
);

has 'uidnumber_start'            => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        return 55100;
    }
);

has 'uidnumber'            => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        return shift->uidnumber_start;
    }
);

has 'ldapbase'              => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        return shift->customer->{start_config}->{LDAP}->{basedn};
    }
);

has 'import_ou'             => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        return (
            shift->customer->{start_config}->{LDAP}->{import_ou} ||
            DEFAULT_IMPORT_OU
        );
    }
);

has 'departments_map'       => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        return (
            shift->customer->{start_config}->{LDAP}->{departments_mapping} ||
            LDAP_TRANSLATE_DEPARTMENTS
        );
    }
);

has 'ldap_system_roles'     => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my $roles = ZAAKSYSTEEM_AUTHORIZATION_ROLES;

        my $ldaproles = {};
        for my $role (keys %{ $roles }) {
            my $data    = $roles->{ $role };

            $ldaproles->{ $data->{ldapname} } = $data;
            $ldaproles->{ $data->{ldapname} }->{
                'internalname'
            } = $role;
        }

        return $ldaproles;
    }
);


sub ldaph {
    my $self    = shift;

    return $self->_ldaph if $self->_ldaph;

    my $ldap_server = $self->config
        ->{authentication}
        ->{realms}
        ->{zaaksysteem}
        ->{store}
        ->{ldap_server};

    my $ldap = Net::LDAP->new($ldap_server, version => 3) or return;

#    $self->log->debug('- Connected to LDAP') if $ldap;

    $ldap->bind($self->config->{LDAP}->{admin},
        password    => $self->config->{LDAP}->{password}
    );

    return $self->_ldaph($ldap);
}

sub sync_users {
    my $self    = shift;
    my $users   = shift;

    $self->log->info('Start synchronizing AD users');

    my $ldap            = $self->ldaph;
    my $local_users     = $self->_retrieve_local_users($ldap);

    my $compared_users  = $self->_compare_users($ldap, $local_users, $users->{Bussum});

    $self->_load_ldap($ldap, $compared_users, $local_users);

    return 1;
}

sub _load_ldap {
    my ($self, $ldap, $compared_users, $local_users)   = @_;

    $self->log->debug('Users: NUM USERS: ' . scalar(keys %{ $compared_users->{modify} }));
    $self->_modify_ldap($ldap, $compared_users->{modify}, $local_users);
    $self->_delete_ldap($ldap, $compared_users->{delete}, $local_users);

    unless (
        exists(
            $self   ->customer
                    ->{start_config}
                    ->{LDAP}
                    ->{skip_removal_after_sync}
        ) && $self  ->customer
                    ->{start_config}
                    ->{LDAP}
                    ->{skip_removal_after_sync}
    ) {
        $self->_delete_ldap($ldap, $compared_users->{delete}, $local_users, 1);
    }

    $self->_create_ldap($ldap, $compared_users->{create});

    return 1;
}

sub _create_ldap {
    my ($self, $ldap, $compared_users)   = @_;

    my $ldap_departments    = $self->departments_map;
    my $departments         = {};

    for my $username (keys %{ $compared_users }) {
        ### Define DN, check for existence
        my $ou          = $self->import_ou;
        my $append_dn   = 'ou=' . $ou . ',' . $self->ldapbase;

        if ($compared_users->{ $username }->{department}) {
            my $department = $compared_users->{ $username }->{department};

            $department    = $ldap_departments->{$department}
                if $ldap_departments->{$department};

            my $sh      = $ldap->search( # perform a search
                base   => $self->ldapbase,
                filter => "(&(objectClass=organizationalUnit)(ou=" .
                    $department
                    . "))"
            );

            if ($sh->count) {
                $self->log->debug('- Found department: ' .
                    $department
                );

                $append_dn = $sh->entry(0)->dn;
            } else {
                $departments->{ $department } = 1;
            }
        }

        my $dn  = "cn=$username," . $append_dn;

        my $add = {};

        for my $column (keys %{
                $self->_return_clean_user($compared_users->{$username})
            }
        ) {
            $add->{$column} =
                $compared_users->{$username}->{$column};
        }

        if ($compared_users->{$username}->{force_uidNumber}) {
            $add->{uidNumber} = $add->{gidNumber} =
                $compared_users->{$username}->{force_uidNumber};
        } else {
            $self->uidnumber($self->uidnumber + 1);
            $add->{uidNumber} = $self->uidnumber;
            $add->{gidNumber} = $self->uidnumber;
        }

        ### DO LDAP
        #$self->log->debug('- ADD LDAP: ' . Dumper([$dn,$add]));
        $self->log->info('- ADD USER: ' . $dn);

        my $result = $ldap->add(
            $dn,
            attr        => [
                %{ $add },
                objectclass => OBJECT_CLASSES
            ],
        );
        $result->code && $self->log->error("failed to add entry: $dn : ", $result->error
            . ' / ' . Dumper($add)
        );

        if (!$result->code) {
            $self->_handle_role($ldap,$dn);
        }
    }

    $self->log->info('Departments NOT FOUND: ' . "\n" . join("\n", keys %{
                $departments })) if scalar(keys %{ $departments });
}

sub _handle_role {
    my ($self, $ldap, $dn)  = @_;

    my $sh      = $ldap->search( # perform a search
        base   => $self->ldapbase,
        filter => "(&(objectClass=posixGroup)(cn=Behandelaar))",
    );

    return unless $sh->count;

    my $behandelaar = $sh->entry(0);

    $sh      = $ldap->search( # perform a search
        base   => $self->ldapbase,
        filter => "(&(objectClass=posixGroup)(cn=Behandelaar)(memberUid=$dn))",
    );

    return 1 if $sh->count;

    $self->log->debug('- Roles: Adding ' . $dn . ' to Behandelaar');

    my $result = $ldap->modify(
        $behandelaar->dn,
        changes => [
            add     => [
                memberUid   => $dn
            ],
        ]
    );
    $result->code && $self->log->error("failed to add role: $dn : ", $result->error);
}

sub _delete_ldap {
    my ($self, $ldap, $compared_users, $local_users, $force)   = @_;

    for my $username (keys %{ $compared_users }) {
        my $dn      = $local_users->{$username}->{dn};

        if ($username =~ /admin/i) {
            $self->log->debug('- Admin user will not be removed');
            next;
        }

        $self->log->debug('- REMOVE LDAP: ' .$dn);
        #$self->log->info('- ADD USER: ' . $dn);

        if ($force) {
            my $result = $ldap->delete( $dn );
            $result->code && $self->log->error("failed to remove entry: $dn : ", $result->error);
        }
    }
}

sub _return_clean_user {
    my ($self, $userdata) = @_;

    my $rv = {};

    my $columns = LDAP_COLUMNS;

    for my $column (@{ $columns }) {
        next unless $userdata->{$column};

        if (
            $column eq 'telephoneNumber' &&
            length($userdata->{$column}) &&
            (
                $userdata->{$column} =~ /^\s+$/ ||
                $userdata->{$column} !~ /^[\+0-9\s-]+$/
            )
        ) {
            next;
        }

        #$userdata->{$column} = latin1($userdata->{$column});

        $rv->{ $column } = $userdata->{$column};
    }

    return $rv;
}

sub _modify_ldap {
    my ($self, $ldap, $compared_users, $local_users)   = @_;

    for my $username (keys %{ $compared_users }) {
        my $dn      = $local_users->{$username}->{dn};

        ### Check for Temp as ou, then check the department
        my ($ou)    = $dn =~ /ou=(.*?),/;

        if (
            $ou eq 'Temp' &&
            $compared_users->{ $username }->{department}
        ) {
            $self->log->info(
                'Found user in Temp, try to create it in '
                .'correct OU by removing this user and creating a new one'
            );
            #Skip this user, create a new user and delete current user
            $self->_delete_ldap(
                $ldap,
                {
                    $username => $compared_users->{$username},
                },
                $local_users,
                1
            );

            $compared_users->{$username}->{force_uidNumber} =
                $local_users->{$username}->{uidNumber};

            $self->_create_ldap(
                $ldap,
                {
                    $username => $compared_users->{$username},
                },
                $local_users
            );

            next;
        }

        delete($local_users->{dn});

        my $modify  = {};

        for my $column (keys %{
                $self->_return_clean_user(
                    $compared_users->{ $username }
                )
            }
        ) {
            next if ($column eq 'gidNumber' || $column eq 'uidNumber');

            if (defined($local_users->{$username}->{$column})) {
                if (
                    $local_users->{$username}->{$column} eq
                    $compared_users->{$username}->{$column}
                ) {
                    next;
                }
                $modify->{replace} = [] unless $modify->{replace};
                push(@{ $modify->{replace} }, $column => $compared_users->{$username}->{$column});
            } else {
                $modify->{add} = [] unless $modify->{add};
                push(@{ $modify->{add} }, $column => $compared_users->{$username}->{$column});
            }
        }

        $self->log->debug('SURNAME: ' . $compared_users->{$username}->{sn});

        if (!scalar(keys(%{ $modify }))) {
            next;
        }

        $self->log->info('Modified user: ' . $dn . ', because: ' .
            Dumper($modify)
        );

        my $result = $ldap->modify(
            $dn,
            changes => [
                %{ $modify }
            ]
        );
        $result->code && $self->log->error("failed to add entry: $dn : ", $result->error
            . ' / ' . Dumper($modify)
        );

        if (!$result->code) {
            $self->_handle_role($ldap,$dn);
        }

        ### DO LDAP
        $self->log->info('- Modified user: ' . $dn);
    }
}

sub _compare_users {
    my ($self, $ldap, $local_users, $users) = @_;

    my $rv = {
        'modify'    => {},
        'create'    => {},
        'delete'    => {},
    };

    for my $username (keys %{ $users }) {
        $username       = lc($username);
        my $userdata    = $users->{$username};

        ### Required
        next unless $userdata->{sn};

        $userdata->{homeDirectory}  = '/home/' . $username;
        $userdata->{loginShell}     = '/bin/bash';
        $userdata->{uid}            = $username;

        $userdata->{initials}       = $userdata->{givenName};
        $userdata->{initials}       =~ s/( ?[a-zA-Z])\w+/$1./g;

        if (
            defined($local_users->{$username}) &&
            $local_users->{$username}->{userPassword} &&
            !$userdata->{userPassword}
        ) {
            $userdata->{userPassword} = $local_users->{$username}->{userPassword};
        }

        $userdata->{initials}       =~ s/( ?[a-zA-Z])\w+/$1./g;

        if (!$local_users->{$username}) {
            $rv->{create}->{$username}  = $userdata;
            next;
        }

        ### Edit
        $rv->{modify}->{$username}      = $userdata;
    }

    for my $username (keys %{ $local_users }) {
        if (!$users->{$username}) {
            $rv->{delete}->{$username}  = $users->{$username};
        }
    }

    return $rv;
}

sub _retrieve_local_users {
    my $self    = shift;
    my $ldap    = shift;

    $self->log->info('- Retrieve all users for comparison');

    my $sh      = $ldap->search( # perform a search
        base   => $self->ldapbase,
        filter => "(&(objectClass=posixAccount))"
    );

    return {} unless $sh->count;

    my @entries = $sh->entries;
    my $columns = LDAP_COLUMNS;

    my $local_users = {};

    my $uidNumber   = 0;

    for my $entry (@entries) {
        my $rv = {};
        for my $column (@{ $columns }) {
            $rv->{ $column } = $entry->get_value($column)
                if $entry->exists($column);
        }

        $rv->{dn}   = $entry->dn;

        if ($entry->get_value('uidNumber') > $uidNumber) {
            $uidNumber = $entry->get_value('uidNumber');
        }

        $local_users->{ lc($entry->get_value('cn')) } = $rv;
    }

    if ($uidNumber > $self->uidnumber_start) {
        $self->uidnumber($uidNumber);
    }

    $self->log->info('UIDNUMBER' . $uidNumber);

    return $local_users;
}

sub sync_user {
    my $self    = shift;
}

sub get_all_roles {
    my $self    = shift;
    my $ldap    = $self->ldaph;
    my $rv      = [];

    my $sh      = $ldap->search( # perform a search
        base   => $self->ldapbase,
        filter => "(&(objectClass=posixGroup))"
    );

    return $rv unless $sh->count;

    my @roles;

    for my $entry ($sh->entries) {
        my ($ou)    = $entry->dn =~ /ou=(.*?),/;

        my $role    = {
            dn  => $entry->dn,
            cn  => $entry->get_value('cn'),
        };

        $role->{ou} = $ou if $ou;

        push(@roles, $role);
    }

    return \@roles;
}

# my $tree = [
#   {
#       name        => Username || Afdeling
#       type        => PosixAccount
#       entry       => {}
#       children    => [
#           {
#               name        => Username || Afdeling,
#               entry       => {},
#           },
#           {
#               name        => Username || Afdeling,
#               entry       => {},
#               children    => blabla
#           }
#       ]
#   }
# ];

sub move_to_ou {
    my ($self, $opts) = @_;

    die('Invalid options') unless ($opts && $opts->{entry_dn} && $opts->{ou_dn});

    my $username    = $opts->{entry_dn};
    my $ou          = $opts->{ou_dn};

    my $ldap    = $self->ldaph;

    my $sh      = $ldap->search( # perform a search
        base   => $username,
        scope   => 'base',
        filter => '(objectClass=*)'
    );

    return unless $sh->count;

    ### Get roles
    my $roles   = $ldap->search( # perform a search
        base   => $self->ldapbase,
        filter => "(&(objectClass=posixGroup)(memberUid=$username))",
    );
    my @user_roles;
    if ($roles->count) {
        for my $role ($roles->entries) {
            push(@user_roles, $role->dn);
        }
    }

    my $newdn   = $username;
    $newdn      =~ s/(cn=.*?),.*/$1,$ou/;

    my $userpart = $username;
    $userpart   =~ s/(cn=.*?),.*/$1/;

    my $result = $ldap->moddn(
        dn              => $username,
        newsuperior     => $ou,
        newrdn          => $userpart,
    );

    $result->code && $self->log->error("failed to user to ou: " .
        $username . " : ", $result->error) && return;

    for my $user_role (@user_roles) {
        $result = $ldap->modify(
            $user_role,
            add  => {
                memberUid   => $newdn
            }
        );

        $result->code && $self->log->error("failed to add role to: " .
            $newdn . " : ", $result->error);

        $result = $ldap->modify(
            $user_role,
            delete  => {
                memberUid   => $username,
            }
        );

        $result->code && $self->log->error("failed to remove role from: " .
            $username . " : ", $result->error);
    }

    return $newdn;
}

sub delete_role_from {
    my ($self, $opts) = @_;

    die('Invalid options') unless ($opts && $opts->{entry_dn} && $opts->{role_dn});

    my $username    = $opts->{entry_dn};
    my $role        = $opts->{role_dn};

    my $ldap    = $self->ldaph;

    my $roles   = $ldap->search( # perform a search
        base    => $role,
        scope   => 'base',
        filter => '(objectClass=*)'
    );

    unless ($roles->count) {
        $self->log->error(
            'Cannot remove role "'
            . $role .'"'
            . ', role not found'
        );

        return;
    }

    $self->log->info('Search for ' . $role . ': '. $roles->count);

    my $result = $ldap->modify(
        $role,
        delete  => {
            memberUid   => $username,
        }
    );

    $result->code && $self->log->error("failed to remove memberUid: " .
        $username . " from role: " .
        $role . ' : ' . $result->error) && return;

    return 1;
}
sub add_role_to {
    my ($self, $opts) = @_;

    die('Invalid options') unless ($opts && $opts->{entry_dn} && $opts->{role_dn});

    my $username    = $opts->{entry_dn};
    my $role        = $opts->{role_dn};

    my $ldap    = $self->ldaph;

    my $sh      = $ldap->search( # perform a search
        base   => $username,
        scope   => 'base',
        filter => '(objectClass=*)'
    );
    return unless $sh->count;

    ### First, remove all memberships
    my $roles   = $ldap->search( # perform a search
        base    => $role,
        scope   => 'base',
        filter => '(objectClass=*)'
    );

    return unless $roles->count;
    warn('Search for ' . $role . ': '. $roles->count);

    my $result = $ldap->modify(
        $role,
        add  => {
            memberUid   => $username,
        }
    );

    $result->code && $self->log->error("failed to add memberUid: " .
        $username . " : ", $result->error) && return;

    return 1;
}

sub get_list_of_entries {
    my $self    = shift;
    my $opts    = shift || {};

    my $ldap    = $self->ldaph;
    my $rv      = [];

    my @objectClass = (
        $opts->{objectClass}
            ? @{ $opts->{objectClass } }
            : ('posixAccount','organizationalUnit','posixGroup')
        );

    my $filter = '(|(objectClass='
            . join(')(objectClass=', @objectClass)
            . '))';

    my $sh      = $ldap->search( # perform a search
        base        => $self->ldapbase,
        scope       => 'sub',
        filter      => $filter
    );

    return $rv unless $sh->count;

    my @entries = $sh->entries;

    my @results;
    for my $entry (@entries) {
        my $layout = $self->_load_tree_entry($entry);

        push(@results, $layout);
    }

    return \@results;
}

sub get_tree_view {
    my $self    = shift;
    my $opts    = shift || {};
    my $parent  = shift;
    my $ldap    = $self->ldaph;
    my $rv      = [];

    my @objectClass = (
        $opts->{objectClass}
            ? @{ $opts->{objectClass } }
            : ('organization', 'posixAccount','organizationalUnit','posixGroup')
        );

    my $filter = '(|(objectClass='
            . join(')(objectClass=', @objectClass)
            . '))';

    my $sh      = $ldap->search( # perform a search
        base        => ($parent || $self->ldapbase),
        scope       => ($parent ? 'one' : 'base'),
        filter      => $filter
    );

    #$self->log->debug('COUNT: ' . $sh->count . ' / ' . ($parent ||
    #        $self->ldapbase) . ' / ' . $filter
    #);

    return $rv unless $sh->count;

    my @entries = $sh->entries;

    my @results;
    for my $entry (@entries) {
        my $layout = $self->_load_tree_entry($entry, $opts);

        push(@results, $layout);
    }

    my @sorted_results = sort {
        $a->{type} cmp $b->{type} ||
        lc($a->{name}) cmp lc($b->{name})
    } @results;

    return \@sorted_results;
}

sub get_array_of_roles {
    my $self        = shift;
    my $opts        = shift;

    my $ldap    = $self->ldaph;

    my @results;

    if ($opts->{parent_ou}) {
        my $filter = '(&(objectClass=organizationalUnit)'
            . '(l=' . $opts->{parent_ou} . '))';

        my $sh      = $ldap->search( # perform a search
            base        => $self->ldapbase,
            scope       => 'sub',
            filter      => $filter
        );

        if ($sh->count) {
            my $entry = $sh->entry(0);

            my $roles = $self->get_tree_view(
                {
                    'objectClass'   => ['posixGroup','organizationalUnit']
                },
                $entry->dn
            );

            $roles = [
                grep {
                    $_->{type} eq 'posixGroup'
                } @{ $roles }
            ];

            push(@results, @{ $roles });

            if (scalar(@results)) {
                push(@results, 'split');
            }
        }
    }

    my $primary_roles = $self->get_tree_view(
        {
            'objectClass'   => ['organization', 'posixGroup','organizationalUnit']
        },
        $self->ldapbase
    );

    push(@results, grep {
            $_->{type} eq 'posixGroup'
        } @{ $primary_roles }
    );

    return \@results;
}

sub _load_tree_entry {
    my ($self, $entry, $opts) = @_;

    my $layout = {
        name    => '',
        entry   => {},
        dn      => $entry->dn,
        has_children => ($self->has_children($entry->dn) || 0),
        system  => 0
    };

    unless ($opts->{counter}) {
        $opts->{counter} = 0;
    }

    $layout->{counter}      = ++$opts->{counter};

    my @object_classes = $entry->get_value('objectClass');
    if (grep { $_ eq 'organization' } @object_classes) {
        $layout->{entry}        = $self->get_organization($entry);
        $layout->{name}         = $layout->{entry}->{o};
        $layout->{type}         = 'organization';
        $layout->{internal_id}  = 0;
        $layout->{children}     = $self->get_tree_view($opts, $entry->dn);
    } elsif (grep { $_ eq 'posixAccount' } @object_classes) {
        $layout->{entry}        = $self->get_medewerker($entry);
        $layout->{name}         = $layout->{entry}->{cn};
        $layout->{type}         = 'posixAccount';
        $layout->{internal_id}  = $layout->{entry}->{uidNumber};
        $layout->{children}     = [];
    } elsif (grep { $_ eq 'organizationalUnit' } @object_classes) {
        $layout->{entry}        = $self->get_organisational_unit($entry);
        $layout->{name}         = $layout->{entry}->{ou};
        $layout->{type}         = 'organizationalUnit';
        $layout->{children}     = $self->get_tree_view($opts, $entry->dn);
        $layout->{internal_id}  = $layout->{entry}->{l};
    } elsif (grep { $_ eq 'posixGroup' } @object_classes) {
        $layout->{entry}        = $self->get_posix_group($entry);
        $layout->{name}         = $layout->{entry}->{cn};
        $layout->{type}         = 'posixGroup';
        $layout->{members}      = $layout->{entry}->{memberUid};
        $layout->{has_children} = $self->has_children($entry->dn);
        $layout->{internal_id}  = $layout->{entry}->{gidNumber};
        $layout->{children}     = [];
    }

    if ($layout->{type} eq 'posixGroup') {
        #$self->log->debug(
        #    'dn: ' . $entry->dn . ' / ' . $layout->{name}
        #);
    }

    if (
        $layout->{type} eq 'posixGroup' &&
        $entry->dn !~ /ou/ &&
        $self->ldap_system_roles->{ $layout->{name} }
    ) {
        $layout->{system} = 1;
    }

    return $layout;
}

sub add_role {
    my ($self, $opts) = @_;

    die('Invalid options') unless ($opts->{role});

    die('Invalid role name') unless $opts->{role} =~ /^[\w\d\s-]+$/;

    warn('Options: ' . Dumper($opts));

    my $ldap    = $self->ldaph;
    my $dn      = 'cn=' . $opts->{role} . ',' . (
        $opts->{parent}
            ? $opts->{parent}
            : $self->ldapbase
    );


    ### Role exists?
    my $sh      = $ldap->search( # perform a search
        base    => $dn,
        scope   => 'base',
        filter  => "(&(objectClass=posixGroup))"
    );

    return if $sh->count;

    my $newid   = $self->_retrieve_new_id(
        {
            objectClass => 'posixGroup',
            id          => 'gidNumber',
        }
    );

    my $result = $ldap->add(
        $dn,
        attr        => [
            cn          => $opts->{role},
            gidNumber   => $newid,
            description => $opts->{role},
            objectclass => OBJECT_CLASSES_POSIXGROUP
        ],
    );

    $result->code && $self->log->error("failed to add role: " .
        $dn . " : ", $result->error) && return;

    $self->log->error("Succesful added role: " .  $dn);

    return 1;
}

sub has_children {
    my ($self, $dn) = @_;
    my $ldap    = $self->ldaph;

    ### Check if there are entries below this object
    my $sh      = $ldap->search( # perform a search
        base    => $dn,
        scope   => 'one',
        filter  => "(&(objectClass=*))"
    );

    return unless $sh->count;

    return $sh->count;
}

sub delete_entry {
    my ($self, $dn) = @_;

    die('No dn given') unless $dn;

    $self->log->info('Trying to delete entry: ' . $dn);

    my $ldap    = $self->ldaph;

    return if $self->has_children($dn);

    $self->log->info('No children found, safe to delete: ' . $dn);

    my $sh      = $ldap->search( # perform a search
        base    => $dn,
        scope   => 'base',
        filter  => "(&(objectClass=*))"
    );

    return unless $sh->count;

    my $result = $self->ldaph->delete($dn);

    $result->code && $self->log->error("failed to remove entry: $dn : ",
        $result->error) && return;

    return 1;
}

sub add_ou {
    my ($self, $opts) = @_;

    die('Invalid options') unless ($opts->{ou});

    die('Invalid ou name') unless $opts->{ou} =~ /^[\w\d\s-]+$/;

    warn('Options: ' . Dumper($opts));

    my $ldap    = $self->ldaph;
    my $dn      = 'ou=' . $opts->{ou} . ',' . (
        $opts->{parent}
            ? $opts->{parent}
            : $self->ldapbase
    );


    ### ou exists?
    my $sh      = $ldap->search( # perform a search
        base    => $dn,
        scope   => 'base',
        filter  => "(&(objectClass=organizationalUnit))"
    );

    return if $sh->count;

    my $newid   = $self->_retrieve_new_id(
        {
            objectClass => 'organizationalUnit',
            id          => 'l',
        }
    );

    my $result = $ldap->add(
        $dn,
        attr        => [
            ou          => $opts->{ou},
            l           => $newid,
            description => $opts->{ou},
            objectclass => OBJECT_CLASSES_OU
        ],
    );

    $result->code && $self->log->error("failed to add ou: " .
        $dn . " : ", $result->error) && return;

    $self->log->error("Succesful added ou: " .  $dn);

    return 1;
}

sub _retrieve_new_id {
    my ($self, $opts) = @_;

    die('Invalid options') unless (
        $opts &&
        $opts->{objectClass} &&
        $opts->{id}
    );

    my $ldap    = $self->ldaph;

    ### Check for new id
    my $sh         = $ldap->search( # perform a search
        base    => $self->ldapbase,
        scope   => 'sub',
        filter  => "(&(objectClass=" . $opts->{objectClass} . "))"
    );

    my @entries = $sh->entries;

    my $maxid = 0;
    for my $entry (@entries) {
        if ($entry->get_value($opts->{id}) > $maxid) {
            $maxid = $entry->get_value($opts->{id});
        }
    }

    return ($maxid + 1);
}

sub get_all_medewerkers {
    my $self    = shift;
    my $ldap    = $self->ldaph;
    my $rv      = [];

    my $sh      = $ldap->search( # perform a search
        base   => $self->ldapbase,
        filter => "(&(objectClass=posixAccount))"
    );

    return $rv unless $sh->count;

    my @entries = $sh->entries;

    for my $entry (@entries) {
        my $mw = $self->get_medewerker($entry);

        push(@{ $rv }, $mw);
    }

    return $rv;
}

sub get_organization {
    my ($self, $entry)  = @_;
    my $ldap            = $self->ldaph;

    return unless $entry && ref($entry);

    return {
        o   => $entry->get_value('o')
    };
}

sub get_organisational_unit {
    my ($self, $entry)  = @_;
    my $ldap            = $self->ldaph;

    return unless $entry && ref($entry);

    my $ou      = {};
    my $dn      = $entry->dn;

    $ou->{ $_ } = $entry->get_value($_) for
        qw/ou description l/;

    return $ou;
}

sub get_posix_group {
    my ($self, $entry)  = @_;
    my $ldap            = $self->ldaph;

    return unless $entry && ref($entry);

    my $group      = {};
    my $dn      = $entry->dn;

    $group->{ $_ } = $entry->get_value($_) for
        qw/cn description gidNumber memberUid/;

    $group->{memberUid} = [ $entry->get_value('memberUid') ];

    return $group;
}

sub get_medewerker {
    my ($self, $entry)  = @_;
    my $ldap            = $self->ldaph;

    return unless $entry && ref($entry);

    my $mw      = {};
    my $dn      = $entry->dn;

    $mw->{ $_ } = $entry->get_value($_) for
        qw/cn displayName uid uidNumber/;

    ### Get OU
    my ($ou)    = $dn =~ /ou=(.*?),/;

    $mw->{ou}   = $ou;

    ### Get roles
    my $roles   = $ldap->search( # perform a search
        base   => $self->ldapbase,
        filter => "(&(objectClass=posixGroup)(memberUid=$dn))",
    );

    my %roles;
    if ($roles->count) {
        for my $role ($roles->entries) {
            $roles{$role->dn} = 1;
        }
    }

    $mw->{roles}    = \%roles;

    return $mw;
}

sub deploy_user_in_roles {
    my ($self, $username, $userroles) = @_;

    my $ldap    = $self->ldaph;

    my $sh      = $ldap->search( # perform a search
        base   => $self->ldapbase,
        filter => "(&(objectClass=posixAccount)(cn=$username))"
    );

    return unless $sh->count;

    my $user    = $sh->entry(0);
    my $dn      = $user->dn;

    ### First, remove all memberships
    my $roles   = $ldap->search( # perform a search
        base   => $self->ldapbase,
        filter => "(&(objectClass=posixGroup)(memberUid=$dn))",
    );

    for my $role ($roles->entries) {
        my $result = $ldap->modify(
            $role->dn,
            delete  => {
                memberUid   => $user->dn,
            }
        );

        $result->code && $self->log->error("failed to delete memberUid: " .
            $user->dn . " : ", $result->error);
    }

    ### Add entries
    for my $role (@{ $userroles }) {
        my $result = $ldap->modify(
            $role,
            add  => {
                memberUid   => $user->dn,
            }
        );

        $result->code && $self->log->error("failed to add memberUid: " .
            $user->dn . " : ", $result->error);
    }
}

sub get_role_by_id {
    my ($self, $role_id) = @_;

    my $ldap    = $self->ldaph;

    ### First, remove all memberships
    my $roles   = $ldap->search( # perform a search
        base   => $self->ldapbase,
        filter => "(&(objectClass=posixGroup)(gidNumber=$role_id))",
    );

    return unless $roles->count;

    return $roles->entry(0);
}

sub get_ou_by_id {
    my ($self, $ou_id) = @_;

    my $ldap    = $self->ldaph;

    ### First, remove all memberships
    my $ous   = $ldap->search( # perform a search
        base   => $self->ldapbase,
        filter => "(&(objectClass=organizationalUnit)(l=$ou_id))",
    );

    return unless $ous->count;

    return $ous->entry(0);
}

sub find {
    my ($self, $dn) = @_;

    my $ldap    = $self->ldaph;

    ### First, remove all memberships
    my $roles   = $ldap->search( # perform a search
        base    => $dn,
        scope   => 'base',
        filter  => "(objectClass=*)",
    );

    return unless $roles->count;

    return $roles->entry(0);
}


__PACKAGE__->meta->make_immutable;



=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

