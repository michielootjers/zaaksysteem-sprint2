package Zaaksysteem::DB::Component::ChecklistItem;

use Moose;

use JSON;

BEGIN { extends 'DBIx::Class'; }

sub TO_JSON {
    my $self = shift;

    return {
        id => $self->id,
        label => $self->label_with_deprecated_answer,
        user_defined => $self->user_defined ? JSON::true : JSON::false,
        checked => $self->state ? JSON::true : JSON::false
    }
}

sub label_with_deprecated_answer {
    my $self = shift;

    my $label = $self->get_column('label');
    my $deprecated_answer = $self->get_column('deprecated_answer');

    return $deprecated_answer ? sprintf('%s (%s)', $label, $deprecated_answer) : $label;
}

1;
