package Zaaksysteem::DB::Component::Logging::File;

use Moose;

has file => ( is => 'ro', lazy => 1, default => sub {
    my $self = shift;

    $self->rs('File')->find($self->data->{ file_id });
});

1;
