package Zaaksysteem::DB::Component::Logging::Scheduler::Run;

use Moose::Role;

use HTML::Strip;
use JSON;

has 'raw_description' => (
    is      => 'ro',
    lazy    => 1,
    default => sub {
        my $self    = shift;

        my $task        = $self
                        ->result_source
                        ->schema
                        ->resultset('ScheduledJobs')
                        ->find($self->data->{task_id});

        my $task_list   = $task->result_source->resultset->task_list;

        my $task_def    = $task_list->{ $task->task };

        return $task_def->{description}->($task);
    }
);

sub onderwerp {
    my $self        = shift;

    return 'Wijziging via PIP: ' . $self->raw_description->{short};
}

around TO_JSON => sub {
    my $orig = shift;
    my $self = shift;

    my $data = $self->$orig(@_);

    my $stripper = HTML::Strip->new;

    $data->{ content } = $stripper->parse($self->raw_description->{long});
 
    $data->{ expanded } = JSON::false;

    return $data;
};

1;