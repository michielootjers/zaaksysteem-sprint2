package Zaaksysteem::DB::Component::Logging::Email::Send;

use Moose::Role;
use HTML::Strip;

has case => ( is => 'ro', lazy => 1, default => sub {
    my $self = shift;

    $self->rs('Zaak')->find($self->data->{ case_id });
});

sub onderwerp {
    my $self = shift;

    my ($subject) = HTML::Strip->new->parse($self->data->{ subject }) =~ m[^\s*(.*?)\s*$];

    sprintf('E-mail "%s" verstuurd naar "%s"', $subject, $self->data->{ recipient });
}

sub event_category { 'contactmoment' }

1;
