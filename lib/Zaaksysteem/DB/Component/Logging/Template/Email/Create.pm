package Zaaksysteem::DB::Component::Logging::Template::Email::Create;

use Moose::Role;

sub onderwerp {
    my $self = shift;

    sprintf(
        'E-mailsjabloon "%s" toegevoegd: %s',
        $self->notification->label,
        $self->data->{ reason }
    );
}

sub _add_magic_attributes {
    shift->meta->add_attribute('template' => ( is => 'ro', lazy => 1, default => sub {
        my $self = shift;

        $self->result_source->schema->resultset('BibliotheekNotificaties')->find($self->data->{ template_id });
    }));
}

1;
