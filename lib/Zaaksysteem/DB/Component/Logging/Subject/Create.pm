package Zaaksysteem::DB::Component::Logging::Subject::Create;

use Moose::Role;

sub onderwerp {
    sprintf('Betrokkene "%s" toegevoegd', shift->subject->naam);
}

sub _add_magic_attributes {
    shift->meta->add_attribute('subject' => ( is => 'ro', lazy => 1, default => sub {
        my $self = shift;

        $self->result_source->resultset->{attrs}->{betrokkene_model}->get(
            {},
            $self->data->{ subject_id }
        );
    }));
}

1;
