package Zaaksysteem::DB::Component::Logging::Casetype::Update::Version;

use Moose::Role;

sub onderwerp {
    my $self = shift;

    sprintf(
        'Versie van zaaktype "%s" opgehoogd naar %s: %s',
        $self->casetype->title,
        $self->data->{ version },
        $self->data->{ reason }
    );
}

sub _set_magic_attributes {
    shift->meta->add_attribute('casetype' => ( is => 'ro', lazy => 1, default => sub {
        my $self = shift;

        $self->result_source->schema->resultset('Zaaktype')->find($self->data->{ casetype_id });
    }));
}

1;
