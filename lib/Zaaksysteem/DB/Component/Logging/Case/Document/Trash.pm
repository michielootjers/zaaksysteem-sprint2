package Zaaksysteem::DB::Component::Logging::Case::Document::Trash;

use Moose::Role;

sub onderwerp {
    sprintf('Document "%s" naar de prullenbak verplaatst.', shift->file->name);
}

1;
