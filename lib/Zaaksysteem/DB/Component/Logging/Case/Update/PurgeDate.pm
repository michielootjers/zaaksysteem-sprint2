package Zaaksysteem::DB::Component::Logging::Case::Update::PurgeDate;

use Moose::Role;

sub onderwerp {
    my $self = shift;

    my $onderwerp = sprintf('Vernietigingsdatum voor zaak %d gewijzigd naar %s',
        $self->data->{ case_id },
        $self->data->{ purge_date },
    );
    if($self->data->{ reason }) {
		$onderwerp .= ', reden: ' . $self->data->{ reason };
    }
    return $onderwerp;
}

sub event_category { 'case-mutation'; }

1;
