package Zaaksysteem::DB::Component::Logging::Case::Resume;

use Moose::Role;

sub onderwerp {
    my $self = shift;

    sprintf(
        'Zaak %d hervat: %s',
        $self->data->{ case_id },
        $self->data->{ reason }
    );
}

sub event_category { 'case-mutation'; }

1;
