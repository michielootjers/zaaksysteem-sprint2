package Zaaksysteem::DB::Component::Logging::Case::Delete;

use Moose::Role;

sub onderwerp {
    my $self = shift;

    sprintf(
        'Zaak %s vernietigd door %s',
        $self->data->{ case_id },
        $self->data->{ acceptee_name }
    );
}

sub event_category { 'case-mutation'; }

1;
