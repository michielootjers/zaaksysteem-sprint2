package Zaaksysteem::DB::Component::Logging::Kb::Question::Ask;

use Moose::Role;

sub onderwerp {
    sprintf('Vraag "%s" gesteld', shift->question->naam);
}

sub _add_magic_attributes {
    shift->meta->add_attribute('question' => ( is => 'ro', lazy => 1, default => sub {
        my $self = shift;

        $self->result_source->schema->resultset('KennisbankVragen')->find($self->data->{ question_id });
    }));
}

sub event_category { 'question' }

1;
