package Zaaksysteem::DB::Component::ZaaktypeNotificatie;

use strict;
use warnings;

use base qw/DBIx::Class/;

#
# since 2.7.0 ZaaktypeNotificaties use a BibliotheekNotificatie for storage of 
# subject, label and message body. there's a conversion script in /bin, also
# with import of a zaaktype that is not aware, this conversion needs to be performed.
#
# look for a matching bibliotheek_notificatie in the database, enter it's id in
# bibliotheek_notificaties_id.
#
sub import_bibliotheek_notificatie {
    my ($self) = @_;

    # already set
    if(
        $self->bibliotheek_notificaties_id &&
        $self->bibliotheek_notificaties_id->id
    ) {
        warn "for " . $self->id . " the bibliotheek_notificaties_id is already set to " . $self->bibliotheek_notificaties_id->id;
        return;
    }

    my $bibliotheek_notificatie = $self->result_source->schema->resultset("BibliotheekNotificaties")->find({
        label       => $self->label,
        subject     => $self->onderwerp,
        message     => $self->bericht,
    });
    
    warn "zaaktype_notificatie id = " . $self->id;
    
    unless(
        $self->zaaktype_node_id &&
        $self->zaaktype_node_id->zaaktype_id &&
        $self->zaaktype_node_id->zaaktype_id->bibliotheek_categorie_id &&
        $self->zaaktype_node_id->zaaktype_id->bibliotheek_categorie_id->id
    ) {
        warn "bibliotheek_categorie not found";
        next;
    }

    my $bibliotheek_categorie_id = $self->zaaktype_node_id->zaaktype_id->bibliotheek_categorie_id->id;
    die "wtf" unless($bibliotheek_categorie_id);
    
    unless($bibliotheek_notificatie) { 
    
        $bibliotheek_notificatie = $self->result_source->schema->resultset("BibliotheekNotificaties")->create({
            label                       => $self->label,
            subject                     => $self->onderwerp,
            message                     => $self->bericht,
            bibliotheek_categorie_id    => $bibliotheek_categorie_id,
        });
    }
    
    die "wtf" unless ($bibliotheek_notificatie && $bibliotheek_notificatie->id);

    $self->bibliotheek_notificaties_id($bibliotheek_notificatie->id);
    $self->update();
}

1;
