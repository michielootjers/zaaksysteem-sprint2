package Zaaksysteem::DB::Component::NatuurlijkPersoon;

use strict;
use warnings;
use Data::Dumper;
use POSIX qw(strftime);
use Date::Parse qw/strptime/;

use base qw/Zaaksysteem::DB::Component::GenericNatuurlijkPersoon/;


sub insert {
    my ($self) = @_;

    # we need the database to generate the new searchable_id param. 
    # if we don't supply it, it will take its chance. so get rid
    # of whatever the app thinks it should pass.
    # is there a way to put this in the Schema? that's be less hacky.
    delete $self->{_column_data}->{searchable_id};

    $self->_set_search_term();
    return $self->next::method(@_);
}


sub update {
    my $self    = shift;
    my $params  = shift;

    if ($params && UNIVERSAL::isa($params, 'HASH')) {
        for my $key (keys %{ $params }) {
            $self->$key($params->{ $key });
        }
    }

    $self->_set_search_term();
    return $self->next::method(@_);
}


sub _set_search_term {
    my ($self) = @_;


    my $search_term = '';

    if($self->voornamen) {
        $search_term .= $self->voornamen . ' ';
    }
    if($self->voorvoegsel) {
        $search_term .= $self->voorvoegsel . ' ';
    }
    if($self->geslachtsnaam) {
        $search_term .= $self->geslachtsnaam . ' ';
    }
    if($self->burgerservicenummer) {
        $search_term .= $self->burgerservicenummer . ' ';
    }
    if($self->geboortedatum) {
        my $geboortedatum = $self->geboortedatum;
        $geboortedatum =~ s|T.*$||is;
        my ($y, $m, $d) = split /-/, $geboortedatum;
        
        my $birthday_dmy = "$d-$m-$y";

        $search_term .= $geboortedatum . ' '. $birthday_dmy . ' ';
    }
    if($self->adres_id && $self->adres_id->straatnaam) {
        $search_term .= $self->adres_id->straatnaam . ' ';
    }
    if($self->adres_id && $self->adres_id->huisnummer) {
        $search_term .= $self->adres_id->huisnummer;
        if($self->adres_id->huisnummertoevoeging) {
            $search_term .= $self->adres_id->huisnummertoevoeging;
        }
        $search_term .= ' ';
    }
    if($self->adres_id && $self->adres_id->woonplaats) {
        $search_term .= $self->adres_id->woonplaats . ' ';
    }
    if($self->adres_id && $self->adres_id->postcode) {
        $search_term .= $self->adres_id->postcode . ' ';
    }

    $self->search_term($search_term);
}

sub TO_JSON {
    my $self = shift;

    return {
        id => $self->id,
        bsn => $self->burgerservicenummer,
        initials => $self->voorletters,
        given_names => $self->voornamen,
        surname => $self->geslachtsnaam,
        surname_preposition => $self->voorvoegsel,
        gender => $self->geslachtsaanduiding eq 'V' ? 'female' : 'male'
    };
}

1;
