package Zaaksysteem::DB::Component::ScheduledJobs;

use strict;
use warnings;

use Data::Dumper;
use DateTime;


use base qw/DBIx::Class/;

sub scheduled_for_utc { 
    shift->scheduled_for->set_time_zone('UTC') 
}

sub run {
    my $self            = shift;

    my $resultset       = $self->result_source->resultset;

    die("ScheduledJobs->run: Unknown Error: no task was set in DB.")
        unless $self->task;

    die("ScheduledJobs->run: Unknown error: invalid task in database.")
        unless defined($resultset->task_list->{ $self->task });

    my $taskdef     = $resultset->task_list->{ $self->task };

    die("ScheduledJobs->run: Unknown error: Don't know how to run task.")
        unless $taskdef->{run};

    my $result;

    eval {
        $self->result_source->schema->txn_do(sub {
            $result     = $taskdef->{run}->($self);

            $self   ->result_source
                    ->schema
                    ->resultset('Logging')
                    ->trigger(
                        'scheduler/run', {
                            zaak_id     => $self->parameters->{case_id},
                            data        => {
                                task_id => $self->id,
                            }
                    });

            $self->deleted(DateTime->now());
            $self->update;
        });
    };

    if ($@) {
        die('ScheduledJobs->run[' . $self->task . '] ERROR: ' . $@);
    }


    return $result;
}

sub reject {
    my $self            = shift;

    $self->deleted(DateTime->now());
    $self->update;
}

1;
