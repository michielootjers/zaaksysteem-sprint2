package Zaaksysteem::DB::Component::ZaaktypeFase;

use Moose;
use Data::FormValidator::Results;
use Data::Dumper;
use Zaaksysteem::Constants qw/ZAAKSYSTEEM_CONSTANTS/;

extends qw/DBIx::Class/;

sub validate_kenmerken {
    my $self        = shift;
    my $data        = shift;
    my $options     = shift;

    my $profile = $self->_build_kenmerken_validatie_profile($options, $data);

    return Data::FormValidator::Results->new($profile, $data);
}

sub _build_kenmerken_validatie_profile {
    my $self        = shift;
    my $options     = shift;
    my $data        = shift;

    my $profile     = {
        required            => [],
        optional            => [qw/
            npc-email
            npc-telefoonnummer
            npc-mobiel
        /],
        constraint_methods  => {
            'npc-email'             => qr/^.+?\@.+\.[a-z0-9]{2,}$/,
            'npc-telefoonnummer'    => qr/^[\d\+]{6,15}$/,
            'npc-mobiel'            => qr/^[\d\+]{6,15}$/,
        },
        msgs                => {
            'format'    => '%s',
            'missing'   => 'Veld is verplicht.',
            'invalid'   => 'Veld is niet correct ingevuld.',
            'constraints' => {
                '(?-xism:^[\d\+]{6,15}$)'         => 'Nummer zonder spatie (e.g: +312012345678)',
            }
        },
    };

    my $kenmerken   = $self->zaaktype_kenmerken->search(
        {},
        {
            prefetch    => 'bibliotheek_kenmerken_id'
        }
    );

    while (my $kenmerk = $kenmerken->next) {
        next unless $kenmerk->bibliotheek_kenmerken_id;
        
        if (
            $options &&
            $options->{ignore_undefined} &&
            !defined($data->{
                    ($options->{with_prefix}
                        ? 'kenmerk_id_'
                        : ''
                    ) . $kenmerk->bibliotheek_kenmerken_id->id
            })
        ) {
            next;
        }

        my $kenmerk_id  = ($options && $options->{with_prefix}
            ? 'kenmerk_id_'
            : ''
        ) . $kenmerk->bibliotheek_kenmerken_id->id;

        if ($kenmerk->value_mandatory) {
            push(@{ $profile->{required} }, $kenmerk_id);
        } else {
            push(@{ $profile->{optional} }, $kenmerk_id);
        }
    }

    return $profile;
}

sub manual_templates {
    my ($self) = @_;

    return scalar $self->zaaktype_sjablonen->search({
        automatisch_genereren   => undef
    }, {
        prefetch => 'bibliotheek_sjablonen_id', 
        order_by => 'me.id' 
    });
}

sub manual_notifications {
    my ($self) = @_;

    return scalar $self->zaaktype_notificaties->search({
        automatic   => undef
    }, {
        prefetch => 'bibliotheek_notificaties_id', 
        order_by => 'me.id' 
    });
}

sub manual_subcases {
    my ($self) = @_;

    return scalar $self->zaaktype_relaties->search({
        automatisch_behandelen => undef,
        relatie_type    => { '!=' => 'vervolgzaak' },
    }, {
        prefetch => 'relatie_zaaktype_id', 
        order_by => 'me.id' 
    });
}

sub notifications {
    my ($self) = @_;

    return scalar $self->zaaktype_notificaties->search({}, {
        prefetch => 'bibliotheek_notificaties_id',
        order_by => {'-asc' => 'me.id'},
    });
}

sub get_steps {
    my $self = shift;
    
    my $groups = $self->zaaktype_kenmerken->search(
        { bibliotheek_kenmerken_id => undef },
        { order_by => { -asc => 'id' } }
    );

    return map { { label => $_->label } } $groups->all;
}

sub is_last {
    my $self = shift;

    my $phases = $self->zaaktype_node_id->zaaktype_statuses;

    return $self->status >= $phases->count;
}

1;
