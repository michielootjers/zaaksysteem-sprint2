package Zaaksysteem::DB::Component::Zaaktype;

use strict;
use warnings;

use Moose;

use Data::Dumper;


extends 'DBIx::Class';





sub insert {
    my $self    = shift;

    # we need the database to generate the new searchable_id param. 
    # if we don't supply it, it will take it's chance. so get rid
    # of whatever the app thinks it should pass.
    # is there a way to put this in the Schema? that's be less hacky.
    delete $self->{_column_data}->{searchable_id};

    $self->_set_search_string({insert => 1});
    $self->next::method(@_);
}

sub update {
    my $self    = shift;
    my $columns = shift;

    $self->_set_search_string;
    $self->next::method(@_);
}

sub title {
    my $self = shift;

    if($self->zaaktype_node_id) {
        return $self->zaaktype_node_id->titel;
    }

    return $self->id;
}

sub _set_search_string {
    my ($self) = @_;
    
    my $search_string = $self->id . ' ';
    
    
    if($self->zaaktype_node_id) {
    
        if($self->zaaktype_node_id->titel) {
            $search_string .= $self->zaaktype_node_id->titel;
        }

        if($self->zaaktype_node_id->zaaktype_trefwoorden) {
            $search_string .= ' ' . $self->zaaktype_node_id->zaaktype_trefwoorden;
        }

        if($self->zaaktype_node_id->zaaktype_omschrijving) {
            $search_string .= ' ' . $self->zaaktype_node_id->zaaktype_omschrijving;
        }
    }

    $self->search_term($search_string);
}


1; #__PACKAGE__->meta->make_immutable;

