package Zaaksysteem::DB::Component::BagOpenbareruimte;

use strict;
use warnings;

use base qw/Zaaksysteem::DB::Component::BagGeneral Zaaksysteem::Geo::BAG/;


=head2 geocode_term

Return value: $geocode_address

Generate a term for geocoding this location with a googlemaps or other
geocoder.

=cut

sub geocode_term {
    my $self    = shift;

    my @terms   = ('Nederland');

    push(@terms, $self->openbareruimte->woonplaats->naam);

    return join(',', @terms);
}

sub to_string {
    my $self = shift;

    return sprintf(
        '%s, %s',
        $self->naam,
        $self->woonplaats->naam
    );
}

sub TO_JSON {
    my $self = shift;

    return {
        id => 'openbareruimte-' . $self->identificatie,
        type => 'street',
        streetname => $self->naam,
        city => $self->woonplaats->naam
    };
}

1;
