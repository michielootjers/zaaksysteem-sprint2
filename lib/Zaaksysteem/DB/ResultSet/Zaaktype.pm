package Zaaksysteem::DB::ResultSet::Zaaktype;

use strict;
use warnings;

use Moose;
use Zaaksysteem::Constants;
use Data::Dumper;

extends 'DBIx::Class::ResultSet', 'Zaaksysteem::Zaaktypen::BaseResultSet';

__PACKAGE__->load_components(qw/Helper::ResultSet::SetOperations/);

# COMPLETE: 100%
use constant    PROFILE => {
    required        => [qw/
        bibliotheek_categorie_id
    /],
    optional        => [qw/
    /],
    msgs            => PARAMS_PROFILE_DEFAULT_MSGS,
};

sub _validate_session {
    my $self            = shift;
    my $profile         = PROFILE;
    my $rv              = {};

    $self->__validate_session(@_, $profile, 1);
}


sub search_with_options {
    my ($self, $args) = @_;

    my $whereClause = {
        'me.deleted' => undef,
    };

    if (defined($args->{trigger}) && $args->{trigger}) {
        $whereClause->{'zaaktype_node_id.trigger'} = {
            'ilike' => '%' . $args->{trigger} .  '%'
        };
    }

    if (defined($args->{betrokkene_type}) && $args->{betrokkene_type}) {
        $whereClause->{'zaaktype_betrokkenens.betrokkene_type'} = 
            $args->{betrokkene_type};
    }

    if($args->{term}) {
        my $search_request  = { ilike => '%' .  $args->{term} . '%' };
        $whereClause->{'-or'} = [
            { 'zaaktype_node_id.titel'                 => $search_request },
            { 'zaaktype_node_id.zaaktype_trefwoorden'  => $search_request },
            { 'zaaktype_node_id.zaaktype_omschrijving' => $search_request },
        ];
    }

    my $options = {
        join => [
            'zaaktype_node_id',
            { zaaktype_node_id => 'zaaktype_betrokkenens' },
        ],
        group_by => [
            'me.id', 'me.zaaktype_node_id', 'me.version', 'me.active',
            'me.created', 'me.last_modified', 'me.deleted',
            'me.bibliotheek_categorie_id',
            'me.search_index',
            'me.search_term','me.object_type','me.searchable_id',
        ],    
    }; 

    return $self->search($whereClause, $options);   
}

1;
