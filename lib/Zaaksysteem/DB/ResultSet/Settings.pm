package Zaaksysteem::DB::ResultSet::Settings;

use strict;
use warnings;

use Moose;
use Zaaksysteem::Constants;
use Data::Dumper;
use Data::Serializer;
use Digest::MD5::File qw/-nofatals file_md5_hex/;
use Data::UUID;
  
extends 'DBIx::Class::ResultSet';


=head1

Get all settings key/value pairs matching the supplied filter. Since settings keys
have a format [group]_[subgroup]_[key] an sensible filter would be "[group]_"

Underscore are used because Template Toolkit will interpret dots like sub-hashkeys.

=cut

sub filter {
    my ($self, $opts) = @_;
    
    my $filter = $opts->{filter} or die "need filter";
    
    my $rs = $self->search({
        key => {
            ilike => $filter . '%',
        }
    });
    
    return [$rs->all];
}



sub store {
    my ($self, $opts) = @_;
    
    my $key     = $opts->{key}      or die "need key";
    my $value   = $opts->{value}    or die "need value";


    my $row = $self->find({
        key => $key
    });
    
    if($row) {
        $row->value($value);
        $row->update;
    } else {
        $self->create({
            key     => $key,
            value   => $value,
        });
    }
}
1;
