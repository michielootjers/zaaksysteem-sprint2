package Zaaksysteem::DB::ResultSet::Checklist;

use Moose;

use Data::Dumper;

BEGIN { extends 'DBIx::Class::ResultSet'; }

sub create_from_case {
    my ($self, $case) = @_;

    for my $phase ($case->zaaktype_node_id->zaaktype_statuses) {
        next if $phase->status == 1;

        my $checklist = $self->new_result({
            case_id => $case->id,
            case_milestone => $phase->status
        });

        $checklist->insert;

        my $iteration = 1;

        for my $case_type_item ($phase->zaaktype_status_checklist_items->search({}, { order_by => 'id' })) {
            $checklist->checklist_items->new_result({
                label => $case_type_item->label,
                user_defined => 0,
                sequence => $iteration++
            })->insert;
        }
    }
}

1;
