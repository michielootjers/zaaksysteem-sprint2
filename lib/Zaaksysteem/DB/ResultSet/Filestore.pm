package Zaaksysteem::DB::ResultSet::Filestore;

use strict;
use warnings;

use Moose;
use Zaaksysteem::Constants;
use Data::Dumper;
use Data::Serializer;
use Digest::MD5::File qw/-nofatals file_md5_hex/;
use Data::UUID;
  
extends 'DBIx::Class::ResultSet';
use Carp qw/cluck/;

sub save_upload {
    my ($self, $opts) = @_;
    
    my $upload      = $opts->{upload}       or die "need Catalyst upload object";
    my $files_dir   = $opts->{files_dir}    or die cluck "need files_dir";

    my $filename    = $upload->filename;
    my $filesize    = $upload->size;
    my $mimetype    = $upload->type;
    
    my $ug = new Data::UUID;
    my $uuid = $ug->create();
    my $uuid_string = $ug->to_string($uuid);

    my $filestore = $self->create({
        filename => $filename, 
        filesize => $filesize,
        mimetype => $mimetype,
        uuid     => $uuid_string,
    });

    if (!$filestore) {
        warn(
            'Filestore::save: Kan filestore entry niet aanmaken: '. $filename
        );
#        $c->flash->{result} = 'ERROR: Kan bestand niet aanmaken op omgeving';
        return;
    }


    # Store content on disk
    my $disk_location = $files_dir . '/filestore/' . $filestore->id;

    warn("disk_location:" . $disk_location);

    if (!$upload->copy_to($disk_location)) {
        $filestore->delete;
        warn(
            'Filestore::save: Kan bestand niet kopieren: '
            . $filename . ' -> ' . $disk_location
        ) . ': ' . $!;
#        $c->flash->{result} = 'ERROR: Kan bestand niet kopieren naar omgeving';
        return;
    }

    # Stored on system and database, now fill in other fields

    # md5sum
    my $md5sum = file_md5_hex($disk_location);
    $filestore->md5sum($md5sum);

    $filestore->update;

    return $filestore;
}


1;
