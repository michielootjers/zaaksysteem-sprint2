package Zaaksysteem::DB::ResultSet::ScheduledJobs;


use strict;
use warnings;

use Moose;
use Data::Dumper;
use Data::Serializer;

use HTML::Strip;

use File::Temp qw/tempfile/;

use constant SCHEDULE_TYPE_MANUAL   => 'manual';
use constant SCHEDULE_TYPE_TIME     => 'time';
use constant SCHEDULE_TYPE_DEFAULT  => SCHEDULE_TYPE_TIME;


extends 'DBIx::Class::ResultSet';


=head2 $resultset->pending()

Return value: $RESULTSET_SCHEDULEDJOBS

  my $rs_jobs = $resultset->pending()

Returns the resultset of jobs of schedule_type = 'TIME' and which are
still in the future (won't run anytime soon)

=cut

sub pending {
    my $self        = shift;

    return $self->search({
        schedule_type   => { '!='   => SCHEDULE_TYPE_MANUAL },
        scheduled_for   => { '>'    => DateTime->now()},
        deleted         => undef,
    }, {
        order_by => {'-asc' => 'scheduled_for'}
    });
}

=head2 $resultset->ready([ \%OPTIONS ])

Return value: $RESULTSET_SCHEDULEDJOBS

  my $rs_jobs = $resultset->ready()

Returns the resultset of jobs which are ready to run, e.g. manual jobs
or jobs which are scheduled in the past.

B<Options>

=over 4

=item schedule_type OPTIONAL (DEFAULT: time)

This defines whether the searched schedule item is of type manual or time.

=back

=cut

sub ready {
    my ($self, $options)    = @_;
    $options                ||= {};

    my $dv              = Data::FormValidator->check(
        $options,
        {
            optional    => [qw/
                schedule_type
            /],
            defaults    => {
                schedule_type => SCHEDULE_TYPE_DEFAULT,
            }
        }
    );

    die("ScheduledJobs->ready: Profile ERROR: " . Dumper($dv))
        unless $dv->success;

    if($options->{ schedule_type } eq SCHEDULE_TYPE_MANUAL) {
        return $self->search({
            schedule_type   => SCHEDULE_TYPE_MANUAL,
            deleted         => undef,
        }, {
            order_by => {'-asc' => 'last_modified'}
        });
    }

    return $self->search({
        schedule_type   => { '!='   => SCHEDULE_TYPE_MANUAL },
        scheduled_for   => {'<=' => DateTime->now()},
        deleted         => undef,
    }, {
        order_by => {'-asc' => 'scheduled_for'}
    });
}


=head1 $resultset->cancel_pending(\%OPTIONS)

Return value: $TRUE_OR_FALSE

  $resultset->cancel_pending({ $zaak_id => 1234 });

Cancels all the jobs that do not have ran yet for the given zaak_id

B<Options>

=over 4

=item zaak_id

A case id defining the case for which all jobs are canceled

=back

=cut 

sub cancel_pending {
    my ($self, $options) = @_;

    my $zaak_id = $options->{zaak_id} or die "need zaak_id";

    my $pending = $self->pending({
        task => 'case/mail'
    });

    while(my $job = $pending->next()) {

        # eagerly awaiting json querying, this will become incrementally slower
        my $job_zaak_id = $job->parameters->{zaak_id} or next;

        if ($job_zaak_id eq $zaak_id) {
            $job->delete;
        }
    }

    return 1;
}

=head2 $resultset->reschedule_pending(\%OPTIONS)

TODO DOC THIS

=cut


sub reschedule_pending {
    my ($self, $options) = @_;

    my $zaak_id         = $options->{zaak_id}           or die "need zaak_id";
    my $scheduled_mails = $options->{scheduled_mails}   or die "need scheduled_mails";
    my $notifications   = $options->{notifications}     or die "need notifications";

    my $rescheduled_mails = {};

    my $notification_index = 0;

    while(my $notification = $notifications->next()) {
        $notification_index++;

        if(my $schedule_mail = $scheduled_mails->{$notification_index}) {

            # mails that have been scheduled for the phase transition will be executed by the
            # action mechanism
            next if $schedule_mail->{phase_transition};

            my $send_date_dt = $schedule_mail->{send_date};

            my $new = $self->create_zaak_notificatie({
                bibliotheek_notificaties_id => $notification->bibliotheek_notificaties_id->id,
                scheduled_for   => $send_date_dt,
                recipient_type  => $notification->rcpt,
                zaak_id         => $zaak_id,
                behandelaar     => $notification->behandelaar,
                email           => $notification->email,
            });

            $rescheduled_mails->{$notification_index}++;
        }
    }

    return $rescheduled_mails;
}

=head2 $resultset->create_zaak_notificatie(\%OPTIONS)

TODO DOC THIS

wrapper around create - purpose is to enforce required parameters

=cut

sub create_zaak_notificatie {
    my ($self, $options) = @_;

    my $scheduled_for   = $options->{scheduled_for}     or die "need scheduled_for";
    my $zaak_id         = $options->{zaak_id}           or die "need zaak_id";
    my $recipient_type  = $options->{recipient_type}    or die "need recipient_type";
    my $behandelaar     = $options->{behandelaar};      # optional
    my $email           = $options->{email};            # optional

    my $bibliotheek_notificaties_id = $options->{bibliotheek_notificaties_id} 
        or die "need bibliotheek_notificaties_id";

    return $self->create({
        task            => 'case/mail',
        parameters      => {
            bibliotheek_notificaties_id => $bibliotheek_notificaties_id,
            recipient_type              => $recipient_type,
            zaak_id                     => $zaak_id,
            behandelaar                 => $behandelaar,
            email                       => $email,
        },
        scheduled_for   => $scheduled_for,
        schedule_type => SCHEDULE_TYPE_TIME
    });
}


=head2 $resultset->_create_task(%OPTIONS)

Return value: $ROW_SCHEDULEDJOBS

    $resultset->_create_task(
        {
            parameters          => {
                beermanufacturer    => 'Grolsch',
                like                => 'No',
            },
            schedule_type       => 'auto',
            scheduled_for       => DateTime->now(),
        }
    )

Creates a task in the scheduler for run at a later time. Tasks could be scheduled
for another time, or for review by a case manager.

B<OPTIONS>

=over 4

=item parameters OPTIONAL

An HASHREF of parameters to send to the given task.

=item task REQUIRED

The task to be run. Needs to be a valid task from L<task_list>.

=item schedule_type REQUIRED

The schedule_type can be one of 'auto' or 'manual'. When an 'auto' type is given,
make sure the scheduled_for is set with a valid L<DateTime> object.

=item scheduled_for REQUIRED when SCHEDULE_TYPE = 'auto'

A valid DateTime object when schedule_type = 'auto'

=back

B<Schedulers>

Below a list of scheduled tasks

=head3

=head3 case/update_kenmerk %OPTIONS

Return value: $ROW_SCHEDULEDJOBS

    $resultset->schedule_kenmerk(
        {
            bibliotheek_kenmerken_id    => 55,
            value                       => 'Nee',
            requestor                   => $case->aanvrager_object,
            case                        => $case,
        }
    )

Schedules a zaak kenmerk for updating. This function only supports scheduling
for approval by a behandelaar (not scheduled on a certain time)

B<Options>

=over 4

=item bibliotheek_kenmerken_id

The kenmerk ID to change the value for.

=item value

Value for the 'to be changed' kenmerk.

=item reason

The reason for this change. A text explaining why the requestor would like to change
this kenmerk

=item case

The case object for this change

=item requestor OPTIONAL

The requestor object for this change.

=back

=cut

use constant    SCHEDULE_TASK_LIST => {
    'case/update_kenmerk'  => {
        'scheduler'     => {
            'profile'       => {
                'missing_optional_valid'  => 1,
                required    => [qw/
                    bibliotheek_kenmerken_id
                    case
                    created_by
                /],
                optional    => [qw/
                    value
                    reason
                    parameters
                /],
                defaults    => {
                    schedule_type   => SCHEDULE_TYPE_MANUAL,
                    parameters      => sub {
                        my ($dfv)   = @_;

                        my $values  = $dfv->get_filtered_data;

                        return unless ($values->{case});

                        my $stripper = HTML::Strip->new;

                        my $reason   = (
                            $values->{'reason'} ?
                            $stripper->parse($values->{'reason'})
                            : ''
                        );

                        return {
                                value                       => (
                                    defined($values->{'value'})
                                        ? $values->{'value'}
                                        : ''
                                ),
                                reason                      => $reason,
                                bibliotheek_kenmerken_id    =>
                                    $values->{'bibliotheek_kenmerken_id'},
                                case_id                     =>
                                    $values->{'case'}->id,
                        };
                    }
                }
            }
        },
        'run'           => sub {
            my $task        = shift;

            my $params      = $task->parameters;

            ### Find case_id
            my $case        = $task ->result_source
                                    ->schema
                                    ->resultset('Zaak')
                                    ->find($params->{case_id});

            my $update_fields_params = {
                new_values  => {
                    $params->{bibliotheek_kenmerken_id} => $params->{value},
                },
                zaak_id     => $case->id
            };

            $case->zaak_kenmerken->update_fields($update_fields_params);
        },
        description   => sub {
            my $task        = shift;

            ### Get kenmerk name:
            my $case        = $task
                            ->result_source
                            ->schema
                            ->resultset('Zaak')
                            ->find($task->parameters->{case_id});

            my $kenmerk     = $case
                            ->zaaktype_node_id
                            ->zaaktype_kenmerken
                            ->search(
                                {
                                    bibliotheek_kenmerken_id => $task
                                            ->parameters
                                            ->{bibliotheek_kenmerken_id}
                                },
                                {
                                    prefetch    => 'bibliotheek_kenmerken_id'
                                }
                            )->first;

            my $label       = $kenmerk->label
                ? $kenmerk->label
                : $kenmerk->bibliotheek_kenmerken_id->naam;

            my $value       = $task->parameters->{'value'} || '';
            $value  = ( UNIVERSAL::isa($value, 'ARRAY')
                ? $value
                : [$value]
            );

            return {
                short   => 'Kenmerk "' . $label
                    . '", wijzigingsvoorstel: "'
                    . join(',', @{ $value })
                    . '"',
                long    => 'Kenmerk "' . $label
                    . '", ' . "\n\n"
                    . 'Wijzigingsvoorstel: ' . "\n- "
                    . join("\n- ", @{ $value })
                    . "\n\nReden:\n"
                    . ($task->parameters->{reason} || '')
            };
        }
    }
};

has 'task_list' => (
    is          => 'ro',
    lazy        => 1,
    default     => sub {
        return SCHEDULE_TASK_LIST();
    }
);

use constant CREATE_TASK_PROFILE => {
    required    => [qw/
        task
        schedule_type
    /],
    optional    => [qw/
        parameters
        scheduled_for
    /],
    constraint_methods => {
        task    => sub {
            my ($dfv, $val) = @_;

            return (SCHEDULE_TASK_LIST()->{$val});
        },
        parameters => sub {
            my ($dfv, $val) = @_;

            return UNIVERSAL::isa($val, 'HASH');
        }
    },
    dependencies    => {
        schedule_type   => sub {
            my $dfv     = shift;
            my $type    = shift;

            if ($type eq SCHEDULE_TYPE_MANUAL) {
                return [''];
            }

            return ['scheduled_for'];
        }
    }
};


sub create_task {
    my ($self, $opts)   = @_;
    $opts               ||= {};

    my ($taskdef, $create_options);

    {
        ### Check for valid INPUT
        die("ScheduledJobs->create_task: input error: no task given.")
            unless $opts && $opts->{task};

        die("ScheduledJobs->create_task: input error: invalid task given.")
            unless defined($self->task_list->{ $opts->{task} });

        $taskdef     = $self->task_list->{ $opts->{task} };

        ### Check for valid TASK
        die("ScheduledJobs->create_task: Definition error: no scheduler set.")
            unless $taskdef && $taskdef->{scheduler};

        die("ScheduledJobs->create_task: Definition error: no profile set.")
            unless $taskdef->{scheduler}->{profile};
    }

    ###
    ### VALIDATE TASK
    ###
    {
        my $scheduler_dv    = Data::FormValidator->check(
            $opts,
            $taskdef->{scheduler}->{profile}
        );

        die(
            "ScheduledJobs[" . $opts->{task} . ']: Profile Error: '
            . Dumper($scheduler_dv)
        ) unless $scheduler_dv->success;

        my $schedule_params = $scheduler_dv->valid;

        $create_options  = { map { $_ => $schedule_params->{ $_ } }
            $self->result_source->columns };
        delete($create_options->{$_}) for qw/id deleted last_modified created/;

        $create_options->{task} = $opts->{task};
    }

    ### Validate database rows for ScheduledJobs
    my $dv              = Data::FormValidator->check(
        $create_options,
        CREATE_TASK_PROFILE
    );

    die("ScheduledJobs->create_task: Profile ERROR: " . Dumper($dv))
        unless $dv->success;

    ### Strip unknown columns
    my $create_params = {
        map { $_ => $dv->valid($_) }
        grep { defined($dv->valid( $_ )) } $self->result_source->columns
    };

    if (my $task = $self->create($create_params)) {
        ### Task created, depending on manual or time, place register it
        return $self->_register_task($task, $opts);
    }
}

=head2 $resultset->_register_task($task)

Return value: $TRUE_OR_FALSE

  $resultset->_register_task($task);

Registers task in the appropriate place. When set to manual, create a hook in
our documents to accept or reject this task.

=cut

sub _register_task {
    my $self        = shift;
    my $task        = shift;
    my $opts        = shift;

    return 1 unless $task->schedule_type eq 'manual';

    my $taskdef     = $self->task_list->{ $task->task };

    my $current_user = $self
                        ->result_source
                        ->schema
                        ->resultset('Zaak')
                        ->current_user;

    my $description     = $taskdef->{description}->($task);

    ### Dump task to tmp file
    my ($fh, $filename) = tempfile();
    $fh->autoflush(1);
    print $fh $description->{long};

    my $name            = $task->task;
    $name               =~ s/[^a-z0-9]/_/;

    ### Dump data as file
    my $result  = $self
                ->result_source
                ->schema
                ->resultset('File')
                ->file_create({
                    file_path => $filename,
                    name      => 'taak.txt',
                    db_params => {
                        publish_website     => 0,
                        publish_pip         => 1,
                        created_by          => $opts->{created_by},
                        case_id             => $task->parameters->{case_id},
                        scheduled_jobs_id   => $task->id,
                    },
                    metadata => {
                        description => $description->{short}
                    },
                });

    return $task;
}

=head2 $resultset->search_manual_tasks_with_case_id

Return value: $RESULTSET_SCHEDULED_JOBS

    $resultset->search_manual_tasks_with_case_id(
        {
            task        => 'case/update_kenmerk',
            case_id     => $case->id,
            created_by  => 'betrokkene-natuurlijker_persoon-42423',
        }
    );

Searches manual tasks with given case_id

=cut

sub search_manual_tasks_with_case_id {
    my $self        = shift;
    my $opts        = shift;

    my $dv          = Data::FormValidator->check(
        $opts,
        {
            required    => [qw/task case_id/],
            optional    => [qw/created_by/],
        }
    );

    die("ScheduledJobs->search_manual_tasks_with_case_id: Profile ERROR: "
        . Dumper($dv)
    ) unless $dv->success;

    my $case        = $self
                    ->result_source
                    ->schema
                    ->resultset('Zaak')
                    ->find($dv->valid('case_id'));

    die(
        "ScheduledJobs->search_manual_tasks_with_case_id: "
        ."could not find case with given case_id"
    ) unless $case;

    my $params      = $dv->valid;
    delete($params->{case_id});

    my $file_params = {
        scheduled_jobs_id   => { '!=' => undef },
        date_deleted        => undef,
    };

    if ($params->{created_by}) {
        $file_params->{created_by}  = $params->{created_by};
        delete($params->{created_by});
    }

    my $file_rs     = $case
                    ->files
                    ->search($file_params);

    my $jobs_rs     = $self->search(
        {
            id      => {
                'in'    => $file_rs
                        ->get_column('scheduled_jobs_id')
                        ->as_query
            },
            deleted => undef,
            %{ $params }
        }
    );
}


1;


