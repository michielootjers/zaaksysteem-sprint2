package Zaaksysteem::Object::ImportStateItem;

use Moose;

has 'id' => ( is => 'rw' );
has 'label' => ( is => 'rw' );
has 'selected_option' => ( is => 'rw' );
has 'config' => ( is => 'rw', isa => 'Maybe[HashRef]' );
has 'source_uri' => ( is => 'rw' );

has 'options' => (
    is => 'rw',
    isa => 'ArrayRef[HashRef]',
    default => sub { [] }
);

sub get_state {
    my $self = shift;
    
    return {
        id => $self->id,
        label => $self->label,
        options => $self->options,
        selected_option => $self->selected_option,
        source_uri => $self->source_uri,
        config => $self->config
    }
}

sub add_option {
    push(@{ shift->options }, {
        option => shift,
        label => shift
    });
}

sub select_option {
    my $self = shift;
    my $selection = shift;
    my $config = shift // {};

    my ($option) = grep { $_->{ option } eq $selection } @{ $self->options };

    unless($option) {
        die('No such option: ' . $selection);
    }

    $self->config($config);
    $self->selected_option($selection);
}

sub selected {
    my $self = shift;
    my $selected = $self->selected_option;

    return unless $selected;

    my ($option) = grep { $_->{ option } eq $selected } @{ $self->options };

    return $option;
}

1;
