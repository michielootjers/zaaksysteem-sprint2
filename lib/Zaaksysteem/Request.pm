package Zaaksysteem::Request;

use Moose;
use Data::Dumper;

extends qw/Catalyst::Request/;

sub _is_xhr_requested_with {
	my $self = shift;

    return 
    	$self->header('x-requested-with') && 
    	$self->header('x-requested-with') eq 'XMLHttpRequest';
}

sub _is_xhr_accept_json {
    my $self = shift;

    my @accept_types = split m[,\s*], ($self->header('Accept') || '');

    return shift @accept_types eq 'application/json';
}

sub is_xhr {
    my $self = shift;

    return $self->_is_xhr_requested_with || $self->_is_xhr_accept_json;
}

1;
