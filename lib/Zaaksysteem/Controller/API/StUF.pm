package Zaaksysteem::Controller::API::StUF;

use strict;
use warnings;

use Data::Dumper;

use Scalar::Util qw/blessed/;

use Moose;

use constant COMPONENT_KENNISGEVING => 'kennisgeving';
use constant MODEL_SBUS             => 'SBUS';

use utf8;

BEGIN {extends 'Zaaksysteem::General::SOAPController'; }

sub bg0204 :Local SOAP('DocumentLiteral')  {
   my ( $self, $c) = @_;

   unless ($self->verify_authorization($c)) {
       $c->stash->{soap}->fault(
           {
               code => '403',
               reason => 'Forbidden',
               detail => 'AUTHORIZATION FAILURE: Invalid SSL Certificate'
           }
       );

       return;
   }

   my $xml             = $c->stash->{soap}->envelope();
   my $return_xml      = $c->forward('handle_kennisgeving', [ $xml ]);

   $c->stash->{soap}->literal_return($return_xml);
}

sub verify_authorization {
    my ($self, $c, $xml)    = @_;

    ### SKIP SSL AUTHENTICATION, TMP
    unless (
        defined($c->engine->env->{SSL_CLIENT_VERIFY}) &&
        defined($c->customer_instance->{start_config}->{SBUS}->{issuer})
    ) {
        $c->log->error(
            'SBUS Authorization: SKIP'
        );
        return 1;
    }

    if ($c->engine->env->{SSL_CLIENT_VERIFY} ne 'SUCCESS') {
        $c->log->error(
            'SBUS Authorization: Failed: SSL_CLIENT_VERIFY: '
            .  $c->engine->env->{SSL_CLIENT_VERIFY}
        );

        return;
    }

    if ($c->engine->env->{SSL_CLIENT_VERIFY} eq 'SUCCESS') {
        $c->log->error(
            'SBUS Authorization: VERIFY OK: '
            .  $c->engine->env->{SSL_CLIENT_VERIFY}
        );

        my $conf_issuers    = $c
                            ->customer_instance
                            ->{start_config}
                            ->{SBUS}
                            ->{issuer};

        my @issuers         = (
            UNIVERSAL::isa($conf_issuers, 'ARRAY')
                ?  @{ $conf_issuers }
                : ( $conf_issuers )
        );

        for my $issuer (@issuers) {
            next unless $issuer eq $c->engine->env->{SSL_CLIENT_S_DN};

            $c->log->error(
                'SBUS Authorization: SUCCESS: CLIENT OK: '
                    . $c->engine->env->{SSL_CLIENT_S_DN}
            );

            return 1;

        }

        $c->log->error(
            'SBUS Authorization: FAILED: COULD NOT FIND ISSUER: '
                . $c->engine->env->{SSL_CLIENT_S_DN}
        );
    }

    return;
}

sub handle_kennisgeving : Private {
    my ($self, $c, $xml)    = @_;

    my $stuf                = $c->model(MODEL_SBUS);

    my $response            = $stuf->response(
        {
            sbus_type   => 'StUF',
            input_raw   => $xml,
        }
    );

    my $return = $stuf->compile_response(
        {
            response    => $response,
            sbus_type   => 'StUF',
        }
    );

    $c->log->debug(
        'Response from sbus: '
        . Dumper($response)
    );

    return $return;
}

sub manual : Local {
    my ($self, $c) = @_;

    $c->assert_any_user_permission('admin');

    $c->stash->{template} = 'beheer/stuf.tt';

    if ($c->req->params->{stuf_message}) {
        my $stuf                = $c->model(MODEL_SBUS);

        my $response            = $stuf->response(
            {
                sbus_type   => 'StUF',
                input_raw   => $c->req->params->{stuf_message},
            }
        );

        my $return = $stuf->compile_response(
            {
                response    => $response,
                sbus_type   => 'StUF',
            }
        );

        if ($return) {
            $c->stash->{success} = 1;

            my $tidy    = XML::Tidy->new(xml => $return->toString);
            my $xml     = $tidy->tidy()->toString();

            $c->stash->{return_message} = $xml;
        }
    }

    $c->detach('View::TT');
}

1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

