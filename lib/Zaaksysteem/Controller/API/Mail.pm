package Zaaksysteem::Controller::API::Mail;

use strict;
use warnings;

use File::Basename;

use parent 'Catalyst::Controller';

sub input : Local {
    my ($self, $c) = @_;

    die('No message found') unless $c->req->params->{message};

    $c->log->debug(
        'API::Mail->input: MAIL handler'
    );

    eval {
        $c->forward('/api/mail/rci/handle');
        $c->forward('/api/mail/yucat/handle');
    };

    if ($@) {
        $c->log->error('Mail error: ' . $@);
    }

    $c->res->body('ok');
}

sub _retrieve_files {
    my ($self, $message) = @_;

    my $parser = new MIME::Parser;

    $parser->extract_uuencode(1);

    my $entity = $parser->parse_data($message);

    return ($parser, $entity->parts_DFS);
}

sub _handle_part {
    my ($self, $part) = @_;

    my $raw_filename = $part->head->recommended_filename;
    if (!$raw_filename) {
        $raw_filename = $part->bodyhandle->path;
    }

    my ($filename, $dir, $ext) = fileparse($raw_filename, '\.[^.]*');

    return ($filename, $part->bodyhandle, $ext);
}

1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

