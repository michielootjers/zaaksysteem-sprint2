package Zaaksysteem::Controller::Zaak::Subcases;

use strict;
use warnings;
use Data::Dumper;
use parent 'Catalyst::Controller';


use constant SUBCASE_FIELDS => [qw/eigenaar_type kopieren_kenmerken ou_id role_id required/];



sub start_subcase : Chained('/zaak/base') : PathPart('start_subcase') : Args(2) {
    my ($self, $c, $zaaktype_relatie_id, $status) = @_;

    my $params = $c->req->params();
    $c->log->debug("params: " . Dumper $params);

    unless ($params->{update}) {
        $c->stash->{action} = '/zaak/' . $c->stash->{zaak}->nr .'/start_subcase/' . $zaaktype_relatie_id . '/' . $status . '/';
        $c->detach('edit');
    }

    
    my $zaaktype_relatie = $c->model('DB::ZaaktypeRelatie')->find($zaaktype_relatie_id);
    die "couldn't load zaaktype_relatie using id $zaaktype_relatie_id" unless($zaaktype_relatie);

    my $args = {
        aanvrager_type                  => $params->{relaties_eigenaar_type},
        actie_automatisch_behandelen    => $params->{relaties_automatisch_behandelen},
        actie_kopieren_kenmerken        => $params->{relaties_kopieren_kenmerken},
        add_days                        => '0',
        ou_id                           => $params->{relaties_ou_id},
        role_id                         => $params->{relaties_role_id},
        type_zaak                       => $zaaktype_relatie->relatie_type || 'deelzaak',
        zaaktype_id                     => $zaaktype_relatie->relatie_zaaktype_id->id,
    };

    my $extra_zaak;

    eval {
        $extra_zaak = $c->model('DB::Zaak')->create_relatie(
            $c->stash->{zaak},
            %$args
        );
    };

    if ($@) {
        my $msg = 'Zaakrelatie kon niet worden aangemaakt';

        if ($@ =~ /create_relatie: need zaak->behandelaar_object/) {
            $msg .= ': huidige zaak heeft geen behandelaar';
        }

        $msg .= '.';

        $c->push_flash_message($msg);
        $c->res->redirect('/zaak/' . $c->stash->{zaak}->id);
        $c->detach;
    }

    my $current_zaak    = $c->stash->{zaak};
    $c->stash->{zaak}   = $extra_zaak;

    delete($c->session->{_zaak_create});

    $c->forward('/zaak/handle_fase_acties', []);

    my $subcase         = $c->stash->{zaak};
    $c->stash->{zaak}   = $current_zaak;


    if($params->{relaties_required}) {
        $current_zaak->register_required_subcase({
            subcase_id              => $subcase->id,
            required                => $params->{relaties_required},
            parent_advance_results  => $params->{relaties_parent_advance_results},
        });
    }

    $c->res->redirect('/zaak/' . $c->stash->{zaak}->id);
}



sub edit : Chained('/zaak/base') : PathPart('subcase') : Args() {
    my ($self, $c, $zaaktype_relatie_id) = @_;

    my $zaaktype_relatie = $c->model('DB::ZaaktypeRelatie')->find($zaaktype_relatie_id);
    die "zaaktype_relatie_id with id $zaaktype_relatie_id not found" unless($zaaktype_relatie);

    my $relatie_params = { $zaaktype_relatie->get_columns };

    $c->log->debug("edit params: " . Dumper $relatie_params);
    $c->stash->{params} = $relatie_params;

    $c->stash->{submit_button} = 'Starten';    
    $c->stash->{template}  = 'beheer/zaaktypen/milestones/edit_relatie.tt';
    $c->stash->{nowrapper} = 1;
}


1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

