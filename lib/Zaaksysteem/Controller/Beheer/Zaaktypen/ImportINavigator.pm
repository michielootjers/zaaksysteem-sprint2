package Zaaksysteem::Controller::Beheer::Zaaktypen::ImportINavigator;
use Moose;
use namespace::autoclean;

use Hash::Merge::Simple qw( clone_merge );
use XML::Simple;

use Data::Dumper;
$Data::Dumper::Sortkeys = 1;

use Zaaksysteem::Constants;
use Archive::Extract;
use Clone qw(clone);
use XML::Dumper;
use Encode;

use constant INAVIGATOR_IMPORT_CONFIG => [
    {
        field => 'node.titel', 
        xpath => 'kernomschrijving',
        label => 'Naam zaaktype',
    },
    {
        field => 'node.code', 
        xpath => '@id', 
        label => 'Identificatie',
    },
    {
        field => 'definitie.procesbeschrijving',
        xpath => 'schemalocatie', 
        label => 'Procesbeschrijving',
    },
    {
        field => 'definitie.handelingsinitiator', 
        xpath => 'zaaktype-naam/structuur/handeling-initiator',     
        label => 'Handeling initiator',
        type  => 'handelinginitiator'
    },
    {
        field => 'node.properties.doel', 
        xpath => 'naam', 
        label => 'Doel',
    },
    {
        field => 'node.properties.archiefclassicatiecode', 
        xpath => 'basisarchiefcode', 
        label => 'Archiefclassificatiecode',
    },
    {
        field => 'node.properties.verantwoordelijke', 
        xpath => 'proceseigenaar', 
        label => 'Verantwoordelijke',
    },
    {
        field => 'node.properties.beroep_mogelijk', 
        xpath => 'beroep-mogelijk', 
        label => 'Bezwaar en beroep mogelijk',
        type  => 'checkbox',
    },
    {
        field => 'node.properties.bag', 
        xpath => 'bag', 
        label => 'BAG',
        type  => 'checkbox',
    },
    {
        field => 'node.properties.lex_silencio_positivo', 
        xpath => 'lex_silencio_positivo', 
        label => 'Lex silencio positivo',
        type  => 'checkbox',
    },
    {
        field => 'node.properties.wet_dwangsom', 
        xpath => 'wet_dwangsom', 
        label => 'Wet dwangsom',
        type  => 'checkbox',
    },
    {
        field => 'node.properties.wkpb', 
        xpath => 'wkpb', 
        label => 'WKPB',
        type  => 'checkbox',
    },
    {
        field => 'node.properties.publicatie', 
        xpath => 'publicatie-indicatie', 
        label => 'Publicatie',
        type  => 'checkbox'
    },
    {
        field => 'node.properties.verdagingstermijn',
        xpath => 'verdagingstermijn',
        label => 'Verdagingstermijn'
    },
    {
        field => 'node.properties.verantwoordingsrelatie', 
        xpath => 'zaaktype-iv3-categorie', 
        label => 'Verantwoordingsrelatie',
    },
    {
        field => 'node.zaaktype_trefwoorden', 
        xpath => 'lokale-trefwoorden', 
        label => 'Trefwoorden',
    },
    {
        field => 'node.zaaktype_omschrijving', 
        xpath => 'toelichting-proces', 
        label => 'Toelichting of omschrijving',
    },
    {
        field => 'definitie.grondslag', 
        xpath => 'grondslag/list/fields[1]/field[@naam="NAAM"][1]', 
        label => 'Wettelijke grondslag',
    },
    {
        field => 'node.properties.lokale_grondslag', 
        xpath => 'lokale-regelgeving', 
        label => 'Lokale grondslag',
    },
    {
        field => 'definitie.servicenorm', 
        xpath => 'wettelijke-afdoeningstermijn', 
        label => 'Doorlooptijd wettelijk',
    },
    {
        field => 'definitie.servicenorm_type', 
        xpath => 'wettelijke-afdoeningstermijn-eenheid', 
        label => 'Doorlooptijd wettelijk eenheid',
        type  => 'servicenorm_type',
    }
];



use constant INAVIGATOR_IMPORT_RESULTS_CONFIG => [
    {
        field => 'label', 
        xpath => 'naam', 
        label => 'Naam (omschrijving)',
    },
    {
        field => 'resultaat', 
        xpath => 'naam-model',
        label => 'Resultaattype-generiek',
        type  => 'resultaattype'
    },
    {
        field => 'selectielijst', 
        xpath => 'vernietigingsgrondslag', 
        label => 'Selectielijst',
    },
    {
        field => 'archiefnominatie', 
        xpath => 'waardering', 
        label => 'Archiefnominatie',
        type  => 'archiefnominatie',
    },
    {
        field => 'bewaartermijn', 
        xpath => 'bewaartermijn', 
        label => 'Bewaartermijn',
        type  => 'bewaartermijn'
    },
    {
        field => 'comments', 
        xpath => 'toelichting-bewaartermijn', 
        label => 'Toelichting',
    },
];


use constant INAVIGATOR_IMPORT_DOCUMENTS_CONFIG => [
    {
        field => 'label', 
        xpath => 'naam',
        label => 'Titel (in zaaktype)',
    },
    {
        field => 'naam', 
        xpath => 'naam-model',
        label => 'Documentnaam',
        type  => 'documentnaam'
    },
    {
        field => 'value_mandatory', 
        xpath => 'indossier',
        label => 'Verplicht',
        type  => 'checkbox'
    },
];
 
BEGIN {extends 'Catalyst::Controller'; }



sub inavigator : Chained('/') : PathPart('beheer/inavigator'): CaptureArgs(0) {
    my ($self, $c) = @_;


    $c->stash->{bib_cat} = $c->model('DB::BibliotheekCategorie')->search({  
        'system'    => { 'is' => undef },
        'pid'       => undef,
    }, {  
        order_by    => ['pid','naam']
    });

    $c->stash->{bibliotheek_kenmerk_rs} = $c->model('DB::BibliotheekKenmerken')->search({
        deleted => undef,
        value_type => 'file'
    }, {
        order_by => 'naam'
    });

    $c->stash->{INAVIGATOR_IMPORT_CONFIG} = INAVIGATOR_IMPORT_CONFIG;
    $c->stash->{INAVIGATOR_IMPORT_RESULTS_CONFIG} = INAVIGATOR_IMPORT_RESULTS_CONFIG;
    $c->stash->{INAVIGATOR_IMPORT_DOCUMENTS_CONFIG} = INAVIGATOR_IMPORT_DOCUMENTS_CONFIG;
}


sub get_xpath {
    my ($self, $c) = @_;

    my $filestore_row = $c->model('DB::Filestore')->find({
        uuid => $c->session->{inavigator_import_uuid},
    });

    my $disk_location = $filestore_row->ustore->getPath($filestore_row->uuid);

    return XML::XPath->new(filename => $disk_location);
}


sub view : Chained('inavigator') : PathPart('view') {
    my ( $self, $c ) = @_;

    eval {
        my $xp = $self->get_xpath($c);

        my $nodeset = $xp->find('/dsp/processen/proces');

        my $casetypes = [];    
        my $count = 1;
        foreach my $node ($nodeset->get_nodelist) {
            $c->forward('prepare_casetype', [{node => $node}]);
            push @$casetypes, $c->stash->{casetype};
            last if($count++ >= 100);
        }

        # make sure valid xml files that contain something completely different are not picked up§
        die "no casetypes in file" unless @$casetypes;

        $c->stash->{casetypes} = $casetypes;
    };
    if($@) {
        $c->res->redirect($c->uri_for('/beheer/object/import/0'));
        $c->push_flash_message('Ongeldig I-Navigator importbestand');
        $c->detach;
    }

    $c->stash->{template}  = 'beheer/zaaktypen/import/inavigator/import.tt';
}


sub documents : Chained('inavigator') : PathPart('documents') {
    my ($self, $c) = @_;

    my $params = $c->req->params();

    #$c->log->debug("params: " . Dumper $params);
    my $proces_id = $params->{proces_id};

    # no worky? just die - this is not called directly. so happy 500.
    my $xp = $self->get_xpath($c);
    my $nodeset = $xp->find('/dsp/processen/proces[@id="' . $proces_id. '"]');
 
    foreach my $node ($nodeset->get_nodelist) {
        $c->forward('prepare_casetype', [{node => $node}]);
    }

    $c->stash->{nowrapper} = 1;
    $c->stash->{template}  = 'beheer/zaaktypen/import/inavigator/documents.tt';
}


sub prepare_casetype :Private {
    my ($self, $c, $options) = @_;

    my $node = $options->{node}  or die "need node";

    my $casetype = $options->{casetype} = {};
    $options->{c} = $c;

    $self->prepare_casetype_attributes({ %$options, config => INAVIGATOR_IMPORT_CONFIG});

    my $existing_casetypes_rs = $c->model('DB::Zaaktype')->search({
        'zaaktype_node_id.code'     => $casetype->{'node.code'},
        'zaaktype_node_id.deleted'  => undef,
        'me.deleted'                => undef,
    }, {
        join => ['zaaktype_node_id']
    });

    if($existing_casetypes_rs->count == 1) {
        $casetype->{zaaktype_id} = $existing_casetypes_rs->first->id;
    }

    $self->prepare_casetype_results({%$options, config => INAVIGATOR_IMPORT_RESULTS_CONFIG});


    if($casetype->{zaaktype_id} || $c->req->params->{zaaktype_id}) {
        $self->prepare_casetype_documents({%$options, config => INAVIGATOR_IMPORT_DOCUMENTS_CONFIG});
        my $zaaktype_id = $c->req->params->{zaaktype_id} || $casetype->{zaaktype_id};
        $casetype->{phases} = $c->model('DB::Zaaktype')->find($zaaktype_id)->zaaktype_node_id->zaaktype_statussen->search({}, {order_by =>'me.status'});
    }


    $casetype->{existing_casetypes_rs} = $existing_casetypes_rs;

    $c->stash->{casetype} = $casetype;
}


sub prepare_casetype_results {
    my ($self, $options) = @_;

    my $casetype = $options->{casetype} or die "need casetype";

    my $nodes = $options->{node}->find('resultaattypen/resultaattype');
    my $config = INAVIGATOR_IMPORT_RESULTS_CONFIG;

    foreach my $node ($nodes->get_nodelist) {
        my $result = {
            map { 
                $_->{field} => $node->find($_->{xpath})->string_value
            } @$config
        };

        my $bewaartermijn           = $node->find('bewaartermijn')->string_value;
        my $bewaartermijn_eenheid   = $node->find('bewaartermijn-eenheid')->string_value;

        # yeah, needs attention.
        my $calc = {
            Werkdag => 1,
            Dag     => 1,
            Week    => 7,
            Maand   => 31,
            Jaar    => 365,
        };

        $result->{bewaartermijn} = $bewaartermijn * $calc->{$bewaartermijn_eenheid};
        push @{ $casetype->{results} ||= [] }, $result;
    }
}


sub prepare_casetype_documents {
    my ($self, $options) = @_;

    my $casetype    = $options->{casetype}  or die "need casetype";
    my $node        = $options->{node}      or die "need node";
    my $config      = $options->{config}    or die "need config";

    my $nodes = $node->find('documenttypen/documenttype');
    my $rs_bibliotheek_kenmerk = $options->{c}->model('DB::BibliotheekKenmerken')->search;

    my $zaaktype_id = $casetype->{zaaktype_id} ||= $options->{c}->req->param('zaaktype_id');

    my $zaaktype = $options->{c}->model('DB::Zaaktype')->find($zaaktype_id);

    my $rs_in_zaaktype = $zaaktype->zaaktype_node_id->zaaktype_kenmerken->search;

    foreach my $document_node ($nodes->get_nodelist) {
        my $document = {
            map { 
                $_->{field} => $document_node->find($_->{xpath})->string_value
            } @$config
        };

        my $row = $rs_bibliotheek_kenmerk->search({naam => $document->{label}})->first;
        if($row) {
            $document->{bibliotheek_kenmerken_id} = $row->id;
            my $zaaktype_kenmerk = $rs_in_zaaktype->search({bibliotheek_kenmerken_id => $row->id})->first;
            if($zaaktype_kenmerk) {
                $document->{fase} = $zaaktype_kenmerk->zaak_status_id->status;
                $document->{in_casetype} = 1;
            }
        }

        push @{ $casetype->{documents} ||= [] }, $document;
    }


}


sub prepare_casetype_attributes {
    my ($self, $options) = @_;

    my $casetype    = $options->{casetype}  or die "need casetype";
    my $node        = $options->{node}      or die "need node";
    my $config      = $options->{config}    or die "need config";

    foreach my $field (@$config) {
        my $xpath_query = $field->{xpath};

        eval {
            $casetype->{$field->{field}} = $node->find($xpath_query)->string_value;
        };

        if($@) {
            warn Dumper $field;
            warn("probrem: ". $@);
        }
    }

    # yeah, needs attention.
    my $calc = {
        Werkdag => 1,
        Dag     => 1,
        Week    => 7,
        Maand   => 31,
        Jaar    => 365,
    };

    my $servicenorm_type = $casetype->{'definitie.servicenorm_type'};
    if($servicenorm_type) {
        $casetype->{'definitie.servicenorm'} = $calc->{$servicenorm_type} * $casetype->{'definitie.servicenorm'};
    }
    
    my $verdagingstermijn_eenheid = $node->find('verdagingstermijn-eenheid')->string_value;

    if($verdagingstermijn_eenheid) {
        $casetype->{ 'node.properties.verdagingstermijn' } = $calc->{ $verdagingstermijn_eenheid } * $casetype->{ 'node.properties.verdagingstermijn'};
    }
}


sub set_error_missing {
    my ($self, $dv, $field) = @_;

    $dv->{success} = 0;
    $dv->{invalid} //= [];
    push @{ $dv->{missing} }, $field;
}


sub set_error_invalid {
    my ($self, $dv, $field) = @_;

    $dv->{success} = 0;
    $dv->{invalid} //= [];
    push @{ $dv->{invalid} }, $field;
}


sub validate : Private {
    my ($self, $c) = @_;

    my $params = $c->req->params();

    my $dv = {success => 1, msgs => {
        'node.titel' => 'Er is al een zaaktype met deze naam, kies een unieke naam'
    }};

    foreach my $required (qw/definitie.grondslag zaaktype_id node.titel/) {
        $self->set_error_missing($dv, $required) unless $params->{$required};
    }

    if($params->{action} eq 'create') {

        $self->set_error_missing($dv, 'zaaktype.bibliotheek_categorie_id') 
            unless $params->{'zaaktype.bibliotheek_categorie_id'};

        if($c->model('DB::Zaaktype')->search({
                'LOWER(zaaktype_node_id.titel)' => lc($params->{'node.titel'}),
                'me.deleted'                    => undef,
            },
            {
                join    => ['zaaktype_node_id']
            })->count
        ) {
            $self->set_error_invalid($dv, 'node.titel');
        }

    }

    $self->validate_documents($c, $dv);

    $c->zcvalidate($dv);
}


sub validate_documents {
    my ($self, $c, $dv) = @_;

    my $params = $c->req->params();

    foreach my $document_prefix ($c->req->param('document')) {

        my $document = {};

        foreach my $key (keys %$params) {
            if($key =~ m|^$document_prefix\.(.*)$|) {
                $document->{$1} = $params->{$key};
            }
        }

        if($document->{action} eq 'create') {
            if($document->{fase}) {
                $self->set_error_missing($dv, $document_prefix .'.bibliotheek_categorie_id') 
                    unless $document->{bibliotheek_categorie_id};
            }
        }

        $self->set_error_missing($dv, $document_prefix .'.label') unless $document->{label};

        my $kenmerk = $c->model('DB::BibliotheekKenmerken')->search({
            naam => $document->{ naam }
        })->first;

        if($kenmerk) {
            # Commented out because this check is non-functional in the UI
            #$self->set_error_invalid($dv, $document_prefix . '.naam');
        }
    }
}


sub import_inavigator : Chained('inavigator') : PathPart('import') :Args() {
    my ($self, $c) = @_;

    my $params = $c->req->params();
    #$c->log->debug("params: " . Dumper $params);

    $c->detach('validate') if $params->{do_validation};

    my $as_clone = 0;

    if($params->{action} eq 'create') {
        $as_clone = 1;
    }

    my $zaaktype = $c->model('Zaaktypen')->retrieve(
        id              => $params->{zaaktype_id},
        as_session      => 1,
        as_clone        => $as_clone,
    );

    if($params->{action} eq 'create') {
        $zaaktype->{node}->{titel} = $params->{'node.titel'};
        $zaaktype->{zaaktype}->{bibliotheek_categorie_id} = $params->{'zaaktype.bibliotheek_categorie_id'};
    }

    my $config = INAVIGATOR_IMPORT_CONFIG;
    foreach my $field (@$config) {
        my $fieldname = $field->{field};

        # yes, i did. generate perl: node.titel ==> {node}->{titel}
        my $expression = join "->", map { "{$_}" } split /\./, $fieldname;
        my $value = $params->{$fieldname} || '';

        # normalize to the ZTB standard
        if($field->{type} eq 'checkbox') {
            $value = $params->{$fieldname} ? 'Ja' : 'Nee';
        }
        my $perl = '$zaaktype->'.$expression."='".$value."'";
        #warn "perl: $perl\n";
        eval($perl);
    }

    eval {
        $c->model('DB')->schema->txn_do(sub {
            $self->import_documents($c, $zaaktype);

            $self->import_results($c, $zaaktype);

            $c->log->debug("committing zaaktype" . Dumper $zaaktype);

            ### Commit the shizzle
            $c->model('Zaaktypen')->commit_session(
                session => $zaaktype,
                commit_message => 'Updated by I-Navigator import' . localtime(),
            );

            #die('TRANSACTION BITTE');
        });
    };

    if ($@) {
        $c->log->error('Erreur: ' . $@);
        $c->stash->{json} = {'success' => 0};
    } else {
        delete($c->session->{zaaktypen}->{$params->{zaaktype_id}});
        $c->stash->{json} = {'success' => 1};
    }


    $c->forward('Zaaksysteem::View::JSONlegacy');

}


sub import_results {
    my ($self, $c, $zaaktype) = @_;

    my $params = $c->req->params();

    ### resultaten ###
    my $results = {};
    my $result_counter = 1;
    foreach my $result_prefix ($c->req->param('result')) {
        my $result = {};

        foreach my $key (keys %$params) {
            if($key =~ m|^$result_prefix\.(.*)$|) {
                $result->{$1} = $params->{$key};
            }
        }
        $results->{$result_counter} = $result;
        $result_counter++;

        warn "result: " . Dumper $result;
    }

    $self->update_results_in_zaaktype({zaaktype => $zaaktype, results => $results});
}



sub import_documents {
    my ($self, $c, $zaaktype) = @_;

    my $params = $c->req->params();

    # if not set, create new bibliotheek_kenmerk, use new id
    my $bibliotheek_kenmerken_model = $c->model('DB::BibliotheekKenmerken');

    foreach my $document_prefix ($c->req->param('document')) {
        my $document = {};

        foreach my $key (keys %$params) {
            if($key =~ m|^$document_prefix\.(.*)$|) {
                $document->{$1} = $params->{$key};
            }
        }

        foreach my $checkbox_field (qw/value_mandatory publish_public pip/) {
            if($document->{$checkbox_field}) {
                $document->{$checkbox_field} = 1;
            } else {
                $document->{$checkbox_field} = 0;                
            }
        }

        my $action = $document->{action};
        if($action eq 'create') {
            if($document->{fase}) {
                $document->{bibliotheek_kenmerken_id} ||= $bibliotheek_kenmerken_model->create({
                    naam                        => $document->{label},
                    value_type                  => 'file',
                    label                       => $document->{label},
                    magic_string                => $c->model('Bibliotheek::Kenmerken')->generate_magic_string($document->{label}),
                    bibliotheek_categorie_id    => $document->{bibliotheek_categorie_id},
                })->id;

                $self->add_document_to_fase({
                    zaaktype    => $zaaktype, 
                    document    => $document
                });             
           }
        } elsif($action eq 'update') {
            $self->update_document_in_zaaktype({
                zaaktype    => $zaaktype, 
                document    => $document
            });
        } elsif($action eq 'update_from_library') {
            if($document->{fase}) {

                $self->add_document_to_fase({
                    zaaktype    => $zaaktype, 
                    document    => $document
                });             
            }
        } else {
            die "incorrect action";
        }
    }
}

sub update_results_in_zaaktype {
    my ($self, $options) = @_;

    my $zaaktype    = $options->{zaaktype}  or die "need zaaktype";
    my $results     = $options->{results}   or die "need results";

    my $statussen = $zaaktype->{statussen};

    # loop through statussen
    my ($max_id) = sort { $b <=> $a } keys %$statussen; # reverse sort only get first = highest

    my $last_status = $statussen->{$max_id};
    $last_status->{elementen}->{resultaten} = $results;
}


sub update_document_in_zaaktype {
    my ($self, $options) = @_;

    my $zaaktype    = $options->{zaaktype}  or die "need zaaktype";
    my $document    = $options->{document}  or die "need document";

    my $statussen = $zaaktype->{statussen};

    # loop through statussen
    foreach my $status (values %$statussen) {
        my $kenmerken = $status->{elementen}->{kenmerken};

        # if kenmerk is encountered with same bibliotheek_kenmerken_id - make sweet love to it
        foreach my $kenmerk (values %$kenmerken) {
            if(
                $kenmerk->{bibliotheek_kenmerken_id} &&
                $kenmerk->{bibliotheek_kenmerken_id} eq $document->{bibliotheek_kenmerken_id}
            ) {
                $kenmerk->{label}           = $document->{label};
                $kenmerk->{value_mandatory} = $document->{value_mandatory};
                $kenmerk->{publish_public}  = $document->{publish_public};
                $kenmerk->{pip}             = $document->{pip};
            }
            warn "knermk" . Dumper $kenmerk;
        }

    }
}


sub add_document_to_fase {
    my ($self, $options) = @_;

    my $zaaktype    = $options->{zaaktype}  or die "need zaaktype";
    my $document    = $options->{document}  or die "need document";

    my $fase        = $document->{fase}     or die "need fase";
    my $kenmerken   = $zaaktype->{statussen}->{$fase}->{elementen}->{kenmerken};

    my ($max_id) = sort { $b <=> $a } keys %$kenmerken; # reverse sort only get first = highest

    my $new_id = $max_id + 1;

    $kenmerken->{$new_id} = {
        label                       => $document->{label},
        naam                        => $document->{label},
        value_mandatory             => $document->{value_mandatory},
        bibliotheek_kenmerken_id    => $document->{bibliotheek_kenmerken_id},
        publish_public              => $document->{publish_public},
        pip                         => $document->{pip},
    };
}             



=head2

Receive an uploaded file. Unzip this file in a temporary directory and store the
zip information in the session.

For IE, the uploaded file is send to this controller, for spanky browsers we receive
a UUID with which a Filestore obj can be obtained.

=cut

sub upload : Chained('inavigator') : PathPart('upload') {
    my ( $self, $c ) = @_;

    delete $c->session->{inavigator_import_uuid};
     
    my $params = $c->req->params();

    my $uuid = $params->{filestore_uuid};
    unless($uuid) {
        # IE
        $c->forward('/upload/index');
        
        $uuid = $c->stash->{uuid};
    }

    die "need uuid" unless $uuid;

    $c->session->{inavigator_import_uuid} = $uuid;

    if($@) {
        $c->res->redirect($c->uri_for('/beheer/object/import/0'));
        $c->push_flash_message('Ongeldig Zaaktype importbestand');
        $c->detach;
    }

    $c->res->redirect($c->uri_for('/beheer/inavigator/view'));
    $c->detach();
}


sub flush : Chained('inavigator') : PathPart('flush') {
    my ($self, $c) = @_;

    delete $c->session->{inavigator_import_uuid};

    $c->res->redirect($c->uri_for('/beheer/object/import/0'));
    $c->detach;
}



__PACKAGE__->meta->make_immutable;


=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

