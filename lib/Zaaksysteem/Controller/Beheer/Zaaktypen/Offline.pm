package Zaaksysteem::Controller::Beheer::Zaaktypen::Offline;
use Moose;
use namespace::autoclean;

use Hash::Merge::Simple qw( clone_merge );
use XML::Simple;

use Data::Dumper;
use Zaaksysteem::Constants;


BEGIN {extends 'Catalyst::Controller'; }

use constant ZAAKTYPEN              => 'zaaktypen';
use constant ZAAKTYPEN_MODEL        => 'DB::Zaaktype';
use constant CATEGORIES_DB          => 'DB::BibliotheekCategorie';




sub base : Chained('/') : PathPart('beheer/zaaktypen'): CaptureArgs(1) {
    my ( $self, $c, $zaaktype_id ) = @_;

    $c->stash->{zaaktype_id} = $zaaktype_id;
    $c->stash->{nowrapper} = 1;

    my $zaaktype = $c->model('DB::Zaaktype')->find($c->stash->{zaaktype_id});
    $c->stash->{zaaktype} = $zaaktype;
    $c->stash->{titel} = $zaaktype->zaaktype_node_id->titel;
}


sub online : Chained('base') : PathPart('online') {
    my ( $self, $c ) = @_;

	my $confirm = $c->req->param('confirmed') || '';
	
	unless($confirm) {
        $c->stash->{confirmation}->{message} =
            'Weet u zeker dat u zaaktype "'
            . $c->stash->{titel} . '"  online wilt zetten?';

        $c->stash->{confirmation}->{type}    = 'yesno';
        $c->stash->{confirmation}->{uri}     = $c->req->uri;
        $c->stash->{confirmation}->{commit_message} = 1;

        $c->forward('/page/confirmation');
        $c->detach;
    } else {
    
    	$c->stash->{zaaktype}->active(1);
    	$c->stash->{zaaktype}->update;

        $c->model('DB::Logging')->trigger('casetype/publish', {
            component => LOGGING_COMPONENT_ZAAKTYPE,
            component_id => $c->stash->{ zaaktype }->id,
            data => {
                casetype_id => $c->stash->{ zaaktype }->id,
                reason => $c->req->param('commit_message')
            }
        });

        my $bibliotheek_categorie_id = $c->stash->{zaaktype}->bibliotheek_categorie_id->id;
		$c->res->redirect($c->uri_for('/beheer/bibliotheek/'.$bibliotheek_categorie_id));
		$c->detach;
    }
}


sub offline : Chained('base') : PathPart('offline') {
    my ( $self, $c ) = @_;

	my $confirm = $c->req->param('confirmed') || '';
	
	unless($confirm) {
        $c->stash->{confirmation}->{message} =
            'Weet u zeker dat u zaaktype "'
            . $c->stash->{titel} . '"  offline wilt zetten?';

        $c->stash->{confirmation}->{type}    = 'yesno';
        $c->stash->{confirmation}->{commit_message} = 1;
        $c->stash->{confirmation}->{uri}     = $c->req->uri;

        $c->forward('/page/confirmation');
        $c->detach;
    } else {
    
    	$c->stash->{zaaktype}->active(0);
    	$c->stash->{zaaktype}->update;

        $c->model('DB::Logging')->trigger('casetype/unpublish', {
            component => LOGGING_COMPONENT_ZAAKTYPE,
            component_id => $c->stash->{ zaaktype }->id,
            data => {
                casetype_id => $c->stash->{ zaaktype }->id,
                reason => $c->req->param('commit_message')
            }
        });
    	
        my $bibliotheek_categorie_id = $c->stash->{zaaktype}->bibliotheek_categorie_id->id;
		$c->res->redirect($c->uri_for('/beheer/bibliotheek/'.$bibliotheek_categorie_id));
		$c->detach;
    }
}

__PACKAGE__->meta->make_immutable;


__END__

sub activate : Chained('base') : PathPart('version/activate') : Args() {
    my ( $self, $c, $zaaktype_node_id ) = @_;
 
    my $zaaktype_row = $c->model("DB::Zaaktype")->find($c->stash->{zaaktype_id});
    $zaaktype_row->zaaktype_node_id($zaaktype_node_id);
    $zaaktype_row->update();
    $c->stash->{nowrapper} = 1;
    $c->stash->{message} = 'Het zaaktype is ingesteld op versie ' .$zaaktype_node_id;
    $c->forward("version");
}



