package Zaaksysteem::Controller::Beheer::WOZ;
use Moose;
use namespace::autoclean;

use Data::Dumper;

BEGIN {extends 'Catalyst::Controller'; }

sub index :Path :Args(0) {
    my ( $self, $c, $updated ) = @_;
  
    unless($c->config->{customer}->{start_config}->{woz_info_on_pip}) {
        $c->res->redirect("/");
        $c->detach();
    }  

    my $settings = $c->model('DB::Settings')->filter({
        filter => 'woz_'
    });

    foreach my $setting (@$settings) {
        $c->stash->{    
            $setting->key
        } = $setting->value;
    }

    $c->stash->{dagobert_editor} = 1;
    $c->stash->{template}   = 'beheer/import/woz/admin.tt';
}


sub updated : Local {
    my ($self, $c) = @_;

    unless($c->config->{customer}->{start_config}->{woz_info_on_pip}) {
        $c->res->redirect("/");
        $c->detach();
    }  
    
    $c->stash->{updated} = 1;

    $c->forward('index');    
}


sub update : Local {
    my ($self, $c) = @_;    

    unless($c->config->{customer}->{start_config}->{woz_info_on_pip}) {
        $c->res->redirect("/");
        $c->detach();
    }  

    my $params = $c->req->params();

    if($params->{filestore_uuid}) {
        my $filestore_uuid = $params->{filestore_uuid};
        
        my $file = $c->model('DB::Filestore')->find({
            uuid => $filestore_uuid,
        });
        
        die "can't find uploaded file, aborting" unless $file;
        
        eval {
            $c->model('DB::WozObjects')->import_woz({
                file_path   => $c->config->{files} . "/filestore/" . $file->id,
                tmp_dir     => $c->config->{files} . "/tmp/"
            });
    
            $c->model('DB::Logging')->trigger('woz/import/upload', {
                component => 'woz_objects',
                data => {
                    file_id => $file->id,
                    file_uuid => $filestore_uuid
                }
            });
        };

        if($@) {
            $c->stash->{error} = "Error during WOZ import: " . $@;
            $c->stash->{template}   = 'beheer/import/woz/admin.tt';
            $c->detach();
        }
       
        $file->remove_file({
            files_dir => $c->config->{files},
        });

        $file->delete();
    }

    my $settings_rs = $c->model('DB::Settings');
    foreach my $param (keys %$params) {
        if($param =~ m|^settings_(.*)|) {
            my $key = $1;
                            
            $settings_rs->store({
                key     => $key, 
                value   => $params->{$param}
            });
        }
    }

    $c->model('DB::Logging')->trigger('woz/update', {
        component => 'woz_objects'
    });

    $c->res->redirect('/beheer/woz/updated');
    $c->detach();
}


__PACKAGE__->meta->make_immutable;


=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

