package Zaaksysteem::Controller::Beheer::Bibliotheek::Sjablonen;

use strict;
use warnings;
use parent 'Catalyst::Controller';
use File::stat;

use Data::Dumper;

use Zaaksysteem::Constants qw/ZAAKSYSTEEM_CONSTANTS LOGGING_COMPONENT_SJABLOON/;

use constant SJABLONEN              => 'sjablonen';
use constant SJABLONEN_MODEL        => 'Bibliotheek::Sjablonen';
use constant SJABLONEN_DB           => 'DB::BibliotheekSjablonen';
use constant CATEGORIES_DB          => 'DB::BibliotheekCategorie';


sub base : Chained('/') : PathPart('beheer/bibliotheek/sjablonen'): CaptureArgs(2) {
    my ( $self, $c, $bibliotheek_categorie_id, $id ) = @_;

    if($bibliotheek_categorie_id && $bibliotheek_categorie_id =~ m|^\d+$|) {
        $c->stash->{categorie} = $c->model('DB::BibliotheekCategorie')->find($bibliotheek_categorie_id);
    }

    $c->assert_any_user_permission('beheer');

    $c->stash->{bib_type}   = SJABLONEN;

    if ($id) {
        my $entry = $c->model(SJABLONEN_DB)->find($id);

        unless($entry) {
            $c->res->redirect($c->uri_for('/beheer/bibliotheek/' . $bibliotheek_categorie_id));
            $c->detach;
        }

        $c->stash->{ bib_entry } = $entry;

        $c->add_trail(
            {
                uri     => $c->uri_for('/beheer/bibliotheek/'
                    . $bibliotheek_categorie_id
                ),
                label   => 'Categorie: ' . $c->stash->{categorie}->naam,
            }
        ) if ($c->stash->{categorie});

        $c->add_trail(
            {
                uri     => $c->uri_for('/beheer/bibliotheek/'
                    . $bibliotheek_categorie_id . '/'
                    . $id
                ),
                label   =>  'Sjabloon: ' . $c->stash->{bib_entry}->naam,
            }
        );

    } else {
        $c->stash->{bib_new}    = 1;
    }
}



sub download : Regex('^beheer/bibliotheek/sjablonen/download/(\d+)') {
    my ($self, $c) = @_;

    # Allow the download?
    $c->assert_any_user_permission('beheer');

    my ($filestore_id) = @{$c->req->captures};
    my ($filestore)    = $c->model('DB::Filestore')->search({id => $filestore_id});
    if (!$filestore) {
        $c->log->debug("Filestore entry with id $filestore_id not found.");
        return;
    }
    my $name      = $filestore->original_name;
    my $file_path = $filestore->ustore->getPath($filestore->uuid);
    my $stat      = stat($file_path);

    $c->res->headers->header(
        'Content-Disposition', "attachment; filename=\"$name\"",
    );

    $c->log->debug(
        'Serving static file: ' . $name
        . ' with filetype: ' . $c->res->content_type
    );

    $c->res->content_length($stat->size);
    $c->serve_static_file($file_path);

    $c->res->headers->content_length($stat->size);
    $c->res->headers->content_type($filestore->mimetype);
    $c->res->content_type($filestore->mimetype);
}


sub view : Chained('base'): PathPart(''): Args(0) {
    my ( $self, $c) = @_;

    $c->stash->{template} = 'beheer/bibliotheek/sjablonen/view.tt';
}

{
    Zaaksysteem->register_profile(
        method  => 'bewerken',
        profile =>
            'Zaaksysteem::Model::Bibliotheek::Sjablonen::bewerken',
    );

    sub bewerken : Chained('base'): PathPart('bewerken'): Args() {
        my ( $self, $c ) = @_;
        my ($dv);

        if ($c->stash->{bib_new}) {
            $c->stash->{bib_id} = 0;
        } else {
            $c->stash->{bib_id} = $c->stash->{bib_entry}->id;
        }

        if ($c->stash->{categorie}) {
            $c->stash->{categorie_id} =
                $c->stash->{categorie}->id;
        }

        $c->stash->{bib_cat}        = $c->model(CATEGORIES_DB)->search(
            {
                'system'    => { 'is' => undef },
                'pid'       => undef,
            },
            {
                order_by    => ['pid','naam']
            }
        );

        my $requested_with = $c->req->header("x-requested-with") || '';

        ### Validation, PROUDLEVEL=7
        if ($c->req->params->{update}) {
            $c->stash->{categorie_id} =
                $c->req->params->{bibliotheek_categorie_id};
            my $validated = 0;

            
            ### Default validation
            if ($dv = $c->zvalidate) {
                if (
                    $c->stash->{bib_new} &&
                    $c->model(SJABLONEN_MODEL)->sjabloon_exists(
                        'naam'  => $c->req->params->{naam}
                    )
                ) {
                    $c->zcvalidate({ invalid => ['naam']});
                } else {
                    $validated = 1;
                }
            }

            if (
                !$validated || $c->req->is_xhr &&
                exists($c->req->params->{do_validation})
            ) {
                $c->detach;
            }

            ### Let's work our magic on the bibliotheek
            my $options = $dv->valid;


            if (
                my $sjabloon = $c->model(SJABLONEN_MODEL)->bewerken(
                    $options
                )
            ) {
                $c->model('DB::Logging')->trigger($c->stash->{ bib_new } ? 'template/create' : 'template/update', {
                    component => LOGGING_COMPONENT_SJABLOON,
                    component_id => $sjabloon->id,
                    data => {
                        template_id => $sjabloon->id,
                        reason => $options->{ commit_message }
                    }
                });

                if ($c->req->params->{json_response}) {
                    $c->stash->{json} = {
                        'id'    => $sjabloon->id
                    };
                    $c->forward('Zaaksysteem::View::JSONlegacy');
                    $c->detach;
                }

                $c->flash->{result} = 'Sjabloon succesvol opgeslagen';
            }
        }

        if ($c->req->is_xhr) {
            if ($c->stash->{bib_id}) {
                $c->stash->{bib_entry} = $c->model(SJABLONEN_MODEL)->retrieve(
                    id  => $c->stash->{bib_id}
                );
            }

            $c->stash->{template} =
                'beheer/bibliotheek/sjablonen/edit.tt';

            $c->detach;
        }

        $c->res->redirect(
            $c->uri_for(
                '/beheer/bibliotheek/'
                . $c->stash->{categorie_id}
            )
        );
        $c->detach;
    }
}

sub verwijderen : Chained('base'): PathPart('verwijderen'): Args() {
    my ( $self, $c )    = @_;
    my $entry           = $c->stash->{bib_entry};

    return unless $entry;

    ### Confirmed
    my $flag_only = 0;
    if (
        $entry->zaaktype_sjablonens->count
    ) {
        ### in depth search
        my $used_in_zaaktype_sjablonen = $entry->zaaktype_sjablonens->search;
        my $notused = 1;
        while (
            $notused &&
            (my $zt_sjabloon = $used_in_zaaktype_sjablonen->next)
        ) {
            if (
                $zt_sjabloon->zaaktype_node_id->id eq
                $zt_sjabloon->zaaktype_node_id->zaaktype_id->zaaktype_node_id->id &&
                !$zt_sjabloon->zaaktype_node_id->zaaktype_id->deleted
            ) {
                $c->stash->{confirmation}->{message} =
                    'Helaas, dit sjabloon is in gebruik door een of meerdere actieve zaaktypen.';
                $notused=0;
                next;
            }

            ### Ok: Er zijn alleen nog verwijderde zaaktypen, is er een zaak
            ### ooit aan gekoppeld?
            if ($zt_sjabloon->zaaktype_node_id->zaaks->count) {
                ### En is minstens 1 zaak _niet_ vernietigd
                if ($zt_sjabloon->zaaktype_node_id->zaaks->search(
                        { status => { '!=' => 'deleted' }}
                    )->count
                ) {
                    $c->log->debug('Vond een actieve zaak met dit sjabloon');
                    $c->stash->{confirmation}->{message} =
                        'Helaas, dit sjabloon is in gebruik door een of meerdere zaken.';
                    $notused = 0;
                    next;
                } else {
                    $flag_only = 1;
                }
            }

            ### Ok, looks like it is not used, er is geen actieve zaaktype
            ### en de inactieve zaaktypen hebben allemaal geen zaken gekoppeld
            ### gehad... Free to wipe, notused=1
        }
        if (!$notused) {
            $c->stash->{confirmation}->{msgonly}    = '1';

            ### Msg
            $c->detach('/page/confirmation');
        }
    }

    ### Post
    if ( $c->req->params->{confirmed}) {
        $c->model('DB::Logging')->trigger('template/remove', {
            component => LOGGING_COMPONENT_SJABLOON,
            component_id => $entry->id,
            data => {
                template_id => $entry->id,
                reason => $c->req->param('commit_message')
            }
        });

        if ($flag_only) {
            $entry->deleted(DateTime->now());
            $entry->update;

            $c->log->debug(
                'Sjabloon ' . $entry->id . ' verwijderd dmv flag'
            );
        } else {
            ### Do not forget to delete magic strings
            $entry->bibliotheek_sjablonen_magic_strings->delete;
            $entry->delete;
            $c->log->debug(
                'Sjabloon ' . $entry->id . ' verwijderd'
            );
        }

        ### Msg
        $c->flash->{result} = 'Sjabloon succesvol verwijderd.';
        $c->res->redirect(
            $c->uri_for(
                '/beheer/bibliotheek/'
                . $entry->bibliotheek_categorie_id->id
            )
        );

        $c->detach;
        return;
    }


    $c->stash->{confirmation}->{message}    =
        'Weet u zeker dat u dit sjabloon wilt verwijderen?'
        . ' Deze actie kan niet ongedaan gemaakt worden. Maar geen zorgen, dit
        sjabloon is niet in gebruik door een zaaktype';


    $c->stash->{confirmation}->{commit_message} = 1;
    $c->stash->{confirmation}->{type}       = 'yesno';

    $c->stash->{confirmation}->{uri}     = $c->uri_for(
                            '/beheer/bibliotheek/' . $c->stash->{bib_type} . '/'
                            . $entry->bibliotheek_categorie_id->id . '/'
                            . $entry->id . '/verwijderen'
        );
    $c->forward('/page/confirmation');
    $c->detach;
}



sub search
    : Chained('/beheer/bibliotheek/base')
    : PathPart('sjablonen/search')
    : Args()
{
    my ( $self, $c )        = @_;

    return unless ($c->req->is_xhr);

    $c->stash->{bib_type}   = SJABLONEN;

    $c->stash->{bib_cat}        = $c->model(CATEGORIES_DB)->search(
        {
            'pid'       => undef,
        },
        {
            order_by    => 'naam'
        }
    );

    if ($c->req->params->{search}) {
        ### Return json response with results
        my $json = [];

        my %search_query    = ();
        $search_query{'lower(naam)'} = {
            'like' => '%' .  lc($c->req->params->{naam}) . '%'
        } if $c->req->params->{naam};
        $search_query{bibliotheek_categorie_id} =
            $c->req->params->{bibliotheek_categorie_id}
                if $c->req->params->{bibliotheek_categorie_id};

        $search_query{'deleted'}    = undef;

        my $kenmerken = $c->model(SJABLONEN_DB)->search(
            \%search_query,
            {
                order_by    => 'naam'
            }
        );

        while (my $kenmerk = $kenmerken->next) {
            push(@{ $json },
                {
                    'naam'                  => $kenmerk->naam,
                    'categorie'             => $kenmerk->bibliotheek_categorie_id->naam,
                    'id'                    => $kenmerk->id,
                }
            );
        }

        $c->stash->{json} = $json;
        $c->detach('Zaaksysteem::View::JSONlegacy');
    }

    $c->stash->{template} = 'widgets/beheer/bibliotheek/search.tt';
}


1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

