package Zaaksysteem::Controller::Upload;

use Moose;

use Data::Dumper;

BEGIN { extends 'Catalyst::Controller'; }

sub index : Path : Args(0) {
    my ($self, $c) = @_;
 
    $c->forward('upload');

    $c->stash->{json} = $c->stash->{upload_result};
    $c->detach('Zaaksysteem::View::JSONlegacy');
}


sub upload : Local {
    my ($self, $c) = @_;

    my $uploaded_files = {};
    my $result = {};
    my $filestore = $c->model('DB::Filestore');

    for my $upload (map { $c->req->upload($_) } keys %{ $c->req->uploads }) {
        my $filestore_obj = $filestore->filestore_create({
            original_name    => $upload->filename,
            file_path        => $upload->tempname,
            ignore_extension => 1,
        });               
        
        if($filestore_obj && $filestore_obj->id) {
            $uploaded_files->{
                $filestore_obj->id
            } = $upload;

            $result->{filename}     = $filestore_obj->original_name;
            $result->{uuid}         = $filestore_obj->uuid;
        }
        $c->stash->{uuid} = $filestore_obj->uuid;
    }

    $c->stash->{upload_result} = $result;
}

1;
