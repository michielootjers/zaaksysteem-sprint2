package Zaaksysteem::Controller::KCC::Call;

use Moose;
use namespace::autoclean;

use Zaaksysteem::Exception;
use File::Temp;
use Data::Dumper;
use XML::XPath;
use Date::Parse;
use Params::Profile;
use JSON;

BEGIN { extends 'Zaaksysteem::Controller'; }

=head1 NAME

Zaaksysteem::Controller::KCC::Call - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 base

Base action setting up required stuff for incoming callbacks from VoiceWorks

Since Voiceworks sends us a non-standard HTTP POST, we do some manual fixing
of the incoming POST body.

=cut

sub base : Chained('/') : PathPart('kcc/call') : CaptureArgs(0) {
    my ( $self, $c ) = @_;

    my $body = "";

    # Fix silly call made by VoiceWorks, incorrectly set content-type causes problems
    for my $param (keys %{$c->req->params}) {
        $body .= sprintf("%s=%s", $param, $c->req->param($param));
    }

    $c->res->content_type('application/json');
    $c->res->body(to_json({ success => 1 }));

    my $xpath = XML::XPath->new(xml => $body);

    my $request_time = DateTime->now(time_zone => 'Europe/Amsterdam');
    my $pbx_time = DateTime->from_epoch(
        epoch => str2time($xpath->find('/message/messageheader/msgdatetime')->string_value)
    );

    $c->log->debug(sprintf(
        'Difference between PBX and request time: %d seconds',
        $pbx_time->subtract_datetime_absolute($request_time)->in_units('seconds')
    ));

    # TODO figure out timezone difference
    # and detach if difference between requestime and pbx time is too great

    $c->stash({
        message => $body,
        xpath => $xpath
    });
}

=head2 create

Main entry for a PBX to register a call in our system

=cut

sub create : Chained('base') : PathPart('create') : Args(0) {
    my ($self, $c) = @_;

    my $xpath = $c->stash->{ xpath };

    unless($xpath->find('/message/messagebody/clip')->string_value =~ /\d{10}/) {
        $c->detach;

        return;
    }

    if ($xpath->find('/message/messagebody/extension')->string_value ne '202')
    {
        return;
    }

    my $event = $c->model('DB::Logging')->new({ event_type => 'kcc/call' });

    $event->data->{ phonenumber } = $xpath->find('/message/messagebody/clip')->string_value;
    $event->data->{ extension } = $xpath->find('/message/messagebody/extension')->string_value;

    $event->insert;

    $c->detach;
}

=head2 register

This controller is the most simplistic way of registering an incoming phonecall.

Hit this URL with a POST request and a new KCC call event will be inserted.

=cut

Params::Profile->register_profile(
    method => 'register',
    profile => {
        required => [qw[phonenumber extension]],
        constraint_methods => {
            phonenumber => qr[^\+?\d{10,}$]
        }
    }
);

sub register : Chained('/') : PathPart('kcc/call/register') : Args(0) {
    my ($self, $c) = @_;

    my $ip = $c->model('DB::Config')->get('pbx_ip');

    unless($ip) {
        throw('zaaksysteem/configuration', 'No PBX host IP configured. Check your Zaaksysteem configuration.');
    }

    # Strip IPv6 crud
    my ($client_ip) = $c->req->address =~ qr[((\d{1,3}\.?){4,})];

    unless($client_ip eq $ip) {
        throw('request/client', 'Invalid client IP, have you registered your PBX host IP?');
    }

    assert_profile($c->req->params);

    unless($c->req->method eq 'POST' || 1) {
        throw('request/method', 'Invalid request method, should be POST.');
    }

    my $event = $c->model('DB::Logging')->trigger('kcc/call', {
        component => 'kcc',
        data => {
            phonenumber => $c->req->param('phonenumber'),
            extension => $c->req->param('extension')
        }
    });

    # Override default created_by
    $event->created_by(undef);
    $event->update;

    $c->stash->{ json } = { $event->get_columns };
    $c->detach('Zaaksysteem::View::JSON');
}


=head2 accept

This controller allows a client to either accept or reject an incoming call.
It should (and can) only be called once. Since this call modifies the state
of Zaaksysteem, it can only be done with the POST request method.

=head3 URL construction

B</kcc/call/accept>

=head3 Parameters

=over 4

=item event_id

The ID of the event that represents the call to be accepted/rejected.

=item accept

Field containing either 'true' or 'false', indicating acceptance or rejection.

=back

=head3 Response body

If the request validates, the response should be an HTTP 200, the body a simple
JSON string C<{ "success": true }>.
In case of validation error, a response as described in
L<View::JSON::Error|Zaaksysteem::View::JSON::Error> should be returned.

=cut

Params::Profile->register_profile(
    method => 'accept',
    profile => {
        required => [qw[event_id accept]],
        constraint_methods => {
            event_id => qr[^\d+$]
        }
    }
);

sub accept : JSON : Chained('/') : PathPart('kcc/call/accept') : Args(0) {
    my ($self, $c) = @_;

    assert_profile($c->req->params);

    unless($c->req->method eq 'POST') {
        throw('request/method', "Invalid request method, should be POST.");
    }

    my $event = $c->model('DB::Logging')->find($c->req->param('event_id'));

    unless($event) {
        throw('event/not_found', "Invalid `event_id` supplied, event could not be found in DB.");
    }

    if(exists $event->data->{ accepted } && 0) {
        throw('event/already_accepted', "Invalid request, a call cannot be accepted or rejected more than once.");
    }

    $event->data->{ accepted } = $c->req->param('accept') eq '1';
    $event->update;

    $c->res->content_type('application/json');
    $c->res->body('{ "success": true }');
    $c->detach;
}

__PACKAGE__->meta->make_immutable;

