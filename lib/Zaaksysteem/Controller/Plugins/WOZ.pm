package Zaaksysteem::Controller::Plugins::WOZ;

use strict;
use warnings;
use parent 'Catalyst::Controller';

use Data::Dumper;
use LWP::UserAgent;
use LWP::Simple;

use XML::LibXML;
use Crypt::OpenSSL::RSA;
use MIME::Base64;
use POSIX qw/strftime/;
use URI::Escape;
use XML::Simple;
use XML::Parser;

sub woz_object_picture : Chained('/') :PathPart('woz_object_picture') : Args() {
    my ($self, $c) = @_;
    
    my $params = $c->req->params();
    
    my $config = $c->config->{customer}->{start_config}->{woz_info};

    my $woz_object_url = 
        'https://atlas.cyclomedia.com/PanoramaRendering/RenderbyAddress/NL/' . 
        $params->{postcode} . 
        '%20' .
        $params->{huisnummer} . 
        '/?format=xml&apikey=' .
        $config->{key};

    my $ua = LWP::UserAgent->new;
    my $req = HTTP::Request->new(GET => $woz_object_url);

    $req->authorization_basic(
        $config->{username}, 
        $config->{password}
    );
    my $response = $ua->request($req);

    unless($response->code() eq '200') {
        die "couldn't get response from cyclomedia: " . $response->content;
    }
    
#    $c->log->debug("response: " .$response->content);    

    my $parser = new XML::Parser( Style => 'Tree' );
    my $tree = $parser->parse(
        $response->content()
    );

    my $attributes = $tree->[1]->[0]
        or die "unexpected format in xml";

#    $c->log->debug("response parsed: " .Dumper $attributes);

    my $recording_id = $attributes->{'recording-id'}
        or die "recording-id not present in xml";
    
    my $yaw = $attributes->{'yaw'}
        or die "yaw not present in xml";

    my $tid = $self->_generate_tid($c, {
        image_id    => $recording_id,
        config      => $config,
    });

    $c->stash->{json} = {
        tid             => $tid,
        woz_object_url  => $woz_object_url,
        recording_id    => $recording_id,
        yaw             => $yaw,
        postcode        => $params->{postcode},
        huisnummer      => $params->{huisnummer},
    };
    $c->detach('Zaaksysteem::View::JSONlegacy');
}


sub woz_object_picture_popup : Chained('/') :PathPart('woz_object_picture_popup') : Args() {
    my ($self, $c) = @_;

    my $params                  = $c->req->params();
    $c->log->debug("picpit: " . Dumper $params);

    $c->stash->{yaw}            = $params->{yaw};
    $c->stash->{tid}            = uri_escape($params->{tid});
    $c->stash->{recording_id}   = $params->{recording_id};
    $c->stash->{postcode}       = $params->{postcode};
    $c->stash->{huisnummer}     = $params->{huisnummer};
    
    $c->stash->{nowrapper}      = 1;
    $c->stash->{template}       = "plugins/woz/globespotter.tt";
}

        
sub _generate_tid {
    my ($self, $c, $opts) = @_;
    
    my $image_id = $opts->{image_id} or die "need image_id";
    my $config   = $opts->{config}   or die "need config";

    my $account_id = '5997';
    my $issued_datetime = strftime "%Y-%m-%d %H:%M:%SZ", gmtime;

    my $tid = 'X' . $account_id . '&' . $image_id . '&' . $issued_datetime;
    
#    $c->log->debug("tid string: " . $tid);

    open FILE, $config->{private_key}
        or die "unable to read private key: " . $config->{private_key} . ": " . $!;

    my $private_key = join "", <FILE>;
    close FILE;

    my $rsa_priv = Crypt::OpenSSL::RSA->new_private_key($private_key);

    my $signature = encode_base64(
        $rsa_priv->sign($tid),
        ''
    );

#    $c->log->debug("signature: [$signature]");

    $tid .= '&' . $signature;
#    $c->log->debug("complete tid: [$tid]");

    return $tid;
}


1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

