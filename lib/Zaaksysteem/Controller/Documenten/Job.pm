package Zaaksysteem::Controller::Documenten::Job;

use strict;
use warnings;
use parent 'Catalyst::Controller';

use Zaaksysteem::Constants;
use Zaaksysteem::Profiles;

=head1 NAME

Zaaksysteem::Controller::Documenten::Notitie - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut

sub base : Chained('/documenten/base') : PathPart('job') : CaptureArgs(0) {
    my ($self, $c) = @_;

}

sub get : Private {
    my ($self, $c) = @_;

    $c->stash->{jobs_map}   = JOBS_INFORMATION_MAP->{
        $c->stash->{document}->job_id->task_context . '::'
        . $c->stash->{document}->job_id->task
    };

    $c->log->debug($c->stash->{document}->job_id->task_context
         . '::'
         . $c->stash->{document}->job_id->task
    );

    if ($c->req->is_xhr) {
        $c->stash->{nowrapper}  = 1;
    }
    $c->stash->{template}   = 'widgets/documenten/job/view.tt';
}

sub accepteer : Private {
    my ($self, $c) = @_;

    if ($c->stash->{document}->job_id->accept) {
        $c->flash->{result} = 'Actie succesvol geaccepteerd en uitgevoerd';
    } else {
        $c->flash->{result} = 'Helaas, actie kon niet uitgevoerd worden';
    }

    $c->res->redirect($c->req->referer);
}

sub weiger : Private {
    my ($self, $c) = @_;

    $c->stash->{zaak}       = $c->stash->{document}->zaak_id;

    $c->stash->{jobs_map}   = JOBS_INFORMATION_MAP->{
        $c->stash->{document}->job_id->task_context . '::'
        . $c->stash->{document}->job_id->task
    };

    if (
        my $dv = $c->forward('/page/dialog', [{
            validatie       => {
                required    => [qw/omschrijving/],
                msgs        => PARAMS_PROFILE_MESSAGES_SUB,
            },
            permissions     => [qw/zaak_edit zaak_beheer/],
            template        => 'widgets/documenten/job/weiger.tt',
            complete_url    => $c->req->referer,
        }])
    ) {
        $c->stash->{document}->job_id->deny(
            {
                'omschrijving'  => $dv->valid('omschrijving')
            }
        );
    }
}


1;
