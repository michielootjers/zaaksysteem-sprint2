package Zaaksysteem::Controller::Documenten::Notitie;

use strict;
use warnings;
use Data::Dumper;

use parent 'Catalyst::Controller';

use Zaaksysteem::Profiles;

=head1 NAME

Zaaksysteem::Controller::Documenten::Notitie - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut

sub base : Chained('/documenten/base') : PathPart('notitie') : CaptureArgs(0) {
    my ($self, $c) = @_;
}

sub bewerken : Chained('base'): PathPart('bewerken') {
    my ($self, $c) = @_;

    my $profile = PROFILE_NOTITIE_CREATE;

    if ($c->stash->{document}) {
        $profile    = {
            required            => ['zaak_id'],
            constraint_methods  => {
                'zaak_id'   => qr/^\d+$/,
            }
        };
    }

    if (
        my $dv = $c->forward('/page/dialog', [{
            validatie           => $profile,
            user_permissions    => [qw/contact_nieuw zaak_eigen/],
            template            => 'widgets/documenten/notitie/bewerken.tt',
        }])
    ) {
        my $params  = $c->req->params;
        $params->{betrokkene_dsn} = $params->{ztc_aanvrager_id};
        $c->log->debug('Notitie toevoegen');

        my $zaak;
        if ($params->{zaak_id}) {
            $zaak = $c->model('DB::Zaak')->find(
                $params->{zaak_id}
            );

            if (
                $zaak &&
                (
                    !$zaak->behandelaar_object || (
                        $zaak->behandelaar_object->betrokkene_identifier ne
                            $c->betrokkene_object->betrokkene_identifier
                    )
                ) &&
                (
                    !$zaak->coordinator_object || (
                        $zaak->coordinator_object->betrokkene_identifier ne
                            $c->betrokkene_object->betrokkene_identifier
                    )
                )
            ) {
                $params->{queue} = 1;
            }
        }

        my ($logmsg);
        if (
            $c->stash->{document} &&
            $zaak &&
            !$zaak->is_afgehandeld &&
            $zaak->status ne 'deleted' &&
            $c->stash->{document}->zaak_id(
                $params->{zaak_id}
            ) &&
            $c->stash->{document}->update
        ) {
            $logmsg = 'Contactmoment toevegoegd aan zaak: '
                . $params->{zaak_id};
        } else {
            if($params->{zaak_id} && !$zaak) 
            {
                $logmsg = 'Fout. Contactmoment is niet toegevoegd omdat er geen zaak met dit zaaknummer gevonden is.';
            } 
            elsif($params->{zaak_id} && !(!$zaak->is_afgehandeld && $zaak->status ne 'deleted')) 
            {
                $logmsg = 'Fout. Contactmoment is niet toegevoegd omdat de zaak met dit zaaknummer afgehandeld is.';                
            } 
            elsif($c->model('DB::Notitie')->create_notitie($params)) 
            {
                my $bo = $c->model('Betrokkene')->get(
                    {},
                    $c->req->params->{ztc_aanvrager_id}
                );

                $logmsg = 'Contactmoment toegevoegd aan betrokkene: ' . $bo->naam
                    . ( $params->{zaak_id}
                        ? ' voor zaak ' . $params->{zaak_id}
                        : ''
                    ) . '.';
                
                $c->model('DB::Logging')->trigger('subject/contact_moment/create', {
                    component => 'betrokkene',
                    zaak_id => $params->{ zaak_id } || undef,
                    betrokkene_id => $bo->id,
                    data => {
                        case_id => $params->{ zaak_id } || undef,
                        subject_id => $bo->id,
                        content => $params->{ bericht }
                    }
                });
            }
        }

        $c->flash->{result} = $logmsg;
        $c->res->redirect('/');
    }
}

sub get : Private {
    my ($self, $c) = @_;

    if ($c->req->is_xhr) {
        $c->stash->{nowrapper}  = 1;
    }

    $c->stash->{template}   = 'widgets/documenten/notitie/view.tt';

}

sub accepteer : Private {
    my ($self, $c) = @_;

    if ($c->stash->{document}->notitie_id->accept) {
        $c->flash->{result} = 'Notitie succesvol gemarkeerd als gelezen';
    }

    $c->res->redirect($c->req->referer);
}


1;
