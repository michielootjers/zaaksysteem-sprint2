package Zaaksysteem::Controller::Betrokkene::Timeline;

use strict;
use warnings;
use parent 'Catalyst::Controller';
use Data::Dumper;

use Zaaksysteem::Constants qw/
    DOCUMENTS_STORE_TYPE_NOTITIE
/;


sub timeline2 : Chained('/betrokkene/view_base'): PathPart('timeline2'): Args(0) {
    my ($self, $c) = @_;

    $c->check_any_user_permission(qw/contact_search contact_nieuw/);

    my $subject_id = 'betrokkene-' . $c->stash->{betrokkene}->btype . '-' . $c->stash->{betrokkene}->ex_id;
    $c->log->debug("subject_id: " . $subject_id);
    
    my $params = $c->req->params();
    
    $c->stash->{json} = $c->model('DB::Logging')->json_view({
        event_type  => $params->{event_type},
        textfilter  => $params->{textfilter},
        rows        => $params->{rows},
        offset      => $params->{offset},
        subject_id  => $subject_id,
    });

    if($params->{debug}) {
        $c->res->body("<pre>" . Dumper ($c->stash->{json}) . "</pre>");
    } else {
        $c->log->debug("timeline data: " . Dumper ($c->stash->{json}));
        $c->forward('Zaaksysteem::View::JSON');
    }
    $c->detach;
}



sub timeline : Chained('/betrokkene/view_base'): PathPart('timeline'): Args(0) {
    my ($self, $c) = @_;

    $c->check_any_user_permission(qw/contact_search contact_nieuw/);

# todo move this to model layer -- when merging with dropout branch
    my $rows = 10;
    my $subject_id = 'betrokkene-' . $c->stash->{betrokkene}->btype . '-' . $c->stash->{betrokkene}->ex_id;
    $c->stash->{subject_id} = $subject_id;
    
 #   warn "--------------------------------------------------------";
# 
#     my $zaak_betrokkenen_rs = $c->model('DB::ZaakBetrokkenen')->search({
#         gegevens_magazijn_id => $c->stash->{betrokkene}->ex_id,
#         betrokkene_type => $c->stash->{betrokkene}->btype
#     });
#     #$zaak_betrokkenen_rs->result_class('DBIx::Class::ResultClass::HashRefInflator');
#     #warn Dumper [$zaak_betrokkenen_rs->all()];
#     my $zaken_rs = $c->model('DB::Zaak')->search({
#         aanvrager => { -in => $zaak_betrokkenen_rs->get_column('betrokkene_id')->as_query },
#     });

#   
    my $zaak_id_rs = $c->model('DB::Zaak')->search({
    },
    {
        order_by => { '-desc' => 'id'},
        rows => 1,
    });
    my $logging_rs = $c->model('DB::Logging')->search({
        zaak_id => { -in => $zaak_id_rs->get_column('id')->as_query },    
        onderwerp => { 'like' => '%aangemaakt' },
    }, {
    });
#     $logging_rs->result_class('DBIx::Class::ResultClass::HashRefInflator');
#   warn Dumper [$logging_rs->all()];
#    warn "--------------------------------------------------------";
    
    $c->log->debug("subject_id : " . $subject_id);
#    my $logging_rs = $c->model('DB::Logging')->search(
#        {
    #        betrokkene_id => $subject_id,
#        },
#        {
#            rows => $rows,
#        }
#    );

    my $documents_rs = $c->model('DB::Documents')->search(
        {
            betrokkene_id => $subject_id,
        },
        {
            rows => $rows,
        }
    );

    # my $betrokkene_notes = $c->stash->{betrokkene}->notes->list();
    
    my $contactmoments_rs = $c->model('DB::Documenten')->search(
        {
            betrokkene_dsn  =>
                $c->stash->{betrokkene}->betrokkene_identifier,
            store_type      => DOCUMENTS_STORE_TYPE_NOTITIE
        },
        {
            join     => 'notitie_id',
        }
    );

    my $results = [];

    my $params = $c->req->params();
    my $options = {};
    
    if($params->{use_filters}) {
        if($params->{filter}) {
            my $filters = $params->{filter};
            $filters = [$filters] unless(UNIVERSAL::isa($filters, 'ARRAY'));

            foreach my $filter (@$filters) {
                $options->{$filter} = 1;
            }
        } else {
            $options = {};
        }
    } else {
        $options = {
            document =>1,
            contactmoment => 1,
            note => 1,
            zaak => 1,
            
        };
    };
    
    $c->log->debug("options: " . Dumper $options);

    if($options->{zaak}) {
        $c->log->debug("fdfdfdfd");
        push @$results, @{$self->_get_rows($c, $logging_rs, 'zaak')};
    }
    if($options->{document}) {
        push @$results, @{$self->_get_rows($c, $documents_rs, 'document')};
    }
    
    if($options->{note}) {
        # push @$results, @{$self->_get_rows($c, $betrokkene_notes, 'note')};
    }
    
    if($options->{contactmoment}) {
        push @$results, @{$self->_get_rows($c, $contactmoments_rs, 'contactmoment')};
    }

#    $c->log->debug("results: " . Dumper $results);

    # poor man's union
    $results = [sort { $b->{epoch} <=> $a->{epoch} } @$results]; 

    $c->stash->{json} = {events => $results};
    $c->forward('Zaaksysteem::View::JSONlegacy');
    $c->detach;
}

sub _format_datetime {
    my ($self, $c, $row) = @_;

#    $c->log->debug("row ref: " . ref $row);
    if(ref $row eq 'Zaaksysteem::Model::DB::Logging') {
        $row->created->set_time_zone('GMT');
    } else {
        $row->created->set_time_zone('UTC');
        $row->created->set_time_zone('Europe/Amsterdam');
    }
    my $created = $row->created->set_locale('nl_NL');
    my $formatted = $created->strftime("%d %B %Y") . ' ' . $created->hms(':');
    return $formatted;
}


sub _get_rows {
    my ($self, $c, $resultset, $class) = @_;

   my $results = [];
 
    while(my $row = $resultset->next()) {
        my $result = {$row->get_columns()};

        $result->{created_formatted} = $self->_format_datetime($c, $row); 
               
        # for sorting
        $result->{epoch} = $row->created->epoch;
        if(ref $row eq 'Zaaksysteem::Model::DB::Logging') {
            $result->{epoch} -= 60*60*2;
        }        

#        $result->{class} = $result->{component} || $component;
        $result->{class} = $class;

        if($result->{class} eq 'contactmoment') {       
            # poor man's DBIx::Class
            $result->{notitie_id} = {$row->notitie_id->get_columns()};
            #$result->{subject} = $self->_check_subject($c, $result->{betrokkene_dsn});
        }
        
        push @$results, $result;
    }

    return $results;
}



sub _check_subject {
    my ($self, $c, $betrokkene) = @_;

    return if $betrokkene eq $c->stash->{subject_id};

    my ($betrokkene_type, $id) = $betrokkene =~ m|^betrokkene-(.*)-(\d+)$|is;

    my $betrokkene_x = $c->model('Betrokkene')->get(
            {
                type    => $betrokkene_type,
                intern  => 0,
            },
            $id
        );

    return $betrokkene_x->naam;
}

1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

