package Zaaksysteem::Controller::Search::Export;

use strict;
use warnings;
use Data::Dumper;
use File::stat;

use parent 'Catalyst::Controller';
use Zaaksysteem::Constants;

use Moose;


use constant EXPORT_FILETYPE_CONFIG => {
    excel       => { mimetype => 'application/vnd.ms-excel', extension => 'xls' },
    oocalc      => { mimetype => 'application/vnd.oasis.opendocument.spreadsheet', extension => 'ods' },
    csv         => { mimetype => 'text/csv', extension => 'csv' },
    documents   => { mimetype => 'text/csv', extension => 'csv'},
};




# ---- controllers ------- #



sub export :Chained("/search/base") :PathPart("export") :Args() {
	my ($self, $c, $filetype) = @_;
	
	$self->_document($c, {filetype => $filetype});
}



sub export_by_id :Chained("/search/id") :PathPart("export") :Args() {
	my ($self, $c, $filetype) = @_;
	
    $self->_document($c, {filetype => $filetype});
}


sub export_zaken :Chained("/search/base") :PathPart("export/overdracht") :Args() {
	my ($self, $c) = @_;

    $self->_export($c);
	warn "export zaken";
}



sub export_zaken_by_id :Chained("/search/id") :PathPart("export/overdracht") :Args() {
	my ($self, $c) = @_;

	warn "Export zaken by id";
    $self->_export($c);
}

sub _export {
    my ($self, $c) = @_;
    
    my $search_query = $self->_search_query($c);

    my $tmp_path = $c->model('DB::Config')->get_value('tmp_location');
    my $file = $search_query->results({c => $c})->export({
        filepath => $tmp_path,
    });
    my $filepath = $tmp_path.$file->{filename};

    die "no file exported" unless($file->{path} && $file->{filename});
    $c->log->debug("exported " . Dumper $file);

    die "file not found: $filepath" unless(-e $filepath);
    $c->serve_static_file($filepath);    

#    system("rm $filepath");
    $c->res->headers->header(
        'Content-Disposition',
        'attachment; filename="' . $file->{filename}
    );
    my $stat = stat($filepath);

    $c->res->headers->content_length( $stat->size );
    $c->res->headers->content_type('application/zip');
    $c->res->content_type('application/zip');

}


# ---- end of controllers ------- #

Params::Profile->register_profile(
    method  => '_document',
    profile => {
        required => [ qw/filetype/ ]
    }
);

sub _document : Private {
    my ($self, $c, $params) = @_;

    my $dv = Params::Profile->check(params  => $params);
    die "invalid options for _document" unless $dv->success;
	
	my $filetype = $params->{filetype};

	my $search_query = $self->_search_query($c);

	die "incorrect filetype" unless(exists EXPORT_FILETYPE_CONFIG->{$filetype});

	my $resultset = $search_query->results({c => $c})->with_progress();

    my $display_fields = $search_query->get_display_fields({ plain_text => 1 });;

    my @results = ();
    my @header_row;
    if($filetype eq 'documents') {
        @header_row = qw/Bestandsnaam Bestandstype Aangemaakt Reden Status Versie ZaakID/;
    } else {
        @header_row = map {$_->{'label'}} @$display_fields;
    }
    push @results, \@header_row;
    
	while (my $zaak = $resultset->next) {	 
	 
	    if($filetype eq 'documents') {
	        my $documents = $self->_get_files($c, {
	            zaak => $zaak,
	        });
	        die "need documents array" unless($documents && ref $documents && ref $documents eq 'ARRAY');

	        if($documents && @$documents) {
                push @results, @$documents;
            }

	    } else {
            my $row = $self->_get_result_row($c, {
                display_fields  => $display_fields,
                zaak            => $zaak,
            });
            push @results, $row;
        }
    }


    $self->_render_file($c, {
        results => \@results,
        filetype => $filetype,
    });
}




Params::Profile->register_profile(
    method  => '_render_file',
    profile => {
        required => [ qw/results filetype/ ]
    }
);
    
sub _render_file : Private {
    my ($self, $c, $params) = @_;

    my $dv = Params::Profile->check(params  => $params);
    die "invalid options for _render_file" unless $dv->success;
	
	my $results = $params->{results};
	my $filetype = $params->{filetype};

	# generate a csv string
    $c->stash->{csv} = { data => $results };

    my $csv = $c->view('CSV')->render($c, $c->stash);


    # cough up a file
    $c->log->debug('Part of CSV: ' . substr($csv,0,1024));

	use HTTP::Request::Common;
    use Encode qw(encode);

    if (
        !$c->config->{libreoffice_version} ||
        $c->config->{libreoffice_version} < 3
    ) {
        $csv = encode('UTF-8', $csv);
    } else {
        $csv = encode('iso-8859-1', $csv);
    }
	my $ua = LWP::UserAgent->new;
	my $result = $ua->request(POST 'http://localhost:8080/converter/service', 
		Content => $csv,
		Content_Type => 'text/csv',
		Accept => EXPORT_FILETYPE_CONFIG->{$filetype}->{'mimetype'},
	);	
	
	$c->log->debug("result: " . $result->code);
	if($result->code ne '200') {
	    $c->log->debug("result: " . $result->content);
	    die "result code of " . $result->code . " received. aborting";
	}

    $c->res->headers->header( 'Content-Type'  => 'application/x-download' );
    $c->res->headers->header(
        'Content-Disposition'  =>
            "attachment;filename=zaaksysteem-" . time()
            . "." . EXPORT_FILETYPE_CONFIG->{ $filetype }->{extension} . "\n\n"
    );

	$c->res->body($result->content);
}



Params::Profile->register_profile(
    method  => '_get_result_row',
    profile => {
        required => [ qw/zaak display_fields/ ]
    }
);



sub _get_result_row : Private {
    my ($self, $c, $params) = @_;

    my $dv = Params::Profile->check(params  => $params);
    die "invalid options for _get_result_row" unless $dv->success;
    
    my $display_fields  = $params->{display_fields};
    my $zaak            = $params->{zaak};

    my @row = ();
    foreach my $display_field (@$display_fields) {
        my $datafieldname = $display_field->{'systeemkenmerk'};
        my $value   = $zaak->systeemkenmerk($datafieldname);

        if ($value &&
            $value =~ /^[\w\d_-]+$/ &&
            (my $translation = $c->loc($datafieldname . '_' . $value)) ne
                $datafieldname . '_' . $value
        ) {
            $value = $translation;
        }

        push @row, $value;
    }

    return \@row;
}


Params::Profile->register_profile(
    method  => '_get_files',
    profile => {
        required => [ qw/zaak/ ]
    }
);



sub _get_files : Private {
    my ($self, $c, $params) = @_;

    my $dv = Params::Profile->check(params  => $params);
    die "invalid options for _get_files" unless $dv->success;
    
    my $zaak  = $params->{zaak};
    my $files = $zaak->files;
    
    my @result = ();
    while(my $file = $files->next()) {
        push @result, [
            $file->name,
            $file->filestore->mimetype,
            $file->date_created,
            'zaakbehandeling',
            $zaak->status eq 'open' ? 'Open' : 'Gearchiveerd',
            $zaak->id,
            $file->version,
        ];
    }
    return \@result;
}




sub _search_query {
	my ($self, $c) = @_;
	die "need c" unless($c);

	my $search_query = $c->model('SearchQuery');
	my $search_query_id = $c->stash->{SEARCH_QUERY_SESSION_VAR()};
	my $model = $c->model(SEARCH_QUERY_TABLE_NAME);
	my $record;
	
	if($search_query_id) {
		my $record = $model->find($search_query_id);
		if($record) {
			$search_query->unserialize($record->settings);
			$c->stash->{'search_query_name'} = $record->name;
			$c->stash->{'record_ldap_id'} = $record->ldap_id;
			$c->stash->{'search_query_access'} = $search_query->access();
			return $search_query;
		} else {
			$c->response->redirect($c->uri_for('/search'));
			$c->detach();
		}
	} else {
		$c->stash->{'search_query_name'} = 'Naamloze zoekopdracht';
		$c->stash->{'search_query_access'} = 'private';
	}

	if($c->session->{SEARCH_QUERY_SESSION_VAR()}) {
		$search_query->unserialize($c->session->{SEARCH_QUERY_SESSION_VAR()});
	}
	
	return $search_query;
}

1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut


