package Zaaksysteem::Controller::Medewerker;

use strict;
use warnings;
use parent 'Catalyst::Controller';

use Zaaksysteem::Constants qw/LOGGING_COMPONENT_USER/;


sub prepare_page : Private {
    my ($self, $c) = @_;

    ### Define the basics
    $c->stash->{org_eenheden} = $c->model('Betrokkene')->search(
        {
            type    => 'org_eenheid',
            intern  => 0,
        },
        {}
    );
}


sub behandelaar : Global {
    my ($self, $c, $zaaktype_node_id) = @_;
    my ( $aanvrager_keuze );

    $c->stash->{template}               = 'form/list.tt';
    $c->session->{zaaksysteem}->{mode}  = $c->stash->{layout_type} = 'simple';


    $c->stash->{behandelaar_form}       = $c->session->{behandelaar_form} = 1;

    ### When id given, redirect to first step
    if ($zaaktype_node_id) {
        $c->res->redirect(
            $c->uri_for(
                '/zaak/create',
                {
                    mode                => 'behandelaar',
                    zaaktype            => $zaaktype_node_id,
                    create              => 1,
                    ztc_trigger         => 'intern',
                    betrokkene_type     => 'medewerker',
                    ztc_contactkanaal   => 'post',
                    ztc_aanvrager_id    => 'betrokkene-medewerker-' .  $c->user->uidnumber
                }
            )
        );
        $c->detach;
    }

    ### Let's get a list of zaaktypen
    $c->stash->{zaaktypen}  = $c->model('Zaaktype')->list(
        {
            'zaaktype_trigger'  => 'intern',
        },
    );
}

sub base : Chained('/') : PathPart('medewerker') : CaptureArgs(1) {
    my ($self, $c, $userid)  = @_;

}

sub index : Chained('/') : PathPart('medewerker') : Args(0) {
    my ($self, $c)      = @_;

    $c->assert_any_user_permission('admin');

    if ($c->req->params->{parent_dn}) {

        $c->stash->{people_entries}        = $c->model('Users')->get_tree_view(
            {
                'objectClass'   => ['posixAccount']
            },
            $c->req->params->{parent_dn}
        );
    } else {
        $c->stash->{ou_entries}        = $c->model('Users')->get_tree_view({
            'objectClass'   => ['organization', 'organizationalUnit']
        });
    }

    $c->stash->{rollen}        = $c->model('Users')->get_tree_view({
        'objectClass'   => ['organization', 'posixGroup','organizationalUnit']
    });

$c->log->debug(Dumper($c->stash->{rollen}));

    $c->stash->{template}       = 'medewerker/index.tt';
}

sub move_to_ou : Chained('/') : PathPart('medewerker/move_to_ou') : Args(0) {
    my ($self, $c, $userid)     = @_;

    $c->assert_any_user_permission('admin');

    my $result = $c->model('Users')->move_to_ou(
        {
            ou_dn     => $c->req->params->{ou_dn},
            entry_dn    => $c->req->params->{entry_dn}
        }
    );

    if ($result) {
        $c->stash->{json} = {
            'newdn'     => $result,
            'succes'    => 1,
            'bericht'   => 'Medewerker succesvol toegevoegd aan OU',
        };

        $c->model('DB::Logging')->trigger('user/update/ou', {
            component => LOGGING_COMPONENT_USER,
            data => {
                user_dn => $c->req->param('entry_dn'),
                ou_dn => $c->req->param('ou_dn')
            }
        });
    } else {
        $c->stash->{json} = {
            'succes'    => 0,
            'bericht'   => 'Medewerker kan niet toegevoegd worden aan ou.'
        };

    }

    $c->forward('Zaaksysteem::View::JSONlegacy');
    $c->detach;
}

my $ADD_MAP = {
    'ou'    => {
        'required'  => [qw/ou/],
        'optional'  => [qw/parent/],
        'constraint_methods' => {
            'ou'    => qr/^[\w\d\s-]+$/,
        }
    },
    'role'  => {
        'required'  => [qw/role/],
        'optional'  => [qw/parent/],
        'constraint_methods' => {
            'role'    => qr/^[\w\d\s-]+$/,
        }
    }
};

sub add : Chained('/') : PathPart('medewerker/add') : Args(1) {
    my ($self, $c, $type ) = @_;

    unless ($type && $ADD_MAP->{$type}) {
        $c->log->error('Ongeldig type voor add');
        $c->res->redirect($c->uri_for('/medewerker'));
        $c->detach;
    }

    if ($type eq 'role' || $type eq 'ou') {
        $c->stash->{tree}        = $c->model('Users')->get_tree_view({
            'objectClass'   => ['organization', 'organizationalUnit']
        });
    }

    $c->stash->{type} = $type;

    if ($c->req->params->{confirmed}) {
        Params::Profile->register_profile(
            method  => 'add',
            profile => $ADD_MAP->{$type}
        );

        my $dv = Params::Profile->check(
            params  => $c->req->params,
        );

        my $opts    = $dv->valid;

        if ($dv->success) {
            my $addmethod = 'add_' . $type;

            if ((my $entry = $c->model('Users')->$addmethod($opts))) {
                $c->log->info(
                    'Users->add: Created entry'
                );

                $c->flash->{result} = 'Succesvol aangemaakt';

                my $msg;
                if ($dv->valid('role')) {
                    $msg    = 'Rol ' . $dv->valid('role') .
                            ' succesvol toegevoegd';
                } else {
                    $msg    = 'Organisatorische eenheid ' . $dv->valid('ou') .
                            ' succesvol toegevoegd';
                }

                $c->model('DB::Logging')->trigger($type . '/create', {
                    component => LOGGING_COMPONENT_USER,
                    data => $opts
                })
            }
        } else {
            $c->log->error('Invalid fields: ' . Dumper($dv));
            $c->flash->{result} = 'Aanmaken mislukt, naam van '
            . $type . ' niet correct';
        } 
    } else {
        $c->stash->{nowrapper} = 1;
        $c->stash->{template} = 'medewerker/add.tt';
        $c->detach;
    }

    $c->res->redirect($c->uri_for('/medewerker'));
}

sub delete : Chained('/') : PathPart('medewerker/delete') : Args(0) {
    my ($self, $c) = @_;

    unless($c->req->params->{confirmed}) {
        $c->stash->{confirmation}->{message}    =
            'Weet u zeker dat u deze entry wilt verwijderen?';
    
        $c->stash->{confirmation}->{type}       = 'yesno';
        $c->stash->{confirmation}->{params}     = {
            dn  => $c->req->params->{dn}
        };
        $c->stash->{confirmation}->{uri}        =
            $c->uri_for(
                '/medewerker/delete'
            );
    
        $c->forward('/page/confirmation');
        $c->detach;
    } 

    my $message;
    if ($c->req->params->{dn}) {
        if ($c->model('Users')->delete_entry($c->req->params->{dn})) {
            my $event = $c->model('DB::Logging')->trigger('role/remove', {
                component => LOGGING_COMPONENT_USER,
                data => { dn => $c->req->param('dn') }
            });

            $c->push_flash_message($event->onderwerp);
        } else {
            $c->push_flash_message('Entry met DN: ' . $c->req->params->{dn}
                . ' kan helaas niet verwijderd worden');
        }

    }

    $c->res->redirect($c->uri_for('/medewerker'));
}

sub delete_role_from : Chained('/') : PathPart('medewerker/delete_role_from') : Args(0) {
    my ($self, $c, $userid)     = @_;

    $c->assert_any_user_permission('admin');

    my $result = $c->model('Users')->delete_role_from(
        {
            role_dn     => $c->req->params->{role_dn},
            entry_dn    => $c->req->params->{entry_dn}
        }
    );

    if ($result) {
        $c->stash->{json} = {
            'succes'    => 1,
            'bericht'   => 'Rol succesvol verwijderd van medewerker.',
        };

        $c->model('DB::Logging')->trigger('user/role/remove', {
            component => LOGGING_COMPONENT_USER,
            data => {
                role_dn => $c->req->param('role_dn'),
                user_dn => $c->req->param('entry_dn')
            }
        });
    } else {
        $c->stash->{json} = {
            'succes'    => 0,
            'bericht'   => 'Rol kan niet verwijderd worden van medewerker.'
        };

    }

    $c->forward('Zaaksysteem::View::JSONlegacy');
    $c->detach;
}

sub add_role_to : Chained('/') : PathPart('medewerker/add_role_to') : Args(0) {
    my ($self, $c, $userid)     = @_;

    $c->assert_any_user_permission('admin');

    my $result = $c->model('Users')->add_role_to(
        {
            role_dn     => $c->req->params->{role_dn},
            entry_dn    => $c->req->params->{entry_dn}
        }
    );

    if ($result) {
        $c->stash->{json} = {
            'succes'    => 1,
            'bericht'   => 'Rol succesvol toegevoegd aan medewerker',
        };

        $c->model('DB::Logging')->trigger('user/role/add', {
            component => LOGGING_COMPONENT_USER,
            data => {
                role_dn => $c->req->param('role_dn'),
                entry_dn => $c->req->param('entry_dn')
            }
        });
    } else {
        $c->stash->{json} = {
            'succes'    => 0,
            'bericht'   => 'Rol kan niet toegevoegd worden aan medewerker, bestaat het al?'
        };

    }

    $c->forward('Zaaksysteem::View::JSONlegacy');
    $c->detach;
}

sub update : Chained('/') : PathPart('medewerker/update_rol') : Args(0) {
    my ($self, $c, $userid)     = @_;

    $c->assert_any_user_permission('admin');

    $c->stash->{medewerkers}    = $c->model('Users')->get_all_medewerkers;

    my $rawroles                   = {
        map {
            my $key = $_;
            $key    =~ s/zsmw_//;
            $key    => $c->req->params->{$_}
        } grep(/^zsmw_/, keys %{ $c->req->params })
    };

    my $roles;
    for my $role (keys %{ $rawroles }) {
        if (UNIVERSAL::isa($rawroles->{$role}, 'ARRAY')) {
            $roles->{$role} = $rawroles->{$role};
        } else {
            $roles->{$role} = [ $rawroles->{$role} ];
        }
    }

    #### Run it
    while (my ($username, $roles) = each %{ $roles }) {
        use Data::Dumper;
        $c->log->debug('USERNAME: ' . $username . ' / Roles: ' . Dumper($roles));
        $c->model('Users')->deploy_user_in_roles(
            $username, $roles
        );
    }

    $c->res->redirect($c->uri_for('/medewerker'));

    $c->detach;
}

sub testimport : Local {
    my ($self, $c) = @_;

    die('DIE STOP HIERE');

    my $userlist        = [
        {
            department => 'DIV en communicatie en WenB werken',
            user        => 'nbh',
            fullname    => 'Nicole Bosch',
        },
        {
            department => 'DIV en communicatie en WenB werken',
            user        => 'don',
            fullname    => 'Dicky Oomen',
        },
        {
            department => 'DIV en communicatie en WenB werken',
            user        => 'wnl',
            fullname    => 'Winny Nagel',
        },
        {
            department => 'DIV en communicatie en WenB werken',
            user        => 'bdr',
            fullname    => 'Bea Daselaar',
        },
        {
            department => 'DIV en communicatie en WenB werken',
            user        => 'mvt',
            fullname    => 'Marian Verkaart',
        },
        {
            department => 'DIV en communicatie en WenB werken',
            user        => 'abm',
            fullname    => 'Aruna Boedhram',
        },
        {
            department => 'DIV en communicatie en WenB werken',
            user        => 'adg',
            fullname    => 'Arnaud Disberg',
        },
        {
            department => 'DIV en communicatie en WenB werken',
            user        => 'mon',
            fullname    => 'Marieke van Oostveen',
        },
        {
            department => 'DIV en communicatie en WenB werken',
            user        => 'agp',
            fullname    => 'Ariean van de Groep',
        },
        {
            department => 'DIV en communicatie en WenB werken',
            user        => 'abg',
            fullname    => 'Alexander van den Berg',
        },
        {
        user=> 'dhn',
        fullname =>'Dennis Hagen',
        department=>'Planvoorbereiding en Groenbeheer'
        },
        {
        user=> 'awj',
        fullname =>'Ad van Wanroij',
        department=>'Planvoorbereiding en Groenbeheer'
        },
        {
        user=> 'cnd',
        fullname =>'Cees Noorland',
        department=>'Planvoorbereiding en Groenbeheer'
        },
        {
        user=> 'dor',
        fullname =>'Dik van den Oudenalder',
        department=>'Planvoorbereiding en Groenbeheer'
        },
        {
        user=> 'ghk',
        fullname =>'Gert Jan Hoitink',
        department=>'Planvoorbereiding en Groenbeheer'
        },
        {
        user=> 'hks',
        fullname =>'Hans Knotters',
        department=>'Planvoorbereiding en Groenbeheer'
        },
        {
        user=> 'jrt',
        fullname =>'Jorrit de Regt',
        department=>'Planvoorbereiding en Groenbeheer'
        },
        {
        user=> 'mvd',
        fullname =>'Marcel Voorveld',
        department=>'Planvoorbereiding en Groenbeheer'
        },
        {
        user=> 'wbe',
        fullname =>'Wim ten Boske',
        department=>'Planvoorbereiding en Groenbeheer'
        },
        {
        user=> 'tbk',
        fullname =>'Thelma Giele- van den Broek',
        department=>'Facilitaire zaken Receptie en ICT beheer'
        },
        {
        user=> 'avs',
        fullname =>'Ank van der Vlies',
        department=>'Facilitaire zaken Receptie en ICT beheer'
        },
        {
        user=> 'fbr',
        fullname =>'Fanny Blokker- van Breukelen',
        department=>'Facilitaire zaken Receptie en ICT beheer'
        },
        {
        user=> 'ftl',
        fullname =>'Frieda Thiel-Drost',
        department=>'Facilitaire zaken Receptie en ICT beheer'
        },
        {
        user=> 'jha',
        fullname =>'Jikkemien van Beek-Hettinga',
        department=>'Facilitaire zaken Receptie en ICT beheer'
        },
        {
        user=> 'mur',
        fullname =>'Meral Akdag-Ulker',
        department=>'Facilitaire zaken Receptie en ICT beheer'
        },
        {
        user=> 'pmk',
        fullname =>'Petra de Munnik',
        department=>'Facilitaire zaken Receptie en ICT beheer'
        },
        {
        user=> 'smw',
        fullname =>'Sjantie Mahadew',
        department=>'Facilitaire zaken Receptie en ICT beheer'
        },
        {
        user=> 'ahd',
        fullname =>'Ton van Hardeveld',
        department=>'Facilitaire zaken Receptie en ICT beheer'
        },
        {
        user=> 'wzd',
        fullname =>'Wendelmoet van Zonneveld',
        department=>'Facilitaire zaken Receptie en ICT beheer'
        },
        {
        user=> 'wmr',
        fullname =>'Wim Mercuur',
        department=>'Facilitaire zaken Receptie en ICT beheer'
        },
        {
        user=> 'thr',
        fullname =>'Thiemen den Hollander',
        department=>'Facilitaire zaken Receptie en ICT beheer'
        },
        {
        user=> 'nnn',
        fullname =>'Nick van Nieuwenhuizen',
        department=>'Facilitaire zaken Receptie en ICT beheer'
        },
        {
        user=> 'mst',
        fullname =>'Madhava Schuit',
        department=>'Facilitaire zaken Receptie en ICT beheer'
        },
        {
        user=> 'jhh',
        fullname =>'Johan de Haan',
        department=>'Facilitaire zaken Receptie en ICT beheer'
        },
        {
        user=> 'fbg',
        fullname =>'Frans van den Berg',
        department=>'Facilitaire zaken Receptie en ICT beheer'
        },
        {
        user=> 'eli',
        fullname =>'Eric Leroi',
        department=>'Facilitaire zaken Receptie en ICT beheer'
        },
        {
        user=> 'jbr',
        fullname =>'Jan van den Bor',
        department=>'Facilitaire zaken Receptie en ICT beheer'
        },
        {
        user=> 'clt',
        fullname =>'Corne Lokhorst',
        department=>'Facilitaire zaken Receptie en ICT beheer'
        },
        {
        user=> 'mdn',
        fullname =>'Marc Dijkman',
        department=>'Facilitaire zaken Receptie en ICT beheer'
        },
        {
        user=> 'ors',
        fullname =>'Onno Renes',
        department=>'Facilitaire zaken Receptie en ICT beheer'
        },
        {
        user=> 'rwk',
        fullname =>'Ronald Wennink',
        department=>'Facilitaire zaken Receptie en ICT beheer'
        },
        {
        user=> 'tkn',
        fullname =>'Tiem Koelewijn',
        department=>'Facilitaire zaken Receptie en ICT beheer'
        },
        {
        user=> 'vmr',
        fullname =>'Victor Meier',
        department=>'Facilitaire zaken Receptie en ICT beheer'
        },
        {
        user=> 'jsr',
        fullname =>'Han Schokker',
        department=>'Facilitaire zaken Receptie en ICT beheer'
        },
    ];

    my $users           = $c->model('Users');

    my $ldap            = $users->ldaph;

    my $ldapusers = {};
    for my $user (@{ $userlist }) {
        my $fullname = $user->{fullname};

        my ($firstname, $lastname) = $fullname =~ /(.*?)\s(.*)$/;

        $ldapusers->{$user->{user}} = {
            #userPassword    => '{SHA}nonexisting',
            displayName     => $lastname . ', ' . $firstname,
            sn              => $lastname,
            givenName       => $firstname,
            mail            => $user->{user} . '@baarn.nl',
            telephoneNumber => '0351234557',
            homeDirectory   => '/home/' . $user->{user},
            loginShell      => '/bin/nologin',
            initials        => uc(substr($firstname, 0, 1)) . '.',
            uid             => $user->{user},
            department      => $user->{department},
        };
    }

    $c->log->debug(Dumper($ldapusers));

    $users->_create_ldap($ldap, $ldapusers);

}


1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

