package Zaaksysteem::Backend::Directory::ResultSet;

use strict;
use warnings;

use Moose;
use Zaaksysteem::Constants;

extends 'DBIx::Class::ResultSet';

use Exception::Class (
    'Zaaksysteem::Backend::Directory::ResultSet::Exception' => {fields => 'code'},
    'Zaaksysteem::Backend::Directory::ResultSet::Exception::General' => {
        isa         => 'Zaaksysteem::Backend::Directory::ResultSet::Exception',
        description => 'General exception',
        alias       => 'throw_general_exception',
    },
    'Zaaksysteem::Backend::Directory::ResultSet::Exception::Parameter' => {
        isa         => 'Zaaksysteem::Backend::Directory::ResultSet::Exception',
        description => 'Parameter exception',
        alias       => 'throw_parameter_exception',
    },
);

=head2 directory_create

Create a Directory entry in the database.

=head3 Arguments

=over

=item name [required]

The name of the directory.

=item case_id [required]

The case this directory belongs to

=back

=head3 Returns

The newly created Directory object.

=cut

Params::Profile->register_profile(
    method  => 'directory_create',
    profile => {
        required => [qw/
        	name
        	case_id
        /],
        constraint_methods => {
            name => qr/^(\w|\s|!|@|#|-|:|\.)+$/,
            case_id => qr/^\d+$/,
        },
    }
);

sub directory_create {
	my $self = shift;
    my $opts = $_[0];

    my $valid = Params::Profile->check(
        params  => $opts,
    );
    if (!$valid->success) {
        throw_parameter_exception(
            code  => '/directory/directory_create/invalid_parameters',
            error => 'Invalid options given',
        );
    }

    # Check if a directory with this name and case already exists
    my $existing = $self->search($opts);
    if ($existing->count > 0) {
        throw_general_exception(
            code  => '/directory/directory_create/directory_exists',
            error => sprintf('Found existing entry with id %d and name %s',
                ($opts->{case_id}, $opts->{name})
            ),
        );
    }

	return $self->create($opts);
}

