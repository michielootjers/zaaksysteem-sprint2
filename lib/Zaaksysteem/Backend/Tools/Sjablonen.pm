package Zaaksysteem::Backend::Tools::Sjablonen;

use warnings;
use strict;

use Zaaksysteem::Constants;
use Params::Profile;
use Exporter;
use Data::Dumper;

use base qw[Exporter];

our @EXPORT = qw[magic_strings_convert];

=head2 magic_strings_convert

Convert the Document' magic strings to actual strings.

=cut

Params::Profile->register_profile(
    method  => 'magic_strings_convert',
    profile => {
        required => [qw/
            case
            document
        /],
    }
);

sub magic_strings_convert {
    my ($opts) = @_;

    return unless($opts->{ document });

    my $dv = Params::Profile->check(
        params  => $opts,
    );

    # Various parameter contraints/checks
    my $valid = $dv->valid;
    if ($dv->has_invalid) {
        my @invalid = join ',',$dv->invalid;
        die "Invalid options: @invalid";
    }
    if ($dv->has_missing) {
        my @missing = join ',',$dv->missing;
        die "Invalid options: @missing";
    }

    my $case     = $valid->{case};
    my $document = $valid->{document};
    my $prepared_kenmerken  = {};

    load_kenmerken(
        $case,
        $prepared_kenmerken,
    );

    load_base_kenmerken(
        $case,
        $prepared_kenmerken,
    );

    load_betrokkene_kenmerken(
        $case,
        $prepared_kenmerken,
    );

    # This is a workaround to prevent issues with nested magic strings.
    my ($loop_protection, $found_magic_strings) = (0, 1);
    while ($found_magic_strings && $loop_protection < 10) {
        $loop_protection++;


        if (!check_for_magic_strings($prepared_kenmerken, $document)) {
            $found_magic_strings = 0;
        } else {
            $document = replace_prepared_kenmerken(
                $prepared_kenmerken, $document
            );
        }
    }

    $document = clear_other_kenmerken($document);
    return $document;
}

sub check_for_magic_strings {
    my ($prepared_kenmerken, $source) = @_;
    
    return unless $source;

    while (my ($key, $value) = each %{ $prepared_kenmerken }) {
        if (UNIVERSAL::isa($source, 'HASH')) {


            my @elements    = $source->selectElementsByContent(
                '\[\[' . $key . '\]\]',
            );

            

            if (scalar(@elements)) {
                return 1;
            }

        } else {
            if ($source =~ /\[\[$key\]\]/g) {
                return 1;
            }
        }
    }

    return;
}

sub replace_prepared_kenmerken {
    my ($prepared_kenmerken,$source) = @_;

    for my $key (keys %{ $prepared_kenmerken }) {
        my $value = $prepared_kenmerken->{$key};
        $source = kenmerk_replace($source, $key, $value);
    }

    return $source;
}

sub load_kenmerken {
    my ($case, $prepared_kenmerken)  = @_;
    my $case_type_node = $case->zaaktype_node_id;

    if ($case_type_node->zaaktype_kenmerken->count) {
        my $kenmerken = $case_type_node->zaaktype_kenmerken->search(
            {is_group => undef},
            {prefetch => 'bibliotheek_kenmerken_id'}
        );

        my $kenmerken_data      = $case->field_values({});

        while (my $kenmerk  = $kenmerken->next) {
            next if $kenmerk->bibliotheek_kenmerken_id->value_type eq 'file';

            my $bibid       = $kenmerk->bibliotheek_kenmerken_id->id;
            my $value       = $kenmerken_data->{$bibid};

            if($kenmerk->bibliotheek_kenmerken_id->value_type eq 'richtext') {
                $prepared_kenmerken->{ $kenmerk->magic_string } = [ html2paragraphs($value) ];

                next;
            }

            # TODO create one routine that retrieves values for zaaksysteem
            # put referential functionality there
            if($kenmerk->referential && $case->pid) {
                my $parent_values = $case->pid->field_values({
                    bibliotheek_kenmerken_id => $bibid,
                }); 
                $value = $parent_values->{$bibid};
            }

            ### Make sure we handle arrays correctly:
            my $replace_value;
            if (UNIVERSAL::isa($value, 'ARRAY')) {
                $replace_value = join(", \n", @{ $value });
            } else {
                $replace_value = $value
            }

            ### Make sure valuta get's showed correctly
            if (
                $kenmerk->bibliotheek_kenmerken_id->value_type =~ /valuta/ &&
                length($replace_value||'')
            ) {
                $replace_value =~ s/,/./g;
                $replace_value = sprintf('%01.2f', $replace_value);
                $replace_value =~ s/\./,/g;
            }
            ### Make sure we show bag items the 'correct' way
            if (
                $kenmerk->bibliotheek_kenmerken_id->value_type =~ /^bag/
                && $replace_value && $replace_value =~ /\w+\-\d+/
            ) {
                my $rs = $case->result_source->schema->resultset('BagNummeraanduiding');

                my $bag = $rs->get_record_by_source_identifier($replace_value);

                $replace_value = $bag->to_string if $bag;
            }

            $prepared_kenmerken->{ $kenmerk->magic_string } = $replace_value;
        }
    }

    return $prepared_kenmerken;
}

sub clear_other_kenmerken {
    my ($source) = @_;

    if (UNIVERSAL::isa($source, 'HASH')) {
        $source->selectElementsByContent(
            '\[\[.*?\]\]',
            ''
        );
    } else {
        $source =~ s/\[\[.*?\]\]//g;
    }

    return $source;
}

sub kenmerk_replace_on_oodoc {
    my ($source, $key, $value) = @_;

    $key = sprintf('\[\[%s\]\]', $key);

    for my $element ($source->selectElementsByContent($key)) {
        if(UNIVERSAL::isa($value, 'ARRAY')) {
            while($element && !$element->isParagraph) {
                $element = $element->getParentNode;
            }

            next unless $element->isParagraph;

            for my $paragraph_text (reverse @{ $value }) {
                my $paragraph = $source->insertParagraph($element,
                    text => $source->outputTextConversion($paragraph_text),
                    position => 'after',
                );

                $source->textStyle($paragraph, $source->textStyle($element));
            }

            $element->delete;
        } else {
            $value  =~ s/(, )?\n/[[BR]]/g;

            $source->substituteText(
                $element,
                $key,
                $source->outputTextConversion($value)
            );

            $source->setChildElements(
                $element, 'text:line-break',
                replace => '\[\[BR\]\]',
            );
        }
    }
}

sub kenmerk_replace {
    my ($source, $key, $value) = @_;

    $value = '' unless defined($value);

    eval {
        if (UNIVERSAL::isa($source, 'HASH')) {
            kenmerk_replace_on_oodoc($source, $key, $value);
        } else {
            if(ref $value eq 'ARRAY') {
                $value = join("\n", @{ $value });
            }

            $source =~ s/\[\[$key\]\]/$value/g;
        }
    };

    return $source;
}

sub load_betrokkene_kenmerken {
    my ($case, $prepared_kenmerken)  = @_;

    my $betrokkenen = $case->zaak_betrokkenen->search_gerelateerd;

    my $kenmerken_h = ZAAKSYSTEEM_BETROKKENE_KENMERK;
    my @kenmerken   = keys %{ $kenmerken_h };

    while (my $betrokkene = $betrokkenen->next) {
        my $magic_string = $betrokkene->magic_string_prefix;
        my $betrokkene_object = $case->betrokkene_object(
            {
                magic_string_prefix => $magic_string,
            }
        ) or next;

        for my $kenmerk_postfix (@kenmerken) {
            my $kenmerk = $magic_string . '_' . $kenmerk_postfix;

            $prepared_kenmerken->{ $kenmerk } = ZAAKSYSTEEM_BETROKKENE_SUB->(
                $case, $betrokkene_object, $kenmerk_postfix
            );
        }
    }

    return $prepared_kenmerken;
}

sub load_base_kenmerken {
    my ($case, $prepared_kenmerken)  = @_;

    my $standaard_kenmerken = ZAAKSYSTEEM_STANDAARD_KENMERKEN;

    for my $kenmerk (keys %{ $standaard_kenmerken }) {
        eval {
            $prepared_kenmerken->{ $kenmerk } = $case->systeemkenmerk($kenmerk);
        };
    }

    return $prepared_kenmerken;
}

sub html2paragraphs {
    my $html = shift || '';
    $html =~ s/<br \/>/\n/g;

    my $builder = HTML::TreeBuilder->new;
    $builder->implicit_body_p_tag(1);
    $builder->p_strict(1);
    $builder->no_space_compacting(1);

    my $tree = $builder->parse($html);
    $builder->eof;

    return map { element2paragraph($_); } $tree->find(qw[p ul ol]);
}

sub element2paragraph {
    my $element = shift;

    my %dispatch = (
        p => sub { split(m[\n], $_->as_text) },

        ul => sub { join("\n", map
            { sprintf('%s %s', chr(0x2022), $_->as_trimmed_text) }
            $_->find('li')
        ) },

        ol => sub {
            my $iter = 1;

            return join("\n", map
                { sprintf('%d. %s', $iter++, $_->as_trimmed_text) }
                $_->find('li')
            );
        }
    );

    return unless exists $dispatch{ $element->tag };

    # Always append a newline after an element by injecting an empty paragraph
    return ($dispatch{ $element->tag }->($element), '');
}

1;
