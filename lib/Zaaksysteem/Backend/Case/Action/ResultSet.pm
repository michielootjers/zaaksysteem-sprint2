package Zaaksysteem::Backend::Case::Action::ResultSet;

use Moose;
use Data::Dumper;

BEGIN { extends 'DBIx::Class::ResultSet'; }

sub type {
    return shift->search({ type => shift });
}

sub milestone {
    return shift->search(
        { 'casetype_status_id.status' => shift },
        { join => 'casetype_status_id' }
    );
}

sub current {
    return shift->search(
        { 'casetype_status_id.status' => { '=' => \'case_id.milestone' } },
        { join => [ 'casetype_status_id', 'case_id' ] }
    );
}

sub active {
    return shift->search({ automatic => 1 });
}

sub sorted {
    my ($self) = @_;

    return $self->search({}, { order_by => { -asc => 'me.id' }});
}


sub create_from_case {
    my $self = shift;
    my $case = shift;

    map { $self->create_from_phase($case, $_) } $case->zaaktype_node_id->zaaktype_statuses;

    $case->case_actions->apply_rules({case => $case});
}

sub create_from_phase {
    my $self = shift;
    my $case = shift;
    my $phase = shift;

    my @actions;

    push(@actions, map { $self->create_from_subcase($case, $phase, $_) } $phase->zaaktype_relaties->search({}, {
        order_by => { -asc => 'me.id' }
    }));

    unless($phase->is_last) {
        push(@actions, $self->create({
            case_id => $case->id,
            casetype_status_id => $phase->id,
            type => 'allocation',
            label => $phase->ou_id . ', ' . $phase->role_id,
            automatic => $phase->role_set,
            data => {
                description => 'Toewijzing',
                ou_id => $phase->ou_id,
                role_id => $phase->role_id,
                role_set => $phase->role_set,
            }
        }));
    }

    push(@actions, map { $self->create_from_template($case, $phase, $_) } $phase->zaaktype_sjablonens->search({}, {
        order_by => { -asc => 'me.id' }
    }));

    push(@actions, map { $self->create_from_email($case, $phase, $_) } $phase->zaaktype_notificaties->search({}, {
        order_by => { -asc => 'me.id' }
    }));

    return @actions;
}

sub create_from_subcase {
    my $self = shift;
    my $case = shift;
    my $phase = shift;
    my $subcase = shift;

    return $self->create({
        case_id => $case->id,
        casetype_status_id => $phase->id,
        type => 'case',
        label => $subcase->relatie_zaaktype_id->zaaktype_node_id->titel,
        automatic => $subcase->status,
        data => {
            ou_id => $subcase->ou_id,
            role_id => $subcase->role_id,
            relatie_type => $subcase->relatie_type,
            eigenaar_type => $subcase->eigenaar_type,
            kopieren_kenmerken => $subcase->kopieren_kenmerken,
            parent_advance_results => $subcase->parent_advance_results,
            required => $subcase->required,
            relatie_zaaktype_id => $subcase->get_column('relatie_zaaktype_id'),
            start_delay => $subcase->start_delay,
            automatisch_behandelen => $subcase->automatisch_behandelen,
        }
    });
}

sub create_from_email {
    my $self  = shift;
    my $case  = shift;
    my $phase = shift;
    my $email = shift;

    my @case_document_attachments;
    my $library_notification_properties = $email->bibliotheek_notificaties_id
        ->bibliotheek_notificatie_kenmerks->search(
            {},
            {
                prefetch    => 'bibliotheek_kenmerken_id'
            }
        );


    my @node_case_type_properties   = $email
                                    ->zaaktype_node_id
                                    ->zaaktype_kenmerkens
                                    ->search({},{ prefetch => 'bibliotheek_kenmerken_id' })
                                    ->all;
    # Filter those without a bibliotheek_kenmerken_id as they will not be what we need for
    # detecting case_type_documents.
    @node_case_type_properties = grep {$_->bibliotheek_kenmerken_id} @node_case_type_properties;
    
    # Next, look at all the library notification properties, which are basically attachments
    while (my $lnp = $library_notification_properties->next) {
        # If it matches an entry in the case type properties list it means it is a valid
        # case_type_document within this case type.
        my ($case_type_property) = grep {
            $_->bibliotheek_kenmerken_id->id eq $lnp->bibliotheek_kenmerken_id->id
        } @node_case_type_properties;

        if ($case_type_property) {
            push @case_document_attachments, {
                selected => 1,
                name => $lnp->bibliotheek_kenmerken_id->naam,
                case_type_document_id => $case_type_property->id,
            }
        }
    }
    
    return $self->create({
        case_id => $case->id,
        casetype_status_id => $phase->id,
        type => 'email',
        label => $email->bibliotheek_notificaties_id->label,
        automatic => !$email->intern_block && $email->automatic ? 1 : 0,
        data => {
            rcpt => $email->rcpt,
            subject => $email->bibliotheek_notificaties_id->subject,
            body => $email->bibliotheek_notificaties_id->message,
            email => $email->email,
            description => 'E-mail',
            behandelaar => $email->behandelaar,
            intern_block => $email->intern_block,
            case_document_attachments => \@case_document_attachments,
            automatic_phase => $email->automatic, #needed when rules disapply
            bibliotheek_notificaties_id => $email->get_column('bibliotheek_notificaties_id')
            # send_date => $email->send_date,
            # schedule_test => $email->schedule_test
        }
    });
}

sub create_from_template {
    my $self = shift;
    my $case = shift;
    my $phase = shift;
    my $template = shift;

    return $self->create({
        case_id => $case->id,
        casetype_status_id => $phase->id,
        type => 'template',
        label => $template->bibliotheek_sjablonen_id->naam,
        automatic => $template->automatisch_genereren,
        data => {
            description => 'Sjabloon',
            bibliotheek_sjablonen_id => $template->get_column('bibliotheek_sjablonen_id'),
            bibliotheek_kenmerken_id => $template->get_column('bibliotheek_kenmerken_id'),
            filename => $template->bibliotheek_sjablonen_id->filestore_id->original_name,
            target_format => $template->target_format || 'odt'
        }
    });
}

sub apply_rules {
    my ($self, $options) = @_;

    my $case = $options->{ case } or die "need case";
    my $milestone = $options->{ milestone } || ($case->milestone + 1);

    my %rule_templates;
    my %scheduled_mails;
    my %toewijzing;
    my %action_counter;

    return $self->reset if $case->is_afgehandeld;

    # We're only going to apply rules to actions in the current milestone, thats the only one that matters
    my $rules_result = $case->execute_rules({ status => $milestone });

    if(exists $rules_result->{ templates }) {
        %rule_templates = map { $_ => 1 } @{ $rules_result->{ templates } };
    }

    while(my $action = $self->next()) {

        # Yeah, the counter state is a bit complex, but let me explain!
        # Since rules are applied to email notifications by their ordering, we need to check the position of the action
        # But we also need to track the phase it's in, otherwise we just increment the count for stati in the first phase
        # And that gives screwy results.
        my $counter = ++$action_counter{ sprintf('%s-%d', $action->type, $action->casetype_status_id->status) };

        # Ignore tainted actons, and actions in a different milestone.
        next if($action->tainted || $action->casetype_status_id->status != ($milestone));

        if($action->type eq 'template' && $rule_templates{ $counter }) {
            warn Dumper \%action_counter;

            unless($action->automatic) {
                $action->automatic(1);
                $action->update;
            }
        } elsif($action->type eq 'email') {
            if(exists $rules_result->{schedule_mail}->{ $counter }) {
                unless($action->automatic) {
                    $action->automatic(1);
                    $action->update;
                }

                my $send_date = $rules_result->{schedule_mail}->{$counter}->{send_date_formatted};
                if($send_date && $action->data->{send_date} ne $send_date) {
                    my $data = $action->data;
                    $data->{send_date} = $send_date;
                    $action->data($data);
                    $action->update;
                }
            } else {
                # fall back to default behaviour. don't uncheck when 
                unless($action->data->{automatic_phase}) {
                    if($action->automatic) {
                        $action->automatic(0);
                        $action->update;
                    }
                }
            }
        } elsif($action->type eq 'allocation') {
            if(my $toewijzing = $rules_result->{toewijzing}) {
                my $data = $action->data;

                unless(
                    $data->{role_id} eq $toewijzing->{role_id} &&
                    $data->{ou_id}   eq $toewijzing->{ou_id} &&
                    $action->automatic
                ) {
                    $data->{role_id}    = $toewijzing->{role_id};
                    $data->{ou_id}      = $toewijzing->{ou_id};
                    $action->automatic(1);
                    $action->data($data);
                    $action->update;
                }
            } else {
                unless($action->data->{role_set}) {
                    if($action->automatic) {
                        $action->automatic(0);
                        $action->update;
                    }
                }
            }
        }
    }

    $self->reset;
}


=head1

Allocation actions work as follows. From ZTB phase management a hardcoded allocation action
can be set. This is mandatory for the first phase, optional for the middle phases, and unavailable
for the last phase.

Rules can be created to override the phase allocation. Although multiple rules can be created, only the
final one is executed. This allows for conditional overriding.

So:
- Phase allocation (if present)
- Final rule allocation (if present)

Case_action rows are created for every phase but the final one, with the 'automatic' flag determining
if the action is executed at phase advancing.

If rules are present they can override the standard behaviour.
The outcome of rules is influenced by user input. When the user input changes back and disables the rules, the ZTB
settings need to remain available in the action.


$action->apply_allocation_rule({
    rule_data => $toewijzing,
});

sub apply_allocation_rule {
    my ($self, $options) = @_;

    my $rule_data = $options->{rule_date} or die "need rule_data";
}

=cut



1;
