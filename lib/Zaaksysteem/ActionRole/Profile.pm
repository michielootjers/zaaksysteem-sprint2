package Zaaksysteem::ActionRole::Profile;

use Moose::Role;
use namespace::autoclean;

use Zaaksysteem::Profiles;
use Data::FormValidator;

use constant    PROFILE_VALIDATION_PARAM            => 'do_validation';
use constant    PROFILE_VALIDATION_ACTION_DENIED    => '/forbidden';

has '_profile_list'     => (
    'is'        => 'rw',
    'default'   => sub { {}; }
);

=head2 execute

Load all profiles into memory and validate input parameters

=cut

around execute      => sub {
    my $orig    = shift;
    my $self    = shift;
    my ($controller, $c) = @_;

    $self->_load_profiles( @_ );

    $self->_validate_profile( @_ );

    return $self->$orig(@_);
};

sub _validate_profile {
    my $self                = shift;
    my ($controller, $c)    = @_;

    my $dv  = $self->_register_profile_results(@_);

#    my $validation_param    = PROFILE_VALIDATION_PARAM;
#    if (
#        $c->req->params->{$validation_param} &&
#        $c->req->is_xhr
#    ) {
#        $self->_generate_json_validation_response($dv, @_);
#        return $dv;
#    }

    if (
        $self->attributes->{AssertProfile} &&
        (
            !UNIVERSAL::isa($dv, 'Data::FormValidator::Results') ||
            !$dv->success
        )
    ) {
        if ($c->req->is_xhr) {
            if (UNIVERSAL::isa($dv, 'Data::FormValidator::Results')) {
                $c->stash->{zapi} = Zaaksysteem::ZAPI::Error->new(
                    category        => 'ZAPIProfile',
                    data            => $dv,
                );
            } else {
                $c->stash->{zapi} = Zaaksysteem::ZAPI::Error->new(
                    category        => 'Unknown error',
                    error_code      => 500,
                    message         => ['Missing params']
                );
            }
            $c->detach('Zaaksysteem::View::ZAPI');
        } else {
            $c->detach(PROFILE_VALIDATION_ACTION_DENIED);
        }
    }

    return $dv;
}

sub _generate_json_validation_response {
    my $self                = shift;
    my $dv                  = shift;
    my ($controller, $c)    = @_;

    $c->stash->{zapi}       = {
        json                => {
            success         => $dv->success
        }
    };

    for my $key (qw/msgs invalid valid missing/) {
        $c->stash->{json}->{json}->{$key}  = $dv->$key;
    }

    $c->forward($c->view('JSONlegacy'));
}

sub _register_profile_results {
    my $self                = shift;
    my ($controller, $c)    = @_;

    $c->stash->{_zapi} = {} unless (
        defined( $c->stash->{_zapi} ) &&
        $c->stash->{_zapi}
    );

    my $caller              = $self->namespace
                            . '.' . $self->name;

    $c->stash->{_zapi}->{dv_result}->{$caller} = undef;

    if (
        defined( $self->_profile_list->{ $self->name } ) &&
        $self->_profile_list->{ $self->name }
    ) {
        my $dv = $self->_validation_check(
            $self->_profile_list->{ $self->name },
            @_
        );

        return $c->stash->{_zapi}->{dv_result}->{$caller} = $dv;
    }

    return;
}

sub _validation_check {
    my $self                = shift;
    my $profile             = shift;
    my ($controller, $c)    = @_;

    return Data::FormValidator->check(
        {
            %{ $c->req->params },
            schema      => $c->model('DB'),
        },
        $profile
    );
}

=head2 $actionrole->_load_profiles($controller)

Load all profiles for the given controller into attr: _profile_list

=cut

sub _load_profiles {
    my $self                = shift;
    my ($controller)        = @_;

    return $self->_profile_list if scalar(keys %{ $self->_profile_list });

    for my $action ($controller->get_action_methods()) {
        $self->_profile_list->{ $action->name } = $self->_get_profile(
            $controller, $action
        );
    }
}

=head2 $actionrole->_get_profile($controller, $action)

Get the profile belonging to the given action within given controller. Profile
is a L<Data::FormValidator> profile.

    e.g.
    {
        required    => [qw/params1 params2/],
        optional    => [qw/param3 param4/],
    }

=cut

sub _get_profile {
    my ($self, $controller, $action)       = @_;
    my @attributes;

    return unless (@attributes = grep(/^Profile\(/, @{ $action->attributes }));

    for my $attr (@attributes) {
        my ($name)  = $attr     =~ /^Profile\((.*?)\)/;
        $name       =~ s/['"]//g;

        if (!$name) {
            $name       = $controller->path_prefix . '.' . $action->name;
        } else {
            if ($name !~ /^\+/) {
                $name   = $controller->path_prefix . '.' . $name;
            }
        }

        if (
            defined($controller->config->{dv_profiles}) &&
            defined($controller->config->{dv_profiles}->{$name}) &&
            $controller->config->{dv_profiles}->{$name}
        ) {
            return $controller->config->{dv_profiles}->{$name};
        } elsif (defined(ZAPI_PROFILES->{ $name })) {
            return ZAPI_PROFILES->{ $name };
        }
    }

    return;
}


1;
