package Zaaksysteem::Schema::BagPand;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::BagPand

=head1 DESCRIPTION

55 : een pand is de kleinste, bij de totstandkoming functioneel en bouwkundig constructief zelfstandige eenheid, die direct en duurzaam met de aarde is verbonden.

=cut

__PACKAGE__->table("bag_pand");

=head1 ACCESSORS

=head2 identificatie

  data_type: 'varchar'
  is_nullable: 0
  size: 16

55.01 : de unieke aanduiding van een pand

=head2 begindatum

  data_type: 'varchar'
  is_nullable: 0
  size: 14

55.91 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een pand een wijziging hebben ondergaan.

=head2 einddatum

  data_type: 'varchar'
  is_nullable: 1
  size: 14

55.92 : de einddatum van een periode waarin er geen wijzigingen hebben plaatsgevonden in de gegevens die worden bijgehouden over een pand.

=head2 officieel

  data_type: 'varchar'
  is_nullable: 0
  size: 1

55.02 : een aanduiding waarmee kan worden aangegeven dat een object in de registratie is opgenomen als gevolg van een feitelijke constatering, zonder dat er op het moment van opname sprake is van een formele grondslag voor deze opname

=head2 bouwjaar

  data_type: 'integer'
  is_nullable: 0

55.30 : de aanduiding van het jaar waarin een pand oorspronkelijk als bouwkundig gereed is opgeleverd.

=head2 status

  data_type: 'varchar'
  is_nullable: 0
  size: 80

55.31 : de fase van de levenscyclus van een pand, waarin het betreffende pand zich bevindt.

=head2 inonderzoek

  data_type: 'varchar'
  is_nullable: 0
  size: 1

55.93 : een aanduiding waarmee wordt aangegeven dat een onderzoek wordt uitgevoerd naar de juistheid van een of meerdere gegevens van het betreffende object.

=head2 documentdatum

  data_type: 'varchar'
  is_nullable: 0
  size: 14

55.97 : de datum waarop het brondocument is vastgesteld, op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een pand heeft plaatsgevonden.

=head2 documentnummer

  data_type: 'varchar'
  is_nullable: 0
  size: 20

55.98 : de unieke aanduiding van het brondocument op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een pand heeft plaatsgevonden, binnen een gemeente.

=head2 correctie

  data_type: 'varchar'
  is_nullable: 0
  size: 1

het gegeven is gecorrigeerd.

=cut

__PACKAGE__->add_columns(
  "identificatie",
  { data_type => "varchar", is_nullable => 0, size => 16 },
  "begindatum",
  { data_type => "varchar", is_nullable => 0, size => 14 },
  "einddatum",
  { data_type => "varchar", is_nullable => 1, size => 14 },
  "officieel",
  { data_type => "varchar", is_nullable => 0, size => 1 },
  "bouwjaar",
  { data_type => "integer", is_nullable => 0 },
  "status",
  { data_type => "varchar", is_nullable => 0, size => 80 },
  "inonderzoek",
  { data_type => "varchar", is_nullable => 0, size => 1 },
  "documentdatum",
  { data_type => "varchar", is_nullable => 0, size => 14 },
  "documentnummer",
  { data_type => "varchar", is_nullable => 0, size => 20 },
  "correctie",
  { data_type => "varchar", is_nullable => 0, size => 1 },
);
__PACKAGE__->set_primary_key("identificatie", "begindatum", "correctie");


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-01-10 07:42:00
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:LhUjzqdxQyT83cT64XZZ5A

__PACKAGE__->has_many(
  "verblijfsobject_panden",
  "Zaaksysteem::Schema::BagVerblijfsobjectPand",
  { "foreign.pand" => "self.identificatie" },
);





# You can replace this text with custom content, and it will be preserved on regeneration
1;
