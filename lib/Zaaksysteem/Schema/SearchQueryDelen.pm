package Zaaksysteem::Schema::SearchQueryDelen;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::SearchQueryDelen

=cut

__PACKAGE__->table("search_query_delen");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'search_query_delen_id_seq'

=head2 search_query_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 ou_id

  data_type: 'integer'
  is_nullable: 1

=head2 role_id

  data_type: 'integer'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "search_query_delen_id_seq",
  },
  "search_query_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "ou_id",
  { data_type => "integer", is_nullable => 1 },
  "role_id",
  { data_type => "integer", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 search_query_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::SearchQuery>

=cut

__PACKAGE__->belongs_to(
  "search_query_id",
  "Zaaksysteem::Schema::SearchQuery",
  { id => "search_query_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-01-10 07:44:26
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:5DnRCT4Ui9fdUPgUaDsRHQ





# You can replace this text with custom content, and it will be preserved on regeneration
1;
