package Zaaksysteem::Schema::ChecklistAntwoord;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::ChecklistAntwoord

=cut

__PACKAGE__->table("checklist_antwoord");

=head1 ACCESSORS

=head2 zaak_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 mogelijkheid_id

  data_type: 'integer'
  is_nullable: 1

=head2 antwoord

  data_type: 'text'
  is_nullable: 1

=head2 vraag_id

  data_type: 'integer'
  is_nullable: 1

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'checklist_antwoord_id_seq'

=cut

__PACKAGE__->add_columns(
  "zaak_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "mogelijkheid_id",
  { data_type => "integer", is_nullable => 1 },
  "antwoord",
  { data_type => "text", is_nullable => 1 },
  "vraag_id",
  { data_type => "integer", is_nullable => 1 },
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "checklist_antwoord_id_seq",
  },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 zaak_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Zaak>

=cut

__PACKAGE__->belongs_to("zaak_id", "Zaaksysteem::Schema::Zaak", { id => "zaak_id" });


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-01-10 07:42:00
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:grHdHjm9oeTEm1886HJQbA

__PACKAGE__->resultset_class('Zaaksysteem::Zaken::ResultSetChecklistAntwoord');

__PACKAGE__->add_columns(
  "id",
  {
    data_type => "INTEGER",
    default_value => undef,
    is_nullable => 1,
    size => undef,
    is_auto_increment => 1,
  }
);




# You can replace this text with custom content, and it will be preserved on regeneration
1;
