package Zaaksysteem::Schema::ZaaktypeRelatie;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::ZaaktypeRelatie

=cut

__PACKAGE__->table("zaaktype_relatie");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'zaaktype_relatie_id_seq'

=head2 zaaktype_node_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 relatie_zaaktype_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 zaaktype_status_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 relatie_type

  data_type: 'text'
  is_nullable: 1

=head2 eigenaar_type

  data_type: 'text'
  default_value: 'aanvrager'
  is_nullable: 0

=head2 start_delay

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 created

  data_type: 'timestamp'
  is_nullable: 1

=head2 last_modified

  data_type: 'timestamp'
  is_nullable: 1

=head2 status

  data_type: 'integer'
  is_nullable: 1

=head2 kopieren_kenmerken

  data_type: 'integer'
  is_nullable: 1

=head2 delay_type

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 ou_id

  data_type: 'integer'
  is_nullable: 1

=head2 role_id

  data_type: 'integer'
  is_nullable: 1

=head2 automatisch_behandelen

  data_type: 'boolean'
  is_nullable: 1

=head2 required

  data_type: 'varchar'
  is_nullable: 1
  size: 12

=head2 parent_advance_results

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "zaaktype_relatie_id_seq",
  },
  "zaaktype_node_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "relatie_zaaktype_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "zaaktype_status_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "relatie_type",
  { data_type => "text", is_nullable => 1 },
  "eigenaar_type",
  { data_type => "text", default_value => "aanvrager", is_nullable => 0 },
  "start_delay",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "created",
  { data_type => "timestamp", is_nullable => 1 },
  "last_modified",
  { data_type => "timestamp", is_nullable => 1 },
  "status",
  { data_type => "integer", is_nullable => 1 },
  "kopieren_kenmerken",
  { data_type => "integer", is_nullable => 1 },
  "delay_type",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "ou_id",
  { data_type => "integer", is_nullable => 1 },
  "role_id",
  { data_type => "integer", is_nullable => 1 },
  "automatisch_behandelen",
  { data_type => "boolean", is_nullable => 1 },
  "required",
  { data_type => "varchar", is_nullable => 1, size => 12 },
  "parent_advance_results",
  { data_type => "text", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 zaaktype_node_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ZaaktypeNode>

=cut

__PACKAGE__->belongs_to(
  "zaaktype_node_id",
  "Zaaksysteem::Schema::ZaaktypeNode",
  { id => "zaaktype_node_id" },
);

=head2 relatie_zaaktype_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Zaaktype>

=cut

__PACKAGE__->belongs_to(
  "relatie_zaaktype_id",
  "Zaaksysteem::Schema::Zaaktype",
  { id => "relatie_zaaktype_id" },
);

=head2 zaaktype_status_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ZaaktypeStatus>

=cut

__PACKAGE__->belongs_to(
  "zaaktype_status_id",
  "Zaaksysteem::Schema::ZaaktypeStatus",
  { id => "zaaktype_status_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-02-25 10:00:14
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:CSRGa/329wYdS0Goyr1oVQ
__PACKAGE__->resultset_class('Zaaksysteem::DB::ResultSet::ZaaktypeRelatie');

__PACKAGE__->load_components(
    "+Zaaksysteem::DB::Component::ZaaktypeRelatie",
    __PACKAGE__->load_components()
);

__PACKAGE__->add_columns('last_modified',
    { %{ __PACKAGE__->column_info('last_modified') },
    set_on_update => 1,
    set_on_create => 1,
});

__PACKAGE__->add_columns('created',
    { %{ __PACKAGE__->column_info('created') },
    set_on_create => 1,
});




# You can replace this text with custom content, and it will be preserved on regeneration
1;
