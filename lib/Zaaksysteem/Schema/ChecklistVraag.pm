package Zaaksysteem::Schema::ChecklistVraag;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::ChecklistVraag

=cut

__PACKAGE__->table("checklist_vraag");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'checklist_vraag_id_seq'

=head2 nr

  data_type: 'integer'
  is_nullable: 1

=head2 vraag

  data_type: 'text'
  is_nullable: 1

=head2 vraagtype

  data_type: 'text'
  is_nullable: 1

=head2 zaaktype_node_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 zaaktype_status_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "checklist_vraag_id_seq",
  },
  "nr",
  { data_type => "integer", is_nullable => 1 },
  "vraag",
  { data_type => "text", is_nullable => 1 },
  "vraagtype",
  { data_type => "text", is_nullable => 1 },
  "zaaktype_node_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "zaaktype_status_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 zaaktype_node_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ZaaktypeNode>

=cut

__PACKAGE__->belongs_to(
  "zaaktype_node_id",
  "Zaaksysteem::Schema::ZaaktypeNode",
  { id => "zaaktype_node_id" },
);

=head2 zaaktype_status_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ZaaktypeStatus>

=cut

__PACKAGE__->belongs_to(
  "zaaktype_status_id",
  "Zaaksysteem::Schema::ZaaktypeStatus",
  { id => "zaaktype_status_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-01-10 07:42:00
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:2DHUJVYKPkEXhwlbIgB6Sg
__PACKAGE__->resultset_class('Zaaksysteem::DB::ResultSet::ChecklistVraag');

__PACKAGE__->has_many(
  "checklist_antwoords",
  "Zaaksysteem::Schema::ChecklistAntwoord",
  { "foreign.vraag_id" => "self.id" },
);






# You can replace this text with custom content, and it will be preserved on regeneration
1;
