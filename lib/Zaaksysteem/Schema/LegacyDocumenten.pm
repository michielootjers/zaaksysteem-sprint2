package Zaaksysteem::Schema::LegacyDocumenten;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::LegacyDocumenten

=cut

__PACKAGE__->table("legacy_documenten");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'legacy_documenten_id_seq'

=head2 folder

  data_type: 'boolean'
  is_nullable: 1

=head2 folder_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 zaak_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 betrokkene_dsn

  data_type: 'text'
  is_nullable: 1

=head2 milestone

  data_type: 'integer'
  is_nullable: 1

=head2 naam

  data_type: 'text'
  is_nullable: 1

=head2 zaaktype_kenmerken_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 document_type

  data_type: 'text'
  is_nullable: 1

=head2 verplicht

  data_type: 'boolean'
  is_nullable: 1

=head2 pip

  data_type: 'boolean'
  is_nullable: 1

=head2 versie

  data_type: 'integer'
  is_nullable: 1

=head2 store_type

  data_type: 'text'
  is_nullable: 1

=head2 filestore_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 notitie_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 queue

  data_type: 'boolean'
  is_nullable: 1

=head2 ontvangstdatum

  data_type: 'timestamp'
  is_nullable: 1

=head2 created

  data_type: 'timestamp'
  is_nullable: 1

=head2 last_modified

  data_type: 'timestamp'
  is_nullable: 1

=head2 deleted

  data_type: 'timestamp'
  is_nullable: 1

=head2 job_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 new_filestore_id

  data_type: 'integer'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "legacy_documenten_id_seq",
  },
  "folder",
  { data_type => "boolean", is_nullable => 1 },
  "folder_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "zaak_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "betrokkene_dsn",
  { data_type => "text", is_nullable => 1 },
  "milestone",
  { data_type => "integer", is_nullable => 1 },
  "naam",
  { data_type => "text", is_nullable => 1 },
  "zaaktype_kenmerken_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "document_type",
  { data_type => "text", is_nullable => 1 },
  "verplicht",
  { data_type => "boolean", is_nullable => 1 },
  "pip",
  { data_type => "boolean", is_nullable => 1 },
  "versie",
  { data_type => "integer", is_nullable => 1 },
  "store_type",
  { data_type => "text", is_nullable => 1 },
  "filestore_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "notitie_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "queue",
  { data_type => "boolean", is_nullable => 1 },
  "ontvangstdatum",
  { data_type => "timestamp", is_nullable => 1 },
  "created",
  { data_type => "timestamp", is_nullable => 1 },
  "last_modified",
  { data_type => "timestamp", is_nullable => 1 },
  "deleted",
  { data_type => "timestamp", is_nullable => 1 },
  "job_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "new_filestore_id",
  { data_type => "integer", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 filestore_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::LegacyFilestore>

=cut

__PACKAGE__->belongs_to(
  "filestore_id",
  "Zaaksysteem::Schema::LegacyFilestore",
  { id => "filestore_id" },
);

=head2 notitie_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Notitie>

=cut

__PACKAGE__->belongs_to(
  "notitie_id",
  "Zaaksysteem::Schema::Notitie",
  { id => "notitie_id" },
);

=head2 job_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Jobs>

=cut

__PACKAGE__->belongs_to("job_id", "Zaaksysteem::Schema::Jobs", { id => "job_id" });

=head2 zaak_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Zaak>

=cut

__PACKAGE__->belongs_to("zaak_id", "Zaaksysteem::Schema::Zaak", { id => "zaak_id" });

=head2 zaaktype_kenmerken_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ZaaktypeKenmerken>

=cut

__PACKAGE__->belongs_to(
  "zaaktype_kenmerken_id",
  "Zaaksysteem::Schema::ZaaktypeKenmerken",
  { id => "zaaktype_kenmerken_id" },
);

=head2 folder_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::LegacyDocumenten>

=cut

__PACKAGE__->belongs_to(
  "folder_id",
  "Zaaksysteem::Schema::LegacyDocumenten",
  { id => "folder_id" },
);

=head2 legacy_documentens

Type: has_many

Related object: L<Zaaksysteem::Schema::LegacyDocumenten>

=cut

__PACKAGE__->has_many(
  "legacy_documentens",
  "Zaaksysteem::Schema::LegacyDocumenten",
  { "foreign.folder_id" => "self.id" },
  {},
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-05-24 14:24:04
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:ENHfv9JC72b2pcsGp0Dnfg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
