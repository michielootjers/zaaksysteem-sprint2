package Zaaksysteem::Schema::Jobs;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::Jobs

=cut

__PACKAGE__->table("jobs");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'jobs_id_seq'

=head2 bericht

  data_type: 'text'
  is_nullable: 1

=head2 task_context

  data_type: 'text'
  is_nullable: 1

=head2 task

  data_type: 'text'
  is_nullable: 1

=head2 raw_input

  data_type: 'text'
  is_nullable: 1

=head2 raw_result

  data_type: 'text'
  is_nullable: 1

=head2 created

  data_type: 'timestamp'
  is_nullable: 1

=head2 last_modified

  data_type: 'timestamp'
  is_nullable: 1

=head2 deleted

  data_type: 'timestamp'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "jobs_id_seq",
  },
  "bericht",
  { data_type => "text", is_nullable => 1 },
  "task_context",
  { data_type => "text", is_nullable => 1 },
  "task",
  { data_type => "text", is_nullable => 1 },
  "raw_input",
  { data_type => "text", is_nullable => 1 },
  "raw_result",
  { data_type => "text", is_nullable => 1 },
  "created",
  { data_type => "timestamp", is_nullable => 1 },
  "last_modified",
  { data_type => "timestamp", is_nullable => 1 },
  "deleted",
  { data_type => "timestamp", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-06-28 09:06:43
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:1/TRpxzFNJA/pdfdgHTVfg

__PACKAGE__->resultset_class('Zaaksysteem::Documenten::ResultSetJobs');

__PACKAGE__->load_components(
    "+Zaaksysteem::Documenten::ComponentJobs",
    __PACKAGE__->load_components()
);

__PACKAGE__->has_many(
  "documenten",
  "Zaaksysteem::Schema::Documenten",
  { "foreign.job_id" => "self.id" },
);

__PACKAGE__->add_columns('last_modified',
    { %{ __PACKAGE__->column_info('last_modified') },
    set_on_update => 1,
    set_on_create => 1,
});

__PACKAGE__->add_columns('created',
    { %{ __PACKAGE__->column_info('created') },
    set_on_create => 1,
});





# You can replace this text with custom content, and it will be preserved on regeneration
1;
