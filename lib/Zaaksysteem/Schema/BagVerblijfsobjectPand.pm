package Zaaksysteem::Schema::BagVerblijfsobjectPand;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::BagVerblijfsobjectPand - koppeltabel voor panden bij verblijfsobject

=cut

__PACKAGE__->table("bag_verblijfsobject_pand");

=head1 ACCESSORS

=head2 identificatie

  data_type: 'varchar'
  is_nullable: 0
  size: 16

56.01 : de unieke aanduiding van een verblijfsobject

=head2 begindatum

  data_type: 'varchar'
  is_nullable: 0
  size: 14

56.91 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een verblijfsobject een wijziging hebben ondergaan.

=head2 pand

  data_type: 'varchar'
  is_nullable: 0
  size: 16

56.90 : de unieke aanduidingen van de panden waarvan het verblijfsobject onderdeel uitmaakt.

=head2 correctie

  data_type: 'varchar'
  is_nullable: 0
  size: 1

het gegeven is gecorrigeerd.

=cut

__PACKAGE__->add_columns(
  "identificatie",
  { data_type => "varchar", is_nullable => 0, size => 16 },
  "begindatum",
  { data_type => "varchar", is_nullable => 0, size => 14 },
  "pand",
  { data_type => "varchar", is_nullable => 0, size => 16 },
  "correctie",
  { data_type => "varchar", is_nullable => 0, size => 1 },
);
__PACKAGE__->set_primary_key("identificatie", "begindatum", "correctie", "pand");


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-01-10 07:42:00
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:SZw+e/W4ujVvacXMpn8BoQ

__PACKAGE__->belongs_to(
  "pand",
  "Zaaksysteem::Schema::BagPand",
  { "identificatie" => "pand" },
);





# You can replace this text with custom content, and it will be preserved on regeneration
1;
