package Zaaksysteem::Schema::KennisbankProducten;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::KennisbankProducten

=cut

__PACKAGE__->table("kennisbank_producten");

=head1 ACCESSORS

=head2 search_index

  data_type: 'tsvector'
  is_nullable: 1

=head2 search_term

  data_type: 'text'
  is_nullable: 1

=head2 object_type

  data_type: 'varchar'
  default_value: 'kennisbank_producten'
  is_nullable: 1
  size: 100

=head2 searchable_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'searchable_searchable_id_seq'

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'kennisbank_producten_id_seq'

=head2 pid

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 bibliotheek_categorie_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 naam

  data_type: 'text'
  is_nullable: 1

=head2 omschrijving

  data_type: 'text'
  is_nullable: 1

=head2 voorwaarden

  data_type: 'text'
  is_nullable: 1

=head2 aanpak

  data_type: 'text'
  is_nullable: 1

=head2 kosten

  data_type: 'text'
  is_nullable: 1

=head2 versie

  data_type: 'integer'
  is_nullable: 1

=head2 author

  data_type: 'text'
  is_nullable: 1

=head2 commit_message

  data_type: 'text'
  is_nullable: 1

=head2 active

  data_type: 'boolean'
  is_nullable: 1

=head2 created

  data_type: 'timestamp'
  is_nullable: 1

=head2 last_modified

  data_type: 'timestamp'
  is_nullable: 1

=head2 deleted

  data_type: 'timestamp'
  is_nullable: 1

=head2 omschrijving_internal

  data_type: 'text'
  is_nullable: 1

=head2 voorwaarden_internal

  data_type: 'text'
  is_nullable: 1

=head2 aanpak_internal

  data_type: 'text'
  is_nullable: 1

=head2 kosten_internal

  data_type: 'text'
  is_nullable: 1

=head2 external

  data_type: 'boolean'
  is_nullable: 1

=head2 internal

  data_type: 'boolean'
  is_nullable: 1

=head2 publication

  data_type: 'varchar'
  is_nullable: 1
  size: 32

=head2 coupling

  data_type: 'varchar'
  is_nullable: 1
  size: 32

=head2 external_id

  data_type: 'varchar'
  is_nullable: 1
  size: 128

=cut

__PACKAGE__->add_columns(
  "search_index",
  { data_type => "tsvector", is_nullable => 1 },
  "search_term",
  { data_type => "text", is_nullable => 1 },
  "object_type",
  {
    data_type => "varchar",
    default_value => "kennisbank_producten",
    is_nullable => 1,
    size => 100,
  },
  "searchable_id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "searchable_searchable_id_seq",
  },
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "kennisbank_producten_id_seq",
  },
  "pid",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "bibliotheek_categorie_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "naam",
  { data_type => "text", is_nullable => 1 },
  "omschrijving",
  { data_type => "text", is_nullable => 1 },
  "voorwaarden",
  { data_type => "text", is_nullable => 1 },
  "aanpak",
  { data_type => "text", is_nullable => 1 },
  "kosten",
  { data_type => "text", is_nullable => 1 },
  "versie",
  { data_type => "integer", is_nullable => 1 },
  "author",
  { data_type => "text", is_nullable => 1 },
  "commit_message",
  { data_type => "text", is_nullable => 1 },
  "active",
  { data_type => "boolean", is_nullable => 1 },
  "created",
  { data_type => "timestamp", is_nullable => 1 },
  "last_modified",
  { data_type => "timestamp", is_nullable => 1 },
  "deleted",
  { data_type => "timestamp", is_nullable => 1 },
  "omschrijving_internal",
  { data_type => "text", is_nullable => 1 },
  "voorwaarden_internal",
  { data_type => "text", is_nullable => 1 },
  "aanpak_internal",
  { data_type => "text", is_nullable => 1 },
  "kosten_internal",
  { data_type => "text", is_nullable => 1 },
  "external",
  { data_type => "boolean", is_nullable => 1 },
  "internal",
  { data_type => "boolean", is_nullable => 1 },
  "publication",
  { data_type => "varchar", is_nullable => 1, size => 32 },
  "coupling",
  { data_type => "varchar", is_nullable => 1, size => 32 },
  "external_id",
  { data_type => "varchar", is_nullable => 1, size => 128 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 pid

Type: belongs_to

Related object: L<Zaaksysteem::Schema::KennisbankProducten>

=cut

__PACKAGE__->belongs_to(
  "pid",
  "Zaaksysteem::Schema::KennisbankProducten",
  { id => "pid" },
);

=head2 kennisbank_productens

Type: has_many

Related object: L<Zaaksysteem::Schema::KennisbankProducten>

=cut

__PACKAGE__->has_many(
  "kennisbank_productens",
  "Zaaksysteem::Schema::KennisbankProducten",
  { "foreign.pid" => "self.id" },
  {},
);

=head2 bibliotheek_categorie_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::BibliotheekCategorie>

=cut

__PACKAGE__->belongs_to(
  "bibliotheek_categorie_id",
  "Zaaksysteem::Schema::BibliotheekCategorie",
  { id => "bibliotheek_categorie_id" },
);

=head2 kennisbank_relaties

Type: has_many

Related object: L<Zaaksysteem::Schema::KennisbankRelaties>

=cut

__PACKAGE__->has_many(
  "kennisbank_relaties",
  "Zaaksysteem::Schema::KennisbankRelaties",
  { "foreign.kennisbank_producten_id" => "self.id" },
  {},
);

=head2 seens

Type: has_many

Related object: L<Zaaksysteem::Schema::Seen>

=cut

__PACKAGE__->has_many(
  "seens",
  "Zaaksysteem::Schema::Seen",
  { "foreign.kennisbank_producten_id" => "self.id" },
  {},
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-05-24 14:24:04
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:6mrrXJO13dA9H2KmTPW6hw

__PACKAGE__->resultset_class('Zaaksysteem::Kennisbank::ResultSetGeneric');

__PACKAGE__->load_components(
    "+Zaaksysteem::Kennisbank::ComponentGeneric",
    __PACKAGE__->load_components()
);

__PACKAGE__->add_columns('last_modified',
    { %{ __PACKAGE__->column_info('last_modified') },
    set_on_update => 1,
    set_on_create => 1,
});

__PACKAGE__->add_columns('created',
    { %{ __PACKAGE__->column_info('created') },
    set_on_create => 1,
});





# You can replace this text with custom content, and it will be preserved on regeneration
1;
