package Zaaksysteem::Schema::NatuurlijkPersoon;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::NatuurlijkPersoon

=cut

__PACKAGE__->table("natuurlijk_persoon");

=head1 ACCESSORS

=head2 search_index

  data_type: 'tsvector'
  is_nullable: 1

=head2 search_term

  data_type: 'text'
  is_nullable: 1

=head2 object_type

  data_type: 'varchar'
  default_value: 'natuurlijk_persoon'
  is_nullable: 1
  size: 100

=head2 searchable_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'searchable_searchable_id_seq'

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'natuurlijk_persoon_id_seq'

=head2 burgerservicenummer

  data_type: 'varchar'
  is_nullable: 1
  size: 9

=head2 a_nummer

  data_type: 'varchar'
  is_nullable: 1
  size: 10

=head2 voorletters

  data_type: 'varchar'
  is_nullable: 1
  size: 10

=head2 voornamen

  data_type: 'varchar'
  is_nullable: 1
  size: 200

=head2 geslachtsnaam

  data_type: 'varchar'
  is_nullable: 1
  size: 200

=head2 voorvoegsel

  data_type: 'varchar'
  is_nullable: 1
  size: 50

=head2 geslachtsaanduiding

  data_type: 'varchar'
  is_nullable: 1
  size: 3

=head2 nationaliteitscode1

  data_type: 'smallint'
  is_nullable: 1

=head2 nationaliteitscode2

  data_type: 'smallint'
  is_nullable: 1

=head2 nationaliteitscode3

  data_type: 'smallint'
  is_nullable: 1

=head2 geboortegemeente

  data_type: 'varchar'
  is_nullable: 1
  size: 75

=head2 geboorteplaats

  data_type: 'varchar'
  is_nullable: 1
  size: 75

=head2 geboortegemeente_omschrijving

  data_type: 'varchar'
  is_nullable: 1
  size: 150

=head2 geboorteregio

  data_type: 'varchar'
  is_nullable: 1
  size: 150

=head2 geboorteland

  data_type: 'varchar'
  is_nullable: 1
  size: 75

=head2 geboortedatum

  data_type: 'timestamp'
  is_nullable: 1

=head2 aanhef_aanschrijving

  data_type: 'varchar'
  is_nullable: 1
  size: 10

=head2 voorletters_aanschrijving

  data_type: 'varchar'
  is_nullable: 1
  size: 20

=head2 voornamen_aanschrijving

  data_type: 'varchar'
  is_nullable: 1
  size: 200

=head2 naam_aanschrijving

  data_type: 'varchar'
  is_nullable: 1
  size: 200

=head2 voorvoegsel_aanschrijving

  data_type: 'varchar'
  is_nullable: 1
  size: 50

=head2 burgerlijke_staat

  data_type: 'char'
  is_nullable: 1
  size: 1

=head2 indicatie_gezag

  data_type: 'varchar'
  is_nullable: 1
  size: 2

=head2 indicatie_curatele

  data_type: 'char'
  is_nullable: 1
  size: 1

=head2 indicatie_geheim

  data_type: 'char'
  is_nullable: 1
  size: 1

=head2 aanduiding_verblijfsrecht

  data_type: 'smallint'
  is_nullable: 1

=head2 datum_aanvang_verblijfsrecht

  data_type: 'date'
  is_nullable: 1

=head2 datum_einde_verblijfsrecht

  data_type: 'date'
  is_nullable: 1

=head2 aanduiding_soort_vreemdeling

  data_type: 'varchar'
  is_nullable: 1
  size: 10

=head2 land_vanwaar_ingeschreven

  data_type: 'smallint'
  is_nullable: 1

=head2 land_waarnaar_vertrokken

  data_type: 'smallint'
  is_nullable: 1

=head2 adres_buitenland1

  data_type: 'varchar'
  is_nullable: 1
  size: 35

=head2 adres_buitenland2

  data_type: 'varchar'
  is_nullable: 1
  size: 35

=head2 adres_buitenland3

  data_type: 'varchar'
  is_nullable: 1
  size: 35

=head2 nnp_ts

  data_type: 'varchar'
  is_nullable: 1
  size: 32

=head2 hash

  data_type: 'varchar'
  is_nullable: 1
  size: 32

=head2 import_datum

  data_type: 'timestamp'
  is_nullable: 1

=head2 adres_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 email

  data_type: 'varchar'
  is_nullable: 1
  size: 32

=head2 telefoon

  data_type: 'varchar'
  is_nullable: 1
  size: 32

=head2 authenticated

  data_type: 'boolean'
  is_nullable: 1

=head2 authenticatedby

  data_type: 'text'
  is_nullable: 1

=head2 deleted_on

  data_type: 'timestamp'
  is_nullable: 1

=head2 verblijfsobject_id

  data_type: 'varchar'
  is_nullable: 1
  size: 16

=head2 datum_overlijden

  data_type: 'timestamp'
  is_nullable: 1

=head2 aanduiding_naamgebruik

  data_type: 'varchar'
  is_nullable: 1
  size: 1

=head2 onderzoek_persoon

  data_type: 'boolean'
  is_nullable: 1

=head2 onderzoek_persoon_ingang

  data_type: 'timestamp'
  is_nullable: 1

=head2 onderzoek_persoon_einde

  data_type: 'timestamp'
  is_nullable: 1

=head2 onderzoek_persoon_onjuist

  data_type: 'boolean'
  is_nullable: 1

=head2 onderzoek_huwelijk

  data_type: 'boolean'
  is_nullable: 1

=head2 onderzoek_huwelijk_ingang

  data_type: 'timestamp'
  is_nullable: 1

=head2 onderzoek_huwelijk_einde

  data_type: 'timestamp'
  is_nullable: 1

=head2 onderzoek_huwelijk_onjuist

  data_type: 'boolean'
  is_nullable: 1

=head2 onderzoek_overlijden

  data_type: 'boolean'
  is_nullable: 1

=head2 onderzoek_overlijden_ingang

  data_type: 'timestamp'
  is_nullable: 1

=head2 onderzoek_overlijden_einde

  data_type: 'timestamp'
  is_nullable: 1

=head2 onderzoek_overlijden_onjuist

  data_type: 'boolean'
  is_nullable: 1

=head2 onderzoek_verblijfplaats

  data_type: 'boolean'
  is_nullable: 1

=head2 onderzoek_verblijfplaats_ingang

  data_type: 'timestamp'
  is_nullable: 1

=head2 onderzoek_verblijfplaats_einde

  data_type: 'timestamp'
  is_nullable: 1

=head2 onderzoek_verblijfplaats_onjuist

  data_type: 'boolean'
  is_nullable: 1

=head2 partner_a_nummer

  data_type: 'varchar'
  is_nullable: 1
  size: 50

=head2 partner_burgerservicenummer

  data_type: 'varchar'
  is_nullable: 1
  size: 50

=head2 partner_voorvoegsel

  data_type: 'varchar'
  is_nullable: 1
  size: 50

=head2 partner_geslachtsnaam

  data_type: 'varchar'
  is_nullable: 1
  size: 50

=head2 datum_huwelijk

  data_type: 'timestamp'
  is_nullable: 1

=head2 datum_huwelijk_ontbinding

  data_type: 'timestamp'
  is_nullable: 1

=head2 system_of_record

  data_type: 'varchar'
  is_nullable: 1
  size: 32

=head2 system_of_record_id

  data_type: 'bigint'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "search_index",
  { data_type => "tsvector", is_nullable => 1 },
  "search_term",
  { data_type => "text", is_nullable => 1 },
  "object_type",
  {
    data_type => "varchar",
    default_value => "natuurlijk_persoon",
    is_nullable => 1,
    size => 100,
  },
  "searchable_id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "searchable_searchable_id_seq",
  },
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "natuurlijk_persoon_id_seq",
  },
  "burgerservicenummer",
  { data_type => "varchar", is_nullable => 1, size => 9 },
  "a_nummer",
  { data_type => "varchar", is_nullable => 1, size => 10 },
  "voorletters",
  { data_type => "varchar", is_nullable => 1, size => 10 },
  "voornamen",
  { data_type => "varchar", is_nullable => 1, size => 200 },
  "geslachtsnaam",
  { data_type => "varchar", is_nullable => 1, size => 200 },
  "voorvoegsel",
  { data_type => "varchar", is_nullable => 1, size => 50 },
  "geslachtsaanduiding",
  { data_type => "varchar", is_nullable => 1, size => 3 },
  "nationaliteitscode1",
  { data_type => "smallint", is_nullable => 1 },
  "nationaliteitscode2",
  { data_type => "smallint", is_nullable => 1 },
  "nationaliteitscode3",
  { data_type => "smallint", is_nullable => 1 },
  "geboortegemeente",
  { data_type => "varchar", is_nullable => 1, size => 75 },
  "geboorteplaats",
  { data_type => "varchar", is_nullable => 1, size => 75 },
  "geboortegemeente_omschrijving",
  { data_type => "varchar", is_nullable => 1, size => 150 },
  "geboorteregio",
  { data_type => "varchar", is_nullable => 1, size => 150 },
  "geboorteland",
  { data_type => "varchar", is_nullable => 1, size => 75 },
  "geboortedatum",
  { data_type => "timestamp", is_nullable => 1 },
  "aanhef_aanschrijving",
  { data_type => "varchar", is_nullable => 1, size => 10 },
  "voorletters_aanschrijving",
  { data_type => "varchar", is_nullable => 1, size => 20 },
  "voornamen_aanschrijving",
  { data_type => "varchar", is_nullable => 1, size => 200 },
  "naam_aanschrijving",
  { data_type => "varchar", is_nullable => 1, size => 200 },
  "voorvoegsel_aanschrijving",
  { data_type => "varchar", is_nullable => 1, size => 50 },
  "burgerlijke_staat",
  { data_type => "char", is_nullable => 1, size => 1 },
  "indicatie_gezag",
  { data_type => "varchar", is_nullable => 1, size => 2 },
  "indicatie_curatele",
  { data_type => "char", is_nullable => 1, size => 1 },
  "indicatie_geheim",
  { data_type => "char", is_nullable => 1, size => 1 },
  "aanduiding_verblijfsrecht",
  { data_type => "smallint", is_nullable => 1 },
  "datum_aanvang_verblijfsrecht",
  { data_type => "date", is_nullable => 1 },
  "datum_einde_verblijfsrecht",
  { data_type => "date", is_nullable => 1 },
  "aanduiding_soort_vreemdeling",
  { data_type => "varchar", is_nullable => 1, size => 10 },
  "land_vanwaar_ingeschreven",
  { data_type => "smallint", is_nullable => 1 },
  "land_waarnaar_vertrokken",
  { data_type => "smallint", is_nullable => 1 },
  "adres_buitenland1",
  { data_type => "varchar", is_nullable => 1, size => 35 },
  "adres_buitenland2",
  { data_type => "varchar", is_nullable => 1, size => 35 },
  "adres_buitenland3",
  { data_type => "varchar", is_nullable => 1, size => 35 },
  "nnp_ts",
  { data_type => "varchar", is_nullable => 1, size => 32 },
  "hash",
  { data_type => "varchar", is_nullable => 1, size => 32 },
  "import_datum",
  { data_type => "timestamp", is_nullable => 1 },
  "adres_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "email",
  { data_type => "varchar", is_nullable => 1, size => 32 },
  "telefoon",
  { data_type => "varchar", is_nullable => 1, size => 32 },
  "authenticated",
  { data_type => "boolean", is_nullable => 1 },
  "authenticatedby",
  { data_type => "text", is_nullable => 1 },
  "deleted_on",
  { data_type => "timestamp", is_nullable => 1 },
  "verblijfsobject_id",
  { data_type => "varchar", is_nullable => 1, size => 16 },
  "datum_overlijden",
  { data_type => "timestamp", is_nullable => 1 },
  "aanduiding_naamgebruik",
  { data_type => "varchar", is_nullable => 1, size => 1 },
  "onderzoek_persoon",
  { data_type => "boolean", is_nullable => 1 },
  "onderzoek_persoon_ingang",
  { data_type => "timestamp", is_nullable => 1 },
  "onderzoek_persoon_einde",
  { data_type => "timestamp", is_nullable => 1 },
  "onderzoek_persoon_onjuist",
  { data_type => "boolean", is_nullable => 1 },
  "onderzoek_huwelijk",
  { data_type => "boolean", is_nullable => 1 },
  "onderzoek_huwelijk_ingang",
  { data_type => "timestamp", is_nullable => 1 },
  "onderzoek_huwelijk_einde",
  { data_type => "timestamp", is_nullable => 1 },
  "onderzoek_huwelijk_onjuist",
  { data_type => "boolean", is_nullable => 1 },
  "onderzoek_overlijden",
  { data_type => "boolean", is_nullable => 1 },
  "onderzoek_overlijden_ingang",
  { data_type => "timestamp", is_nullable => 1 },
  "onderzoek_overlijden_einde",
  { data_type => "timestamp", is_nullable => 1 },
  "onderzoek_overlijden_onjuist",
  { data_type => "boolean", is_nullable => 1 },
  "onderzoek_verblijfplaats",
  { data_type => "boolean", is_nullable => 1 },
  "onderzoek_verblijfplaats_ingang",
  { data_type => "timestamp", is_nullable => 1 },
  "onderzoek_verblijfplaats_einde",
  { data_type => "timestamp", is_nullable => 1 },
  "onderzoek_verblijfplaats_onjuist",
  { data_type => "boolean", is_nullable => 1 },
  "partner_a_nummer",
  { data_type => "varchar", is_nullable => 1, size => 50 },
  "partner_burgerservicenummer",
  { data_type => "varchar", is_nullable => 1, size => 50 },
  "partner_voorvoegsel",
  { data_type => "varchar", is_nullable => 1, size => 50 },
  "partner_geslachtsnaam",
  { data_type => "varchar", is_nullable => 1, size => 50 },
  "datum_huwelijk",
  { data_type => "timestamp", is_nullable => 1 },
  "datum_huwelijk_ontbinding",
  { data_type => "timestamp", is_nullable => 1 },
  "system_of_record",
  { data_type => "varchar", is_nullable => 1, size => 32 },
  "system_of_record_id",
  { data_type => "bigint", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 adres_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Adres>

=cut

__PACKAGE__->belongs_to("adres_id", "Zaaksysteem::Schema::Adres", { id => "adres_id" });


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-04-05 13:53:08
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:z/DAYlRoTC73KImLtnNc4w

__PACKAGE__->load_components(
    "+Zaaksysteem::DB::Component::NatuurlijkPersoon",
    __PACKAGE__->load_components()
);

__PACKAGE__->resultset_class('Zaaksysteem::SBUS::ResultSet::NatuurlijkPersoon');

__PACKAGE__->add_columns('import_datum',
    { %{ __PACKAGE__->column_info('import_datum') },
    set_on_create => 1,
});

__PACKAGE__->belongs_to(
  "verblijfsobject_id",
  "Zaaksysteem::Schema::BagVerblijfsobject",
  { "identificatie" => "verblijfsobject_id" },
);

__PACKAGE__->load_components(
    "+Zaaksysteem::DB::Component::GmNatuurlijkPersoon",
    __PACKAGE__->load_components()
);




# You can replace this text with custom content, and it will be preserved on regeneration
1;
