package Zaaksysteem::Schema::BibliotheekCategorie;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::BibliotheekCategorie

=cut

__PACKAGE__->table("bibliotheek_categorie");

=head1 ACCESSORS

=head2 search_index

  data_type: 'tsvector'
  is_nullable: 1

=head2 search_term

  data_type: 'text'
  is_nullable: 1

=head2 object_type

  data_type: 'varchar'
  default_value: 'bibliotheek_categorie'
  is_nullable: 1
  size: 100

=head2 searchable_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'searchable_searchable_id_seq'

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'bibliotheek_categorie_id_seq'

=head2 naam

  data_type: 'varchar'
  is_nullable: 1
  size: 256

=head2 label

  data_type: 'text'
  is_nullable: 1

=head2 description

  data_type: 'text'
  is_nullable: 1

=head2 help

  data_type: 'text'
  is_nullable: 1

=head2 created

  data_type: 'timestamp'
  is_nullable: 1

=head2 last_modified

  data_type: 'timestamp'
  is_nullable: 1

=head2 system

  data_type: 'integer'
  is_nullable: 1

=head2 pid

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "search_index",
  { data_type => "tsvector", is_nullable => 1 },
  "search_term",
  { data_type => "text", is_nullable => 1 },
  "object_type",
  {
    data_type => "varchar",
    default_value => "bibliotheek_categorie",
    is_nullable => 1,
    size => 100,
  },
  "searchable_id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "searchable_searchable_id_seq",
  },
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "bibliotheek_categorie_id_seq",
  },
  "naam",
  { data_type => "varchar", is_nullable => 1, size => 256 },
  "label",
  { data_type => "text", is_nullable => 1 },
  "description",
  { data_type => "text", is_nullable => 1 },
  "help",
  { data_type => "text", is_nullable => 1 },
  "created",
  { data_type => "timestamp", is_nullable => 1 },
  "last_modified",
  { data_type => "timestamp", is_nullable => 1 },
  "system",
  { data_type => "integer", is_nullable => 1 },
  "pid",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 pid

Type: belongs_to

Related object: L<Zaaksysteem::Schema::BibliotheekCategorie>

=cut

__PACKAGE__->belongs_to(
  "pid",
  "Zaaksysteem::Schema::BibliotheekCategorie",
  { id => "pid" },
);

=head2 bibliotheek_categories

Type: has_many

Related object: L<Zaaksysteem::Schema::BibliotheekCategorie>

=cut

__PACKAGE__->has_many(
  "bibliotheek_categories",
  "Zaaksysteem::Schema::BibliotheekCategorie",
  { "foreign.pid" => "self.id" },
  {},
);

=head2 bibliotheek_kenmerkens

Type: has_many

Related object: L<Zaaksysteem::Schema::BibliotheekKenmerken>

=cut

__PACKAGE__->has_many(
  "bibliotheek_kenmerkens",
  "Zaaksysteem::Schema::BibliotheekKenmerken",
  { "foreign.bibliotheek_categorie_id" => "self.id" },
  {},
);

=head2 bibliotheek_notificaties

Type: has_many

Related object: L<Zaaksysteem::Schema::BibliotheekNotificaties>

=cut

__PACKAGE__->has_many(
  "bibliotheek_notificaties",
  "Zaaksysteem::Schema::BibliotheekNotificaties",
  { "foreign.bibliotheek_categorie_id" => "self.id" },
  {},
);

=head2 bibliotheek_sjablonens

Type: has_many

Related object: L<Zaaksysteem::Schema::BibliotheekSjablonen>

=cut

__PACKAGE__->has_many(
  "bibliotheek_sjablonens",
  "Zaaksysteem::Schema::BibliotheekSjablonen",
  { "foreign.bibliotheek_categorie_id" => "self.id" },
  {},
);

=head2 kennisbank_productens

Type: has_many

Related object: L<Zaaksysteem::Schema::KennisbankProducten>

=cut

__PACKAGE__->has_many(
  "kennisbank_productens",
  "Zaaksysteem::Schema::KennisbankProducten",
  { "foreign.bibliotheek_categorie_id" => "self.id" },
  {},
);

=head2 kennisbank_vragens

Type: has_many

Related object: L<Zaaksysteem::Schema::KennisbankVragen>

=cut

__PACKAGE__->has_many(
  "kennisbank_vragens",
  "Zaaksysteem::Schema::KennisbankVragen",
  { "foreign.bibliotheek_categorie_id" => "self.id" },
  {},
);

=head2 zaaktypes

Type: has_many

Related object: L<Zaaksysteem::Schema::Zaaktype>

=cut

__PACKAGE__->has_many(
  "zaaktypes",
  "Zaaksysteem::Schema::Zaaktype",
  { "foreign.bibliotheek_categorie_id" => "self.id" },
  {},
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-01-10 07:42:00
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:7GvTw1HB4ry7HNKJNXnj3A

__PACKAGE__->resultset_class('Zaaksysteem::DB::ResultSet::BibliotheekCategorie');

__PACKAGE__->load_components(
    "+Zaaksysteem::DB::Component::BibliotheekCategorie",
    "+DBIx::Class::Helper::Row::ToJSON",
    __PACKAGE__->load_components()
);


__PACKAGE__->has_many(
  "categorien",
  "Zaaksysteem::Schema::BibliotheekCategorie",
  { "foreign.pid" => "self.id" },
);




# You can replace this text with custom content, and it will be preserved on regeneration
1;
