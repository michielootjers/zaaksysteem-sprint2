package Zaaksysteem::Schema::BeheerPlugins;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::BeheerPlugins

=cut

__PACKAGE__->table("beheer_plugins");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'beheer_plugins_id_seq'

=head2 label

  data_type: 'text'
  is_nullable: 1

=head2 naam

  data_type: 'text'
  is_nullable: 1

=head2 help

  data_type: 'text'
  is_nullable: 1

=head2 versie

  data_type: 'text'
  is_nullable: 1

=head2 actief

  data_type: 'integer'
  is_nullable: 1

=head2 last_modified

  data_type: 'timestamp'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "beheer_plugins_id_seq",
  },
  "label",
  { data_type => "text", is_nullable => 1 },
  "naam",
  { data_type => "text", is_nullable => 1 },
  "help",
  { data_type => "text", is_nullable => 1 },
  "versie",
  { data_type => "text", is_nullable => 1 },
  "actief",
  { data_type => "integer", is_nullable => 1 },
  "last_modified",
  { data_type => "timestamp", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-01-10 07:42:00
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:0VKnyQDfgxl0bxj7DqfLZQ





# You can replace this text with custom content, and it will be preserved on regeneration
1;
