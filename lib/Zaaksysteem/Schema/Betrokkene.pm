package Zaaksysteem::Schema::Betrokkene;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "Core");
__PACKAGE__->table("betrokkene");
__PACKAGE__->add_columns(
  "id",
  {
    data_type => "integer",
    default_value => "nextval('betrokkene_id_seq'::regclass)",
    is_nullable => 0,
    size => 4,
  },
  "btype",
  {
    data_type => "character varying",
    default_value => undef,
    is_nullable => 1,
    size => 50,
  },
  "gm_natuurlijk_persoon_id",
  { data_type => "integer", default_value => undef, is_nullable => 1, size => 4 },
  "gm_bedrijf_id",
  { data_type => "integer", default_value => undef, is_nullable => 1, size => 4 },
  "medewerker_id",
  { data_type => "integer", default_value => undef, is_nullable => 1, size => 4 },
  "org_eenheid_id",
  { data_type => "integer", default_value => undef, is_nullable => 1, size => 4 },
  "naam",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->add_unique_constraint("betrokkene_pkey", ["id"]);
__PACKAGE__->has_many(
  "auth_users",
  "Zaaksysteem::Schema::AuthUsers",
  { "foreign.betrokkene_id" => "self.id" },
);
__PACKAGE__->belongs_to(
  "gm_natuurlijk_persoon_id",
  "Zaaksysteem::Schema::GmNatuurlijkPersoon",
  { id => "gm_natuurlijk_persoon_id" },
);
__PACKAGE__->belongs_to(
  "gm_bedrijf_id",
  "Zaaksysteem::Schema::GmBedrijf",
  { id => "gm_bedrijf_id" },
);


# Created by DBIx::Class::Schema::Loader v0.04006 @ 2012-05-16 12:12:12
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:56xJ2PcnsKFpd4ihIsWMVQ


# You can replace this text with custom content, and it will be preserved on regeneration
1;
