package Zaaksysteem::Schema::ZaakKenmerk;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::ZaakKenmerk

=cut

__PACKAGE__->table("zaak_kenmerk");

=head1 ACCESSORS

=head2 zaak_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 bibliotheek_kenmerken_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 value

  data_type: 'text'
  is_nullable: 0

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'zaak_kenmerk_id_seq'

=cut

__PACKAGE__->add_columns(
  "zaak_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "bibliotheek_kenmerken_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "value",
  { data_type => "text", is_nullable => 0 },
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "zaak_kenmerk_id_seq",
  },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->add_unique_constraint(
  "zaak_kenmerk_zaak_id_key",
  ["zaak_id", "bibliotheek_kenmerken_id", "value"],
);

=head1 RELATIONS

=head2 bibliotheek_kenmerken_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::BibliotheekKenmerken>

=cut

__PACKAGE__->belongs_to(
  "bibliotheek_kenmerken_id",
  "Zaaksysteem::Schema::BibliotheekKenmerken",
  { id => "bibliotheek_kenmerken_id" },
);

=head2 zaak_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Zaak>

=cut

__PACKAGE__->belongs_to("zaak_id", "Zaaksysteem::Schema::Zaak", { id => "zaak_id" });


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-06-28 09:06:43
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:HBmrsL6J8NscEvshtROfgw

__PACKAGE__->resultset_class('Zaaksysteem::Zaken::ResultSetZaakKenmerk');

__PACKAGE__->load_components(
    "+Zaaksysteem::Zaken::ComponentZaakKenmerk",
    __PACKAGE__->load_components()
);






# You can replace this text with custom content, and it will be preserved on regeneration
1;
