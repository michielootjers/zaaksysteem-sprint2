package Zaaksysteem::Schema::Betrokkenen;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::Betrokkenen

=cut

__PACKAGE__->table("betrokkenen");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'betrokkenen_id_seq'

=head2 btype

  data_type: 'integer'
  is_nullable: 1

=head2 gm_natuurlijk_persoon_id

  data_type: 'integer'
  is_nullable: 1

=head2 naam

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "betrokkenen_id_seq",
  },
  "btype",
  { data_type => "integer", is_nullable => 1 },
  "gm_natuurlijk_persoon_id",
  { data_type => "integer", is_nullable => 1 },
  "naam",
  { data_type => "text", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-01-10 07:42:00
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:kmjURXX+usBjGbLYtFIbCQ





# You can replace this text with custom content, and it will be preserved on regeneration
1;
