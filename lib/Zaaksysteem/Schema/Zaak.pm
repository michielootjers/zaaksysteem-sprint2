package Zaaksysteem::Schema::Zaak;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::Zaak

=cut

__PACKAGE__->table("zaak");

=head1 ACCESSORS

=head2 search_index

  data_type: 'tsvector'
  is_nullable: 1

=head2 search_term

  data_type: 'text'
  is_nullable: 1

=head2 object_type

  data_type: 'varchar'
  default_value: 'zaak'
  is_nullable: 1
  size: 100

=head2 searchable_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'searchable_searchable_id_seq'

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'zaak_id_seq'

=head2 pid

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 relates_to

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 zaaktype_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 zaaktype_node_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 status

  data_type: 'enum'
  extra: {custom_type_name => "zaaksysteem_status",list => ["new","open","resolved","stalled","deleted","overdragen"]}
  is_nullable: 0

=head2 milestone

  data_type: 'integer'
  is_nullable: 0

=head2 contactkanaal

  data_type: 'varchar'
  is_nullable: 0
  size: 128

=head2 aanvraag_trigger

  data_type: 'enum'
  extra: {custom_type_name => "zaaksysteem_trigger",list => ["extern","intern"]}
  is_nullable: 0

=head2 onderwerp

  data_type: 'varchar'
  is_nullable: 1
  size: 256

=head2 resultaat

  data_type: 'text'
  is_nullable: 1

=head2 besluit

  data_type: 'text'
  is_nullable: 1

=head2 coordinator

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 behandelaar

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 aanvrager

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 route_ou

  data_type: 'integer'
  is_nullable: 1

=head2 route_role

  data_type: 'integer'
  is_nullable: 1

=head2 locatie_zaak

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 locatie_correspondentie

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 streefafhandeldatum

  data_type: 'timestamp'
  is_nullable: 1

=head2 registratiedatum

  data_type: 'timestamp'
  is_nullable: 0

=head2 afhandeldatum

  data_type: 'timestamp'
  is_nullable: 1

=head2 vernietigingsdatum

  data_type: 'timestamp'
  is_nullable: 1

=head2 created

  data_type: 'timestamp'
  is_nullable: 0

=head2 last_modified

  data_type: 'timestamp'
  is_nullable: 0

=head2 deleted

  data_type: 'timestamp'
  is_nullable: 1

=head2 vervolg_van

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 aanvrager_gm_id

  data_type: 'integer'
  is_nullable: 1

=head2 behandelaar_gm_id

  data_type: 'integer'
  is_nullable: 1

=head2 coordinator_gm_id

  data_type: 'integer'
  is_nullable: 1

=head2 uuid

  data_type: 'uuid'
  is_nullable: 1
  size: 16

=cut

__PACKAGE__->add_columns(
  "search_index",
  { data_type => "tsvector", is_nullable => 1 },
  "search_term",
  { data_type => "text", is_nullable => 1 },
  "object_type",
  {
    data_type => "varchar",
    default_value => "zaak",
    is_nullable => 1,
    size => 100,
  },
  "searchable_id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "searchable_searchable_id_seq",
  },
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "zaak_id_seq",
  },
  "pid",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "relates_to",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "zaaktype_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "zaaktype_node_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "status",
  {
    data_type => "enum",
    extra => {
      custom_type_name => "zaaksysteem_status",
      list => ["new", "open", "resolved", "stalled", "deleted", "overdragen"],
    },
    is_nullable => 0,
  },
  "milestone",
  { data_type => "integer", is_nullable => 0 },
  "contactkanaal",
  { data_type => "varchar", is_nullable => 0, size => 128 },
  "aanvraag_trigger",
  {
    data_type => "enum",
    extra => {
      custom_type_name => "zaaksysteem_trigger",
      list => ["extern", "intern"],
    },
    is_nullable => 0,
  },
  "onderwerp",
  { data_type => "varchar", is_nullable => 1, size => 256 },
  "resultaat",
  { data_type => "text", is_nullable => 1 },
  "besluit",
  { data_type => "text", is_nullable => 1 },
  "coordinator",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "behandelaar",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "aanvrager",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "route_ou",
  { data_type => "integer", is_nullable => 1 },
  "route_role",
  { data_type => "integer", is_nullable => 1 },
  "locatie_zaak",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "locatie_correspondentie",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "streefafhandeldatum",
  { data_type => "timestamp", is_nullable => 1 },
  "registratiedatum",
  { data_type => "timestamp", is_nullable => 0 },
  "afhandeldatum",
  { data_type => "timestamp", is_nullable => 1 },
  "vernietigingsdatum",
  { data_type => "timestamp", is_nullable => 1 },
  "created",
  { data_type => "timestamp", is_nullable => 0 },
  "last_modified",
  { data_type => "timestamp", is_nullable => 0 },
  "deleted",
  { data_type => "timestamp", is_nullable => 1 },
  "vervolg_van",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "aanvrager_gm_id",
  { data_type => "integer", is_nullable => 1 },
  "behandelaar_gm_id",
  { data_type => "integer", is_nullable => 1 },
  "coordinator_gm_id",
  { data_type => "integer", is_nullable => 1 },
  "uuid",
  { data_type => "uuid", is_nullable => 1, size => 16 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 case_actions

Type: has_many

Related object: L<Zaaksysteem::Schema::CaseAction>

=cut

__PACKAGE__->has_many(
  "case_actions",
  "Zaaksysteem::Schema::CaseAction",
  { "foreign.case_id" => "self.id" },
  {},
);

=head2 case_relation_case_id_as

Type: has_many

Related object: L<Zaaksysteem::Schema::CaseRelation>

=cut

__PACKAGE__->has_many(
  "case_relation_case_id_as",
  "Zaaksysteem::Schema::CaseRelation",
  { "foreign.case_id_a" => "self.id" },
  {},
);

=head2 case_relation_case_id_bs

Type: has_many

Related object: L<Zaaksysteem::Schema::CaseRelation>

=cut

__PACKAGE__->has_many(
  "case_relation_case_id_bs",
  "Zaaksysteem::Schema::CaseRelation",
  { "foreign.case_id_b" => "self.id" },
  {},
);

=head2 checklists

Type: has_many

Related object: L<Zaaksysteem::Schema::Checklist>

=cut

__PACKAGE__->has_many(
  "checklists",
  "Zaaksysteem::Schema::Checklist",
  { "foreign.case_id" => "self.id" },
  {},
);

=head2 contactmoments

Type: has_many

Related object: L<Zaaksysteem::Schema::Contactmoment>

=cut

__PACKAGE__->has_many(
  "contactmoments",
  "Zaaksysteem::Schema::Contactmoment",
  { "foreign.case_id" => "self.id" },
  {},
);

=head2 directories

Type: has_many

Related object: L<Zaaksysteem::Schema::Directory>

=cut

__PACKAGE__->has_many(
  "directories",
  "Zaaksysteem::Schema::Directory",
  { "foreign.case_id" => "self.id" },
  {},
);

=head2 files

Type: has_many

Related object: L<Zaaksysteem::Schema::File>

=cut

__PACKAGE__->has_many(
  "files",
  "Zaaksysteem::Schema::File",
  { "foreign.case_id" => "self.id" },
  {},
);

=head2 loggings

Type: has_many

Related object: L<Zaaksysteem::Schema::Logging>

=cut

__PACKAGE__->has_many(
  "loggings",
  "Zaaksysteem::Schema::Logging",
  { "foreign.zaak_id" => "self.id" },
  {},
);

=head2 relates_to

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Zaak>

=cut

__PACKAGE__->belongs_to(
  "relates_to",
  "Zaaksysteem::Schema::Zaak",
  { id => "relates_to" },
);

=head2 zaak_relates_toes

Type: has_many

Related object: L<Zaaksysteem::Schema::Zaak>

=cut

__PACKAGE__->has_many(
  "zaak_relates_toes",
  "Zaaksysteem::Schema::Zaak",
  { "foreign.relates_to" => "self.id" },
  {},
);

=head2 pid

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Zaak>

=cut

__PACKAGE__->belongs_to("pid", "Zaaksysteem::Schema::Zaak", { id => "pid" });

=head2 zaak_pids

Type: has_many

Related object: L<Zaaksysteem::Schema::Zaak>

=cut

__PACKAGE__->has_many(
  "zaak_pids",
  "Zaaksysteem::Schema::Zaak",
  { "foreign.pid" => "self.id" },
  {},
);

=head2 behandelaar

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ZaakBetrokkenen>

=cut

__PACKAGE__->belongs_to(
  "behandelaar",
  "Zaaksysteem::Schema::ZaakBetrokkenen",
  { id => "behandelaar" },
);

=head2 zaaktype_node_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ZaaktypeNode>

=cut

__PACKAGE__->belongs_to(
  "zaaktype_node_id",
  "Zaaksysteem::Schema::ZaaktypeNode",
  { id => "zaaktype_node_id" },
);

=head2 vervolg_van

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Zaak>

=cut

__PACKAGE__->belongs_to(
  "vervolg_van",
  "Zaaksysteem::Schema::Zaak",
  { id => "vervolg_van" },
);

=head2 zaak_vervolg_vans

Type: has_many

Related object: L<Zaaksysteem::Schema::Zaak>

=cut

__PACKAGE__->has_many(
  "zaak_vervolg_vans",
  "Zaaksysteem::Schema::Zaak",
  { "foreign.vervolg_van" => "self.id" },
  {},
);

=head2 locatie_zaak

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ZaakBag>

=cut

__PACKAGE__->belongs_to(
  "locatie_zaak",
  "Zaaksysteem::Schema::ZaakBag",
  { id => "locatie_zaak" },
);

=head2 zaaktype_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Zaaktype>

=cut

__PACKAGE__->belongs_to(
  "zaaktype_id",
  "Zaaksysteem::Schema::Zaaktype",
  { id => "zaaktype_id" },
);

=head2 locatie_correspondentie

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ZaakBag>

=cut

__PACKAGE__->belongs_to(
  "locatie_correspondentie",
  "Zaaksysteem::Schema::ZaakBag",
  { id => "locatie_correspondentie" },
);

=head2 coordinator

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ZaakBetrokkenen>

=cut

__PACKAGE__->belongs_to(
  "coordinator",
  "Zaaksysteem::Schema::ZaakBetrokkenen",
  { id => "coordinator" },
);

=head2 pid

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Zaak>

=cut

__PACKAGE__->belongs_to("pid", "Zaaksysteem::Schema::Zaak", { id => "pid" });

=head2 zaak_pids

Type: has_many

Related object: L<Zaaksysteem::Schema::Zaak>

=cut

__PACKAGE__->has_many(
  "zaak_pids",
  "Zaaksysteem::Schema::Zaak",
  { "foreign.pid" => "self.id" },
  {},
);

=head2 aanvrager

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ZaakBetrokkenen>

=cut

__PACKAGE__->belongs_to(
  "aanvrager",
  "Zaaksysteem::Schema::ZaakBetrokkenen",
  { id => "aanvrager" },
);

=head2 zaak_bags

Type: has_many

Related object: L<Zaaksysteem::Schema::ZaakBag>

=cut

__PACKAGE__->has_many(
  "zaak_bags",
  "Zaaksysteem::Schema::ZaakBag",
  { "foreign.zaak_id" => "self.id" },
  {},
);

=head2 zaak_betrokkenens

Type: has_many

Related object: L<Zaaksysteem::Schema::ZaakBetrokkenen>

=cut

__PACKAGE__->has_many(
  "zaak_betrokkenens",
  "Zaaksysteem::Schema::ZaakBetrokkenen",
  { "foreign.zaak_id" => "self.id" },
  {},
);

=head2 zaak_kenmerks

Type: has_many

Related object: L<Zaaksysteem::Schema::ZaakKenmerk>

=cut

__PACKAGE__->has_many(
  "zaak_kenmerks",
  "Zaaksysteem::Schema::ZaakKenmerk",
  { "foreign.zaak_id" => "self.id" },
  {},
);

=head2 zaak_metas

Type: has_many

Related object: L<Zaaksysteem::Schema::ZaakMeta>

=cut

__PACKAGE__->has_many(
  "zaak_metas",
  "Zaaksysteem::Schema::ZaakMeta",
  { "foreign.zaak_id" => "self.id" },
  {},
);

=head2 zaak_subcase_zaak_ids

Type: has_many

Related object: L<Zaaksysteem::Schema::ZaakSubcase>

=cut

__PACKAGE__->has_many(
  "zaak_subcase_zaak_ids",
  "Zaaksysteem::Schema::ZaakSubcase",
  { "foreign.zaak_id" => "self.id" },
  {},
);

=head2 zaak_subcase_relation_zaak_ids

Type: has_many

Related object: L<Zaaksysteem::Schema::ZaakSubcase>

=cut

__PACKAGE__->has_many(
  "zaak_subcase_relation_zaak_ids",
  "Zaaksysteem::Schema::ZaakSubcase",
  { "foreign.relation_zaak_id" => "self.id" },
  {},
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-06-28 09:40:35
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:HOvnCE+YD9ExOpLawbhvJA

__PACKAGE__->resultset_class('Zaaksysteem::Zaken::ResultSetZaak');

__PACKAGE__->load_components(
    "+Zaaksysteem::Zaken::ComponentZaak",
    '+DBIx::Class::Helper::Row::ToJSON',
    __PACKAGE__->load_components()
);

__PACKAGE__->add_columns('last_modified',
    { %{ __PACKAGE__->column_info('last_modified') },
    set_on_update => 1,
    set_on_create => 1,
});

__PACKAGE__->add_columns('created',
    { %{ __PACKAGE__->column_info('created') },
    set_on_create => 1,
});

__PACKAGE__->mk_group_accessors('column' => 'days_running');
__PACKAGE__->mk_group_accessors('column' => 'days_left');
__PACKAGE__->mk_group_accessors('column' => 'days_perc');

### Language
__PACKAGE__->has_many(
  "zaak_kenmerken",
  "Zaaksysteem::Schema::ZaakKenmerk",
  { "foreign.zaak_id" => "self.id" },
);

__PACKAGE__->has_many(
  "zaak_betrokkenen",
  "Zaaksysteem::Schema::ZaakBetrokkenen",
  { "foreign.zaak_id" => "self.id" },
);

__PACKAGE__->has_many(
  "zaak_relatives",
  "Zaaksysteem::Schema::Zaak",
  { "foreign.relates_to" => "self.id" },
);
  
__PACKAGE__->has_many(
  "zaak_parents",
  "Zaaksysteem::Schema::Zaak",
  { "foreign.pid" => "self.id" },
);
  
__PACKAGE__->belongs_to(
  "zaaktype_node_id",
  "Zaaksysteem::Schema::ZaaktypeNode",
  { id => "zaaktype_node_id" },
  { join_type   => 'left' },
);
__PACKAGE__->belongs_to(
  "relates_to",
  "Zaaksysteem::Schema::Zaak",
  { id => "relates_to" },
  { join_type   => 'left' },
);
__PACKAGE__->has_many(
  "zaak_relates_toes",
  "Zaaksysteem::Schema::Zaak",
  { "foreign.relates_to" => "self.id" },
  { join_type   => 'left' },
);
__PACKAGE__->belongs_to(
  "zaaktype_id",
  "Zaaksysteem::Schema::Zaaktype",
  { id => "zaaktype_id" },
  { join_type   => 'left' },
);
__PACKAGE__->belongs_to(
  "zaaktype_node_id",
  "Zaaksysteem::Schema::ZaaktypeNode",
  { id => "zaaktype_node_id" },
  { join_type   => 'left' },
);
__PACKAGE__->belongs_to(
  "coordinator",
  "Zaaksysteem::Schema::ZaakBetrokkenen",
  { id => "coordinator" },
  { join_type   => 'left' },
);
__PACKAGE__->belongs_to(
  "behandelaar",
  "Zaaksysteem::Schema::ZaakBetrokkenen",
  { id => "behandelaar" },
  { join_type   => 'left' },
);
__PACKAGE__->belongs_to(
  "aanvrager",
  "Zaaksysteem::Schema::ZaakBetrokkenen",
  { id => "aanvrager" },
  { join_type   => 'left' },
);
__PACKAGE__->belongs_to(
  "locatie_zaak",
  "Zaaksysteem::Schema::ZaakBag",
  { id => "locatie_zaak" },
  { join_type   => 'left' },
);
__PACKAGE__->belongs_to(
  "locatie_correspondentie",
  "Zaaksysteem::Schema::ZaakBag",
  { id => "locatie_correspondentie" },
  { join_type   => 'left' },
);
__PACKAGE__->belongs_to(
  "zaaktype_node",
  "Zaaksysteem::Schema::ZaaktypeNode",
  { "id" => "zaaktype_node_id" },
  { join_type => 'left' },
);
  
  
__PACKAGE__->has_many(
  "documenten",
  "Zaaksysteem::Schema::Documenten",
  { "foreign.zaak_id" => "self.id" },
);
__PACKAGE__->has_many(
  "logging",
  "Zaaksysteem::Schema::Logging",
  { "foreign.zaak_id" => "self.id" },
);
__PACKAGE__->has_many(
  "checklist",
  "Zaaksysteem::Schema::ChecklistAntwoord",
  { "foreign.zaak_id" => "self.id" },
);
__PACKAGE__->has_many(
  "case_relations",
  "Zaaksysteem::Schema::CaseRelation",
  { -or => [
    { "foreign.case_id_a" => "self.id" },
    { "foreign.case_id_b" => "self.id" }
  ]}
);
__PACKAGE__->has_many(
  "zaak_vervolgers",
  "Zaaksysteem::Schema::Zaak",
  { "foreign.vervolg_van" => "self.id" },
);
__PACKAGE__->has_many(
  "zaak_children",
  "Zaaksysteem::Schema::Zaak",
  { "foreign.pid" => "self.id" },
);
__PACKAGE__->has_many(
  "zaak_meta",
  "Zaaksysteem::Schema::ZaakMeta",
  { "foreign.zaak_id" => "self.id" },
);
  
__PACKAGE__->has_many(
    "zaak_subcases",
    "Zaaksysteem::Schema::ZaakSubcase",
    { "foreign.zaak_id" => "self.id" },
);

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
