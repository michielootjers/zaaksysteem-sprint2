package Zaaksysteem::Schema::BagVerblijfsobjectGebruiksdoel;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::BagVerblijfsobjectGebruiksdoel - koppeltabel voor gebruiksdoelen bij verblijfsobject

=cut

__PACKAGE__->table("bag_verblijfsobject_gebruiksdoel");

=head1 ACCESSORS

=head2 identificatie

  data_type: 'varchar'
  is_nullable: 0
  size: 16

56.01 : de unieke aanduiding van een verblijfsobject

=head2 begindatum

  data_type: 'varchar'
  is_nullable: 0
  size: 14

56.91 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een verblijfsobject een wijziging hebben ondergaan.

=head2 gebruiksdoel

  data_type: 'varchar'
  is_nullable: 0
  size: 80

56.30 : een categorisering van de gebruiksdoelen van het betreffende verblijfsobject, zoals dit  formeel door de overheid als zodanig is toegestaan.

=head2 correctie

  data_type: 'varchar'
  is_nullable: 0
  size: 1

het gegeven is gecorrigeerd.

=cut

__PACKAGE__->add_columns(
  "identificatie",
  { data_type => "varchar", is_nullable => 0, size => 16 },
  "begindatum",
  { data_type => "varchar", is_nullable => 0, size => 14 },
  "gebruiksdoel",
  { data_type => "varchar", is_nullable => 0, size => 80 },
  "correctie",
  { data_type => "varchar", is_nullable => 0, size => 1 },
);
__PACKAGE__->set_primary_key("identificatie", "begindatum", "correctie", "gebruiksdoel");


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-01-10 07:42:00
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:tMCBsRBMbTCbJ1qcEEkLoQ





# You can replace this text with custom content, and it will be preserved on regeneration
1;
