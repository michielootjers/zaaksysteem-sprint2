package Zaaksysteem::Documenten::ResultSetNotitie;

use strict;
use warnings;

use Moose;
use Data::Dumper;
use Params::Profile;

use Digest::MD5 qw/md5_hex/;

use Zaaksysteem::Constants;
use Zaaksysteem::Profiles;

extends qw/DBIx::Class::ResultSet Zaaksysteem::Documenten/;

use constant MD5_COLUMNS => [
    'onderwerp',
    'bericht',
];

Params::Profile->register_profile(
    method  => 'create_notitie',
    profile => PROFILE_NOTITIE_CREATE,
);

sub create_notitie {
    my ($self,$args) = @_;

    # use Carp qw(cluck);
    # use Data::Dumper;
    # warn Dumper cluck;
    # die 'Old create_notitie call - cluck in warn output';

    $args->{betrokkene_dsn} = $args->{ztc_aanvrager_id};

    my $opts        = $self->_validate($args);

    $opts->{md5}    = $self->_calc_md5($opts);

    unless ($opts->{onderwerp}) {
        $opts->{onderwerp} = ( length($opts->{bericht}) > 255
            ? substr($opts->{bericht}, 0, 250) . ' ...'
            : $opts->{bericht}
        );
    }

    my ($document, $notitie);
    $self->result_source->schema->txn_do(sub {
        my $notitie_args    = {};

        for my $key ($self->result_source->columns) {
            $notitie_args->{ $key } = $opts->{ $key } if defined
                $opts->{ $key };
        }

        $notitie    = $self->create($notitie_args)
            or die('create_notitie fail');

        # TODO Fix this black magic
        my $created = $notitie
            ->created
            ->set_time_zone('UTC')
            ->set_time_zone('Europe/Amsterdam');

        my $naam    = 'Contactmoment (' .
            $created->dmy . ' ' .
            $created->hms . ')';

        $document    = $notitie->documenten->create_document(
            {
                %{ $args },
                naam        => $naam,
                store_type  => DOCUMENTS_STORE_TYPE_NOTITIE,
                notitie_id  => $notitie->id,
            }
        );
    });

    return $document;
}

sub _calc_md5 {
    my ($self, $opts) = @_;

    my $MD5_COLUMNS = MD5_COLUMNS;

    my $string      = '';

    $string         .= $opts->{ $_ } for @{ $MD5_COLUMNS };

    return md5_hex($string);
}

1;
