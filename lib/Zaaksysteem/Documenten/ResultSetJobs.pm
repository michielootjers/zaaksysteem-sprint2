package Zaaksysteem::Documenten::ResultSetJobs;

use strict;
use warnings;

use Moose;
use Data::Dumper;
use Data::Serializer;
use Params::Profile;

use Zaaksysteem::Constants;
use Zaaksysteem::Profiles;

extends qw/DBIx::Class::ResultSet Zaaksysteem::Documenten/;

Params::Profile->register_profile(
    method  => 'create_job',
    profile => PROFILE_JOB_CREATE,
);

sub create_job {
    my ($self,$args) = @_;

    my $opts        = $self->_validate($args);

    my $obj = Data::Serializer->new(
        'serializer'    => 'Storable',
    );

    $opts->{raw_input}  = $obj->serialize($opts->{opts});

    use Data::Dumper;
    die 'Died in create_job with options: ' . Dumper $opts;

    my ($document, $job);
    $self->result_source->schema->txn_do(sub {
        my $job_args    = {};

        for my $key ($self->result_source->columns) {
            $job_args->{ $key } = $opts->{ $key } if defined
                $opts->{ $key };
        }

        $job    = $self->create($job_args)
            or die('create_job fail');

        $document    = $job->documenten->create_document(
            {
                %{ $args },
                naam        => $opts->{naam},
                store_type  => DOCUMENTS_STORE_TYPE_JOB,
                job_id      => $job->id,
            }
        );
    });

    return $document;
}

1;
