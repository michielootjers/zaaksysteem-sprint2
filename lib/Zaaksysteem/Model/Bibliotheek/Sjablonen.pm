package Zaaksysteem::Model::Bibliotheek::Sjablonen;

use strict;
use warnings;
use Zaaksysteem::Constants;
use Zaaksysteem::Backend::Tools::Sjablonen;

use parent 'Catalyst::Model';

use Data::Dumper;
use Digest::MD5::File qw/-nofatals file_md5_hex/;
use OpenOffice::OODoc;
use File::Copy;
use File::Temp;
use File::stat;
use File::Slurp;

use Encode qw/encode/;

use constant SJABLONEN              => 'sjablonen';
use constant SJABLONEN_DB           => 'DB::BibliotheekSjablonen';
use constant SJABLONEN_STRINGS_DB   => 'DB::BibliotheekSjablonenMagicString';
use constant FILESTORE_DB           => 'DB::Filestore';

use Moose;

use utf8;

has 'c' => (
    is  => 'rw',
);

{
    Zaaksysteem->register_profile(
        method  => 'bewerken',
        profile => {
            required => [ qw/
                naam
                bibliotheek_categorie_id
                commit_message
            /],
            optional => [ qw/
                filename
                id
                label
                description
                help
            /],
            constraint_methods  => {
                naam            => qr/^.{2,64}$/,
            },
            'require_some'      => {
                'filename_or_id'    => [1, qw/id filename/]
            },
            msgs                => PARAMS_PROFILE_DEFAULT_MSGS,
        }
    );

    sub bewerken {
        my ($self, $params) = @_;

        my ($magic_strings, $file_store_id, $old_sjabloon);

        my $dv = $self->c->check(
            params  => $params,
        );

        unless ($dv->success) {
            die "could not save sjabloon: " . Dumper $dv;
        }

        my $valid_options = $dv->valid;

        ### Rewrite some values
        my %options = map {
            $_ => $valid_options->{ $_ }
        } keys %{ $valid_options };

        if ($options{filename}) {
            $options{filename}  = $options{naam} . '.odt';
        }

        if ($options{filename}) {
            $magic_strings      = $self->_parse_file(%options)
                or return;

            $file_store_id      = $self->_store_file(%options)
                or return;
        } else {
            $old_sjabloon       = $self->c->model(SJABLONEN_DB)->find(
                $options{id}
            ) or return;

            return unless $old_sjabloon->filestore_id;

            $file_store_id      = $old_sjabloon->filestore_id->id;
        }

        $options{filestore_id} = $file_store_id;

        ### Remove unnecessary variables
        delete($options{id}) unless $options{id};
        delete($options{filename});
        delete($options{commit_message}); #handled by controller (but why :)

        ### Ram er maar in
        my $kenmerk = $self->c->model(SJABLONEN_DB)->update_or_create(\%options)
            or return;


        if ($kenmerk->bibliotheek_sjablonen_magic_strings->count) {
            $kenmerk->bibliotheek_sjablonen_magic_strings->delete;
        }

        if (ref($magic_strings)) {
            for my $magic_string (@{ $magic_strings }) {
                $self->c->log->debug('Creating template magic string: ' . $magic_string);

                $self->c->model(SJABLONEN_STRINGS_DB)->create({
                    'bibliotheek_sjablonen_id'  => $kenmerk->id,
                    'value'                     => $magic_string,
                });
            }
        }

        return $kenmerk;
    }
}

sub sjabloon_exists {
    my ($self, %opts)   = @_;

    return unless $opts{naam};

    return $self->c->model(SJABLONEN_DB)->search({
        'naam'  => $opts{naam}
    })->count;
}

sub _store_file {
    my ($self, %options) = @_;
    my ($filename);

    $filename = $options{filename};

    my $upload      = $self->c->req->upload('filename');

    my $options     = {
        original_name    => $filename,
        file_path        => $upload->tempname,
        ignore_extension => 1,
    };

    my $filestore   = $self->c->model(FILESTORE_DB)->filestore_create($options);

    if (!$filestore) {
        $self->c->log->error("Hm, kan filestore entry niet aanmaken: {$filename}");
        $self->c->push_flash_message('Sjabloonbestand kon niet worden opgeslagen');

        return;
    }

    return $filestore->id;
}

sub _parse_file {
    my ($self, %options) = @_;
    my (@magic_strings);

    my $filename        = $options{filename};

    if (!$self->c->req->upload('filename')) {
        $self->c->log->error("Bizar, kan file niet vinden: {$filename}");
        $self->c->push_flash_message('Sjabloonbestand upload mislukt');

        return;
    }

    ### Parse filename
    my $fh = $self->c->req->upload('filename')->fh or do {
        $self->c->log->error('Kan filehandle niet openen');
        $self->c->push_flash_message('Sjabloonbestand kon niet geopend worden, neem contact op met systeembeheer');

        return;
    };

    my $encoding    = $OpenOffice::OODoc::XPath::LOCAL_CHARSET;
    my $doc         = odfDocument(
        file            => $self->c->req->upload('filename')->tempname,
        local_encoding  => $encoding
    ) or do {
        $self->c->log->error('Kan opendocument file niet openen');
        $self->c->push_flash_message('Ge&uuml;pload sjabloonbestand niet een geldig ODF document');

        return;
    };

    my $rawtext = $doc->getTextContent();
    (@magic_strings) = $rawtext =~ /\[\[([\w0-9_]+)\]\]/g;

    for (@magic_strings) {
        $self->c->log->debug('String: ' . $_);


        if (!$_ || $_ !~ /\[\[[\w0-9_]+\]\]/) { next; }

        my (@line_magic_strings) = $_ =~ /\[\[([\w0-9_]+)\]\]/g;

        push (@magic_strings, @line_magic_strings);
    }

    $self->c->log->debug(
        'Vond magic strings: '
        . "\n -" . join("\n -", @magic_strings)
    );

    return \@magic_strings if scalar(@magic_strings);
    return 1;
}

sub retrieve {
    my ($self, %opt) = @_;

    return $self->c->model(SJABLONEN_DB)->find($opt{id});
}



{
    Zaaksysteem->register_profile(
        method  => 'download_sjabloon',
        profile => {
            required => [ qw/
                filename
                document_id
                output_filetype
                mimetype
            /],
            constraint_methods  => {
                document_id     => qr/^\d+$/,
            },
        }
    );

    sub download_sjabloon {
        my ($self, %params) = @_;

        my $dv              = $self->c->check(
            params  => \%params
        );

        return unless $dv->success;
        my $valid_options       = $dv->valid;

        return unless (
            exists(ZAAKSYSTEEM_CONSTANTS->{document}->{sjabloon}->{export_types}->{
                $valid_options->{output_filetype}
            })
        );

        my $document_files_dir  = $self->c->config->{files} . '/documents';
        my $document_file       = $document_files_dir . '/' .
            $valid_options->{document_id};

        $self->convert_and_download($document_file, $valid_options->{mimetype}, 
            $valid_options->{filename}, 
            $valid_options->{output_filetype}
        );
        return;


    }

    sub _send_to_browser {
        my ($self, %opt)    = @_;


        my $outputfiletype  = $opt{filetype};
        my $outputfilename  = $opt{filename};
        my $showfilename    = $opt{showfilename};

        utf8::downgrade($outputfiletype);
        utf8::downgrade($outputfilename);
        utf8::downgrade($showfilename);

        my $stat            = stat($outputfilename);

        # Filename
        {
            my $filename    = $showfilename;
            $filename       =~ s/\.[\w\d]+$//;
            $self->c->res->headers->header(
                'Content-Disposition',
                'attachment; filename="'
                    . $filename . '.' . $outputfiletype
                    . '"'
            );
        }

        my $filetypeinfo = ZAAKSYSTEEM_CONSTANTS->{document}->{sjabloon}->{export_types}->{
            $outputfiletype
        };

        $self->c->serve_static_file($outputfilename);
        $self->c->res->headers->content_length( $stat->size );
        $self->c->res->headers->content_type($filetypeinfo->{mimetype});
        $self->c->res->content_type($filetypeinfo->{mimetype});
        $self->c->res->content_length( $stat->size );

        return 1;
    }

    sub _convert_to_tmp {
        my ($self, %opt) = @_;

        my $tmp_doc_h    = File::Temp->new(
            UNLINK => 1
        );

        utf8::downgrade($opt{filetype});

        copy(
            $opt{filename},
            $tmp_doc_h->filename . '.odt'
        );

        my $tmph                = File::Temp->new(
            UNLINK => 1
        );

        my $outputfile          = $tmph->filename . '.' .
            $opt{filetype};

        system(
            '/usr/bin/jodconverter '
            . $tmp_doc_h->filename . '.odt '
            . $outputfile
        );

        return $outputfile;
    }
    
    sub convert_and_download {
        my ($self, $input_document_file, $input_mimetype, $output_filename, $output_filetype) = @_;
        
         #File::Slurp
        my $content = read_file($input_document_file);
        
        if($output_filetype ne 'odt') {
            my $output_mimetype = ZAAKSYSTEEM_CONSTANTS->{document}->{sjabloon}->{export_types}->{$output_filetype}->{mimetype};

            use HTTP::Request::Common;
            my $ua = LWP::UserAgent->new;
            my $result = $ua->request(POST 'http://localhost:8080/converter/service', 
                Content => $content,
                Content_Type => $input_mimetype,
                Accept => $output_mimetype,
            );
            if($result->code ne 200) {
                die "could not convert $input_document_file to $output_filetype using jodconvertor, result: " . $result->code . " result content: " . $result->content();
            }
            $content = $result->content();
        
            $output_filename =~ s|\.odt$|'.'.$output_filetype|eis;
        }
        $self->c->log->debug('input_document_file: '. $input_document_file . ', input_mimetype: ' . $input_mimetype . ', filename: ' . $output_filename . ', filetype: ' . $output_filetype);
        
        utf8::downgrade($output_filename);

        $self->c->res->headers->header( 'Content-Type'  => 'application/x-download' );
        $self->c->res->headers->header(
            'Content-Disposition'  =>
                "attachment;filename=\"" . $output_filename . "\"\n\n"
        );        
    
        $self->c->res->body($content);
    }
        
}



{
    Zaaksysteem->register_profile(
        method  => 'create_sjabloon',
        profile => {
            optional => [ qw/
                direct_download
                filetype
                filename
            /],
            'require_some'      => {
                'sjabloon_id_or_naam'    => [
                    1,
                    qw/
                        sjabloon_id
                        systeem_document
                    /
                ],
                'document_id_or_naam'    => [
                    1,
                    qw/
                        document_id
                        systeem_document
                    /
                ],
                'zaak_nr_or_naam'    => [
                    1,
                    qw/
                        zaak_nr
                        systeem_document
                    /
                ],
            },
            constraint_methods  => {
                sjabloon_id     => qr/^\d+$/,
                document_id     => qr/^\d+$/,
            },
        }
    );

    sub create_sjabloon {
        my ($self, %params) = @_;

        my $dv = $self->c->check(
            params  => \%params
        );


        $self->c->log->debug('B::S->create_sjabloon: validating' . Dumper (\%params) . 'success: ' . $dv->success);
        unless($dv->success) {
            $self->c->log->debug('B::S->create_sjabloon: validation error' . Dumper $dv);
            return;
        }

        ### Check for existence sjabloon
        my $valid_options   = $dv->valid;

        my ($case) = $self->c->model('DB::Zaak')->search({'me.id' => $valid_options->{zaak_nr}});

        if ($valid_options->{systeem_document}) {
            $self->c->log->debug('Systeem document: ' .
                $valid_options->{systeem_document}
            );
            my $sys_sjablonen = $self->c->model(SJABLONEN_DB)->search(
                {
                    naam        => $valid_options->{systeem_document},
                }
            );

            return unless $sys_sjablonen->count;

            $valid_options->{sjabloon_id} = $sys_sjablonen->first->id;
        }

        my $sjabloon = $self->retrieve(
            'id'    => $valid_options->{sjabloon_id}
        );
        
        if (!$sjabloon) {
            $self->c->log->debug('B::S->create_sjabloon: sjabloon '.$valid_options->{sjabloon_id}. ' not found in db');
            return;
        }

        my $filestore = $sjabloon->filestore_id;
        if (!$filestore) { 
            $self->c->push_flash_message("Sjabloon bestand kon niet gevonden worden");
            $self->c->detach;
            return;
        }

        $self->c->log->debug('B::S->create_sjabloon: decode file');

        # Create file
        my $workdir  = $self->c->model('DB::Config')->get_value('tmp_location');
        my $encoding = $OpenOffice::OODoc::XPath::LOCAL_CHARSET;
        odfWorkingDirectory($workdir);
        my $doc         = odfDocument(
            file            => $filestore->ustore->getPath($filestore->uuid),
            local_encoding  => $encoding,
	        work_dir        => $workdir,
        ) or do {
            $self->c->log->error(
                'Kan opendocument file niet openen'
            );
            return;
        };

        $self->c->log->debug('B::S->create_sjabloon: replace kenmerken');

        ### Make sure the parse of kenmerken won't kill the copy here.
        eval {
            Zaaksysteem::Backend::Tools::Sjablonen::magic_strings_convert({
                case     => $case,
                document => $doc,
            });
        };
        if ($@) {
            $self->c->log->debug(
                'B::S->create_sjabloon: errors with replacing kenmerken: ' . $@
            );
            die $@;
        }

        my $temp_path = $workdir.$filestore->uuid;
        $doc->save($temp_path);

        if ($valid_options->{direct_download}) {
            my $outputfile = $self->_convert_to_tmp(
                'filename'  => $temp_path,
                'filetype'  => $valid_options->{filetype}
            );

            $self->_send_to_browser(
                'filename'  => $outputfile,
                'filetype'  => $valid_options->{filetype}
            );
        } else {
            # Retrieve the subject
            my $subject;
            if ($self->c->user_exists) {
                $subject = 'betrokkene-medewerker-'.$self->c->user->uidnumber;
            } else {
                $subject = $self->c->stash->{zaak}->aanvrager_object->betrokkene_identifier;
            }

            my $case_type_property_id = $self->c->req->param('catalogus');
            my $pt_name = 'private';
            if ($case_type_property_id) {
                my $ctp = $self->c->model('DB::ZaaktypeKenmerken')->find($case_type_property_id);
                if ($ctp->pip) {
                    $pt_name = 'pip';
                }
            }

            $self->c->model('DB::File')->file_create({
                db_params => {
                    name       => $self->c->req->param('filename').'.odt',
                    created_by => $subject,
                    case_id    => $case->id,
                    case_type_document_id => $case_type_property_id || undef,
                },
                file_path             => $temp_path,
                publish_type_name     => $pt_name,
                ignore_extension      => 1,
            });
            return 1;             
        }
        unlink $temp_path;

        return;
    }
}

sub ACCEPT_CONTEXT {
    my ($self, $c) = @_;

    $self->{c} = $c;

    return $self;
}


1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

