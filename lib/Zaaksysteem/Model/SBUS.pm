package Zaaksysteem::Model::SBUS;
use Moose;
use namespace::autoclean;

extends 'Catalyst::Model::Factory::PerRequest';

__PACKAGE__->config(
    class       => 'Zaaksysteem::SBUS',
    constructor => 'new',
);

sub prepare_arguments {
    my ($self, $app) = @_;

    return {
        'config'    => {
            home    => $app->config->{home},
            %{ $app
                ->customer_instance
                ->{start_config}
                ->{SBUS}
            }
        },
        'log'       => $app->log,
        'schema'    => $app->model('DB'),
    };
}

1;

__PACKAGE__->meta->make_immutable;

