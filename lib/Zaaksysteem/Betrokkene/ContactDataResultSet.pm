package Zaaksysteem::Betrokkene::ContactDataResultSet;

use Moose;
use Data::Dumper;

extends qw/DBIx::Class::ResultSet/;

sub find_by_phonenumber {
    my ($self, $phonenumber) = @_;

    $self->search([
        { telefoonnummer => $phonenumber },
        { mobiel => $phonenumber }
    ]);
}
