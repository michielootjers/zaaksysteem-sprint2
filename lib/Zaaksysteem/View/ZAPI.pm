package Zaaksysteem::View::ZAPI;

use Moose;

use Data::Dumper;
use Zaaksysteem::ZAPI::Response;
use Zaaksysteem::ZAPI::Error;

BEGIN { extends 'Catalyst::View::JSON'; }

__PACKAGE__->config(
    expose_stash    => 'zapi',
);

=head2

Extends the default process sub

=cut

around 'process' => sub {
    my $orig    = shift;
    my $self    = shift;
    my $c       = shift;

    die('Error calling this View, missing "zapi" stash attribute')
        unless (
            exists($c->stash->{zapi}) &&
            $c->stash->{zapi}
        );

    if ($c->stash->{ json_content_type }) { 
        $c->res->headers->header(
            'Content-Type' => $c->stash->{ json_content_type }
        );
    }

    if (
        UNIVERSAL::isa(
            $c->stash->{zapi},
            'Zaaksysteem::ZAPI::Response'
        ) ||
        UNIVERSAL::isa(
            $c->stash->{zapi},
            'Zaaksysteem::ZAPI::Error'
        )
    ) {
        $c->stash->{zapi} = $c->stash->{zapi}->response;
        return $self->$orig($c, @_);
    }

    $c->stash->{zapi} = Zaaksysteem::ZAPI::Response->new(
        unknown         => $c->stash->{zapi},
        uri_prefix      => $c->uri_for(
            $c->action,
            $c->req->captures,
            @{ $c->req->args },
            $c->req->params
        ),
        page_current    => $c->req->params->{next} || 1,
    )->response;

    return $self->$orig($c, @_);
};

sub encode_json {
    my($self, $c, $data) = @_;

    my $encoder = JSON::XS->new->utf8->pretty->allow_nonref->allow_blessed->convert_blessed;
    $encoder->encode($data);
}

1;


1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

