/* ***** BEGIN LICENSE BLOCK ********************************************
 * Version: EUPL 1.1
 *
 * The contents of this file are subject to the EUPL, Version 1.1 or
 * - as soon they will be approved by the European Commission -
 * subsequent versions of the EUPL (the "Licence");
 * you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://joinup.ec.europa.eu/software/page/eupl
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is Zaaksysteem
 *
 * The Initial Developer of the Original Code is
 * Mintlab B.V. <info@mintlab.nl>
 * 
 * Portions created by the Initial Developer are Copyright (C) 2009-2011
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 * Michiel Ootjers <michiel@mintlab.nl>
 * Jonas Paarlberg <jonas@mintlab.nl>
 * Jan-Willem Buitenhuis <jw@mintlab.nl>
 * Peter Moen <peter@mintlab.nl>
 *
 * ***** END LICENSE BLOCK ******************************************** */


$(document).ready(function() {
    if ($('form.webform input[name="plugin"]').val() == 'bussum_parkeergebieden_2.0') {
        ezra_plugin_parkeergebied_20();
    }
});

function ezra_plugin_parkeergebied_20() {
    var form        = $('form.webform');

    // parkeergebied
    var input_parkeergebied;

    var vergunningen_kenmerk = 'kenmerk_id_'
        + form.find('input[name="plugin_aantal_vergunningen"]').val();

    var parkeergebied_kenmerk   = 'kenmerk_id_'
        + form.find('input[name="plugin_parkeergebied_id"]').val();

    var prijs_kenmerk = 'kenmerk_id_'
        + form.find('input[name="plugin_prijs"]').val();

    var vergunninghouder_kenmerk = 'kenmerk_id_'
        + form.find('input[name="plugin_vergunninghouder_id"]').val();

    var geldigheid_kenmerk = 'kenmerk_id_'
        + form.find('input[name="plugin_geldigheid_id"]').val();

    var betaalwijze_kenmerk = 'kenmerk_id_'
        + form.find('input[name="plugin_betaalwijze_id"]').val();

    var vergunningtype_kenmerk = 'kenmerk_id_'
        + form.find('input[name="plugin_vergunningtype_id"]').val();

    var startdatum_kenmerk = 'kenmerk_id_'
        + form.find('input[name="plugin_startdatum_id"]').val();

    var einddatum_kenmerk = 'kenmerk_id_'
        + form.find('input[name="plugin_einddatum_id"]').val();

    var geldigheidsdagen_kenmerk = 'kenmerk_id_'
        + form.find('input[name="plugin_geldigheidsdagen_id"]').val();

    var kenteken_kenmerk = 'kenmerk_id_'
        + form.find('input[name="plugin_kenteken_id"]').val();

    var vergunninghouder_kenmerk = 'kenmerk_id_'
        + form.find('input[name="plugin_vergunninghouder_id"]').val();

    if (form.find('input[name="' + parkeergebied_kenmerk + '"]').attr('type') == 'hidden') {
        input_parkeergebied = form.find('input[name="' + parkeergebied_kenmerk + '"]').val();
    } else {
        input_parkeergebied = form.find('input[name="' + parkeergebied_kenmerk + '"]:checked').val();
    }

    var json_params = {
        ztc_aanvrager_id: form.find('input[name="ztc_aanvrager_id"]').val(),
        parkeergebied: input_parkeergebied,
        parkeergebied_vergunningen: form.find('select[name="' + vergunningen_kenmerk + '"] :selected').val(),
        parkeergebied_bezoeker: 1
    };

    $.getJSON(
        '/plugins/parkeergebied/get_parkeergebied',
        json_params,
        function(data) {
            var parkeergebied = data.json;

            if (!parkeergebied.success) {
                return;
            }

            var hide_parkeergebied = function() {
                $('form.webform input[name="' + parkeergebied_kenmerk + '"]').closest('td')
                    .html(
                        '<input type="hidden" name="' + parkeergebied_kenmerk + '" value="'
                        + parkeergebied.parkeergebied + '" />'
                        + parkeergebied.parkeergebied
                    );
            };

            var update_webform_callback = function() {
                hide_parkeergebied();

                $('form.webform .webformcontent').find(
                    '.regels_enabled_kenmerk input, .regels_enabled_kenmerk select,.regels_enabled_kenmerk textarea').change(function() {
                    loadWebform({
                        form: $(this).closest('form.webform'),
                        callback: update_webform_callback
                    });
                });
            };

            update_webform_callback();

//            loadWebform($('form.webform'), update_webform_callback);
        }
    );
}

