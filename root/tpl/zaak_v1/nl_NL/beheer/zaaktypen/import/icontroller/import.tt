[% USE Dumper %]
[% USE Scalar %]


[% BLOCK inavigator_import_options %]
    [% settings_block = 'inavigator_' _ item.type _ '_options' %]
    [% PROCESS $settings_block %]
[% END %]


[% BLOCK adjustment %]
    <div class="import_dependency_adjustment import_overlay" style="display:none;">
        <div class="import_item_active import_dependency_option import_item_inner top clearfix">
            [% aspects = {
                'attribute' => 'Algemene attributen',
                'document'  => 'Zaakdocumenten',
                'result'    => 'Resultaten'
            } %]
            [% FOREACH aspect = aspects.keys.sort %]
                <div class="import_item import_object_item">
                    [% item = { label => aspects.$aspect, type=> aspect} %]
                    [% IF casetype.solution %]
                        [% item.selected = { id => item.solution } %]
                    [% END %]
                    [% PROCESS inavigator_import_options %]
                </div>
            [% END %]
        </div>
    </div>
[% END %]


[% BLOCK casetype_header %]
    <div class="import_item_inner clearfix import_item_error">
        <div class="ezra_inavigator_zaaktype_title">
            <div class="title">
                <div class="toggle"></div>
                <strong class="[% dependency_type %]">
                    <span>[% casetype.item('node.titel') %]</span>
                </strong>
            </div>
            <div class="rfa">
                <div class="action">
                    [% item = { label => 'Actie', type=> 'action'} %]
                    <span>[% PROCESS inavigator_import_options %]</span>
                </div>
            </div>
            <div class="actions-nav actions-nav-small button_num_1 ezra_actie_button_handling_navigation">
                <a class="actions-nav-save ezra_inavigator_validate" 
                    href="#overzicht_voegtoe"><span class="nav-icon">Opnieuw</span></a>
            </div>
            <div class="ezra_inavigator_import_checkbox right import_checkbox">
                <input type="checkbox" name="do_import" />
            </div>
        </div>
    </div>
[% END %]



[% BLOCK inavigator_action_options %]
    Actie: [% existing_match_count = casetype.existing_casetypes_rs.count %]

    [% IF existing_match_count == 0 %]
        <input type="hidden" name="action" value="create" /> 
        Nieuw zaaktype aanmaken op basis van zaaktype-sjabloon
    [% ELSIF existing_match_count == 1 %]
        <input type="hidden" name="action" value="update" checked="checked" />
        <input type="hidden" name="zaaktype_id" value="[% casetype.zaaktype_id %]"  />
        Bestaand zaaktype '<strong>[% casetype.existing_casetypes_rs.first.zaaktype_node_id.titel %]</strong>' bijwerken
    [% ELSIF existing_match_count > 1 %]
        Bestaand zaaktype bijwerken (kies hieronder)
        <input type="hidden" name="action" value="update" checked="checked" />
    [% END %]
[% END %]


[% BLOCK inavigator_attribute_options %]
    [% existing_match_count = casetype.existing_casetypes_rs.count %]

    [% IF existing_match_count == 0 %]
        <div class="import_inavigator_group import_inavigator_group_first">
            <h3><div class="toggle"></div>Nieuw zaaktype aanmaken</h3>
            <div class="import_inavigator_group_inner">
                <table>
                 [% PROCESS zaaktype/select.tt 
                    zaaktype_select_label = 'Zaaktype-sjabloon'
                    hide_remember_zaaktype = 1
                %]  
                [% PROCESS beheer/zaaktypen/import/icontroller/bibliotheek_categorie_selector.tt 
                    fieldname = 'zaaktype.bibliotheek_categorie_id'
                    hidden = ''
                %]
                </table>
            </div>
        </div>
    [% ELSIF existing_match_count > 1 %]
        <div class="import_inavigator_group import_inavigator_group_first open">
            <h3><div class="toggle"></div>Bestaand zaaktype bewerken</h3>
            <div class="import_inavigator_group_inner">
                <table>
                    <tr>
                        <td>Bestaand zaaktype:</td>
                        <td>
                            [% existing_casetypes_rs = casetype.existing_casetypes_rs.reset %]
                            <select class="ezra_inavigator_multiple_zaaktype_id_selector" name="zaaktype_id">
                                [% WHILE(existing_casetype = existing_casetypes_rs.next()) %]
                                   <option value="[% existing_casetype.id %]">[% existing_casetype.zaaktype_node_id.titel %]</option>
                                [% END %]
                            </select>
                        </td>
                        <td>[% PROCESS widgets/general/validator.tt %]</td>
                    </tr>
                </table>
            </div>
        </div>
    [% END %]

    <div class="import_inavigator_group">
        <h3><div class="toggle"></div>Algemene attributen</h3>

        <div class="import_inavigator_group_inner" style="display: none;">
            <table> <!-- need this for validator -->
            [% prefix = '' %]
            [% FOREACH field = ICONTROLLER_IMPORT_CONFIG %]
                [% PROCESS beheer/zaaktypen/import/icontroller/validate_field.tt 
                    field_settings = field
                    value = casetype.item(field.field)
                %]
            [% END %]
            </table>
        </div>
    </div>
[% END %]


[% BLOCK inavigator_document_options %]
    <div class="import_inavigator_group import_inavigator_group_results">
        <div class="ezra_inavigator_documents" data-uri="[% c.uri_for('/beheer/icontroller/documents') %]">
            [% IF casetype.zaaktype_id %]
                [% PROCESS beheer/zaaktypen/import/icontroller/documents.tt %]
            [% ELSE %]
                <h3 class="inactive">Zaakdocumenten (kies eerst een zaaktype)</h3>
            [% END %]
        </div>
    </div>
[% END %]



[% BLOCK inavigator_result_options %]
    <div class="import_inavigator_group import_inavigator_group_results">
        <h3 class="blue"><div class="toggle"></div>Resultaten ([% casetype.results.size %])</h3>
        <div class="import_inavigator_group_inner" style="display: none;">    
            [% count = 0 %]
            [% results = casetype.results %]
            [% FOREACH result = results %]
            <table>
                [% prefix = 'result.' _ count %]
                <input type="hidden" name="result" value="[% prefix %]"/>
                [% FOREACH field = ICONTROLLER_IMPORT_RESULTS_CONFIG %]
                    [% PROCESS beheer/zaaktypen/import/icontroller/validate_field.tt 
                        field_settings = field
                        value = result.item(field.field)
                        prefix = prefix
                    %]
                [% END %]
                [% count = count + 1 %]
            </table>
            [% END %]
        </div>
    </div>
[% END %]




[% BLOCK casetype_settings %]
    <form method="POST" action="[% c.uri_for('/beheer/icontroller/import') %]" class="ezra_inavigator_do_import ezra_no_waitstart">
        <div class="import_group_inner">    
            <div class="import_dependency_item ezra_inavigator_zaaktype import_item clearfix">
                [% PROCESS casetype_header %]
                [% PROCESS adjustment %]
            </div>
        </div>
    </form>
[% END %]


<div class="block clearfix import import_inavigator">
    <div class="header">
        I-Controller zaaktypen importeren
        <a href="/beheer/icontroller/flush" class="knop no-icon in-header">Ander bestand importeren</a>
    </div>
    <div class="blockcontent">
        <div class="import-uitleg">
            <div>Hieronder vind je een overzicht van de gevulde zaaktypen uit het importbestand. Dit overzicht is vanwege de performance gemaximeerd tot 100 zaaktypen. Controleer dit overzicht en de instellingen van de zaaktypen voordat je importeert!</div>
        </div>
        <div class="import_dependency import_group import_object_item_header ezra_[% dependency_type %] import_inavigator_actions_toggle">
            <div class="ezra_import_dependency_group_inner">
                [% FOREACH casetype = casetypes %]
                    [% casetype.label = casetype.name %]
                    [% PROCESS casetype_settings 
                        dependency_type = 'Zaaktype'
                    %]             
                [% END %]
            </div>
        </div>
                
        <form class="ezra_import_inavigator_import_all ezra_no_waitstart">
            <div class="form-actions clearfix">
                <input type="submit" name="import" class="button140 right" value="Importeren"/>
            </div>
        </form>
    </div>
</div>
