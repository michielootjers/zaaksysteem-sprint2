(function ( ) {
	
	var registered = {};
	var resolved = {};
	
	// define functions in the global namespace
	
	define = function ( alias, factory ) {
		if(registered[alias]) {
			throw new Error('cannot redefine alias ' + alias);
		}
		registered[alias] = factory;
	};
	
	fetch = function ( alias ) {
		if(!resolved[alias]) {
			resolved[alias] = registered[alias]();
		}
		return resolved[alias];
	};
	
})();
/*global angular,window,$,document,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem', [ 'Zaaksysteem.admin', 'Zaaksysteem.form', 'Zaaksysteem.docs', 'Zaaksysteem.timeline', 'Zaaksysteem.kcc', 'Zaaksysteem.case', 'Zaaksysteem.net', 'Zaaksysteem.data' ])
		.config([ '$httpProvider', '$interpolateProvider', 'smartHttpProvider', '$anchorScrollProvider', function ( $httpProvider, $interpolateProvider, smartHttpProvider, $anchorScrollProvider) {
					
			$interpolateProvider.startSymbol('<[');
			$interpolateProvider.endSymbol(']>');
			
			$httpProvider.defaults.useXDomain = true;
			$httpProvider.defaults.withCredentials = true;
			
			$anchorScrollProvider.disableAutoScrolling();
			
			var prefix = '/',
				loc = window.location.href;
				
			if(loc.match(/^http:\/\/localhost/)) {
				prefix = 'http://dev1.munt.zaaksysteem.nl:3009/';
			}
			
			smartHttpProvider.defaults.prefix = prefix;
			
		}])
		.run([ '$rootScope', '$compile', '$cookies', function ( $rootScope, $compile, $cookies) {
			
			var safeApply = fetch('nl.mintlab.utils.safeApply');
			
			$rootScope.$on('legacyDomLoad', function ( event, jq ) {
				var i,
					l,
					el,
					scope;
					
				for(i = 0, l = jq.length; i < l; ++i) {
					el = jq[i];
					scope = angular.element(el).scope();
					safeApply(scope, function ( ) {
						$compile(el)(scope);
					});
				}
			});
			
			// FIXME(dario): this is very unclean, perhaps a define/fetch() method?
			window.getXSRFToken = function ( ) {
				return $cookies['XSRF-TOKEN'];
			};

            if($cookies['XSRF-TOKEN']) {
                $.ajaxSetup({
                    headers: { 'X-XSRF-TOKEN': $cookies['XSRF-TOKEN'] }
                });
            }
			
			$(document).ajaxSend(function ( event, xhr/*, options*/ ) {
				var accessToken = $cookies['XSRF-TOKEN'];
				xhr.setRequestHeader('X-XSRF-TOKEN', accessToken);
			});
			
		}]);
})();

/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.admin', [ ]);
	
})();
/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.case', [ 'Zaaksysteem.net', 'Zaaksysteem.locale' ]);
	
})();
/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.form', []);
	
})();
/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.data', []);
	
})();
/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.docs', [ 'Zaaksysteem.events', 'Zaaksysteem.net', 'Zaaksysteem.dom' ] )
		.run([ '$document', function ( /*$document*/ ) {
			//$document.find('head').append(angular.element('<link href="/css/docs.css" rel="stylesheet" type="text/css" media="all"/>'));
		}]);
	
	
})();
/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.dom', [ 'Zaaksysteem.events' ]);
	
})();
/*global angular*/
(function ( ) {
	angular.module('Zaaksysteem.events', []);
	
})();
/*global angular*/
(function ( ) {
	angular.module('Zaaksysteem.kcc', [ 'Zaaksysteem.events', 'ngCookies' ]);
	
})();
/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.locale', [ ]);
	
})();
/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.net', [ 'Zaaksysteem.events'] );
	
})();
/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.timeline', [ 'Zaaksysteem.events' ] );
	
})();
/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.admin')
		.controller('nl.mintlab.admin.EmailTemplateController', [ '$scope', '$window', 'smartHttp', 'translationService', function ( $scope, $window, smartHttp, translationService ) {
			
			var indexOf = _.indexOf,
				sortBy = _.sortBy,
				catsById = {};
			
			$scope.categories = [];
			$scope.attachments = [];
			
			$scope.reloadData = function ( ) {
				smartHttp.connect({
					method: 'GET',
					url: 'beheer/bibliotheek/categories'
				})
					.success(function onSuccess ( data ) {
						
						var cats = sortBy(data.result, function ( cat ) { return cat.naam.toLowerCase(); }),
							cat,
							i,
							l,
							ordered = [];
							
						function addToParent ( child, parent ) {
							
							if(parent) {
								parent = catsById[parent.id];
								if(!parent.children) {
									parent.children = [];
								}
								parent.children.push(child);
							}
						}
						
						function addToOrderedList ( child, depth ) {
							var i,
								l,
								children = child.children;
								
							child.depth = depth;
								
							ordered.push(child);
							
							for(i = 0, l = children ? children.length : 0; i < l; ++i) {
								addToOrderedList(children[i], depth+1);
							}
						}
							
						for(i = 0, l = cats.length; i < l; ++i) {
							cat = cats[i];
							catsById[cat.id] = cat;
						}
						
						for(i = 0, l = cats.length; i < l; ++i) {
							cat = cats[i];
							addToParent(cat, cat.pid);
						}
						
						for(i = 0, l = cats.length; i < l; ++i) {
							cat = cats[i];
							if(!cat.pid) {
								addToOrderedList(cat, 0);
							}
						}
						
						$scope.categories = ordered;
						
					})
					.error(function onError ( /*data*/ ) {
						$scope.$emit('systemMessage', {
							type: 'error',
							content: translationService.get('Documentcategorieën konden niet worden geladen')
						});
					});
				
				if($scope.itemId) {
					smartHttp.connect({
						method: 'GET',
						url: 'beheer/bibliotheek/notificaties/get/' + $scope.itemId
					})
						.success(function onSuccess ( data ) {
							var templateData = data.result[0];
							
							$scope.label = templateData.label;
							$scope.subject = templateData.subject;
							$scope.message = templateData.message;
							$scope.category = templateData.bibliotheek_categorie_id.id;
							$scope.attachments = templateData.attachments;
														
						})
						.error(function onError ( /*data*/ ) {
							$scope.$emit('systemMessage', {
								type: 'error',
								content: translationService.get('Data sjabloon kon niet worden geladen')
							});
							$scope.closePopup();
						});
				}
					
			};
			
			$scope.saveTemplate = function ( ) {
				var id = $scope.itemId,
					update = (id !== 0 ? 1 : 0),
					label = $scope.label,
					subject = $scope.subject,
					message = $scope.message,
					category = $scope.category,
					commitMessage = $scope.commitMessage,
					attachments = [],
					caseDoc,
					i,
					l;
					
				for(i = 0, l = $scope.attachments.length; i < l; ++i) {
					caseDoc = $scope.attachments[i];
					if(caseDoc.id !== undefined) {
						attachments.push(caseDoc.id);
					} else {
						attachments.push(caseDoc.bibliotheek_kenmerk_id);
					}
				}
				
				smartHttp.connect({
					method: 'POST',
					url: 'beheer/bibliotheek/notificaties/' + $scope.itemId + '/bewerken/' + $scope.category,
					data: {
						bibliotheek_notificatie_id: id,
						update: update,
						label: label,
						subject: subject,
						message: message,
						bibliotheek_categorie_id: category,
						commit_message: commitMessage,
						attachments: attachments
					}
				})
					.success(function onSuccess ( /*data*/ ) {
						$window.location.reload();
						$scope.closePopup();
					})
					.error(function onError ( /*data*/ ) {
						$scope.$emit('systemMessage', {
							type: 'error',
							content: translationService.get('Sjabloon kon niet worden opgeslagen')
						});
					});
					
			};
			
			$scope.detachCaseDoc = function ( caseDoc ) {
				var index = indexOf($scope.attachments, caseDoc);
				if(index !== -1) {
					$scope.attachments.splice(index, 1);
				}
			};
			
			$scope.getCaseDocLabel = function ( caseDoc ) {
				var label = '';
				
				if(caseDoc) {
					label = caseDoc.naam || caseDoc.naam;
				}
				return label;
			};
			
			$scope.init = function ( ) {
				if($scope.itemId === 0) {
					$scope.commitMessage = translationService.get('Aanmaken');
				} else {
					$scope.commitMessage = translationService.get('Wijzigen');
				}
				
				$scope.category = parseInt($scope.category, 10);
				
				$scope.reloadData();
			};
			
			$scope.isAttached = function ( caseDoc ) {
				var i,
					l,
					attachments = $scope.attachments;
									
				for(i = 0, l = attachments.length; i < l; ++i) {
					if(attachments[i].id === caseDoc.id) {
						return true;
					}
				}
				
				return false;
				
			};
			
			$scope.getCategoryLabel = function ( cat ) {
				var label = cat.naam,
					prefix = '';
					
				if(cat.depth) {
					prefix = new Array(cat.depth + 1).join('--') + ' ';
				}
				return prefix + label;
			};
			
			$scope.$watch('caseDoc', function ( nw/*, old*/ ) {
				if(nw) {
					if(!$scope.isAttached(nw)) {
						$scope.attachments.push(nw);
					}
					$scope.caseDoc = null;
				}
			});
			
		}]);
	
})();
/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.case')
		.controller('nl.mintlab.case.CaseActionController', [ '$scope', 'smartHttp', 'translationService', function ( $scope, smartHttp, translationService ) {
			
			$scope.updateItem = function ( item ) {
				
				smartHttp.connect({
					method: 'POST',
					url: 'zaak/' + $scope.caseId + '/action/update',
					data: {
						id: item.id,
						automatic: item.automatic
					}
				})
					.success(function ( data ) {
						
						var itemData = data.result[0];
						
						for(var key in itemData) {
							item[key] = itemData[key];
						}
						
					})
					.error(function ( /*data*/ ) {
						item.automatic = !item.automatic;
					});
					
			};
			
			$scope.handleCheckboxClick = function ( event ) {
				event.stopPropagation();
			};
			
			$scope.resetAction = function ( event, item ) {
				var wasTainted = item.tainted;
				
				item.tainted = false;
				
				smartHttp.connect({
					method: 'POST',
					url: 'zaak/' + $scope.caseId + '/action/untaint',
					data: {
						id: item.id
					}
				})
					.success(function ( data ) {
						var itemData = data.result[0];
						
						for(var key in itemData) {
							item[key] = itemData[key];
						}
						
						$scope.reloadData();
					})
					.error(function ( /*data*/  ) {
						item.tainted = wasTainted;						
						$scope.$emit('systemMessage', {
							type: 'error',
							content: translationService.get('Actie kon niet hersteld worden')
						});
					});
				
				event.stopPropagation();
			};
			
			$scope.isDisabled = function ( action ) {
				var disabled;
				disabled = $scope.closed || ($scope.lastMilestone && action.data && action.data.relatie_type === 'deelzaak');
				return disabled;
			};
			
		}]);
	
})();
/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.case')
		.controller('nl.mintlab.case.CaseController', [ '$scope', 'smartHttp', 'translationService', function ( $scope, smartHttp, translationService ) {
			
			$scope.templates = [];
			
			function loadTemplates ( ) {
				var caseId = $scope.caseId;
				
				smartHttp.connect({
					url: 'zaak/' + caseId + '/get_sjablonen',
					method: 'GET'
				})
					.success(function onSuccess ( data ) {
						$scope.templates = data.result;
					})
					.error(function onError ( /*data*/ ) {
						$scope.$emit('systemMessage', {
							content: translationService.get('Fout bij het laden van de sjablonen'),
							type: 'error'
						});
					});
			}
			
			$scope.init = function ( caseId ) {
				$scope.caseId = caseId;
				loadTemplates();
			};
			
		}]);
	
})();
/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem.case')
		.controller('nl.mintlab.case.CaseRelationController', [ '$scope', 'smartHttp', function ( $scope, smartHttp ) {
			
			var indexOf = fetch('nl.mintlab.utils.shims.indexOf'),
				arrayMove = fetch('nl.mintlab.utils.collection.arrayMove');
			
			$scope.relations = [];
			
			function parseData ( data ) {
				var relations = data.result || [],
					i,
					l,
					rel;
					
				for(i = 0, l = relations.length; i < l; ++i) {
					rel = relations[i];
					rel.order = i;
				}
				
				$scope.relations = relations;
				
			}
			
			$scope.reloadData = function ( ) {
				smartHttp.connect( {
					method: 'GET',
					url: 'zaak/' + $scope.caseId + '/relations'
				})
					.success(function onSuccess ( data ) {
						parseData(data);
					})
					.error(function onError ( ) {
					});
			};
			
			$scope.init = function ( ) {
				$scope.reloadData();	
			};
			
			$scope.relate = function ( caseObj ) {
				
				var caseId = caseObj.id,
					relation;
					
					
				relation = {
					'case': caseObj
				};
				
				$scope.relations.push(relation);
				
				smartHttp.connect({
					method: 'POST',
					url: 'zaak/' + $scope.caseId + '/relations/add',
					data: {
						case_id: caseId
					}
				})
					.success(function onSuccess ( data ) {
						var result = data.result[0];
						for(var key in result) {
							relation[key] = result[key];			
						}
					})
					.error(function onError ( /*data*/ ) {
						var index = indexOf($scope.relations, relation);
						if(index !== -1) {
							$scope.relations.splice(index, 1);
						}
					});
				
			};
			
			$scope.unrelate = function ( relation ) {
				var relationId = relation.id,
					index = indexOf($scope.relations, relation);
					
				if(index !== -1) {
					$scope.relations.splice(index, 1);
				}
				
				smartHttp.connect({
					method: 'POST',
					url: 'zaak/' + $scope.caseId + '/relations/remove',
					data: {
						relation_id: relationId
					}
				})
					.success(function onSuccess ( /*data*/ ) {
					})
					.error(function onError ( /*data*/ ) {
						$scope.relations.push(relation);
					});
			};
			
			$scope.$on('sort', function ( event, data, from, to ) {
				
				var after = $scope.relations[to-1],
					afterId = after ? after.id : null;
				
				smartHttp.connect({
					url: 'zaak/' + $scope.caseId + '/relations/move',
					method: 'POST',
					data: {
						relation_id: data.id,
						after: afterId
					}
				})
					.success(function onSuccess ( /*data*/ ) {
						
					})
					.error(function onError ( data ) {
						arrayMove($scope.relations, data, from);
					});
				
			});
			
			
			
		}]);
})();
/*global angular,$*/
(function ( ) {
	
	angular.module('Zaaksysteem.case')
		.controller('nl.mintlab.case.CaseTemplateController', [ '$scope', 'smartHttp', 'translationService', function ( $scope, smartHttp, translationService ) {
			
			$scope.getCaseDocById = function ( id ) {
				var caseDoc,
					i,
					l;
					
				for(i = 0, l = $scope.caseDocs.length; i < l; ++i) {
					caseDoc = $scope.caseDocs[i];
					if(caseDoc.bibliotheek_kenmerken_id.id === id) {
						return caseDoc;
					}
				}
				
				return null;
				
			};
			
			$scope.getExtension = function ( template ) {
				return '.' + (template.target_format || 'odt').toLowerCase();
			};
			
			$scope.useTemplate = function ( ) {
				var filename = $scope.filename,
					caseDocId = $scope.caseDocument ? $scope.caseDocument.id : null,
					targetFormat = $scope.targetFormat;
					
				
					
				$('.tab-documents > a').click();
				var templateControllerScope = angular.element($('[data-ng-controller="nl.mintlab.docs.TemplateController"]')[0]).scope();
				templateControllerScope.createFileFromTemplate(filename, $scope.template, caseDocId, targetFormat).then(
					function onSuccess ( ) {
						$scope.$emit('systemMessage', {
							type: 'info',
							content: translationService.get('Sjabloon "%s" toegevoegd', $scope.filename + '.' + targetFormat)
						});
						$scope.closePopup();
					}, function onError ( ) {
						$scope.$emit('systemMessage', {
							type: 'error',
							content: translationService.get('Sjabloon "%s" kon niet worden toegevoegd', $scope.filename + '.' + targetFormat)
						});
						$scope.closePopup();
				});
			};
			
			$scope.saveTemplate = function ( ) {
				
				$scope.action.data.filename = $scope.filename;
				$scope.action.data.bibliotheek_kenmerken_id = $scope.caseDocument ? $scope.caseDocument.bibliotheek_kenmerken_id.id : null;
				
				var data = {
					id: $scope.action.id,
					filename: $scope.action.data.filename,
					bibliotheek_kenmerken_id: $scope.action.data.bibliotheek_kenmerken_id,
					target_format: $scope.targetFormat
				};
				
				smartHttp.connect({
					method: 'POST',
					url: 'zaak/' + $scope.caseId + '/action/data',
					data: data
				})
					.success(function ( data ) {
						
						var itemData = data.result[0];
						for(var key in itemData) {
							$scope.action[key] = itemData[key];
						}
						
						$scope.targetFormat = $scope.action.data.target_format;
						
						$scope.$emit('systemMessage', {
							type: 'info',
							content: translationService.get('Instellingen voor "%s" opgeslagen', $scope.action.label)
						});
						$scope.closePopup();
					})
					.error(function ( /*data*/ ) {
						$scope.$emit('systemMessage', {
							type: 'error',
							content: translationService.get('Instellingen voor "%s" konden niet worden opgeslagen', $scope.action.label)
						});
						$scope.closePopup();
					});
			};
			
			
			$scope.filename = $scope.action.data.filename;
			$scope.caseDocument = $scope.getCaseDocById($scope.action.data.bibliotheek_kenmerken_id);
			$scope.targetFormat = $scope.action.data.target_format;
			
			$scope.$watch('templates', function ( /*nw, old*/ ) {
				var templates = $scope.templates || [];
				for(var i = 0, l = templates.length; i < l; ++i) {
					if(templates[i].bibliotheek_sjablonen_id.id === $scope.action.data.bibliotheek_sjablonen_id) {
						$scope.template = templates[i];
						break;
					}
				}
			});
			
			
		}]);
	
})();
/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem.case')
		.controller('nl.mintlab.case.ChecklistController', [ '$scope', 'smartHttp', function ( $scope, smartHttp ) {
			
			var indexOf = fetch('nl.mintlab.utils.shims.indexOf');
			
			$scope.addItem = function ( label ) {
				var item = {
						id: -1,
						label: label,
						user_defined: true,
						checked: false
					};
				
				$scope.checklistItems.push(item);
				
				smartHttp.connect({
					method: 'POST',
					url: 'zaak/' + $scope.caseId + '/checklist/add_item',
					data: {
						label: label,
						milestone: $scope.phaseId
					}
				})
					.success(function ( data ) {
						var itemData = data.result[0];
						for(var key in itemData) {
							item[key] = itemData[key];
						}
					})
					.error(function ( ) {
						var index = indexOf($scope.checklistItems, item);
						$scope.checklistItems.splice(index, 1);
					});
			};
			
			$scope.removeItem = function ( item ) {
				
				var index = indexOf($scope.checklistItems, item);
				
				$scope.checklistItems.splice(index, 1);
				
				smartHttp.connect( {
					method: 'POST',
					url: 'zaak/' + $scope.caseId + '/checklist/remove_item',
					data: {
						item_id: item.id
					}
				})
					.success(function ( /*data*/ ) {
						
					})
					.error(function ( /*data*/ ) {
						$scope.checklistItems.push(item);
					});
			};
			
			$scope.updateItem = function ( item ) {
				
				var data = {
					
				};
				
				if($scope.closed) {
					item.checked = !item.checked;
					return;
				}
				
				data[item.id] = item.checked;
				
				smartHttp.connect( {
					method: 'POST',
					url: 'zaak/' + $scope.caseId + '/checklist/update',
					data: {
						data: data
					}
				})
					.success(function ( /*data*/ ) {
						
					})
					.error(function ( /*data*/ ) {
						
					});
				
			};
			
		}]);
	
})();
/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.case')
		.controller('nl.mintlab.case.EmailTemplateController', [ '$scope', function ( $scope ) {
			
			
			
		}]);
		
		
})();
/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.case')
		.controller('nl.mintlab.case.SidebarController', [ '$scope', 'smartHttp', function ( $scope, smartHttp ) {
			
			$scope.caseId = null;
			$scope.actions = [];
			$scope.checklistItems = [];
			$scope.caseDocs = [];
			
			function getData ( ) {
				
				smartHttp.connect({
					method: 'GET',
					url: 'zaak/' + $scope.caseId + '/actions',
					params: {
						milestone: $scope.phaseId
					}
				})
					.success(function ( data) {
						$scope.actions = data.result || [];
					})
					.error(function ( /*data*/ ) {
						
					});
					
				if($scope.showChecklist) {
					smartHttp.connect({
						method: 'GET',
						url: 'zaak/' + $scope.caseId + '/checklist/view',
						params: {
							milestone: $scope.phaseId
						}
					})
						.success(function ( data ) {
							$scope.checklistItems = data.result || [];
						})
						.error(function ( /*data*/ ) {
							
						});
				}
				
				smartHttp.connect( {
					url: 'zaak/' + $scope.caseId + '/case_type_documents',
					method: 'GET'
				})
					.success(function ( data ) {
						$scope.caseDocs = data.result;
					})
					.error(function ( /*data*/ ) {
						
					});
			}
			
			$scope.reloadData = function ( ) {
				getData();
			};
			
			$scope.init = function ( caseId, phaseId, closed, showChecklist ) {
				$scope.caseId = caseId;
				$scope.phaseId = phaseId;
				$scope.closed = closed;
				$scope.showChecklist = showChecklist;
				// getData();
			};
			
		}]);
	
})();
/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.controller('nl.mintlab.core.crud.CrudActionController', [ '$scope', function ( $scope ) {
			
			$scope.handleClickAction = function ( event, action ) {
				
			};
			
		}]);

})();
/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.controller('nl.mintlab.core.crud.CrudInterfaceController', [ '$scope', function ( $scope ) {
			
			var indexOf = _.indexOf,
				forEach = _.forEach;
			
			$scope.selectedItems = [];
			
			function applySelected ( selected ) {
				$scope.selectedItems = selected;
			}
			
			$scope.$on('zsSelectableList:change', function ( event, selected ) {
				if(!$scope.$$phase && !$scope.$root.$$phase) {
					$scope.$apply(function ( ) {
						applySelected(selected);
					});
				} else {
					applySelected(selected);
				}
			});
			
			$scope.isAllSelected = function ( ) {
				return $scope.selectedItems && $scope.items && $scope.selectedItems.length === $scope.items.length;
			};
			
			$scope.isSelected = function ( item ) {
				return indexOf($scope.selectedItems, item) !== -1;
			};
			
			$scope.handleSelectAllClick = function ( /*event*/ ) {
				if($scope.isAllSelected()) {
					$scope.deselectAll();
				} else {
					$scope.selectAll();
				}
			};
			
			$scope.selectAll = function ( ) {
				var items = $scope.items || [];
				
				$scope.selectedItems.length = 0;
				$scope.selectedItems.push.apply($scope.selectedItems, items);
			};
			
			$scope.deselectAll = function ( ) {
				$scope.selectedItems.length = 0;	
			};
			
			$scope.deselectItem = function ( item ) {
				var index = indexOf($scope.selectedItems, item);
				if(index !== -1) {
					$scope.selectedItems.splice(index, 1);
				}
			};
			
			$scope.deleteItems = function ( ) {
				var selected = $scope.selectedItems.concat();
				forEach(selected, function ( item/*, key*/ ) {
					$scope.deselectItem(item);
					$scope.items.splice(indexOf($scope.items, item));
				});
			};
			
		}]);
	
})();
/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.controller('nl.mintlab.core.crud.CrudTableController', [ '$scope', '$parse', function ( $scope, $parse ) {
			
			$scope.sortedOn = null;
			$scope.reversed = false;
			
			$scope.sortOn = function ( column ) {
				if($scope.isSortedOn(column)) {
					$scope.reverse();
				}
				$scope.sortedOn = column;
			};
			
			$scope.isSortedOn = function ( column ) {
				return $scope.sortedOn === column;
			};
			
			$scope.reverse = function ( ) {
				$scope.reversed = !$scope.reversed;
			};
			
			$scope.getColumnValue = function ( column, item ) {
				var resolve = column.resolve + (column.filter ? '|' + column.filter : ''),
					getter = $parse(resolve);
				return getter(item);
			};
			
			$scope.getSortValue = function ( item ) {
				var col = $scope.sortedOn,
					getter;
					
				if(!col) {
					return $scope.items.indexOf(item);
				}
				
				if(!col.sort) {
					return $scope.getColumnValue(col, item);
				}
				
				if(typeof col.sort === 'string') {
					getter = $parse(col.sort);
				} else {
					getter = $parse(col.sort.resolve);
				}
				return getter(item);
			};
			
		}]);
	
})();
/*global angular*/
(function ( ) {
	
	var TEMPLATE_URL = '/html/core/crud/crud-interface.html';
	
	angular.module('Zaaksysteem')
		.directive('zsCrudTemplateParser', [ '$compile', '$parse', '$q', '$timeout', 'smartHttp', 'templateCompiler', function ( $compile, $parse, $q, $timeout, smartHttp, templateCompiler ) {
			
			return {
				compile: function ( ) {
					
					return function link ( scope, element, attrs ) {
						
						var configUrl = scope.$eval(attrs.zsCrudTemplateParser),
							templateUrl = scope.$eval(attrs.zsCrudTemplateUrl) || TEMPLATE_URL,
							baseUrl,
							templateElement;
							
						function compileInterface ( ) {
								
							templateCompiler.getCompiler(templateUrl).then(function ( compiler ) {
								compiler(scope, function ( clonedElement ) {
									for(var i = 0, l = clonedElement.length; i < l; ++i) {
										element[0].appendChild(clonedElement[0]);
									}
								}); 
							});
							
						}
						
						function setActions ( actions ) {
							scope.actions = actions;
						}
						
						function setColumns ( columns ) {
							scope.columns = columns;
						}
						
						function setUrl ( url ) {
							baseUrl = url;
						}
						
						function setTemplateElement ( el ) {
							templateElement = el;
						}
						
						function setItems ( items ) {
							scope.items = items;
						}
						
						function setOptions ( options ) {
							scope.options = options;
						}
						
						function loadData ( ) {
							smartHttp.connect({
								url: baseUrl,
								method: 'GET'
							})
								.success(function ( data ) {
									setItems(data.result);
								})
								.error(function ( data ) {
									console.log('Encountered error while collecting item data', data);
								});
						}
						
						function setConfig ( config ) {
							setActions(config.actions);
							setColumns(config.columns);
							setOptions(config.options);
							setUrl(config.url);
						}
												
						$q.all([
									smartHttp.connect({
										url: configUrl,
										method: 'GET'
									})
										.success(function ( data ) {
											setConfig(data);
										}),
									templateCompiler.getElement(templateUrl).then(function ( element ) {
										setTemplateElement(element);
									})
								]
						).then(function ( ) {
							$timeout(function ( ) {
								compileInterface();
								loadData();
							});
						});
						
					};
				}
				
			};
			
		}]);
	
})();
/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.form')
		.controller('nl.mintlab.core.form.FormController', [ '$scope', '$parse', '$interpolate', 'smartHttp', function ( $scope, $parse, $interpolate, smartHttp ) {
			
			var forEach = _.forEach,
				indexOf = _.indexOf,
				promises = [];
				
			function watch ( promise ) {
				var obj = {
					promise: promise
				};
				
				obj.unwatch = $scope.$watch(promise.watch, function ( nw/*, old*/ ) {
					if(nw===$parse(promise.when)($scope)) {
						$parse(promise.then)($scope);
					}
				});
				
				promises.push(obj);
				
			}
			
			function unwatch ( promise ) {			
				forEach(promises.concat(), function ( p ) {
					if(p.promise === promise) {
						p.unwatch();
						promises.splice(indexOf(p, 1));
					}
				});
			}
			
			function setFieldData ( ) {
				var fields = $scope.fields || [],
					field,
					i,
					l,
					val;
					
				for(i = 0, l = fields.length; i < l; ++i) {
					field = fields[i];
					val = undefined;
					if(field.value !== undefined) {
						val = field.value;
					} else {
						val = field['default'];
					}
					if(val !== undefined) {
						$scope[field.name] = $interpolate(val)($scope);
					}
				}
			}
			
			function resolve ( field ) {
				var value = $scope[field.name];
				if(field.data && field.data.resolve) {
					value = $parse(field.data.resolve)(value);
				}
				return value;
			}
			
			function getForm ( ) {
				return $scope[$scope.getName()];
			}
			
			function submitForm ( action ) {
				var url = action.data.url,
					data = {},
					fields = $scope.fields || [],
					field,
					i,
					l;
					
				$scope.submitting = true;
				
				for(i = 0, l = fields.length; i < l; ++i) {
					field = fields[i];
					if($scope.showField(field)) {
						data[field.name] = resolve(field);
					}
				}
				
				smartHttp.connect( {
					url: url,
					method: 'POST',
					data: data
				})
					.success(function onSuccess ( data ) {
						$scope.submitting = false;
					})
					.error(function onError ( data ) {
						$scope.submitting = false;
						validate(data);
					});
			}
			
			function validate ( data ) {
				var form = getForm(),
					fields = data.data || [],
					messages = data.messages || [],
					field,
					control,
					validityType,
					i,
					l;
					
				for(i = 0, l = fields.length; i < l; ++i) {
					field = fields[i];
					switch(field.result) {
						default:
						validityType = '';
						break;
						
						case 'missing':
						validityType = 'required';
						break;
						
						case 'invalid':
						validityType = 'pattern';
						break;
					}
					control = form[field.parameter];
					if(control) {
						control.$setValidity(validityType, false);
					}
				}
				
				for(i = 0, l = messages.length; i < l; ++i) {
					
				}
			}
			
			$scope.handleActionClick = function ( action/*, event*/ ) {
				
				switch(action.type) {
					case 'submit':
					submitForm(action);
					break;
				}
				
			};
			
			$scope.isActionDisabled = function ( action ) {
				var form = getForm(),
					isDisabled;
										
				switch(action.type) {
					case 'submit':
					isDisabled = !form.$valid;
					break;
					
					default:
					isDisabled = true;
					break;
				}
				
				return isDisabled;
			};
			
			$scope.showField = function ( field ) {
				var when = field.when;
				return when === undefined || $parse(when)($scope);
			};
			
			$scope.getRequired = function ( field ) {
				var required = field.required,
					isRequired;
					
				if(typeof required === 'boolean') {
					isRequired = required;
				} else {
					isRequired = $parse(required)($scope);
				}
				return isRequired;
			};
			
			$scope.getFieldId = function ( field ) {
				return field.name;
			};
			
			$scope.getPlaceholder = function ( field ) {
				var placeholder = field.data ? field.data.placeholder : undefined;
				return placeholder !== undefined ? $parse(placeholder)($scope) : '';
			};
			
			$scope.getName = function ( ) {
				return $scope.name;
			};
			
			// we can't use a dynamic ngModel value because
			// it's not interpolated (just parsed), so we
			// expose the scope object
			// see: https://github.com/angular/angular.js/issues/1404
			$scope.scope = $scope;
			
			$scope.$watch('promises', function ( nw, old ) {
				
				if(old && old.length) {
					forEach(old, function ( value ) {
						unwatch(value);
					});
				}
				
				if(nw && nw.length) {
					forEach(nw, function ( value ) {
						watch(value);
					});
				}
				
			});
			
			$scope.$watch('fields', function ( /*nw, old*/ ) {
				setFieldData();
			});
			
		}]);
	
})();
/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.form')
		.controller('nl.mintlab.core.form.ObjectSearchController', [ '$scope', function ( $scope ) {
			
			var indexOf = _.indexOf,
				objectUnwatch;
			
			$scope.limit = Number.MAX_VALUE;
			
			$scope.objects = [];
						
			$scope.addObject = function ( obj ) {
				var index = indexOf($scope.objects, obj);
				if(index === -1) {
					
					while($scope.objects.length >= $scope.limit) {
						$scope.removeObject($scope.objects[0]);
					}
					
					$scope.objects.push(obj);
				}
			};
			
			$scope.removeObject = function ( obj ) {
				var index = indexOf($scope.objects, obj);
				if(index !== -1) {
					$scope.objects.splice(index, 1);
				}
			};
			
			objectUnwatch = $scope.$watch('objects', function ( nw/*, old*/ ) {
				if(nw && nw.length && $scope.limit === 1) {
					$scope.newObject = nw[0];
				}
			});
			 
			$scope.$watch('newObject', function ( nw, old ) {
				if(nw) {
					$scope.addObject(nw);
					if($scope.limit > 1) {
						$scope.newObject = null;
					}
				}
				if(!nw && old && $scope.limit === 1) {
					$scope.removeObject(old);
				}
			});
			
			$scope.$watch('limit', function ( nw/*, old*/ ) {
				if(!nw) {
					throw new Error('Limit has to be larger than 0');
				}
			});
			
		}]);
	
})();
/*global angular,$,updateField,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.form')
		.directive('zsBagInput', [ '$timeout', function ( $timeout ) {
			
			var isEqual = _.isEqual;
			
			return {
				
				compile: function ( /*tElement, tAttrs, transclude*/ ) {
					
					return function link ( scope, element, attrs ) {
						
						var modelId = scope.$eval(attrs.zsBagInput);
						
						scope.$watch(modelId, function ( nw, old ) {
							if(!isEqual(nw, old)) {
								$timeout(function ( ) {
									updateField($(element[0]), null, scope.$eval(attrs.zsBagInputName));
								}, 0);
							}
						}, true);
						
					};
				}
			};
			
		}]);
	
})();
/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.form')
		.directive('zsFormField', [ function ( ) {
			
			return {
				priority: 100,
				require: [ 'ngModel' ],
				compile: function ( ) {
					
					return function link ( scope, element, attrs, controllers ) {
						
						var ngModel = controllers[0];
						
						attrs.$set('name', scope.field.name);
						element.attr('name', scope.field.name);
						ngModel.$name = scope.field.name;
						
					};
					
				}
			};
			
		}]);
	
})();
/*global angular*/
(function ( ) {
	
	var TEMPLATE_URL = '/html/core/form/form.html';
	
	angular.module('Zaaksysteem.form')
		.directive('zsFormTemplateParser', [ '$q', '$timeout', 'smartHttp', 'templateCompiler', '$compile', function ( $q, $timeout, smartHttp, templateCompiler, $compile ) {
			
			return {
				scope: true,
				compile: function ( /*tElement, tAttrs, transclude*/ ) {
					
					return function link ( scope, element, attrs ) {
						
						var configUrl = scope.$eval(attrs.zsFormTemplateParser),
							templateUrl = scope.$eval(attrs.zsFormTemplateUrl) || TEMPLATE_URL,
							compiler;
							
						function compileInterface ( ) {
							compiler(scope, function ( clonedElement ) {
								for(var i = 0, l = clonedElement.length; i < l; ++i) {
									element[0].appendChild(clonedElement[0]);
								}
							}); 
						}
						
						function setConfig ( config ) {
							var data = config.data;
							
							scope.name = config.name;
							scope.fields = config.fields;
							scope.actions = config.actions;
							scope.promises = config.promises;
							
							for(var key in data) {
								scope[key] = data[key];
							}
						}
												
						$q.all([
							smartHttp.connect({
								url: configUrl,
								method: 'GET'
							})
								.success(function ( data ) {
									setConfig(data);
								}),
							templateCompiler.getElement(templateUrl).then(function ( element ) {
								var clone = element.clone();
								for(var i = 0, l = clone.length; i < l; ++i) {
									if(clone[i].tagName.toLowerCase() === 'form') {
										clone.eq(i).attr('name', scope.name);
										break;
									}
								}
								compiler = $compile(clone);
							})
						])
						.then(function ( ) {
							$timeout(function ( ) {
								compileInterface();
							});
						});
						
					};
				}
				
			};
			
		}]);
	
})();
/*global angular,fetch,window,setTimeout*/
(function ( ) {
	
	angular.module('Zaaksysteem.data')
		.provider('dataStore', [ function ( ) {
			
			var	win = window,
				isArray = angular.isArray,
				EventDispatcher = fetch('nl.mintlab.events.EventDispatcher'),
				data = {},
				listeners = {};
			
			function set ( id, d ) {
				data[id] = d;
				getListener(id).trigger();
			}
			
			function get ( id ) {
				return data[id];
			}
			
			function unset ( id ) {
				set(id, undefined);
				delete data[id];
			}
			
			function push ( id, d ) {
				var arr = data[id];
				if(!isArray(arr)) {
					arr = [];
				}
				arr.push(d);
				set(id, arr);
			}
			
			function observe ( id, callback ) {
				setTimeout(function ( ) {
					getListener(id).trigger();
				},0);
				return getListener(id).observe(callback);
			}
			
			function getListener ( id ) {
				var listener = listeners[id];
				if(!listener) {
					listener = listeners[id] = createListener(id);
				}
				return listener;
			}
			
			function createListener ( id ) {
				var dispatcher = new EventDispatcher();
				return {
					observe: function ( callback ) {
						dispatcher.subscribe('dataChange', callback);
						return function ( ) {
							dispatcher.unsubscribe('*', callback);
						};
					},
					trigger: function ( ) {
						dispatcher.publish('dataChange', get(id));
					}
				};
			}
			
			
			var dataStore = {
				get: get,
				set: set,
				unset: unset,
				push: push,
				observe: observe
			};
			
			win.dataStore = dataStore;
			
			return {
				$get: function ( ) {
					return dataStore;
				}
			};
			
			
		}]);
	
})();
/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem.docs')
		.controller('nl.mintlab.docs.DocumentController', [ '$scope', 'smartHttp', '$filter', '$routeParams', 'fileUploader', '$timeout', function ( $scope, smartHttp, $filter, $routeParams, fileUploader, $timeout ) {
				
			var Folder = fetch('nl.mintlab.docs.Folder'),
				File = fetch('nl.mintlab.docs.File');
			
			$scope.trash = new Folder( { name: 'Prullenbak' });
			$scope.list = new Folder( { name: 'Map' });
			$scope.intake = new Folder( { name: 'Intake' });
			$scope.caseDocs = [];
			$scope.docCats = [];
			$scope.initialized = false;
			$scope.readOnly = false;
			$scope.loading = false;
			
			$scope.view = 'list';
			
			$scope.reloadData = function ( ) {
				
				var callsLeft = 4;
				
				function onComplete ( ) {
					callsLeft--;
					if(!callsLeft) {
						$scope.loading = false;
					}
				}
				
				$scope.caseDocs.length = 0;
				$scope.docCats.length = 0;
				
				$scope.trash.empty();
				$scope.list.empty();
				$scope.intake.empty();
				
				$scope.$broadcast('reload');
				
				$scope.loading = true;
				
				smartHttp.connect({
					url: ($scope.pip ? 'pip/' : '') + 'file/search/case_id/' + $scope.caseId,
					method: 'GET'
				})
					.success(processFileData)
					.then(onComplete, onComplete);
					
				if(!$scope.pip) {
					smartHttp.connect( {
						url: 'directory/search/case_id/' + $scope.caseId,
						method: 'GET'
					})
						.success(processFolderData)
						.then(onComplete, onComplete);
				}
				
				if(!$scope.pip) {	
					smartHttp.connect( {
						url: 'zaak/' + $scope.caseId + '/case_type_documents',
						method: 'GET'
					})
						.success(processCaseDocData)
						.then(onComplete, onComplete);
				}
					
				if(!$scope.pip) {
					smartHttp.connect( {
						url: 'file/document_categories',
						method: 'GET'
					})
						.success(processDocCatData)
						.then(onComplete, onComplete);
				}
			};
			
			$scope.setView = function ( view ) {
				$scope.view = view;
			};
			
			$scope.addFolder = function ( folder, folderName ) {
				var child = new Folder( { name: folderName });
				folder.add(child);
			};
			
			$scope.uploadFile = function ( files ) {
				
				var url = $scope.pip ? 'pip/file/create' : 'file/create';
				
				angular.forEach(files, function ( value ) {
					fileUploader.upload(value, smartHttp.getUrl(url), {
						'case_id': $scope.caseId
					}).then(function ( upload ) {
						var file = new File(),
							result = upload.getData().result,
							data = result ? result[0] : null;
							
						file.updating = true;
							
						file.updateWith(data);
						
						$timeout(function ( ) {
							file.updating = false;
						});
						
						if(!file.accepted) {
							$scope.intake.add(file);
						} else {
							$scope.list.add(file);
						}
						
					}, function ( /*upload*/ ) {
						
					});
				});
			};
			
			$scope.replaceFile = function ( file, replace ) {
				var replacement = replace[0];
				
				file.updating = true;
				
				if(replacement) {
					fileUploader.upload(replacement, smartHttp.getUrl('file/update_file'), {
						'file_id': file.id
					}).then(function ( upload ) {
						var result = upload.getData().result,
							data = result ? result[0] : null;
						file.updating = false;
						file.updateWith(data);
					}, function ( /*upload*/ ) {
						file.updating = false;
					});
				}
			};
			
			function processFileData ( data ) {
				var files = data.result || [],
					parsedFiles = [];
					
				angular.forEach(files, function ( file ) {
					parsedFiles.push(parseFileData(file));
				});
				
				$scope.initialized = true;
			}
			
			function processFolderData ( data ) {
				var folders = data.result || [];
				
				angular.forEach(folders, function ( folder ) {
					parseFolderData(folder);
				});
			}
			
			function processCaseDocData ( data ) {
				$scope.caseDocs = data.result || [];
			}
			
			function processDocCatData ( data ) {
				var docCats = data.result || [],
					result = [];
				
				for(var i = 0, l = docCats.length; i < l; ++i) {
					result.push({
						index: i,
						label: docCats[i],
						value: docCats[i]
					});
				}
				
				$scope.docCats = result;
			}
			
			function parseFileData ( fileData ) {
				var file = new File(),
					parent;
					
				file.updateWith(fileData);
				
				if(file.accepted && !file.date_deleted) {
					if(file.directory_id) {
						parent = $scope.list.getEntityByPath(file.directory_id.id) || parseFolderData(file.directory_id);
					}
					
					if(!parent) {
						parent = $scope.list;
					}
					
				} else if(!file.accepted) {
					parent = $scope.intake;
				} else {
					parent = $scope.trash;
				}
				parent.add(file);
			}
			
			function parseFolderData ( folderData ) {
				var folder = $scope.list.getEntityByPath( [ 'folder_' + folderData.id ]);
					
				if(!folder){
					folder = new Folder( { id: folderData.id, uuid: folderData.id } );
					$scope.list.add(folder);
				}
					
				folderData.uuid = folderData.id;
					
				folder.updateWith(folderData);
				
				return folder;
			}
			
			$scope.getCaseDocLabel = function ( caseDoc ) {
				var label = '';
				
				if(caseDoc) {
					label = caseDoc.label || caseDoc.bibliotheek_kenmerken_id.naam;
				}
				return label;
			};
			
			$scope.isSameCaseDoc = function ( caseDocA, caseDocB ) {
				if(caseDocA === caseDocB) {
					return true;
				}
				
				if(!caseDocA || !caseDocB) {
					return false;
				}
				
				return caseDocA.id === caseDocB.id;
			};
			
			$scope.isNotReferentialCaseDoc = function ( caseDoc ) {
				return !caseDoc || !caseDoc.referential;
			};
			
			$scope.init = function ( ) {
				
			};
			
			$scope.$watch('caseId', function ( nw/*, old*/ ) {
				if(nw) {
					$scope.reloadData();
				}
			});
			
			$scope.$on('drop', function ( event, data, mimeType ) {
				if(mimeType === 'Files') {
					$scope.uploadFile(data);
				}
			});
			
		}]);
})();
/*global angular,fetch,$*/
(function ( ) {
	
	angular.module('Zaaksysteem.docs')
		.controller('nl.mintlab.docs.DocumentIntakeController', [ '$scope', 'smartHttp', function ( $scope, smartHttp ) {
			
			var File = fetch('nl.mintlab.docs.File'),
				Folder = fetch('nl.mintlab.docs.Folder');
			
			$scope.readOnly = false;
			$scope.docGlobal = new Folder( { name: 'global' } );
			$scope.loading = false;
			
			$scope.initialize = function ( ) {
				
				$scope.loading = true;
				
				smartHttp.connect({
					url: 'file/search_queue',
					method: 'GET'
				})
					.success(function ( data ) {
						var docs,
							file,
							i,
							l;
							
						docs = data.result || [];
						
						for(i = 0, l = docs.length; i < l; ++i) {
							file = new File();
							file.updateWith(docs[i]);
							$scope.docGlobal.add(file);
						}
						
					})
					.error(function ( ) {
						
					})
					.then(function ( ) {
						$scope.loading = false;
					}, function ( ) {
						$scope.loading = false;
					});
			};
			
			$scope.registerCase = function ( file ) {
				
				$('#ezra_nieuwe_zaak_tooltip').trigger({
					type: 'nieuweZaakTooltip',
					show: 1,
					popup: 1,
					action: '/zaak/create/?actie=doc_intake&amp;actie_value=' + file.id + '&amp;actie_description=Document%20aan%20zaak%20toevoegen'
				});
				
			};
			
			
		}]);
})();
/*global angular,fetch*/
(function ( ) {
	angular.module('Zaaksysteem.docs')
		.controller('nl.mintlab.docs.DocumentListController', [ '$scope', '$q', 'smartHttp', function ( $scope, $q, smartHttp ) {
			
			// TODO(dario): refactor to use different controllers for
			// list,intake,trash, using js inheritance
			
			var File = fetch('nl.mintlab.docs.File'),
				Folder = fetch('nl.mintlab.docs.Folder'),
				indexOf = fetch('nl.mintlab.utils.shims.indexOf'),
				words,
				cutFiles = [],
				_url = 'file/';
			
			$scope.root = null;
			$scope.viewType = 'listView';
			$scope.selectedFiles = [];
			
			$scope.maxDepth = 1;
			$scope.filterQuery = '';
			$scope.dragging = false;
			
			function clearData ( ) {
				$scope.deselectAll();
			}
			
			$scope.onToggleClick = function ( event, entity ) {
				if($scope.isSelected(entity)) {
					$scope.deselectEntity(entity);
				} else {
					$scope.selectEntity(entity);
				}
				
				if(event) {
					event.stopPropagation();
				}
			};
			
			$scope.onEntityClick = function ( event, entity ) {
				if($scope.isSelected(entity) && $scope.selectedFiles.length === 1) {
					$scope.deselectEntity(entity);
				} else {
					$scope.deselectAll();
					$scope.selectEntity(entity);
				}
			};
			
			$scope.onSelectAllClick = function ( /*event*/ ) {
				if($scope.isSelected($scope.root)) {
					$scope.deselectAll();
				} else {
					$scope.selectAll();
				}
			};
			
			$scope.selectEntity = function ( entity, recursive ) {
				if(!entity.getSelected()) {
					entity.setSelected(true);
					if(entity !== $scope.root) {
						$scope.selectedFiles.push(entity);
					}
				}
				
				if(recursive === true && entity instanceof Folder) {
					angular.forEach(entity.getFolders(), function ( value ) {
						$scope.selectEntity(value, true);
					});
					angular.forEach(entity.getFiles(), function ( value ) {
						$scope.selectEntity(value);
					});
				}
				
				setRootState();
			};
			
			$scope.deselectEntity = function ( entity, recursive ) {
				var selectedFiles = $scope.selectedFiles,
					index = indexOf(selectedFiles, entity);
				
				if(entity.getSelected()) {
					entity.setSelected(false);
					if(entity !== $scope.root) {
						selectedFiles.splice(index, 1);
					}
				}
				
				if(recursive === true && entity instanceof Folder) {
					angular.forEach(entity.getFolders(), function ( value ) {
						$scope.deselectEntity(value, true);
					});
					angular.forEach(entity.getFiles(), function ( value ) {
						$scope.deselectEntity(value);
					});
				}
					
				
				$scope.root.setSelected(false);
			};
			
			$scope.selectAll = function ( ) {
				$scope.selectEntity($scope.root, true);
			};
			
			$scope.deselectAll = function ( ) {
				$scope.deselectEntity($scope.root, true);
				while($scope.selectedFiles.length) {
					$scope.deselectEntity($scope.selectedFiles[0]);
				}
			};
			
			$scope.isSelected = function ( entity ) {
				if(!entity) {
					return null;
				}
				return entity.getSelected();
			};
			
			$scope.moveEntity = function ( entities, target ) {
				angular.forEach(entities, function ( entity ) {
					
					var currentParent = entity.getParent(),
						directoryId = target.getDepth() > 0 ? target.id : null;
					
					if(validateEntity(entity, target)) {
						target.add(entity);
					}
					
					entity.updating = true;
					
					smartHttp.connect({
						url: 'file/update/',
						method: 'POST',
						data: {
							file_id: entity.id,
							directory_id: directoryId
						}
					})
						.success(function ( ) {
							entity.directory_id = target.id;
							entity.updating = false;
						})
						.error(function ( ) {
							currentParent.add(entity);
							entity.updating = false;
						});
				});
			};
			
			$scope.removeEntity = function ( entities ) {
				var deferred = $q.defer(),
					queue;
				
				entities = entities.concat();
				queue = entities.concat();
				
				function complete ( entity ) {
					var index = indexOf(queue, entity);
					if(index !== -1) {
						queue.splice(index, 1);
						if(queue.length === 0) {
							deferred.resolve();
						}
					}
					entity.updating = false;
				}
				
				angular.forEach(entities, function ( entity ) {
					
					var parent = entity.getParent();
					
					entity.updating = true;
					
					if(!parent || parent === $scope.trash) {
						// file was already removed
						complete(entity);
						return;
					}
					
					if($scope.isSelected(entity)) {
						$scope.deselectEntity(entity);
					}
					
					if(entity.id === -1) {
						// file hasn't been saved
						parent.remove(entity);
						complete(entity);
						return;
					}
					
					if(entity instanceof File) {
						if($scope.caseId) {
							$scope.trash.add(entity);
						} else {
							parent.remove(entity);
						}
						smartHttp.connect({
							url: _url + 'update/',
							method: 'POST',
							data: {
								file_id: entity.id,
								deleted: true
							}
						})
							.success(function ( data ) {
								if(data.result && data.result[0]) {
									entity.updateWith(data.result[0]);
								}
								entity.deleted = true;
							})
							.error(function ( ) {
								parent.add(entity);
							})
							.then(function ( ) {
								complete(entity);
							});
							
					} else if(entity.getFiles().length === 0) {
						parent.remove(entity);
						smartHttp.connect({
							url: 'directory/delete/',
							method: 'POST',
							data: {
								directory_id: entity.id
							}
						})
							.success(function ( ) {
							
							})
							.error(function ( ) {
								parent.add(entity);
							})
							.then(function ( ) {
								complete(entity);
							});
					} else {
						var children = entity.getFiles();
						$scope.removeEntity(children).then(function ( ) {
							$scope.removeEntity([entity]);
						});
					}
					
				});
				
				if(!queue.length) {
					deferred.resolve();
				}
				
				return deferred.promise;
				
			};
			
			$scope.restoreEntity = function ( entities ) {
				entities = entities.concat();
				angular.forEach(entities, function ( entity ) {
					$scope.list.add(entity);
					if($scope.isSelected(entity)) {
						$scope.deselectEntity(entity);
					}
					
					smartHttp.connect({
						url: _url + 'update/',
						method: 'POST',
						data: {
							file_id: entity.id,
							deleted: false
						}
					})
						.success(function ( ) {
							entity.deleted = false;
						})
						.error(function ( ) {
							$scope.trash.add(entity);
						});
				});
			};
			
			$scope.destroyEntity = function ( entities ) {
				
				entities = entities.concat();
				angular.forEach(entities, function ( entity ) {
					
					entity.getParent().remove(entity);
					
					if($scope.isSelected(entity)) {
						$scope.deselectEntity(entity);
					}
					
					smartHttp.connect( {
						url: _url + 'update/',
						method: 'POST',
						data: {
							file_id: entity.id,
							destroyed: true
						}
					})
						.success(function ( ) {
							
						})
						.error(function ( ) {
							$scope.trash.add(entity);
						});
				});
				
			};
			
			$scope.acceptFile = function ( files ) {
				files = files.concat();
				angular.forEach(files, function ( file ) {
					
					file.updating = true;
					
					if($scope.isSelected(file)) {
						$scope.deselectEntity(file);
					}
					
					if(Number(file.is_revision) !== 1) {
						$scope.list.add(file);
						smartHttp.connect({
							url: _url + 'update/',
							method: 'POST',
							data: {
								file_id: file.id,
								accepted: true
							}
						})
							.success(function ( data ) {
								var fileData = data.result[0];
								file.updateWith(fileData);
								file.updating = false;
								if(file.destroyed) {
									file.getParent().remove(file);
								}
							})
							.error(function ( ) {
								$scope.intake.add(file);
								file.updating = false;
							});
					} else {
						
						var flatFileList = getFlatFileList(),
							i = 0,
							l = flatFileList.length,
							f,
							oldFile,
							parent = $scope.list;
							
						for(; i < l; ++i) {
							f = flatFileList[i];
							if(f.id === file.is_duplicate_of) {
								oldFile = f;
								break;
							}
						}
						
						if(oldFile) {
							parent = oldFile.getParent();
							parent.remove(oldFile);
						}
						
						parent.add(file);
						
						smartHttp.connect({
							url: 'file/update_file',
							method: 'POST',
							data: {
								file_id: file.is_duplicate_of,
								existing_file_id: file.id
							}
						})
							.success(function ( data ) {
								var fileData = data.result[0];
								file.updateWith(fileData);
								file.updating = false;
							})
							.error(function ( ) {
								$scope.intake.add(file);
								file.updating = false;
							});
					}
					
					
				});
			};
			
			$scope.rejectFile = function ( files ) {
				files = files.concat();
				angular.forEach(files, function ( file ) {
					if($scope.isSelected(file)) {
						$scope.deselectEntity(file);
					}
					$scope.intake.remove(file);
					smartHttp.connect({
						url: _url + 'update/',
						method: 'POST',
						data: {
							file_id: file.id,
							accepted: false,
							//TODO(dario): get this from ui
							rejection_reason: 'foo'
						}
					})
						.success(function ( ) {
						})
						.error(function ( ) {
							$scope.intake.add(file);
						});
				});
			};
			
			$scope.setCaseDoc = function ( files, caseDoc, clearPrevious ) {
				
				angular.forEach(files, function ( file ) {
					
					var prevCaseDocId = file.case_type_document_id,
						prevCaseDocOwner,
						hasChanged = !$scope.isSameCaseDoc(file.case_type_document_id, caseDoc),
						data;
						
					if(!hasChanged) {
						return;
					}
						
					file.updating = true;
					
					if(clearPrevious && caseDoc) {
						
						clearPrevious.case_type_document_id = null;
						prevCaseDocOwner = clearPrevious;
						
					}
					
					file.case_type_document_id = caseDoc;
					
					data = {
						file_id: file.id,
						case_type_document_id: caseDoc ? caseDoc.id : null
					};
					
					if(clearPrevious) {
						data.case_type_document_clear_old = prevCaseDocOwner.id;
					}
					
					smartHttp.connect({
						url: 'file/update',
						method: 'POST',
						data: data
					})
						.success(function ( data ) {
							file.updateWith(data.result[0]);
							$scope.$emit('casedocupdate');
						})
						.error(function ( ) {
							file.case_type_document_id = prevCaseDocId;
							if(prevCaseDocOwner) {
								prevCaseDocOwner.case_type_document_id = caseDoc;
							}
						})
						.then(function ( ) {
							file.updating = false;
						});
                });
			};
			
			$scope.getCaseDocAssignment = function ( caseDoc ) {
				
				var id = caseDoc ? caseDoc.id : null,
					flatFileList = getFlatFileList(),
					i,
					l,
					file;
					
					
				for(i = 0, l = flatFileList.length; i < l; ++i) {
					file = flatFileList[i];
					if(file.case_type_document_id && file.case_type_document_id.id === id) {
						return file;
					}
				}
				
				return null;
				
				
			};
			
			$scope.filterEntity = function ( entity ) {
				var name = entity.name,
					desc = entity.desc,
					type = entity.type ? entity.type.name : '',
					i,
					l,
					j,
					m,
					toTest = [],
					word,
					match;
				
				if($scope.filterQuery === '') {
					return true;
				}
					
				if(name) {
					toTest.push(name.toLowerCase());
				}
				if(desc) {
					toTest.push(desc.toLowerCase());
				}
				if(type) {
					toTest.push(type.toLowerCase());
				}
				
				for(i = 0, l = words.length, m = toTest.length; i < l; ++i) {
					word = words[i].toLowerCase();
					match = false;
					for(j = 0; j < m; ++j) {
						if(toTest[j].indexOf(word) !== -1) {
							match = true;
							break;
						}
					}
					if(!match) {
						return false;
					}
				}
				
				return true;
			};
			
			$scope.getFilteredChildren = function ( source ) {
				
				var filtered = [];
				
				function filterChildren ( parent, filterRoot ) {
					var i,
						l,
						children,
						entity;
						
						
					if(filterRoot && $scope.filterEntity(parent)) {
						filtered.push(parent);
					}
					
					children = parent.getFolders();
						
					for(i = 0, l = children.length; i < l; ++i) {
						entity = children[i];
						if($scope.filterEntity(entity)) {
							filtered.push(entity);
						}
						filterChildren(entity, true);
					}
					
					children = parent.getFiles();
					for(i = 0, l = children.length; i < l; ++i) {
						entity = children[i];
						if($scope.filterEntity(entity)) {
							filtered.push(entity);
						}
					}
				}
				
				if(!source) {
					source = $scope.root;
				}
			
				filterChildren(source, false);
				
				return filtered;
				
			};
			
			$scope.getAttachments = function ( ) {
				var attachments = [],
					selection = $scope.selectedFiles.concat(),
					file;
					
				for(var i = 0, l = selection.length; i < l; ++i) {
					file = selection[i];
					if(file.getEntityType() === 'file') {
						attachments.push(file);
					}
				}
				return attachments;
			};
			
			$scope.clearCutSelection = function ( ) {
				cutFiles.length = 0;
			};
			
			$scope.cutFiles = function ( files ) {
				var i,
					l,
					file;
					
				$scope.clearCutSelection();
				
				for(i = 0, l = files.length; i < l; ++i) {
					file = files[i];
					if(file.getEntityType() === 'file') {
						cutFiles.push(file);
					}
				}
			};
			
			$scope.pasteFiles = function ( ) {
				var target,
					files = $scope.selectedFiles;
				
				if(files.length && files[0].getEntityType() === 'folder') {
					target = files[0];
				} else if(files.length) {
					target = files[0].getParent();
				}
				
				$scope.moveEntity(cutFiles, target);
				$scope.clearCutSelection();
				
			};
			
			$scope.isCut = function ( file ) {
				return indexOf(cutFiles, file) !== -1;
			};
			
			$scope.hasCutFiles = function ( ) {
				return cutFiles && cutFiles.length;
			};
			
			$scope.isCuttable = function ( ) {
				var files = $scope.selectedFiles,
					i,
					l,
					file;
					
				for(i = 0, l = files.length; i < l; ++i) {
					file = files[i];
					if(file.getEntityType() === 'file') {
						return true;
					}
				}
				
				return false;
			};
			
			$scope.isPastable = function ( ) {
				return cutFiles.length;
			};
			
			$scope.hasFilesInSubdirectories = function ( ) {
				var selection = $scope.selectedFiles,
					file,
					root = $scope.root;
					
				for(var i = 0, l = selection.length; i < l; ++i) {
					file = selection[i];
					if(file.getParent() !== root) {
						return true;
					}
				}
				
				return false;
			};
			
			function validateEntity ( entity/*, target*/ ) {
				if(entity instanceof Folder) {
					return false;
				}
				
				return true;
			}
			
			function moveEntities ( targetFolder, data ) {
				var dropData = data,
					selectedFiles = $scope.selectedFiles;
					
				if(indexOf(selectedFiles, dropData) === -1) {
					$scope.moveEntity( [ dropData ], targetFolder);
				} else {
					$scope.moveEntity(selectedFiles, targetFolder);
				}
			}
			
			function setRootState ( ) {
				var files = $scope.root.getFiles(),
					folders = $scope.root.getFolders(),
					isSelected;
					
				if(files.length || folders.length) {
					isSelected = checkSelected($scope.root);
				} else {
					isSelected = $scope.root.getSelected();
				}
				
				$scope.root.setSelected(isSelected);
				
			}
			
			function checkSelected ( parent ) {
				var files = parent.getFiles(),
					folders = parent.getFolders(),
					i,
					l;
					
				for(i = 0, l = folders.length; i < l; ++i) {
					if(!folders[i].getSelected() || !checkSelected(folders[i])) {
						return false;
					}
				}
				
				for(i = 0, l = files.length; i < l; ++i) {
					if(!files[i].getSelected()) {
						return false;
					}
				}
				return true;
			}
			
			function getFlatFileList ( ) {
				var files = [];
					
				function getChildrenOf ( parent ) {
					var children = parent.getFolders(),
						i,
						l;
						
					files = files.concat(parent.getFiles());
					
					for(i = 0, l = children.length; i < l; ++i) {
						getChildrenOf(children[i]);
					}
				}
				
				getChildrenOf($scope.root);
				getChildrenOf($scope.list);
				
				return files;
			}
			
			$scope.$on('drop', function ( event, data, mimeType ) {
				var targetFolder = event.targetScope.entity;
				if(mimeType !== 'Files') {
					moveEntities(targetFolder, data);
				}
				$scope.dragging = false;
				$scope.$apply();
			});
			
			$scope.$on('reload', function ( ) {
				clearData();
			});
			
			$scope.$watch('filterQuery', function ( ) {
				words = $scope.filterQuery.split(' ');
			});
			
			$scope.$on('fileselect', function ( event, files ) {
				$scope.uploadFile(files);
			});
			
			$scope.$on('startdrag', function ( event, element, mimeType ) {
				
				var entity = event.targetScope.entity;
				if(entity && !$scope.isSelected(entity)) {
					$scope.deselectAll();
					$scope.selectEntity(entity);
				}
				
				$scope.dragging = mimeType;
				$scope.$apply();
			});
			
			$scope.$on('stopdrag', function ( /*event, element, mimeType*/ ) {
				$scope.dragging = '';
				$scope.$apply();
			});
			
			$scope.$on('contextmenuopen', function ( event ) {
				var entity = event.targetScope.entity;
				event.stopPropagation();
				if(!$scope.isSelected(entity)) {
					$scope.deselectAll();
					$scope.selectEntity(entity);
				}
			});
			
	}]);
})();

/*global angular*/
(function () {
		
	angular.module('Zaaksysteem.docs')
		.controller('nl.mintlab.docs.DocumentListViewController', [ '$scope', function ( $scope ) {
			
			$scope.sort = 'name';
			$scope.reversed = false;
			
			$scope.sortBy = function ( by ) {
				if($scope.sort === by) {
					$scope.reversed = !$scope.reversed;
				} else {
					$scope.reversed = false;
				}
				$scope.sort = by;
			};
			
			$scope.getSort = function ( ) {
				var sort = '';
				switch($scope.sort) {
					case 'type':
					sort = '[ metadata_id.document_category, extension ]';
					break;
					
					case 'edit':
					sort = 'date_modified';
					break;
					
					default:
					sort = $scope.sort;
					break;
				}
				return sort;
			};
			
			
			
		}]);
})();
/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem.docs')
		.controller('nl.mintlab.docs.DocumentUploadController', [ '$scope', 'fileUploader', function ( $scope, fileUploader ) {
			
			var indexOf = fetch('nl.mintlab.utils.shims.indexOf');
			
			$scope.uploadQueue = [];
			$scope.toGo = [];
			
			$scope.completed = [];
			$scope.failed = [];
			
			$scope.totalBytes = 0;
			$scope.loadedBytes = 0;
			$scope.progress = 0;
			
			$scope.clearQueue = function ( ) {
				
				angular.forEach($scope.uploadQueue.concat(), function ( upload ) {
					if(upload.completed || upload.error) {
						$scope.uploadQueue.splice(indexOf($scope.uploadQueue, upload), 1);
					}
				});
				
				updateTotalProgress();
				
				$scope.completed.length = 0;
				$scope.failed.length = 0;
			};
			
			$scope.abortUpload = function ( ) {
				angular.forEach($scope.uploadQueue.concat(), function ( upload ) {
					if(!upload.completed && !upload.error) {
						upload.abort();
					}
				});
				
				$scope.clearQueue();
			};
			
			$scope.getProgressBarWidth = function ( ) {
				return $scope.toGo.length ? Math.round(($scope.progress||0)*100) : 0;
			};
			
			function updateTotalProgress ( ) {
				var totalBytes = 0,
					loadedBytes = 0,
					allFilesInit = true;
					
				angular.forEach($scope.uploadQueue, function ( upload ) {
					if(!isNaN(upload.totalBytes)) {
						totalBytes += upload.totalBytes;
						if(!upload.error) {
							loadedBytes += upload.loadedBytes;
						} else {
							loadedBytes += upload.totalBytes;
						}
					} else {
						allFilesInit = false;
					}
				});
				$scope.loadedBytes = loadedBytes;
				$scope.totalBytes = totalBytes;
				$scope.progress = allFilesInit ? loadedBytes/totalBytes : 0;
			}
			
			function onFileAdd ( event, upload ) {
				
				$scope.clearQueue();
				
				
				
				upload.subscribe('init', onFileInit);
				upload.subscribe('progress', onFileProgress);
				upload.subscribe('complete', onFileComplete);
				upload.subscribe('end', onFileEnd);
				upload.subscribe('error', onFileError);
				
				$scope.uploadQueue.push(upload);
				$scope.toGo.push(upload);
				$scope.$apply();
			}
			
			function onFileInit ( /*event*/ ) {
				updateTotalProgress();
				$scope.$apply();
			}
			
			function onFileProgress ( /*event, upload, progress*/ ) {
				updateTotalProgress();
				$scope.$apply();
			}
			
			function onFileComplete ( event, upload ) {
				var data;
				
				updateTotalProgress();
				if(!upload.error) {
					$scope.completed.push(upload);
				} else {
					data = upload.getData();
					if(data.error_code && data.error_code === '/filestore/assert_allowed_filetype/extension_not_allowed') {
						// TODO(dario): use a pseudo-class
						$scope.$emit('systemMessage', {
							content: data.messages[0],
							type: 'error',
							code: data.error_code
						});
					}
				}
				$scope.$apply();
			}
			
			function onFileEnd ( event, upload ) {
				var index = indexOf($scope.toGo, upload);
				$scope.toGo.splice(index, 1);
			}
			
			function onFileError ( event, upload ) {
				updateTotalProgress();
				$scope.failed.push(upload);
			}
			
			fileUploader.subscribe('fileadd', onFileAdd);
			
			
		}]);
	
})();
/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.docs')
		.controller('nl.mintlab.docs.EmailAttachmentsController', [ '$scope', 'smartHttp', 'translationService', function ( $scope, smartHttp, translationService ) {
			
			var indexOf = _.indexOf,
				forEach = _.forEach;
			
			$scope.template = null;
			$scope.templates = [];
			
			function getDataFromActionItem ( ) {
				
				var data = $scope.action.data,
					attachments = [],
					rcpt = data.rcpt;
				
				$scope.emailSubject = data.subject;
				$scope.emailContent = data.body;
				
				if(data.case_document_attachments) {
					forEach(data.case_document_attachments, function ( caseDoc ) {
						attachments.push({ id: caseDoc.case_type_document_id, naam: caseDoc.naam } );
					});
				}
				
				if(rcpt === 'aanvrager') {
					$scope.typeRecipient = 'appealer';
				} else if(rcpt === 'coordinator') {
					$scope.typeRecipient = 'coordinator';
				} else if(data.email) {
					$scope.typeRecipient = 'other';
					$scope.recipient = data.email;
				} else {
					$scope.typeRecipient = 'coworker';
					$scope.recipient = rcpt;
				}
				
			}
			
			$scope.reloadData = function ( ) {
				smartHttp.connect({
					method: 'GET',
					url: 'zaak/' + $scope.caseId + '/get_sjablonen',
					params: {
						type: 'notifications'
					}
				})
					.success(function ( data ) {
						$scope.templates = data.result;
						if(!$scope.typeEmail) {
						 	$scope.typeEmail = $scope.templates && $scope.templates.length ? 'template' : 'custom';
						}
					})
					.error(function ( /*data*/ ) {
						
					});
			};
			
			$scope.sendEmail = function ( ) {
				
				var data,
					recipient,
					typeRecipient,
					subject,
					from,
					body,
					caseId,
					attachments = [],
					fileAttachments = [],
					caseTypeDocumentAttachments = [],
					templateAttachments;
					
				switch($scope.typeRecipient) {
					case 'coworker':
					recipient = $scope.recipient.id;
					typeRecipient = 'medewerker_uuid';
					break;
					
					case 'appealer':
					typeRecipient = 'aanvrager';
					break;
					
					case 'coordinator':
					typeRecipient = 'coordinator';
					break;
					
					case 'other':
					recipient = $scope.recipientAddress;
					typeRecipient = 'custom_address';
					break;
				}
				
				subject = $scope.emailSubject;
				body = $scope.emailContent;
				from = $scope.userId;
				caseId = $scope.caseId;
				
				attachments = [];
				
				forEach($scope.selectedAttachments, function ( value ) {
					attachments.push(value.id);
				});
				
				if($scope.typeEmail === 'template' && $scope.template) {
					templateAttachments = $scope.template.bibliotheek_notificaties_id.attachments;
					forEach(templateAttachments, function ( attachment ) {
						caseTypeDocumentAttachments.push(attachment.bibliotheek_kenmerk_id);
					});
					
					subject = $scope.template.bibliotheek_notificaties_id.subject;
					body = $scope.template.bibliotheek_notificaties_id.message;
				}
				
				data = {
					recipient: recipient,
					recipient_type: typeRecipient,
					subject: subject,
					from: from,
					body: body,
					case_id: caseId
				};
				
				if(attachments.length) {
					if($scope.context === 'docs') {
						fileAttachments = attachments;
					} else {
						caseTypeDocumentAttachments = caseTypeDocumentAttachments.concat(attachments);
					}
				}
				
				if(fileAttachments.length) {
					data.file_attachments = fileAttachments;
				}
				
				if(caseTypeDocumentAttachments.length) {
					data.case_type_document_attachments = caseTypeDocumentAttachments;
				}
				
				smartHttp.connect({
					method: 'POST',
					url: 'zaak/send_mail',
					data: data
				})
					.success(function onSuccess ( /*data*/ ) {
						$scope.$emit('systemMessage', {
							type: 'info',
							content: translationService.get('E-mail verstuurd')
						});
						$scope.closePopup();
					})
					.error(function onError ( /*data*/ ) {
						
					});
			};
			
			$scope.saveTemplate = function ( ) {
				
			};
			
			$scope.detach = function ( attachment ) {
				var index = indexOf($scope.selectedAttachments, attachment);
				if(index !== -1) {
					$scope.selectedAttachments.splice(index, 1);
				}
			};
			
			$scope.isAttached = function ( attachment ) {
				return indexOf($scope.selectedAttachments, attachment) !== -1;
			};
			
			$scope.getName = function ( attachment ) {
				var context = $scope.context,
					name = '';
				
				if(context === 'actions') {
					name = attachment.naam;
				} else if(context === 'docs') {
					name = attachment.name + attachment.extension;
				}
				return name;
			};
			
			$scope.init = function ( ) {
				
				if(!$scope.context) {
					throw new Error('Context not defined for EmailAttachmentsController');
				}
				
				if($scope.context === 'actions') {
					getDataFromActionItem();
				}
				
				if(!$scope.typeRecipient) {
					$scope.typeRecipient = 'coworker';
				}
				
				if(!$scope.attachments) {
					$scope.attachments = [];
				}
				
				$scope.selectedAttachments = $scope.attachments.concat();
				
				$scope.reloadData();
				
			};
			
			$scope.$watch('templates', function ( nw/*, old*/ ) {
				if(nw && nw.length) {
					$scope.template = nw[0];
				}
			});
			
			$scope.$watch('typeRecipient', function ( /*nw, old*/ ) {
				$scope.recipient = null;
			});
			
			$scope.$watch('newAttachment', function ( nw/*, old*/ ) {
				var attachment,
					i,
					l;
				
				if(nw) {
					for(i = 0, l = $scope.attachments.length; i < l; ++i) {
						attachment = $scope.attachments[i];
						if(attachment.id === nw.id) {
							return;
						}
					}
					
					$scope.attachments.push(nw);
					$scope.selectedAttachments.push(nw);
				}
				
				$scope.newAttachment = null;
			});
			
		}]);
	
})();
/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.docs')
		.controller('nl.mintlab.docs.EmailTemplateController', [ '$scope', function ( $scope ) {
			
		}]);
	
})();
/*global define, fetch*/
(function ( ) {
	
	define('nl.mintlab.docs.File', function ( ) {
		
		var inherit = fetch('nl.mintlab.utils.object.inherit'),
			StoredEntity = fetch('nl.mintlab.docs.StoredEntity');
		
		function File ( ) {
			this._entityType = 'file';
			File.uber.constructor.apply(this, arguments);
		}
		
		inherit(File, StoredEntity);
		
		File.prototype.setAsRevision = function ( asRevision ) {
			this._asRevision = asRevision;
		};
		
		File.prototype.getAsRevision = function ( ) {
			return this._asRevision;
		};
		
		return File;
		
	});
})();
/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.docs')
		.controller('nl.mintlab.docs.FileController', [ '$scope', '$window', 'smartHttp',  function ( $scope, $window, smartHttp ) {
			
			$scope.preview = false;
			$scope.collapsed = true;
			$scope.update = null;
			$scope.editMode = false;
			
			$scope.validTrustLevels = [
				'Openbaar',
				'Beperkt openbaar',
				'Intern',
				'Zaakvertrouwelijk',
				'Vertrouwelijk',
				'Confidentieel',
				'Geheim',
				'Zeer geheim'
			];
			
			$scope.validOrigins = [
				'Inkomend',
				'Uitgaand',
				'Intern'
			];
			
			
			$scope.toggleCollapse = function ( event ) {
				$scope.collapsed = !$scope.collapsed;
				if(event) {
					event.stopPropagation();
				}
			};
			
			$scope.onNameClick = function ( event ) {
				var isIntake = $scope.isIntake,
					url = isIntake ? ('zaak/intake/' + $scope.entity.id + '/download') : ('zaak/' + $scope.caseId + '/document/' + $scope.entity.id + '/download');
					
                if($scope.pip) {
                    url = 'pip/' + url;
                }

				$window.open(smartHttp.getUrl(url));
				event.stopPropagation();
			};
			
			$scope.onRemoveCaseDocClick = function ( event ) {
				$scope.setCaseDoc([ $scope.entity ], null);
				event.stopPropagation();
			};
			
			$scope.saveAttr = function ( key ) {
				var data = {
						file_id: $scope.entity.id
					};
								
				if(key.indexOf('metadata_id') === 0) {
					data.metadata = $scope.update.metadata_id;
				} else {
					data[key] = $scope.update[key];
				}
				
				$scope.entity.updating = true;
				
				smartHttp.connect({
					url: 'file/update',
					method: 'POST',
					data: data
				})
					.success(function ( data ) {
						var fileData = data.result ? data.result[0] : null;
						$scope.entity.updateWith(fileData);
						if(key === 'is_revision') {
							$scope.entity.is_revision = $scope.update.is_revision;
						}
					})
					.error(function ( /*data*/ ) {
						$scope.update = $scope.entity.clone();
					})
					.then(function ( ) {
						$scope.entity.updating = false;
					});
				
			};
			
			$scope.setEditMode = function ( editMode ) {
				$scope.editMode = editMode;
			};
			
			$scope.onPropertyFormButtonClick = function ( event ) {
				event.stopPropagation();
			};
			
			// TODO(dario): refactor common methods in StoredEntityController
			$scope.$on('editsave', function ( event/*, key, value*/ ) {
				event.stopPropagation();
			});
			
			$scope.$on('editcancel', function ( event/*, key, value*/ ) {
				event.stopPropagation();
			});
			
			$scope.$on('drop', function ( event, data/*, mimetype*/ ) {
				var caseDoc = data.caseDoc,
					clearPrevious = data.clearPrevious;
				
				$scope.setCaseDoc( [ $scope.entity ] , caseDoc, clearPrevious);
				event.stopPropagation();
			});
			
			$scope.$watch('entity', function ( ) {
				if($scope.entity && $scope.entity.is_revision === undefined) {
					$scope.entity.is_revision = $scope.entity.is_duplicate_of ? 1 : 0;
				}
				$scope.update = $scope.entity ? $scope.entity.clone() : null;
			});
			
		}]);
	
})();

/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.docs')
		.controller('nl.mintlab.docs.FileIntegrityController', [ '$scope', 'smartHttp', function ( $scope, smartHttp ) {
			
			$scope.status = 'unverified';
			$scope.loading = false;
			
			$scope.verifyIntegrity = function ( ) {
				$scope.loading = true;
				smartHttp.connect({
					url: 'file/verify/file_id/' + $scope.entity.id,
					method: 'GET'
				})
					.success(function ( data ) {
						$scope.loading = false;
						$scope.status = data.result ? 'verified' : 'error';
					})
					.error(function ( /*data*/ ) {
						$scope.status = 'error';
						$scope.loading = false;
					});
			};
			
		}]);
	
})();
/*global angular,fetch,define,XMLHttpRequest,FormData*/
(function ( ) {
	
	define('nl.mintlab.docs.FileUpload', function ( ) {
		
		var inherit = fetch('nl.mintlab.utils.object.inherit'),
			bind = angular.bind,
			EventDispatcher = fetch('nl.mintlab.events.EventDispatcher'),
			addEventListener = fetch('nl.mintlab.utils.events.addEventListener');
		
		function FileUpload ( file ) {
			this.completed = false;
			this.error = null;
			this.ended = false;
			this.progress = 0;
			this.loadedBytes = NaN;
			this.totalBytes = NaN;
			this.file = file;
		}
		
		inherit(FileUpload, EventDispatcher);
		
		function onProgress ( event ) {
			// chrome fires a progress event with loaded&total=0
			if(event.total) {
				this.loadedBytes = event.loaded;
				this.totalBytes = event.total;
			}
			this.progress = this.loadedBytes/this.totalBytes;
			this.publish('progress', this, this.progress);
		}
		
		function onLoad ( /*event*/ ) {
			this.progress = 1;
			this.publish('progress', this, this.progress);
			this.completed = true;
			this.publish('complete', this, this.xhr.responseText);
			bind(this, end)();
		}
		
		function onError ( event ) {
			this.error = true;
			this.publish('error', this, event);
			bind(this, end)();
		}
		
		function onAbort ( event ) {
			this.error = true;
			this.publish('error', this, event);
			bind(this, end)();
		}
		
		function end ( ) {
			if(!this.ended) {
				this.ended = true;
				this.publish('end', this);
			}
		}
		
		function onReadyStateChange ( event ) {
			var readyState = event.target.readyState,
				status;
				
			if(readyState !== 2) {
				return;
			}
			
			status = event.target.status;
			if(status >= 400) {
				this.error = true;
				this.publish('error', this, event);
				end.call(this);
			}
		}
		
		FileUpload.prototype.send = function ( url, params ) {
			
			var xhr = new XMLHttpRequest(),
				fd = new FormData(),
				key,
				token = window.getXSRFToken();
				
			this.xhr = xhr;
				
			addEventListener(xhr.upload, 'progress', bind(this, onProgress));
			// ff doesn't fire a progress event for 1
			addEventListener(xhr.upload, 'load', bind(this, onProgress));
			addEventListener(xhr.upload, 'error', bind(this, onError));
			addEventListener(xhr, 'load', bind(this, onLoad));
			addEventListener(xhr, 'error', bind(this, onError));
			addEventListener(xhr, 'abort', bind(this, onAbort));
			addEventListener(xhr, 'readystatechange', bind(this, onReadyStateChange));
				
			xhr.open('POST', url);
			
			if(token) {
				xhr.setRequestHeader('XSRF-TOKEN', token);
			}
			
			xhr.withCredentials = true;
			fd.append('file', this.file);
			for(key in params) {
				fd.append(key, params[key]);
			}
			xhr.send(fd);
			this.publish('start', this);
		};
		
		FileUpload.prototype.abort = function ( ) {
			this.xhr.abort();
		};
		
		FileUpload.prototype.getData = function ( ) {
			var data = this.xhr.responseText;
			try {
				data = JSON.parse(data);
			} catch( error ) {
				data = {};
			}
			return data;
		};
		
		return FileUpload;
		
	});
		
})();
/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem.net')
		.service('fileUploader', [ '$q', '$window', function ( $q, $window ) {
			
			var EventDispatcher = fetch('nl.mintlab.events.EventDispatcher'),
				inherit = fetch('nl.mintlab.utils.object.inherit'),
				FileUpload = fetch('nl.mintlab.docs.FileUpload'),
				IFrameUpload = fetch('nl.mintlab.docs.IFrameUpload'),
				indexOf = fetch('nl.mintlab.utils.shims.indexOf'),
				supports = !!$window.File;
				
			function FileUploader ( ) {
				this._uploadList = [];
			}
			
			inherit(FileUploader, EventDispatcher);
			
			FileUploader.prototype.upload = function ( file, url, params ) {
				var upload = supports ? new FileUpload(file) : new IFrameUpload(file),
					deferred = $q.defer(),
					promise = deferred.promise,
					uploadList = this._uploadList;
					
				if(!supports) {
					params.return_content_type = 'text/plain';
				}
				
				uploadList.push(upload);
				
				upload.subscribe('complete', function ( ) {
					deferred.resolve(upload);
				});
				upload.subscribe('error', function ( ) {
					deferred.reject(upload);
				});
				upload.subscribe('end', function ( ) {
					uploadList.splice(indexOf(uploadList, upload), 1);
				});
				
				this.publish('fileadd', upload);
				upload.send(url, params);
				
				return promise;
			};
			
			FileUploader.prototype.getUploadList = function ( ) {
				return this._uploadList;
			};
			
			FileUploader.prototype.supports = function ( ) {
				return supports;	
			};
			
			
			return new FileUploader();
			
			
		}]);
	
})();
/*global define,fetch*/
(function ( ) {
	
	define('nl.mintlab.docs.Folder', function ( ) {
		
		var inherit = fetch('nl.mintlab.utils.object.inherit'),
			StoredEntity = fetch('nl.mintlab.docs.StoredEntity'),
			File = fetch('nl.mintlab.docs.File'),
			indexOf = fetch('nl.mintlab.utils.shims.indexOf');
		
		function Folder ( ) {
			this._folders = [];
			this._files = [];
			this._childrenByUid = {};
			this._entityType = 'folder';
			Folder.uber.constructor.apply(this, arguments);
		}
		
		inherit(Folder, StoredEntity);
		
		Folder.prototype.add = function ( child ) {
			var collection,
				parent = child.getParent();
			if(parent === this) {
				return;
			}
			if(child instanceof Folder) {
				collection = this._folders;
			} else if(child instanceof File) {
				collection = this._files;
			}
			if(parent) {
				parent.remove(child);
			}
			collection.push(child);
			child.setDepth(this.getDepth() + 1);
			child.setParent(this);
			this._childrenByUid[child.getUid()] = child;
			this.publish('add', child);
		};
		
		Folder.prototype.remove = function ( child ) {
			var collection,
				parent = child.getParent();
			if(parent !== this) {
				throw new Error('Child ' + child.name + ' is not in this folder');
			}
			if(child instanceof Folder) {
				collection = this._folders;
			} else {
				collection = this._files;
			}
			
			var index = indexOf(collection, child);
			collection.splice(index, 1);
			child.setParent(null);
			child.setDepth(-1);
			delete this._childrenByUid[child.getUid()];
			this.publish('remove', child);
		};
		
		Folder.prototype.getFolders = function ( ) {
			return this._folders;
		};
		
		Folder.prototype.getFiles = function ( ) {
			return this._files;
		};
		
		Folder.prototype.getChildByUid = function ( uid ) {
			return this._childrenByUid[uid];
		};
		
		Folder.prototype.getEntityByPath = function ( path ) {
			var child = this.getChildByUid(path[0]);
			if(!child) {
				return null;
			}
			var subPath = path.concat();
			subPath.shift();
			if(subPath.length === 0) {
				return child;
			} else if(child instanceof Folder) {
				return child.getEntityByPath(subPath);
			}
			return null;
		};
		
		Folder.prototype.empty = function ( ) {
			
			while(this._folders.length) {
				this.remove(this._folders[0]);
			}
			
			while(this._files.length) {
				this.remove(this._files[0]);
			}
			
		};
		
		return Folder;
	});
	
})();
/*global angular*/
(function ( ) {
	
	
	
	angular.module('Zaaksysteem.docs')
		.controller('nl.mintlab.docs.FolderController', [ '$scope', 'smartHttp', function ( $scope, smartHttp ) {
			
			$scope.collapsed = false;
			$scope.folderName = '';
			$scope.fileName = '';
			$scope.filtered = [];
			$scope.preview = false;
			$scope.editName = false;
			
			function createFolder ( name ) {
				var entity = $scope.entity;
					
				entity.name = name;
				
				entity.updating = true;
				
				smartHttp.connect({
					url: 'directory/create/',
					method: 'POST',
					data: {
						case_id: $scope.caseId,
						name: name
					}
				})
					.success(function ( data ) {
						var folderData = data.result[0];
						$scope.entity.updateWith(folderData);
						$scope.editName = false;
						entity.updating = false;
					})
					.error(function ( ) {
						$scope.editName = true;
						entity.updating = false;
					});
			}
			
			function updateFolder ( name ) {
				var entity = $scope.entity,
					tmp = entity.name;
				
				entity.name = name;
				
				entity.updating = true;
				
				smartHttp.connect({
					url: 'directory/update/',
					method: 'POST',
					data: {
						directory_id: entity.id,
						name: name
					}
				})
					.success(function ( ) {
					
					})
					.error(function ( ) {
						entity.name = tmp;
					})
					.then(function ( ) {
						entity.updating = false;
					});
			}
			
			$scope.toggleCollapse = function ( event ) {
				$scope.collapsed = !$scope.collapsed;
				if(event) {
					event.stopPropagation();
				}
			};
			
			$scope.onNameClick = function ( event ) {
				$scope.toggleCollapse();
				event.stopPropagation();
			};
			
			$scope.saveName = function ( name ) {
				updateFolder(name);
			};
			
			$scope.editSave = function ( name ) {
				if($scope.entity.id === -1) {
					createFolder(name);
				} else {
					updateFolder(name);
				}
			};
			
			$scope.$on('editsave', function ( event/*, key, value*/ ) {
				event.stopPropagation();
				$scope.editName = false;
			});
			
			$scope.$on('editcancel', function ( event/*, key, value*/ ) {
				if($scope.entity.id === -1) {
					$scope.removeEntity([ $scope.entity] );
				}
				event.stopPropagation();
				$scope.editName = false;
			});
			
			$scope.$on('drop', function ( event/*, data, mimetype*/ ) {
				if(event.currentScope === event.targetScope) {
					$scope.collapsed = false;
				}
			});
			
			$scope.$watch('entity', function ( ) {
				if($scope.entity) {
					$scope.update = $scope.entity.clone();
					$scope.editName = $scope.entity.id === -1;
					if($scope.editName) {
						$scope.folderName = $scope.entity.name;
					}
				}
			});
			
			$scope.$watch('filterQuery', function ( ) {
				if($scope.filterQuery) {
					$scope.collapsed = false;
				}
			});
			
			if($scope.entity && $scope.entity.id === -1) {
				$scope.collapsed = true;
			}
			
		}]);
	
})();
/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem.docs')
		.controller('nl.mintlab.docs.GlobalIntakeActionController', [ '$scope', 'smartHttp', function ( $scope, smartHttp ) {
			
			var forEach = angular.forEach,
				indexOf = fetch('nl.mintlab.utils.shims.indexOf');
			
			$scope.caseId = null;
			
			function processCaseDocData ( data ) {
				$scope.caseDocs = data.result || [];
			}
		
			$scope.assignFiles = function ( ) {
				
				var	caseId = $scope.caseId.id,
					files = $scope.selectedFiles.concat(),
					queue = [];
				
				$scope.loading = true;
				
				$scope.deselectAll();
				
				function onComplete ( file ) {
					var index = indexOf(files, file);
					files.splice(index, 1);
					if(!files.length) {
						$scope.loading = false;
						$scope.closePopup();
					}
				}
					
				forEach(files, function ( file ) {
					
					$scope.root.remove(file);
					
					file.case_id = caseId;
					
					queue.push(file);
					
					smartHttp.connect({
						url: 'file/update',
						method: 'GET',
						params: {
							file_id: file.id,
							case_id: caseId,
							case_type_document_id: file.case_type_document_id ? file.case_type_document_id.id : null,
							case_type_document_clear_old: false
						}
					})
						.success(function ( ) {
							onComplete(file);
							$scope.$broadcast('systemMessage', {
								type: 'info',
								code: 'assign-file-success',
								data: {
									files: files
								}
							});
						})
						.error(function ( ) {
							onComplete(file);
							$scope.root.add(file);
							file.case_id = null;
							file.case_type_document_id = null;
						});
				});
				
			};
			
			$scope.getFileNames = function ( ) {
				var fileNames = [];
				forEach($scope.selectedFiles, function ( file ) {
					fileNames.push(file.name + file.extension);
				});
				
				return fileNames.join(', ');
			};
			
			$scope.$watch('caseId', function ( ) {
				$scope.caseDocs = [];
				
				$scope.loading = true;
				
				if($scope.caseId) {
					smartHttp.connect({
						method: 'GET',
						url: 'zaak/' + $scope.caseId.id + '/case_type_documents'
					})
						.success(function ( data ) {
							$scope.loading = false;
							processCaseDocData(data);
						})
						.error(function ( ) {
							$scope.loading = false;
						});
				}
			});
		
	}]);
	
})();
/*global define,document,fetch*/
(function ( ) {
	
	var doc = document;
	
	define('nl.mintlab.docs.IFrameUpload', function ( ) {
		
		var inherit = fetch('nl.mintlab.utils.object.inherit'),
			addEventListener = fetch('nl.mintlab.utils.events.addEventListener'),
			removeEventListener = fetch('nl.mintlab.utils.events.removeEventListener'),
			EventDispatcher = fetch('nl.mintlab.events.EventDispatcher'),
			generateUid = fetch('nl.mintlab.utils.generateUid');
			
		function getIFrame ( ) {
			var iframe = doc.createElement('iframe');
			iframe.id = iframe.name = generateUid();
			iframe.style.display = 'none';
			doc.body.appendChild(iframe);
			return iframe;
		}
		
		function IFrameUpload ( file ) {
			
			this.completed = false;
			this.error = null;
			this.ended = false;
			this.progress = 0;
			this.loadedBytes = NaN;
			this.totalBytes = NaN;
			this.file = file;
			
			var that = this;
			
			function end ( ) {
				removeEventListener(that.iframe, 'error', onError);
				removeEventListener(that.iframe, 'error', onLoad);
				that.ended = true;
				that.getData();
				that.publish('end');
				doc.body.removeChild(that.iframe);
				that.iframe = null;
			}
			
			function triggerLoad ( ) {
				that.progress = 1;
				that.publish('progress', that, that.progress);
				that.completed = true;
				that.publish('complete', that, that.getData());
				end();
			}
			
			function triggerError ( ) {
				that.progress = 1;
				that.publish('progress', that, that.progress);
				that.error = true;
				that.publish('error', that, that.getData());
				that.completed = true;
				that.publish('complete', that, that.getData());
				end();
			}
			
			function onLoad ( /*event*/ ) {
				if(!that.getData().result) {
					triggerError();
				} else {
					triggerLoad();
				}
			}
			
			function onError ( /*event*/ ) {
				triggerError();
			}
			
			this.send = function ( url, params ) {
				var iframe = getIFrame(),
					form = this.file.form,
					input,
					hiddenFields = [];
				
				this.iframe = iframe;
					
				for(var id in params) {
					input = doc.createElement('input');
					input.type = 'hidden';
					input.name = id;
					input.value = params[id];
					hiddenFields.push(input);
					form.appendChild(input);
				}
				
				form.target = iframe.name;
				form.submit();
				
				for(var i = 0, l = hiddenFields.length; i < l; ++i) {
					form.removeChild(hiddenFields[i]);	
				}
				
				addEventListener(iframe, 'error', onError);
				addEventListener(iframe, 'load', onLoad);
				
			};
			
			this.abort = function ( ) {
				this.error = true;
				this.publish('error', this, null);
				end();
			};
			
			this.getData = function ( ) {
				if(this.data === undefined) {
					var body = (this.iframe.contentDocument || this.iframe.contentWindow.document).body,
						content = 'textContent' in body ? body.textContent : body.innerText;
					
					try {
						this.data = JSON.parse(content);
					} catch ( error ) {
						this.data = {};
					}
				}
				return this.data;
			};
		}
		
		inherit(IFrameUpload, EventDispatcher);
		
		return IFrameUpload;
		
	});
	
})();
	/*global fetch, define*/
(function ( ) {
	
	define('nl.mintlab.docs.StoredEntity', function ( ) {
		
		var inherit = fetch('nl.mintlab.utils.object.inherit'),
			clone = fetch('nl.mintlab.utils.object.clone'),
			EventDispatcher = fetch('nl.mintlab.events.EventDispatcher'),
			generateUid = fetch('nl.mintlab.utils.generateUid');
		
		function StoredEntity ( params ) {
			
			var key;
			
			this.name = 'Unknown';
			this.id = -1; // unsaved
			this._parent = null;
			this._depth = 0; // top level
			this._selected = false;
			
			this._uid = generateUid(this.getEntityType() + '_');
			
			for(key in params) {
				this[key] = params[key];
			}
		}
		
		inherit(StoredEntity, EventDispatcher);
		
		StoredEntity.prototype.getPath = function ( ) {
			var path = [ this.getUid() ],
				parent = this.getParent();
			
			while(parent) {
				path.unshift(parent.getUid());
				parent = parent.getParent();
			}
			path.shift();
			return path;
		};
		
		StoredEntity.prototype.getParent = function ( ) {
			return this._parent;
		};
		
		StoredEntity.prototype.setParent = function ( parent ) {
			this._parent = parent;
		};
		
		StoredEntity.prototype.getDepth = function ( ) {
			return this._depth;
		};
		
		StoredEntity.prototype.setDepth = function ( depth ) {
			this._depth = depth;
		};
		
		StoredEntity.prototype.getEntityType = function ( ) {
			return this._entityType;
		};
		
		StoredEntity.prototype.getSelected = function ( ) {
			return this._selected;
		};
		
		StoredEntity.prototype.setSelected = function ( selected ) {
			this._selected = selected;
		};
		
		StoredEntity.prototype.getUid = function ( ) {
			var uid;
			if(this.uuid !== undefined) {
				uid = this.getEntityType() + '_' + this.uuid;
			} else {
				uid = this._uid;
			}
			return uid;
		};
		
		StoredEntity.prototype.getMimeType = function ( ) {
			var mimetype = "Unknown";
			if(this.filestore_id && this.filestore_id.mimetype) {
				mimetype = this.filestore_id.mimetype.replace('application/', '').replace('image/', '');
			}
			return mimetype;
		};
		
		StoredEntity.prototype.updateWith = function ( data ) {
			for(var key in data) {
				this[key] = data[key];
			}
		};
		
		StoredEntity.prototype.clone = function ( ) {
			return clone(this, true);
		};
		
		
		return StoredEntity;
	});
})();
/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem.docs')
		.controller('nl.mintlab.docs.TemplateController', [ '$scope', 'smartHttp', 'translationService', '$timeout', function ( $scope, smartHttp, translationService, $timeout ) {
			
			var File = fetch('nl.mintlab.docs.File'),
				defaultTemplateId;
			$scope.template = null;
			
			$scope.createFileFromTemplate = function ( name, template, caseDocId, targetFormat ) {
				
				var file = new File(),
					params,
					templateId,
					mimetype;
					
				if(typeof template === 'string' || typeof template === 'number') {
					templateId = template;
				} else {
					templateId = template.bibliotheek_sjablonen_id.id;
				}
					
				file.name = name;
				file.extension = '.' + targetFormat;
				file.extension_dotless = targetFormat;
				
				if(targetFormat === 'odt') {
					mimetype = 'application/vnd.oasis.opendocument.text';
				} else {
					mimetype = 'application/pdf';
				}
				
				file.filestore_id = {
					mimetype: mimetype
				};
				
				file.updating = true;
				
				params = {
					case_id: $scope.caseId,
					name: name,
					zaaktype_sjabloon_id: template.id,
					target_format: targetFormat
				};
				
				if(caseDocId) {
					params.case_type_document_id = caseDocId;
				}
				
				function onComplete ( ) {
					$timeout(function ( ) {
						file.updating = false;
					});
				}
				
				return smartHttp.connect( {
					url: 'file/file_create_sjabloon',
					method: 'POST',
					data: params
				})
					.success(function ( data ) {
						
						var fileData = data.result[0];
				
						if(fileData.accepted) {
							$scope.list.add(file);
						} else {
							$scope.intake.add(file);
						}
						
						file.updateWith(fileData);
						onComplete();
					})
					.error(function ( /*data*/ ) {
						$scope.$emit('systemMessage', {
							type: 'error',
							content: translationService.get('Sjabloon "%s" kon niet worden toegevoegd', name + '.' + targetFormat)
						});
						onComplete();
					});
			};
			
			$scope.setTemplateId = function ( tplId ) {
				var i,
					l,
					tpl,
					templates = $scope.templates;
				
				for(i = 0, l = templates.length; i < l; ++i) {
					tpl = templates[i];
					if(tpl.id === tplId) {
						$scope.template = tpl;
						return;
					}
				}
				
				defaultTemplateId = tplId;
				
			};
			
			$scope.$watch('templates', function ( nw/*, old*/ ) {
				if(nw && nw.length) {
					if(!defaultTemplateId) {
						$scope.template = $scope.templates[0];
					} else {
						$scope.setTemplateId(defaultTemplateId);
					}
				}
			});
			
			$scope.$watch('template', function ( nw, old ) {
				var template = $scope.template,
					name = $scope.name;
					
				if(!name || old && old.bibliotheek_sjablonen_id.naam === name) {
					if(!template) {
						name = '';
					} else {
						name = template.bibliotheek_sjablonen_id.naam;
					}
					$scope.name = name;	
				}
				
				$scope.targetFormat = $scope.template && $scope.template.target_format ? $scope.template.target_format : 'odt';
				
			});
			
			
		}]);
	
})();
/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.docs')
		.controller('nl.mintlab.docs.VersionController', [ '$scope', 'smartHttp', function ( $scope, smartHttp ) {
			
			$scope.revisions = [];
			
			$scope.revertTo = function ( revision ) {
				
				$scope.entity.updating = true;
				
				smartHttp.connect( {
					url: 'file/revert_to',
					method: 'POST',
					data: {
						file_id: revision.file_id
					}
				})
					.success(function ( data ) {
						var fileData = data.result ? data.result[0] : null;
						
						$scope.entity.updateWith(fileData);
						
						reloadData();
					})
					.error(function ( ) {
						
					})
					.then(function ( ) {
						$scope.entity.updating = false;
					})
			};
			
			function reloadData ( ) {
			
				smartHttp.connect( {
					url: 'file/version_info/file_id/' + $scope.entity.id
				})
					.success(function ( data ) {
						var revs = data.result || [];
						$scope.revisions = revs;
					})
					.error(function ( error ) {
						
					});
					
			}
			
			reloadData();
			
		}]);
	
})();
/*global angular,fetch*/
(function ( ) {
	
	var EMPTY_GIF = 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==';
	
	angular.module('Zaaksysteem.docs')
		.directive('zsFilePreview', [ '$document', '$window', 'templateCompiler', '$timeout', function ( $document, $window, templateCompiler, $timeout ) {
			
			var fromLocalToGlobal = fetch('nl.mintlab.utils.dom.fromLocalToGlobal'),
				fromGlobalToLocal = fetch('nl.mintlab.utils.dom.fromGlobalToLocal'),
				getViewportSize = fetch('nl.mintlab.utils.dom.getViewportSize'),
				setMouseEnabled = fetch('nl.mintlab.utils.dom.setMouseEnabled'),
				getComputedStyle = $window.getComputedStyle,
				body = $document.find('body'),
				popup,
				img,
				arrow,
				center;
			
			function createPopup ( element ) {
				popup = angular.element(element);
				
				setMouseEnabled(popup[0]);
				
				body.append(popup);
				img = angular.element(popup[0].querySelector('img.file-preview-image'));
				img.bind('load', onImageLoad);
				arrow = angular.element(popup[0].querySelector('.file-preview-arrow'));
				popup.bind('webkitTransitionEnd transitionend', onTransitionEnd);
			}
			
			function showPopup ( cnt ) {
				center = cnt;
				popup.addClass('file-preview-visible');
				positionPopup();
			}
			
			function hidePopup ( ) {
				popup.removeClass('file-preview-visible');
			}
			
			function onTransitionEnd ( ) {
				if(getComputedStyle(popup[0]).opacity === '0') {
					img.attr('src', EMPTY_GIF);
				}
			}
			
			function setImage ( url ) {
				img.attr('src', url);
			}
			
			function onImageLoad ( ) {
				$timeout(function ( ) {
					positionPopup();
				});
			}
			
			function positionPopup ( ) {
				var target = fromGlobalToLocal(popup[0].offsetParent, center),
					clientWidth = popup[0].clientWidth,
					clientHeight = popup[0].clientHeight,
					hOrient,
					viewportSize = getViewportSize(),
					arrowY = clientHeight/2 - arrow[0].clientHeight/2;
					
				if(target.x - clientWidth > 0) {
					hOrient = 'left';
					target.x -= clientWidth;
				} else {
					hOrient = 'right';
				}
					
				target.y -= clientHeight / 2;
				
				if(target.y < 0) {
					arrowY = -target.y;
					target.y = 0;
				} else if(target.y + clientHeight > viewportSize.height) {
					arrowY += (target.y - (viewportSize.height - clientHeight));
					target.y = viewportSize.height - clientHeight;
				}
				
				popup.css('left', target.x + 'px');
				popup.css('top', target.y + 'px');
				
				popup.attr('data-zs-file-preview-horizontal-orientation', hOrient);
				
				arrow.css('top', arrowY + 'px');
				
			}
			
			templateCompiler.getElement('/partials/directives/popup/file-preview.html').then(createPopup);
			
			return {
				compile: function ( /*tElement, tAttrs, transclude*/ ) {
					
					return function link ( scope, element/*, attrs*/ ) {
						
						scope.showPreview = function ( ) {
							var center,
								origin,
								url = scope.pip ? scope.entity.pip_thumbnail_url : scope.entity.thumbnail_url;
							
							if(!url) {
								return;
							}
								
							setImage(url);
								
							center = { x: element[0].clientWidth/2, y: element[0].clientHeight / 2 };
							origin = fromLocalToGlobal(element[0], center);
							showPopup(origin);
						};
						
						scope.hidePreview = function ( ) {
							hidePopup();
						};
						
					};
				}
			};
			
		}]);
	
})();
/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem.dom')
		.service('dropManager', [ function ( ) {
			
			var indexOf = fetch('nl.mintlab.utils.shims.indexOf'),
				intersects = fetch('nl.mintlab.utils.dom.intersects'),
				generateUid = fetch('nl.mintlab.utils.generateUid');
			
			function DropManager ( ) {
				
				var droppables = [],
					drops = {},
					sorted = false,
					currentDrop;
					
				function invalidateSorting ( ) {
					sorted = false;
				}
				
				function getNestingDepth ( element ) {
					var depths = [],
						p = element[0],
						index;
						
					while(p) {
						index = p.parentElement ? indexOf(p.parentElement.childNodes, p) : 0;
						depths.push(index);
						p = p.parentElement;
					}
					
					return depths;
				}
				
				function sort ( ) {
					droppables.sort(function ( a, b ) {
						var depthsA = a.data('zs-drop-manager-nesting-depth'),
							depthsB = b.data('zs-drop-manager-nesting-depth'),
							i = 0,
							l = Math.min(depthsA.length, depthsB.length);
							
						for(; i < l; ++i) {
							if(a[i] !== b[i]) {
								return a[i] > b[i] ? -1 : 1;
							}
						}
						return 0;
					});
					
					sorted = true;
				}
				
				function getSortedList ( ) {
					if(!sorted) {
						sort();
					}
					return droppables;
				}
				
				this.register = function ( element ) {
					
					element.data('zs-drop-manager-nesting-depth', getNestingDepth(element));
					
					if(indexOf(droppables, element) === -1) {
						droppables.push(element);
					}
					invalidateSorting();
				};
				
				this.unregister = function ( element ) {
					var index = indexOf(droppables, element);
					if(index !== -1) {
						droppables.splice(index, 1);
					}
					element.removeData('zs-drop-manager-nesting-depth', getNestingDepth(element));
					invalidateSorting();
				};
				
				this.createDrop = function ( mimetype, data ) {
					var dropId = generateUid(),
						drop = {
							mimetype: mimetype,
							data: data
						};
						
					drops[dropId] = drop;
					currentDrop = drop;
					return dropId;
				};
				
				this.getDrop = function ( dropId ) {
					return drops[dropId];
				};
				
				this.destroyDrop = function ( dropId ) {
					var drop = drops[dropId];
					if(drop === currentDrop) {
						currentDrop = null;
					}
					delete drops[dropId];	
				};
				
				this.getCurrentDrop = function ( ) {
					return currentDrop;	
				};
				
				this.getDroppable = function ( pos, draggable ) {
					var list = getSortedList(),
						i = 0,
						l = list.length,
						el,
						mimetype = draggable.attr('data-ng-drag-mimetype');
					
					for(; i < l; ++i) {
						el = list[i];
						if(el !== draggable && el.attr('data-ng-drop-mimetype') === mimetype && intersects(el[0], pos)) {
							//if(el.scope().validateDrop(dropData)) {
								return el;
							//}
						}
					}
					
					return null;
					
				};
				
			}
			
			
			return new DropManager();
			
		}]);
})();
/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem.dom')
		.service('templateCompiler', [ '$q', '$templateCache', '$compile', '$http', function ( $q, $templateCache, $compile, $http ) {
			
			var trim = fetch('nl.mintlab.utils.shims.trim'),
				templatePromises = {},
				templateData = {},
				compilerPromises = {},
				compilers = {};
			
			function TemplateCompiler ( ) {
				
			}
			
			function compileElement ( template ) {
				var deferred = $q.defer(),
					promise = deferred.promise;
					
				if(templatePromises[template]) {
					templatePromises[template].then(function ( ) {
						deferred.resolve(templateData[template]);
					});
					return promise;
				}
				
				templatePromises[template] = $q.when($templateCache.get(template) || $http.get(template, { cache: true })).then(function ( tpl ) {
									
					if(angular.isObject(tpl)) {
						tpl = tpl.data;
					}
					
					var element = angular.element(trim(tpl));
					
					
					templateData[template] = element;
					
					deferred.resolve(element);
					
				});
				
				return promise;
			}
			
			function getCompiler ( template ) {
				
				var deferred;
				
				if(!compilerPromises[template]) {
					deferred = $q.defer();
					compilerPromises[template] = deferred.promise;
					compileElement(template).then(function ( element ) {
						var compiler = $compile(element);
						compilers[template] = compiler;
						deferred.resolve(compiler);
					});
				}
				
				return compilerPromises[template];
			}
			
			TemplateCompiler.prototype.getCompiler = function ( template ) {
				return getCompiler(template);
			};
			
			TemplateCompiler.prototype.getElement = function ( template ) {
				return compileElement(template);
			};
			
			return new TemplateCompiler();
			
		}]);
	
})();


/*global define, fetch*/
(function ( ) {
	
	define('nl.mintlab.events.EventDispatcher', function ( ) {
		
		var indexOf = fetch('nl.mintlab.utils.shims.indexOf');
		
		function toArray ( array ) {
			return Array.prototype.slice.call(array, 0);
		}

		
		function EventDispatcher ( ) {
			
		}
	
		EventDispatcher.prototype.getListenerCollection = function ( event ) {
			// lazy initialization to enable mixins
			if(!this._listeners) {
				this._listeners = [];
			}
			var listeners = this._listeners;
			if(!listeners[event]) {
				listeners[event] = [];
			}
			return listeners[event];
		};

		EventDispatcher.prototype.publish = function ( ) {
			var event = arguments[0],
				collection = this.getListenerCollection(event),
				i,
				l;
				
			for(i = 0, l = collection.length; i < l; ++i) {
				collection[i].apply(null, toArray(arguments));
			}
		};

		EventDispatcher.prototype.subscribe = function ( event, callback ) {
			this.getListenerCollection(event).push(callback);
		};

		EventDispatcher.prototype.unsubscribe = function ( event, callback ) {
			var collection = this.getListenerCollection(event);
			var index = indexOf(collection, callback);
			if(index !== -1) {
				collection.splice(index, 1);
			}
		};

		EventDispatcher.prototype.isSubscribed = function ( event, callback ) {
			return indexOf(this.getListenerCollection(event), callback) !== -1;
		};
		
		return EventDispatcher;
		
	});
	
})();
/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.kcc')
		.controller('nl.mintlab.kcc.ActiveSubjectController', [ '$scope', function ( $scope ) {
			
		}]);
})();
/*global define*/
(function ( ) {
	
	define('nl.mintlab.kcc.Call', function ( ) {
		
		function Call ( ) {
			
		}
		
		
		return Call;
	});
	
	
	
})();
/*global angular,fetch*/
(function () {
	
	angular.module('Zaaksysteem.kcc')
		.controller('nl.mintlab.kcc.CallController', [ '$scope', 'smartHttp', '$cookieStore', '$rootScope', function ( $scope, smartHttp, $cookieStore, $rootScope ) {
			
			var Call = fetch('nl.mintlab.kcc.Call');
			
			var _incomingCalls = [];
			
			$scope.activeSubject = null;
			$scope.initialized = false;
			$scope.tabData = null;
			
			function loadActiveSubject ( ) {
				var cookiedSubject = $cookieStore.get('activesubject');
				if(cookiedSubject) {
					setActiveSubject(cookiedSubject);
				}
				smartHttp.connect({
					method: 'GET',
					url: 'betrokkene/get_session'
				})
					.success(handleSuccess)
					.error(handleError);
			}
			
			function handleSuccess ( data ) {
				var subj;
				
				if(data && data.result && data.result.length) {
					subj = data.result[0].subject;
					setActiveSubject(subj);
					$scope.setTabData(subj);
				} else {
					setActiveSubject(null);
				}
			}
			
			function handleError ( ) {
				setActiveSubject(null);
			}
			
			function setActiveSubject ( subject ) {
				$scope.activeSubject = subject;
				setCookie();
			}
			
			function setCookie ( ) {
				if($scope.activeSubject) {
					$cookieStore.put('activesubject', $scope.activeSubject);
				} else {
					$cookieStore.remove('activesubject');
				}
			}
			
			$scope.enableSession = function ( contact ) {
				var betrokkeneId = contact.identifier;
				
				smartHttp.connect( {
					method: 'POST',
					url: 'betrokkene/enable_session/' + betrokkeneId
				})
					.success(function ( data ) {
						var subj = data && data.result && data.result.length ? data.result[0].subject : null;
						setActiveSubject(subj);
						$scope.setTabData($scope.activeSubject);
					});
			};
			
			$scope.disableSession = function ( ) {
				smartHttp.connect( {
					method: 'POST',
					url: 'betrokkene/disable_session'
				})
					.success(function ( /*data*/ ) {
						setActiveSubject(null);
						if(_incomingCalls.length) {
							$scope.setTabData(_incomingCalls[0]);
						} else {
							$scope.setTabData(null);
						}
					})
					.error(function ( ) {
						
					});
			};
			
			$scope.getSubjectByline = function ( ) {
				var byline = [],
					activeSubject = $scope.activeSubject;
					
				if(!activeSubject) {
					return '';
				}
				
				if(activeSubject.street) {
					byline.push(activeSubject.street);
				}
				if(activeSubject.postal_code) {
					byline.push(activeSubject.postal_code);
				}
				if(activeSubject.city) {
					byline.push(activeSubject.city);
				}
				if(activeSubject.telephone_numbers && activeSubject.telephone_numbers.length) {
					byline.push(activeSubject.telephone_numbers[0]);
				}
				if(activeSubject.email_addresses && activeSubject.email_addresses.length) {
					byline.push(activeSubject.email_addresses[0]);
				}
				return byline.join(', ');
			};
			
			$scope.getDisplayName = function ( obj ) {
				var displayName = '',
					contact;
				
				if(obj instanceof Call) {
					contact = obj.contact;
					if(contact && contact.natural_person) {
						displayName = contact.natural_person.given_names + ' ' + contact.natural_person.surname;
					} else if(contact && contact.non_natural_person) {
						displayName = contact.non_natural_person.name;
					} else {
						displayName = obj.phonenumber;
					}
				} else {
					contact = obj;
					if(contact) {
						displayName = contact.name;
					}
				}
				
				return displayName;
			};
			
			$scope.setTabData = function ( tabData ) {
				$scope.tabData = tabData;
			};
			
			$scope.acceptCall = function ( call ) {
				smartHttp.connect( {
					method: 'POST',
					url: 'kcc/call/accept',
					data: {
						event_id: call.id,
						accept: true
					}
				})
					.success(function ( /*data*/ ) {
						$scope.$broadcast('callaccept', call);
						if(call.contact) {
							$scope.enableSession(call.contact);
						}
					})
					.error(function ( /*data*/ ) {
						
					});
			};
			
			$scope.rejectCall = function ( call ) {
				smartHttp.connect( {
					method: 'POST',
					url: 'kcc/call/accept',
					data: {
						event_id: call.id,
						accept: false
					}
				})
					.success(function ( /*data*/ ) {
						$scope.$broadcast('callreject', call);
						if($scope.tabData === call) {
							$scope.setTabData($scope.activeSubject||_incomingCalls[0]);
						}
					})
					.error(function ( /*data*/ ) {
						
					});
			};

			$scope.$on('incomingcall', function ( event, calls) {
				
				_incomingCalls = calls;
				
				if(!$scope.tabData && calls[0]) {
					$scope.setTabData(calls[0]);
				}
			});
			
			// TODO(dario): find a cleaner way to set subject
			$rootScope.$on('legacy-activesubject', function ( /*event, result*/ ) {
				$scope.$apply(function ( ) {
					loadActiveSubject();
				});
			});
			
			loadActiveSubject();
			
		}]);
	
})();
/*global angular, fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem.kcc')
		.controller('nl.mintlab.kcc.IncomingCallController', [ '$scope', 'smartHttp', '$timeout', function ( $scope, smartHttp, $timeout ) {
			
			var Call = fetch('nl.mintlab.kcc.Call'),
				indexOf = fetch('nl.mintlab.utils.shims.indexOf');
				
			var _timeout = 5000,
				_relevance = 30000,
				_polling,
				_timeoutPromise,
				_callsById = {};
				
			$scope.incomingCalls = [];
				
			$scope.setEnabled = function ( enabled ) {
				if(enabled) {
					$scope.startPoll();
				}
			};
			
			$scope.startPoll = function ( ) {
				if(!_polling) {
					_polling = true;
					poll();
				}
			};
			
			$scope.stopPoll = function ( ) {
				if(_polling) {
					_polling = false;
					$timeout.cancel(_timeoutPromise);
					_timeoutPromise = null;
				}
			};
			
			function poll ( ) {
				_timeoutPromise = null;
				smartHttp.connect({
					method: 'GET',
					url: 'event/list/kcc/call',
					interval: _relevance / 1000
				})
					.success(function ( data ) {
						handleData(data);
						_timeoutPromise = $timeout(poll, _timeout);
					})
					.error(function ( data ) {
						handleError(data);
						_timeoutPromise = $timeout(poll, _timeout);
					});
			}
			
			function handleData ( data ) {
				var callEvent,
					call,
					calls = data && data.result ? data.result : [],
					relevantTime = new Date().getTime() - _relevance;
					
					
				for(var i = 0, l = calls.length; i < l; ++i) {
					callEvent = calls[i];
					if(_callsById[callEvent.id]) {
						call = _callsById[callEvent.id];
					} else {
						call = new Call();
						_callsById[callEvent.id] = call;
						$scope.incomingCalls.push(call);
					}
					for(var key in callEvent) {
						call[key] = callEvent[key];
					}
				}
				
				calls = $scope.incomingCalls.concat();
				for(i = 0, l = calls; i < l; ++i) {
					call = calls[i];
					if(new Date(call.timestamp).getTime() < relevantTime) {
						removeCallFromQueue(call);
					}
				}
				$scope.$emit('incomingcall', $scope.incomingCalls);
			}
			
			function handleError ( ) {
				
			}
			
			function removeCallFromQueue ( call ) {
				var index = indexOf($scope.incomingCalls, call);
				delete _callsById[call.id];
				if(index !== -1) {
					$scope.incomingCalls.splice(indexOf($scope.incomingCalls, call), 1);
				}
			}
			
			$scope.$on('callaccept', function ( event, call ) {
				removeCallFromQueue(call);
			});
			
			$scope.$on('callreject', function ( event, call ) {
				removeCallFromQueue(call);
			});
			
		}]);
		
})();
/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.locale')
		.service('translationService', [ function ( ) {
			
			var labels = {};
			
			return {
				get: function ( id ) {
					var label = labels[id] !== undefined ? labels[id] : id,
						args = Array.prototype.slice.call(arguments, 1),
						ses = args.concat();
					
					if(args.length) {
						label = label.replace(/%(s|\d)/g, function ( match, operator ) {
							var r;
							if(operator === 's') {
								r = ses.shift();
							} else {
								r = args[match];
							}
							return r;
						});
					}
					
					return label;
				},
				set: function ( id, value ) {
					labels[id] = value;
				}
			};
		}]);
})();
/*global angular,fetch*/
(function ( ) {

	angular.module('Zaaksysteem')
		.controller('nl.mintlab.message.SystemMessageController', [ '$scope', '$rootScope', 'dataStore', function ( $scope, $rootScope, dataStore ) {
			
			var indexOf = fetch('nl.mintlab.utils.shims.indexOf'),
				safeApply = fetch('nl.mintlab.utils.safeApply');

			$scope.messages = [
			];

			function addMessage ( message ) {
				$scope.messages.push(message);
			}

			$scope.closeMessage = function ( message ) {
				var index = indexOf($scope.messages, message);
				if(index !== -1) {
					$scope.messages.splice(index, 1);
				}
			};
			
			$scope.shouldAutohide = function ( message ) {
				return message.type !== 'error';	
			};
			
			$scope.getTimeout = function ( message ) {
				return $scope.shouldAutohide(message) ? undefined : 0;
			};

			$rootScope.$on('systemMessage', function ( event, message ) {
				safeApply($scope, function ( ) {
					addMessage(message);
				});
			});
			
			dataStore.observe('messages', function ( event, messages ) {
				
				if(!messages) {
					// FIXME
					return;
				}
				
				function addToScope ( ) {
					var i,
						l,
						message;

					for(i = 0, l = messages.length; i < l; ++i) {
						message = messages[i];
						if(indexOf($scope.messages, message) === -1) {
							addMessage(message);
						}
					}
				}
				
				if(!$scope.$$phase && !$scope.$root.$$phase) {
					$scope.$apply(addToScope);
				} else {
					addToScope();
				}
					
			});
			
			$scope.$on('zsTimerComplete', function ( event ) {
				safeApply($scope, function ( ) {
					var message = event.targetScope.message;
					if(message) {
						$scope.closeMessage(message);
					}
					event.stopPropagation();
				});
			});

		}]);

})();
(function ( ) {
	
	
	
})();
/*global angular, fetch*/
(function () {

	angular.module('Zaaksysteem.net')
		.provider('smartHttp', [ function () {

			var inherit = fetch('nl.mintlab.utils.object.inherit'),
				bind = angular.bind,
				EventDispatcher = fetch('nl.mintlab.events.EventDispatcher'),
				indexOf = fetch('nl.mintlab.utils.shims.indexOf');
				
			return {
				defaults: {
					prefix: ''
				},
				$inject: [ '$http', '$rootScope' ],
				$get: [ '$http', function ( $http ) {
					
					var defaults = this.defaults;

					function SmartHttp ( ) {
						this._requests = [];
						this._blockingRequests = [];
						this.defaults = defaults;
					}

					inherit(SmartHttp, EventDispatcher);

					SmartHttp.prototype.connect = function ( config ) {
						var method,
							promise,
							that = this;
						
						if(arguments.length>1) {
							throw new Error('Connect called with legacy arguments');
						}
						
						if(config.method === undefined) {
							config.method = "GET";
						}
						
						method = config.method.toLowerCase();
						
						if(config.blocking === undefined && method === 'post') {
							config.blocking = true;
						}
						
						if(config.blocking) {
							this._blockingRequests.push(config);
						}
						
						if(config.url.indexOf('/') !== 0 && !config.url.match(/^(http|ftp|https):\/\//)) {
							config.url = this.defaults.prefix + config.url;
						}
										
						this._requests.push(config);
						this.publish('connect', config);
						
						promise = $http(config);
						
						function onComplete ( ) {
							var index;
							
							index = indexOf(that._blockingRequests, config);
							
							if(index !== -1) {
								that._blockingRequests.splice(index, 1);
							}
							index = indexOf(that._requests, config);
							if(index !== -1) {
								that._requests.splice(index, 1);
								that.publish('close', config);
							}
						}
						
						promise.then(onComplete, onComplete);
						
						return promise;
					};
					
					SmartHttp.prototype.getRequests = function ( ) {
						return this._requests;
					};
					
					SmartHttp.prototype.getBlockingRequests = function ( ) {
						return this._blockingRequests;
					};
					
					SmartHttp.prototype.getUrl = function ( url ) {
						return this.defaults.prefix + url;
					};
					
					return new SmartHttp();
				}]
			};
			
		}]);
	
})();
/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.net')
		.controller('nl.mintlab.net.SpinnerController', [ '$scope', 'smartHttp', function ( $scope, smartHttp ) {
			
			$scope.uiBlocked = false;
			
			smartHttp.subscribe('connect', function ( ) {
				setLoadState();
			});
			
			smartHttp.subscribe('close', function ( ) {
				setLoadState();
			});
			
			
			function setLoadState ( ) {
				$scope.uiBlocked = $scope.loading || smartHttp.getBlockingRequests().length > 0;
				if($scope.$$phase !== '$digest' && $scope.$$phase !== '$apply') {
					$scope.$apply();
				}
			}
			
			$scope.$watch('loading', function ( ) {
				setLoadState();
			});
			
			setLoadState();
			
			
		}]);
	
})();
/*global angular,fetch*/
(function ( ) {
	
	var MAX_CONTENT_LENGTH = 400;
	
	angular.module('Zaaksysteem.timeline')
		.controller('nl.mintlab.timeline.TimelineController', [ '$scope', '$window', 'smartHttp', function ( $scope, $window, smartHttp ) {
			
			var TimelineEvent = fetch('nl.mintlab.timeline.TimelineEvent'),
				indexOf = fetch('nl.mintlab.utils.shims.indexOf');
			
			var _lastResult,
				_itemsById = {},
				_contactChannels = [
					{
						value: 'behandelaar',
						label: 'Behandelaar'
					},
					{
						value: 'balie',
						label: 'Balie'
					},
					{
						value: 'telefoon',
						label: 'Telefoon'
					},
					{
						value: 'post',
						label: 'Post'
					},
					{
						value: 'email',
						label: 'Email'
					},
					{
						value: 'webformulier',
						label: 'Webformulier'
					}
				],
				_filtersById = {};

            $scope.availableFilters = [
                {
                    id: 'case',
                    label: 'Zaken'
                },
                {
                    id: 'contactmoment',
                    label: 'Contactmomenten'
                },
                {
                    id: 'document',
                    label: 'Documenten'
                },
                {
                    id: 'note',
                    label: 'Notities'
                },
                {
                    id: 'question',
                    label: 'Vragen'
                },
                {
                    id: 'product',
                    label: 'Producten'
                }
            ];
			
			$scope.activeFilters = [];
			$scope.items = [];
			$scope.loading = false;
			
            function processFilters ( ) {
				angular.forEach($scope.availableFilters, function ( filter ) {
					_filtersById[filter.id] = filter;
				});
            }

            processFilters();
						
			function loadData ( ) {
				var url,
					params;
					
				if(!$scope.loading && (!_lastResult || _lastResult.next)) {
					if(_lastResult) {
						url = _lastResult.next;
						params = {};
					} else {
						url = 'event/list';
						params = {
							'for': $scope.id,
							'category': $scope.category
						};
					}
					
					$scope.loading = true;
					smartHttp.connect({
						method: 'GET',
						url: url,
						params: params
					})
						.success(handleSuccess)
						.error(handleError);
				} else {
					$scope.loading = false;
				}
			}
			
			function handleSuccess ( data ) {
				$scope.loading = false;
				var items = data && data.result ? data.result : [],
					event;
				
				_lastResult = data;
				
				angular.forEach(items, function ( item ) {
					event = getItemById(item.id);
					for(var key in item) {
						event[key] = item[key];
					}
				});
				invalidateScroll();
			}
			
			function handleError ( ) {
				$scope.loading = false;
			}
			
			function getItemById ( id ) {
				if(_itemsById[id]) {
					return _itemsById[id];
				}
				var event = new TimelineEvent();
				_itemsById[id] = event;
				$scope.items.push(event);
				return event;
			}
			
			function invalidateScroll ( ) {
				$scope.$broadcast('scrollinvalidate');
			}
			
			$scope.toggleFilter = function ( filter ) {
				if(indexOf($scope.activeFilters, filter) === -1) {
					$scope.activateFilter(filter);
				} else {
					$scope.deactivateFilter(filter);
				}
			};
			
			$scope.activateFilter = function ( filter ) {
				if(indexOf($scope.activeFilters, filter) === -1) {
					$scope.activeFilters.push(filter);
				}
				invalidateScroll();
			};
			
			$scope.deactivateFilter = function ( filter ) {
				var index = indexOf($scope.activeFilters, filter);
				if(index !== -1) {
					$scope.activeFilters.splice(index, 1);
				}
				invalidateScroll();
			};
			
			$scope.isFiltered = function ( filterId ) {
				var filter = _filtersById[filterId];
				return $scope.activeFilters.length === 0 || indexOf($scope.activeFilters, filter) !== -1;
			};
			
			$scope.addNote = function ( content ) {
				var note,
					url;
				
				if(!content) {
					return;
				}
				
				note = new TimelineEvent();
				note.description = 'Notitie aangemaakt';
				note.content = content;
				note.id = -1;
				note.timestamp = new Date().toISOString();
				note.event_category = 'note';
				$scope.items.push(note);
				
				if($scope.category === 'subject') {
					url = 'betrokkene/' + $scope.id + '/notes/create';
				} else if($scope.category === 'case') {
					url = 'zaak/' + $scope.id + '/notes/create';
				}
				
				smartHttp.connect( {
					method: 'POST',
					url: url,
					data: {
						content: content
					}
				})
					.success(function ( data ) {
						var noteData = data.result[0];
						for(var key in noteData) {
							note[key] = noteData[key];
						}
					})
					.error(function ( ) {
						var index = indexOf($scope.items, note);
						if(index !== -1) {
							$scope.items.splice(index, 1);
						}
					});
			};
			
			$scope.addContactmoment = function ( content, contactchannel, rel ) {
				
				
				var contactmoment,
					url,
					data,
					caseId,
					betrokkeneId;
					
				if($scope.category === 'case') {
					caseId = $scope.id;
					betrokkeneId = 'subject_identifier' in rel ? rel.subject_identifier : 'betrokkene-' + rel.object_type + '-' + rel.id;
				} else {
					caseId = rel.id;
					betrokkeneId = $scope.id;
				}
					
				contactmoment = new TimelineEvent();
				contactmoment.description = 'Contactmoment toegevoegd';
				contactmoment.content = content;
				contactmoment.contact_channel = contactchannel;
				contactmoment.case_id = caseId;
				contactmoment.id = -1;
				contactmoment.timestamp = new Date().toISOString();
				contactmoment.event_category = 'contactmoment';
				
				data = {
					contactkanaal: contactchannel,
					content: content,
					case_id: caseId
				};
				
				url = 'betrokkene/' + betrokkeneId + '/contactmoment/create';
				
				$scope.items.push(contactmoment);
				smartHttp.connect( {
					method: 'POST',
					url: url,
					data: data
				})
					.success(function ( data ) {
						var contactMomentData = data.result[0];
						for(var key in contactMomentData) {
							contactmoment[key] = contactMomentData[key];
						}
						$scope.contactmomentContent = '';
						$scope.caseId = null;
					})
					.error(function ( ) {
						var index = indexOf($scope.items, contactmoment);
						if(index !== -1) {
							$scope.items.splice(index, 1);
						}
					});
					
			};
			
			$scope.getItemVisibility = function ( item ) {
				return $scope.isFiltered(item.event_category);
			};
			
			$scope.getContactChannels = function ( ) {
				return _contactChannels;
			};
			
			$scope.initialize = function ( category, id ) {
				$scope.category = category;
				$scope.id = id;
				
				

                if(category === 'case') {
                    $scope.availableFilters = [
                        { id: 'contactmoment', label: 'Contactmomenten' },
                        { id: 'case-mutation', label: 'Wijzigingen' },
                        { id: 'document', label: 'Documenten' },
                        { id: 'note', label: 'Notities' }
                    ];

                    processFilters();
                }
                
				loadData();
			};
			
			$scope.loadMoreData = function ( ) {
				if(!$scope.loading) {
					loadData();
				}
			};
			
			$scope.getTruncatedText = function ( item ) {
				var content = item.content,
					match;
				if(!content){
					return '';
				}
				
				if(content.length > MAX_CONTENT_LENGTH) {
					content = content.substr(0, MAX_CONTENT_LENGTH);
					match = content.match(/^([\s\S]*)\s+/m);
					if(match) {
						content = match[0];
					}
					content += ' ...';
				}
				
				return content;
			};
			
			$scope.isTruncated = function ( item ) {
				return item.content && (!$scope.isEmail(item) || (item.content.length > MAX_CONTENT_LENGTH));
			};
			
			$scope.showItemContent = function ( item, expanded ) {
				return !$scope.isEmail(item)&&!(item.expanded === false && !expanded);
			};
			
			$scope.getSpotEnlighterLabel = function ( rel ) {
				var label;
					
				if($scope.category === 'case') {
					if(rel && rel.object_type === 'bedrijf') {
						label = 'handelsnaam';
					} else if(rel && 'voorletters' in rel) {
						label ='voorletters + \' \' + geslachtsnaam';
					} else {
						label = 'naam';
					}
				} else {
					label = 'id + \': \' + zaaktype_node_id.titel';
				}
				
				return label;
			};
			
			$scope.downloadLog = function ( ) {
				$window.open('/event/download?for=' + $scope.id + '&category=' + $scope.category + '&context=none');
			};
			
			$scope.isEmail = function ( item ) {
				return item.event_type && item.event_type.indexOf('email') === 0;
			};
			
			$scope.$on('scrollend', function ( event/*, from, upto*/ ) {
				loadData();
				event.stopPropagation();
			});
			
		}]);
})();

/*global define*/
(function ( ) {
	
	function TimelineEvent ( ) {
	}
	
	TimelineEvent.prototype.getMimetype = function ( ) {
		var mimetype = this.mimetype || 'Unknown';
		mimetype = mimetype.replace('application/', '').replace('image/','');
		return mimetype;
	};
	
	// FIXME(dario): this is a fix for a backend bug where
	// timezones are not given for timestamps
	TimelineEvent.prototype.getTimestamp = function ( ) {
		var timestamp = this.timestamp || '';
		if(timestamp.substr(timestamp.length-1) !== 'Z') {
			timestamp += 'Z';
		}
		return timestamp;
	};
	
	define('nl.mintlab.timeline.TimelineEvent', function ( ) {
		return TimelineEvent;
	});
	
})();
/*global define,fetch*/
(function ( ) {
	
	define('nl.mintlab.utils.collection.arrayMove', function ( ) {
		
		var indexOf = fetch('nl.mintlab.utils.shims.indexOf');
		
		// modified from http://www.redips.net/javascript/array-move/
		return function ( array, object, index  ) {
			// local variables
			var i,
				tmp,
				pos1 = indexOf(array, object),
				pos2 = parseInt(index, 10);
				
			// if positions are different and inside array
			if (pos1 !== pos2 &&
				0 <= pos1 && pos1 <= array.length &&
				0 <= pos2 && pos2 <= array.length) {
				// save element from position 1
				tmp = array[pos1];
				// move element down and shift other elements up
				if (pos1 < pos2) {
					for (i = pos1; i < pos2; i++) {
						array[i] = array[i + 1];
					}
				}
				// move element up and shift other elements down
				else {
					for (i = pos1; i > pos2; i--) {
						array[i] = array[i - 1];
					}
				}
				// put element from position 1 to destination
				array[pos2] = tmp;
			}
		};
		
	});
	
})();
/*global angular,fetch*/
(function ( ) {

	angular.module('Zaaksysteem')
		.directive('ngBlur', function ( ) {
			
			var safeApply = fetch('nl.mintlab.utils.safeApply');
			
			return function ( scope, element, attrs ) {
				element.bind('blur', function ( ) {
					safeApply(scope, function ( ) {
						scope.$eval(attrs.ngBlur);
					});
				});
			};
			
		});
	
})();
/*global angular,fetch*/
(function ( ) {
	
	var MIN_OFFSET = 3;
	
	angular.module('Zaaksysteem')
		.directive('ngDraggable', [ '$window', '$document', '$compile', '$timeout', 'dropManager', 'templateCompiler', function ( $window, $document, $compile, $timeout, dropManager, templateCompiler  ) {
			
			var getMousePosition = fetch('nl.mintlab.utils.dom.getMousePosition'),
				fromLocalToGlobal = fetch('nl.mintlab.utils.dom.fromLocalToGlobal'),
				fromGlobalToLocal = fetch('nl.mintlab.utils.dom.fromGlobalToLocal'),
				addEventListener = fetch('nl.mintlab.utils.events.addEventListener'),
				removeEventListener = fetch('nl.mintlab.utils.events.removeEventListener'),
				cancelEvent = fetch('nl.mintlab.utils.events.cancelEvent'),
				setMouseEnabled = fetch('nl.mintlab.utils.dom.setMouseEnabled'),
				element = $document[0].createElement('div'),
				body = $document.find('body'),
				hasPartialDragNDropSupport,
				hasFullDragNDropSupport,
				defaultMimetype = 'text/json',
				link;
				
			hasFullDragNDropSupport = 'draggable' in element;
			hasPartialDragNDropSupport = !hasFullDragNDropSupport && !!element.dragDrop;
			
			link = function ( scope, element, attrs ) {
				
				var	enabled = false;
				var that = this;
				
				if(attrs.ngDragMimetype === undefined) {
					attrs.ngDragMimetype = defaultMimetype;
					element.attr('data-ng-drag-mimetype', defaultMimetype);
				}
				
				if(attrs.ngDragImage) {
					templateCompiler.getCompiler(scope.$eval(attrs.ngDragImage)).then(function ( cpl ) {
						that.setDragImage(cpl);
					});
				}
				
				function evaluateDirective ( ) {
					var shouldBeEnabled = attrs.ngDraggable !== '' ? scope.$eval(attrs.ngDraggable) : true;
					if(shouldBeEnabled !== enabled) {
						if(shouldBeEnabled) {
							enabled = true;
							that.enable();
						} else {
							enabled = false;
							that.disable();
						}
					}
				}
				
				scope.$watch(function ( ) {
					evaluateDirective();
				});
				
			};
			
			if(hasFullDragNDropSupport) {
				
				return function ( scope, element, attrs ) {
					
					var that = {},
						mimetype,
						dragImage;
					
					that.enable = function ( ) {
						element.attr('draggable', 'true');
						element.bind('dragstart', onDragStart);
						element.bind('dragend', onDragEnd);
					};
					
					that.disable = function ( ) {
						element.removeAttr('draggable');
						element.unbind('dragstart', onDragStart);
						element.unbind('dragend', onDragEnd);
					};
					
					that.setDragImage = function ( cpl ) {
						dragImage = cpl;
					};
					
					function onDragStart ( event ) {
						event.stopPropagation();
						initDrag(event);
					}
					
					function onDragEnd ( event ) {
						exitDrag(event);
					}
					
					function initDrag ( event ) {
						var dataTransfer = event.dataTransfer,
							data = scope.$eval(attrs.ngDragData),
							dropId = dropManager.createDrop(mimetype, data);
							
						function applyDragImage ( ) {
							if(dragImage) {
								dragImage(scope, function ( clonedElement/*, scope*/ ) {
									body.append(clonedElement);
									dataTransfer.setDragImage(clonedElement[0], 0, 0);
									$timeout(function ( ) {
										clonedElement.remove();
									});
								});
							}	
						}
						
						if(!scope.$$phase && !scope.$root.$$phase) {
							scope.$apply(applyDragImage);
						} else {
							applyDragImage();
						}
						
						dataTransfer.setData('text', dropId);
						scope.$emit('startdrag', element, mimetype);
					}
					
					function exitDrag ( /*event*/ ) {
						scope.$emit('stopdrag', element, mimetype);
					}
					
					link.call(that, scope, element, attrs);
					mimetype = attrs.ngDragMimetype;
					
				};
				
			} else {
				return {
					compile: function ( /*tElement, tAttrs, transclude*/ ) {
						
						return function ( scope, element, attrs ) {
					
							var that = {},
								mimetype,
								prevDroppable = null,
								dragImage,
								dragImageRendered,
								offset,
								origin;
								
							// use addEventListener because somehow bind only triggers 
							// the event the first time
					
							that.enable = function ( ) {
								addEventListener(element[0], 'mousedown', onMouseDown);
							};
							
							that.disable = function ( ) {
								removeEventListener(element[0], 'mousedown', onMouseDown);
							};
							
							that.setDragImage = function ( cpl ) {
								dragImage = cpl;
							};
							
							function onMouseDown ( event ) {
								
								origin = getMousePosition(event);
								
								body.bind('mouseup', onMouseUp);
								body.bind('mousemove', onInitMouseMove);
								
								return cancelEvent(event, false);
							}
							
							function onInitMouseMove ( event ) {
								var pos = getMousePosition(event);
								
								if(!(Math.abs(origin.x - pos.x) >= MIN_OFFSET || Math.abs(origin.y - pos.y) >= MIN_OFFSET)) {
									return;
								}
								
								body.unbind('mousemove', onInitMouseMove);
								startDrag(event);
								return cancelEvent(event, true);
							}
							
							function startDrag ( event ) {
								
								body.bind('mousemove', onMouseMove);
								body.bind('keyup', onKeyUp);
								
								if(dragImage) {
									dragImage(scope, function ( clonedElement/*, scope*/ ) {
										dragImageRendered = clonedElement;
										body.append(dragImageRendered);
										setMouseEnabled(dragImageRendered[0], false);
										positionDragImage(event, true);
									});
								}
								
								element.addClass('drag-active');
								scope.$emit('startdrag', element, mimetype);
							}
							
							function attemptDrop ( ) {
								if(prevDroppable) {
									prevDroppable.scope().performDrop(getDropData(), mimetype);
									prevDroppable.removeClass('drag-over');
									prevDroppable = null;
								}
								stopDrag();
							}
							
							function cancelDrag ( ) {
								if(prevDroppable) {
									prevDroppable.removeClass('drag-over');
									prevDroppable = null;
								}
								stopDrag();
							}
							
							function stopDrag ( ) {
								
								body.unbind('mousemove', onInitMouseMove);
								body.unbind('mousemove', onMouseMove);
								body.unbind('mouseup', onMouseUp);
								body.unbind('keyup', onKeyUp);
								
								if(dragImageRendered) {
									dragImageRendered.remove();
									dragImageRendered = null;
								}
								element.removeClass('drag-active');
								scope.$emit('stopdrag', element, mimetype);
							}
							
							function positionDragImage ( event, isInitialMove ) {
								var mousePos,
									pos;
									
								if(!dragImageRendered) {
									return;
								}
								
								mousePos = getMousePosition(event);
								pos = fromLocalToGlobal(body[0], mousePos);
								
								if(isInitialMove) {
									if(attrs.ngDragImage) {
										offset = { x: 0, y: 0 };
									} else {
										offset = fromGlobalToLocal(element[0], pos);
									}
								}
								
								pos.y -= offset.y;
								pos.x -= offset.x;
								
								dragImageRendered.css('top', pos.y + 'px');
								dragImageRendered.css('left', pos.x + 'px');
								
							}
							
							function onMouseMove ( event ) {
								var mousePos = getMousePosition(event),
									pos = fromLocalToGlobal(body[0], mousePos),
									droppable = dropManager.getDroppable(pos, element);
									
								positionDragImage(event);
								
								if(prevDroppable !== droppable) {
									if(prevDroppable) {
										prevDroppable.removeClass('drag-over');
									}
									prevDroppable = droppable;
									if(droppable) {
										droppable.addClass('drag-over');
									}
								}
							}
							
							function onMouseUp ( /*event*/ ) {
								attemptDrop();
							}
							
							function onKeyUp ( event ) {
								if(event.keyCode === 27) {
									cancelDrag();
								}
							}
							
							function getDropData ( ) {
								return scope.$eval(attrs.ngDragData);
							}
							
							that.setDragImage(function ( scope, callback ) {
								var clone = element.clone();
								clone.addClass('as-drag-image');
								callback(clone,scope);
							});
							
							link.call(that, scope, element, attrs);
							
							element.find('*').attr('unselectable', 'on');
							
							mimetype = element.attr('data-ng-drag-mimetype');
						};
					}
				};
			}
			
		} ]);
	
})();
/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('ngDroppable', [ '$document', 'dropManager', function ( $document, dropManager ) {
			
			var element = $document[0].createElement('div'),
				hasPartialDragNDropSupport,
				hasFullDragNDropSupport,
				defaultMimetype = 'text/json',
				indexOf = fetch('nl.mintlab.utils.shims.indexOf'),
				link;
				
			hasFullDragNDropSupport = 'draggable' in element;
			hasPartialDragNDropSupport = !hasFullDragNDropSupport && !!element.dragDrop;
			
			if(hasFullDragNDropSupport) {
				link = function ( scope, element, attrs ) {
					
					var _enabled = false;
											
					function evaluateDirective ( ) {
						var shouldBeEnabled = attrs.ngDroppable !== '' ? scope.$eval(attrs.ngDroppable) : true;
						
						if(shouldBeEnabled === undefined) {
							shouldBeEnabled = true;
						}
						
						if(shouldBeEnabled !== _enabled) {
							if(shouldBeEnabled) {
								enable();
							} else {
								disable();
							}
						}
					}
					
					function enable ( ) {
						_enabled = true;
						element.bind('dragenter', onDragEnter);
						element.bind('dragover', onDragOver);
						element.bind('dragleave', onDragLeave);
						element.bind('drop', onDrop);
					}
					
					function disable ( ) {
						_enabled = false;
						element.unbind('dragenter', onDragEnter);
						element.unbind('dragover', onDragOver);
						element.unbind('dragleave', onDragLeave);
						element.unbind('drop', onDrop);
					}
					
					function onDragEnter ( event ) {
						if(isValidDrag(event)) {
							enterDragMode(event);
							return false;
						}
					}
					
					function onDragOver ( event ) {
						if(isValidDrag(event)) {
							enterDragMode(event);
							return false;
						}
					}
					
					function onDragLeave ( event ) {
						exitDragMode(event);
					}
					
					function enterDragMode ( event ) {
						
						event.preventDefault();
						event.stopPropagation();
						
						element.addClass('drag-over');
					}
					
					function exitDragMode ( /*event*/ ) {
						element.removeClass('drag-over');
					}
					
					function isValidDrag ( event ) {
						
						var mimetype = attrs.ngDropMimetype || defaultMimetype,
							dataTransfer = event.dataTransfer,
							isValidMimetype = false,
							drop = dropManager.getCurrentDrop();
						
						if(drop) {
							isValidMimetype = drop.mimetype === mimetype;
						} else {
							if(hasFullDragNDropSupport) {
								if(angular.isArray(dataTransfer.types)) {
									isValidMimetype = indexOf(dataTransfer.types, mimetype) !== -1;
								} else if('contains' in dataTransfer.types) {
									isValidMimetype = dataTransfer.types.contains(mimetype);
								}
							}	
						}
						
						return isValidMimetype;
					}
					
					function onDrop ( event ) {
						var mimetype = attrs.ngDropMimetype || defaultMimetype,
							data;
							
						if(!isValidDrag(event)) {
							return;
						}
														
						if(mimetype === 'Files') {
							data = event.dataTransfer.files;
						} else {
							data = dropManager.getCurrentDrop().data;
						}
						
						scope.$emit('drop', data, mimetype);
						
						exitDragMode();
						
						event.preventDefault();
						event.stopPropagation();
					}
					
					scope.$watch('ngDroppable', function ( ) {
						evaluateDirective();
					});
				};
			} else {
				link = function ( scope, element, attrs ) {
					
					var _enabled;
					
					function evaluateDirective ( ) {
						var shouldBeEnabled = attrs.ngDroppable !== '' ? scope.$eval(attrs.ngDroppable) : true;
						if(shouldBeEnabled === undefined) {
							shouldBeEnabled = true;
						}
						
						if(shouldBeEnabled !== _enabled) {
							if(shouldBeEnabled) {
								enable();
							} else {
								disable();
							}
						}
					}
					
					function enable ( ) {
						_enabled = true;
						dropManager.register(element);
					}
					
					function disable ( ) {
						_enabled = false;
						dropManager.unregister(element);
					}
					
					scope.performDrop = function ( data, mimetype ) {
						scope.$emit('drop', data, mimetype);
					};
					
					scope.$watch('ngDroppable', function ( ) {
						evaluateDirective();
					});
					
					scope.$on('$destroy', function ( ) {
						disable();
					});
				};
			}
			
			return link;
			
			
		}]);
	
})();
/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('ngEditable', [ '$timeout', function ( $timeout ) {
			return {
				link: function ( scope, element, attrs ) {
					
					var input = element.find('input'),
						label = element.find('span'),
						display = element.css('display');
						
					if(input.length === 0) {
						return;
					}
						
					scope.edit = function ( ) {
						input.css('display', display);
						label.css('display', 'none');
						$timeout(function ( ) {
							input[0].focus();
						});
					};
					
					scope.unedit = function ( ) {
						input.css('display', 'none');
						label.css('display', display);
					};
					
					function save ( ) {
						scope.$emit('editsave', attrs.ngEditable, input[0].value);
					}
					
					function cancel ( ) {
						scope.$emit('editcancel', attrs.ngEditable, input[0].value);
					}
					
					function setInputValue ( ) {
						var val = scope.$eval(attrs.ngModel);
						input.text(val);
					}
					
					input.bind('keyup', function ( event ) {
						if(event.keyCode === 13) {
							if(input.hasClass('ng-invalid')) {
								return;
							}
							save();
						} else if(event.keyCode === 27) {
							cancel();
						} else {
							return;
						}
						scope.unedit();
						scope.$apply();
					});
					
					input.bind('blur', function ( /*event*/ ) {
						if(scope.editMode) {
							save();
							scope.unedit();
							scope.$apply();
						}
					});
					
					attrs.$observe('ngEditableActive', function ( ) {
						var isActive = scope.$eval(attrs.ngEditableActive);
						if(isActive) {
							scope.edit();
						} else {
							scope.unedit();
						}
					});
					
					scope.$watch('ngModel', function ( ) {
						setInputValue();
					});
					
				}
			};
		}]);
})();
/*global angular*/
(function ( ) {

	angular.module('Zaaksysteem')
		.directive('ngEnter', function ( ) {
			return function ( scope, element, attrs ) {
				element.bind('keyup', function ( event ) {
					if(event.keyCode === 13) {
						scope.$eval(attrs.ngEnter);
					}
				});
				
			};
		});
})();
/*global angular*/
(function ( ) {

	angular.module('Zaaksysteem')
		.directive('ngEscape', function ( ) {
			return function ( scope, element, attrs ) {
				element.bind('keyup', function ( event ) {
					if(event.keyCode === 27) {
						scope.$eval(attrs.ngEscape);
					}
				});
				
			};
		});
})();
/*global angular*/
(function ( ) {

	angular.module('Zaaksysteem')
		.directive('ngFocusIn', function ( ) {
			
			return function ( scope, element, attrs ) {
				element.bind('focusin', function ( ) {
					scope.$eval(attrs.ngFocusIn);
				});
			};
			
		});
	
})();
/*global angular*/
(function ( ) {

	angular.module('Zaaksysteem')
		.directive('ngFocusOut', function ( ) {
			
			return function ( scope, element, attrs ) {
				element.bind('focusout', function ( ) {
					scope.$eval(attrs.ngFocusOut);
				});
			};
			
		});
	
})();
/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('ngMultiselect', [ function ( ) {
			
			var query = 'input[type="checkbox"]',
				indexOf = fetch('nl.mintlab.utils.shims.indexOf'),
				cancelEvent = fetch('nl.mintlab.utils.events.cancelEvent');
			
			return function ( scope, element, attrs ) {
				
				var listeningTo = [],
					lastClicked,
					isEnabled,
					unwatch;
					
				function evaluateDirective ( ) {
					var shouldBeEnabled = attrs.ngMultiselect !== ''  ? scope.$eval(attrs.ngMultiselect) : true;
					if(shouldBeEnabled !== isEnabled) {
						if(shouldBeEnabled) {
							enable();
						} else {
							disable();
						}
					}
				}
				
				function enable ( ) {
					unwatch = scope.$watch(function ( ) {
						setListeners();
					});
				}
				
				function disable ( ) {
					
					var input;
					
					while(listeningTo.length) {
						input = listeningTo.shift();
						angular.element(input).unbind('click', onInputClick);
					}
					
					if(unwatch) {
						unwatch();
					}
					
					unwatch = null;
					
				}
				
				function getInputList ( ) {
					return element[0].querySelectorAll(query);
				}
				
				function setListeners ( ) {
					var list = getInputList(),
						i,
						l,
						input,
						queue = listeningTo.concat(),
						index;
						
					for(i = 0, l = list.length; i < l; i++) {
						input = list[i];
						index = indexOf(queue, input);
						if(index === -1) {
							listeningTo.push(input);
							angular.element(input).bind('click', onInputClick);
						} else {
							listeningTo.splice(index, 1);
							queue.splice(index, 1);
						}
					}
					
					for(i = 0, l = queue.length; i < l; ++i) {
						input = queue[i];
						angular.element(input).unbind('click', onInputClick);
					}
					
				}
				
				function onInputClick ( event ) {
					var list,
						input,
						i,
						l,
						start,
						end;
						
					if(!event) {
						return;
					}
					
					if(event.shiftKey && lastClicked) {
						cancelEvent(event);
						
						list = getInputList();
						
						start = indexOf(list, lastClicked);
						end = indexOf(list, event.target);
						
						if(end < start) {
							i = end,
							l = i + (start-end + 1);
						} else {
							i = start;
							l = i + (end-start + 1);
						}
						
						for(; i < l; ++i) {
							input = angular.element(list[i]);
							if(input.attr('checked') !== 'checked') {
								input[0].click();
							}
						}
						
					}
					lastClicked = event.target;
				}
				
				scope.$watch('ngMultiselect', function ( ) {
					evaluateDirective();
				});
			};
		}]);
})();
/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('ngUpload', [ '$parse', '$window', '$document', 'fileUploader', function ( $parse, $window, $document, fileUploader ) {
			
			var supportsFileApi = fileUploader.supports();
			
			return {
				link: function ( scope, element, attrs ) {
						
					var callback = $parse(attrs.ngUpload),
						button = element.find('button'),
						input = element.find('input'),
						form = element.find('form'),
						addEventListener = fetch('nl.mintlab.utils.events.addEventListener'),
						removeEventListener = fetch('nl.mintlab.utils.events.removeEventListener');
						
					
					if(supportsFileApi) {
						form.css('display', 'none');
						button.bind('click', function ( ) {
							// reset value to enable upload of same file
							input[0].value = '';
							input[0].click();
						});
						// input.bind('change') doesn't work for some reason
						addEventListener(input[0], 'change', function ( ) {
						
							var files = input[0].files,
								fileArray = [],
								i,
								l;
								
							if(files.length) {
								for(i = 0, l = files.length; i < l; ++i) {
									fileArray.push(files[i]);
								}
								callback(scope, { '$files': files } );
							}
						});
						element.attr('data-zs-file-api-supported', 'true');
					} else {
						
						(function ( ) {
							
							// replace input element because value is read only in IE
							
							function onChange ( ) {
								var files = [],
									fileName,
									file,
									val;
								
								val = input[0].value.split('/');
								fileName = val[val.length-1];
								
								file = {
									name: fileName,
									form: form[0]
								};
								
								files.push(file);
								
								callback(scope, { '$files': files } );
								
								reset();
							}
							
							function initialize ( ) {
								addEventListener(input[0], 'change', onChange);
							}
							
							function reset ( ) {
								
								var clone = input.clone(true),
									before = angular.element(input[0].previousSibling);
									
								if(!before.length) {
									input.parent().prepend(clone);
								} else {
									before.after(clone);
								}
								removeEventListener(input[0], 'change', onChange);
								input.remove();
								
								input = clone;
								
								initialize();
							}
							
							initialize();
							
							element.attr('data-zs-file-api-supported', 'false');
						})();
						
						
						
					}
					
					
					
					
				}
			};
		}]);
})();
/*global angular,$,fetch,setInterval,clearInterval*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsAccordion', [ '$window', function ( $window ) {
			
			var addEventListener = fetch('nl.mintlab.utils.events.addEventListener'),
				removeEventListener = fetch('nl.mintlab.utils.events.removeEventListener');
			
			return {
				compile: function ( /*tELement, tAttrs, transclude*/ ) {
					
					return function link ( scope, element, attrs ) {
						
						var initialized,
							initUnwatch,
							refreshUnwatch,
							destroyUnwatch,
							intervalId;
						
						function initWidget ( ) {
							var widget = $(element[0]);
							widget.accordion( { active: 0, autoHeight: false, fillSpace: true } );
							initialized = true;
							setActive();
						}
						
						function onDocResize ( ) {
							resize();
						}
						
						function resize ( ) {
							var widget = $(element[0]);
							widget.accordion('resize');
							widget.accordion('refresh');
						}
						
						function setActive ( ) {
							var widget = $(element[0]),
								active = parseInt(scope.$eval(attrs.zsAccordionActive), 10) || 0;
							
							widget.accordion( { active: active } );
						}
						
						initUnwatch = scope.$watch(function ( ) {
							if(element.children().length) {
								
								initUnwatch();
								initUnwatch = null;
								
								initWidget();
								addListeners();
								
								intervalId = setInterval(function ( ) {
									resize();
								}, 1000);
							}
						});
						
						function addListeners ( ) {
							refreshUnwatch = scope.$watch(function ( ) {
								if(initialized) {
									resize();
								}
							});
							
							addEventListener($window, 'resize', onDocResize);
						}
						
						destroyUnwatch = scope.$on('$destroy', function ( ) {
							if(initUnwatch) {
								initUnwatch();
							}
							if(refreshUnwatch) {
								refreshUnwatch();
							}
							
							if(intervalId) {
								clearInterval(intervalId);
							}
							
							removeEventListener($window, 'resize', onDocResize);
							
							destroyUnwatch();
						});
						
						attrs.$observe('zsAccordionActive', function ( ) {
							if(initialized) {
								setActive();
							}
						});
						
					};
					
				}
			};
			
		}]);
	
})();
/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsAutogrow', [ '$document', function ( $document ) {
			
			return {
				link: function ( scope, element, attrs ) {
					
					var minRows = attrs.zsAutogrow || 1;
					
					element.attr('rows', minRows);
					element.css('height', 'auto');
					
					element.bind('change keypress paste focus textInput input', function ( ) {
						setSize();
					});
					
					function setSize ( ) {
						
						var el = element[0],
							rows = minRows;
						
						element.attr('rows', rows);
						
						while(el.scrollHeight > el.clientHeight) {
							element.attr('rows', rows++);
						}
						
					}
					
				}
			};
		}]);
	
})();
/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.case')
		.directive('zsCaseProgress', [ function ( ) {
			
			return {
				replace: false,
				templateUrl: '/partials/directives/case/case-progress.html',
				scope: {
					progress: '@zsCaseProgress'
				},
				compile: function ( /*tElement, tAttrs, transclude*/ ) {
					
				}
			};
			
		}]);
	
})();
/*global angular,window,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsConfirm', [ function ( ) {
			
			// TODO(dario): use template compiling from documentairly
			// to generate a decent dialog
			
			var addEventListener = fetch('nl.mintlab.utils.events.addEventListener'),
				cancelEvent = fetch('nl.mintlab.utils.events.cancelEvent'),
				win = window;
			
			return {
				// make sure directive is executed before ng-click
				priority: 101,
				compile: function ( /*tElement, tAttrs, transclude*/ ) {
					
					return function link ( scope, element, attrs ) {
						
						// FIXME(dario): does not work in IE8 as events
						// are executed in random order. this is in line
						// with the spec, so should be addressed anyway.
						
						function onClick ( event ) {
							var lbl = attrs.zsConfirm;
							if(lbl && !win.confirm(lbl)) {
								cancelEvent(event, true);
							}
						}
						
						addEventListener(element[0], 'click', onClick);
						
					};
					
				}
			};
		}]);
	
})();
/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsContextmenu', [ '$document', '$timeout', 'templateCompiler', function ( $document, $timeout, templateCompiler ) {
			
			var getMousePosition = fetch('nl.mintlab.utils.dom.getMousePosition'),
				getViewportSize = fetch('nl.mintlab.utils.dom.getViewportSize'),
				cancelEvent = fetch('nl.mintlab.utils.events.cancelEvent'),
				body = $document.find('body'),
				overlay = angular.element($document[0].createElement('div'));
				
			overlay.addClass('context-menu-overlay');
			overlay.css('position', 'fixed');
			overlay.css('top', '0');
			overlay.css('left', '0');
			overlay.css('width', '100%');
			overlay.css('height', '100%');
			overlay.css('background', 'transparent');
			
			return {
				compile: function ( /*tElement, tAttrs*/ ) {
					
					return function link ( scope, element, attrs ) {
						
						var tpl,
							compiler,
							menu,
							isOpen;
						
						function onContextMenu ( event ) {
							var pos;
								
							if(!compiler) {
								return;
							}
							
							pos = getMousePosition(event);
							
							cancelEvent(event);
							openMenu(pos.x, pos.y);
						}
						
						function onOverlayClick ( /*event*/ ) {
							closeMenu();
						}
						
						function onMenuClick ( /*event*/ ) {
							closeMenu();
						}
						
						function positionMenu ( cx, cy ) {
							var width = menu[0].clientWidth,
								height = menu[0].clientHeight,
								viewportSize = getViewportSize(),
								x,
								y;
							
							if(cx + width > viewportSize.width) {
								x = cx - width;
							} else {
								x = cx;
							}
							
							if(cy + height > viewportSize.height) {
								y = cy - height;
							} else {
								y = cy;
							}
							
							menu.css('top', y + 'px');
							menu.css('left', x + 'px');
							
						}
						
						function openMenu ( x, y ) {
							
							if(isOpen) {
								return;
							}
							
							isOpen = true;
							body.append(overlay);
							
							$timeout(function ( ) {
								compiler(scope, function ( clonedElement/*, scope*/ ) {
									menu = clonedElement;
									menu.css('opacity', '0');
									menu.css('position', 'fixed');
									menu.bind('click', onMenuClick);
									overlay.after(menu);
									$timeout(function ( ) {
										menu.css('opacity', '1');
										positionMenu(x, y);
									});
								});
							});
							
							overlay.bind('click', onOverlayClick);
							body.bind('contextmenu', onBodyContextMenu);
							$document.bind('keydown', onKeyDown);
							
							scope.$emit('contextmenuopen');
							
						}
						
						function closeMenu ( ) {
							
							if(!isOpen) {
								return;
							}
							
							isOpen = false;
							
							if(menu) {
								menu.unbind('click', onMenuClick);
								menu.remove();
								menu = null;
							}
							
							overlay.remove();
							overlay.unbind('click', onOverlayClick);
							body.unbind('contextmenu', onBodyContextMenu);
							$document.unbind('keydown', onKeyDown);
							
							
							scope.$emit('contextmenuclose');
						}
						
						function onKeyDown ( event ) {
							if(event.keyCode === 27) {
								closeMenu();
							}
						}
						
						function onBodyContextMenu ( event ) {
							cancelEvent(event);
						}
						
						attrs.$observe('zsContextmenu', function ( ) {
							tpl = scope.$eval(attrs.zsContextmenu);
							if(tpl) {
								templateCompiler.getCompiler(tpl).then(function ( cpl ) {
									compiler = cpl;
								});
							} else {
								compiler = null;
							}
						});
						
						element.bind('contextmenu', onContextMenu);
						
						
					};
				}
			};
			
		}]);
	
})();
/*global angular,fetch*/
(function ( ) {
	angular.module('Zaaksysteem')
		.directive('zsDropdownMenu', [ '$document', function ( $document ) {
			
			var contains = fetch('nl.mintlab.utils.dom.contains'),
				body = $document.find('body');
			
			return function ( scope, element/*, attrs*/ ) {
				
				var menu = element.find('ul'),
					button = element.find('button').eq(0);
				
				function toggleMenu ( ) {
					if(isOpen()) {
						closeMenu();
					} else {
						openMenu();
					}
				}
				
				function openMenu ( ) {
					menu.addClass('menu-open');
					body.bind('click', onBodyClick);
				}
				
				function closeMenu ( ) {
					menu.removeClass('menu-open');
					body.unbind('click', onBodyClick);
				}
				
				function onBodyClick ( event ) {
					var targetEl = event.target,
						menuContains = contains(menu[0], targetEl),
						elementContains = !menuContains && contains(element[0], targetEl);
						
					if(isOpen() && ((menuContains && angular.element(targetEl).parent('button').length) || !elementContains)) {
						closeMenu();
					}
					
				}
				
				function isOpen ( ) {
					return menu.hasClass('menu-open');
				}
				
				button.bind('click', function ( ) {
					toggleMenu();
				});
					
				closeMenu();
				
			};
			
		}]);
})();
/*global angular,fetch*/
(function ( ) {
	
	var OFFSET_X = 0;
	
	angular.module('Zaaksysteem')
		.directive('zsHoverMenu', [ function ( ) {
			
			var getViewportSize = fetch('nl.mintlab.utils.dom.getViewportSize'),
				fromLocalToGlobal = fetch('nl.mintlab.utils.dom.fromLocalToGlobal'),
				fromGlobalToLocal = fetch('nl.mintlab.utils.dom.fromGlobalToLocal');
		
			return {
				compile: function ( /*tElement, tAttrs, transclude*/ ) {
					
					return function ( scope, element, attrs ) {
						
						var menu = element.find('ul'),
							orientation = (attrs.zsHoverMenu === 'vertical' ? 'vertical' : 'horizontal');
						
						function onMouseOver ( ) {
							openMenu();
						}
						
						function onMouseOut ( ) {
							closeMenu();
						}
						
						function openMenu ( ) {
							menu.addClass('menu-open');
							element.addClass('element-menu-open');
							positionMenu();
						}
						
						function positionMenu ( ) {
							var width = menu[0].clientWidth,
								height = menu[0].clientHeight,
								viewportSize = getViewportSize(),
								x = 0,
								y = 0,
								parent = angular.element(menu[0].offsetParent),
								p = fromLocalToGlobal(parent[0], { x: 0, y: 0 }),
								parentBounds = { x: p.x, y: p.y, height: parent[0].clientHeight, width: parent[0].clientWidth };
								
							if(orientation === 'horizontal') {
								
								if(parentBounds.x + parentBounds.width + width > viewportSize.width) {
									x = parentBounds.x - width + OFFSET_X;
									parent.attr('data-zs-hover-menu-horizontal-orient', 'left');
								} else {
									x = parentBounds.x + parentBounds.width - OFFSET_X;
									parent.attr('data-zs-hover-menu-horizontal-orient', 'right');
								}
								
								if(parentBounds.y + parentBounds.height + height > viewportSize.height) {
									y = parentBounds.y + parentBounds.height - height;
									parent.attr('data-zs-hover-menu-vertical-orient', 'top');
								} else {
									y = parentBounds.y;
									parent.attr('data-zs-hover-menu-vertical-orient', 'bottom');
								}
							} else {
								throw new Error('vertical orientation not yet implemented in zsHoverMenu');
							}
							
							p = fromGlobalToLocal(parent[0], { x: x, y: y });
							
							menu.css('top', p.y + 'px');
							menu.css('left', p.x + 'px');
						}
						
						function closeMenu ( ) {
							menu.removeClass('menu-open');
							element.removeClass('element-menu-open');
						}
						
						function onMenuClick ( ) {
							closeMenu();
						}
						
						element.bind('mouseover', onMouseOver);
						element.bind('mouseout', onMouseOut);
						menu.bind('click', onMenuClick);
						
						closeMenu();
						
					};
					
				}
			};
		
	}]);
	
})();
/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsInfiniteScroll', [ '$timeout', '$document', '$window', function ( $timeout, $document, $window ) {
			
			var getViewportPosition = fetch('nl.mintlab.utils.dom.getViewportPosition'),
				getWindowHeight = fetch('nl.mintlab.utils.dom.getWindowHeight');
			
			return {
				scope: {
					
				},
				link: function ( scope, element, attrs ) {
					
					var _timeoutInterval = 1000,
						_invalidateInterval = 250,
						_contained = attrs.zsInfiniteScroll === 'contained',
						_dispatching = false,
						_dispatchedScrollHeight = NaN;
					
					if(_contained) {
						element.bind('scroll', onScroll);
					} else {
						$document.bind('scroll', onScroll);
					}
					
					$timeout(onInterval, _timeoutInterval);
					
					checkBoundaries();
					
					scope.$on('scrollinvalidate', function ( ) {
						_dispatchedScrollHeight = NaN;
						checkBoundaries();
					});
										
					function onScroll ( ) {
						checkBoundaries();
					}
					
					function onInterval ( ) {
						$timeout(onInterval, _timeoutInterval, false);
						checkBoundaries();
					}
					
					function invalidate ( from, upto ) {
						var scrollHeight = element[0].scrollHeight;
						if(!_dispatching && scrollHeight !== _dispatchedScrollHeight) {
							_dispatching = true;
							_dispatchedScrollHeight = scrollHeight;
							$timeout(function ( ) {
								_dispatching = false;
								scope.$emit('scrollend', from, upto);
							}, null, false);
						}
					}
					
					function checkBoundaries ( ) {
						//TODO(dario): implement contained
						var elementTop = getViewportPosition(element[0]).y,
							scrollHeight = element[0].scrollHeight,
							elementBottom = elementTop + scrollHeight,
							// IE8 does not support innerHeight
							viewportHeight = getWindowHeight(),
							visibleTop = Math.max(0, -elementTop),
							visibleBottom = scrollHeight - Math.max(0, elementBottom - viewportHeight),
							from = visibleTop / scrollHeight || 0,
							upto = visibleBottom/scrollHeight;
							
						if(upto === 1) {
							invalidate(from, upto);
						}
					}
					
					
				}
			};
			
		}]);
	
})();
/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsModal', [ '$document', function ( $document ) {
			
			var addEventListener = fetch('nl.mintlab.utils.events.addEventListener'),
				removeEventListener = fetch('nl.mintlab.utils.events.removeEventListener'),
				doc = $document[0];
			
			return {
				scope: true,
				transclude: true,
				templateUrl: '/partials/directives/popup/modal.html',
				compile: function ( /*tElement, tAttrs, transclude*/ ) {
					
					return function ( scope/*, element, attrs*/ ) {
						
						var closeUnbind;
						
						function onKeyUp ( event ) {
							if(event.keyCode === 27) {
								clean();
								scope.closePopup();
							}
						}
						
						function clean ( ) {
							removeEventListener(doc, 'keyup', onKeyUp);
							if(closeUnbind) {
								closeUnbind();
							}
							if(destroyUnbind) {
								destroyUnbind();
							}
						}
					
						addEventListener(doc, 'keyup', onKeyUp);
						
						closeUnbind = scope.$on('popupclose', clean);
						
						var destroyUnbind = scope.$on('destroy', function ( ) {
							clean();
						});
						
						
					};
				}
			};
			
		}]);
	
})();
/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsMultiselect', [ function ( ) {
			
			var element = angular.element,
				indexOf = fetch('nl.mintlab.utils.shims.indexOf');
			
			function findMultiselectTarget ( trg ) {
				var target = element(trg);
				while(target.length) {
					// TODO: combine ng-multiselect and zs-multiselects
					if(target.attr('data-ng-multiselect') !== undefined) {
						return null;
					}
					if(target.attr('data-zs-multiselectable') !== undefined) {
						return target;
					}
					if(target.attr('data-zs-multiselect') !== undefined) {
						return null;
					}
					target = target.parent();
				}
			}
			
			return {
				compile: function ( /*tElement, tAttrs, transclude*/ ) {
					
					return function link ( scope, element, attrs ) {
						
						var lastClicked = null,
							list = null,
							enabled,
							unwatch;
							
						function enable ( ) {
							enabled = true;
							
							unwatch = scope.$watch(function ( ) {
								list = null;
							});
							
							element.bind('click', onElementClick);
						}
						
						function disable ( ) {
							enabled = false;
							
							if(unwatch) {
								unwatch();
							}
							
							element.unbind('click', onElementClick);
						}
													
						function onElementClick ( event ) {
							var target = findMultiselectTarget(event.target);
							if(!target) {
								return;
							}
							if(lastClicked && event.shiftKey) {
								selectUpto(target);
							} else {
								lastClicked = target;
							}
						}
						
						function selectUpto ( target ) {
							var i,
								l,
								nativeTarget = target[0],
								nativeLast = lastClicked[0],
								input,
								selectableList = getSelectableList(),
								start = indexOf(list, nativeLast),
								end = indexOf(list, nativeTarget);
							
							if(end < start) {
								i = end,
								l = i + (start-end + 1);
							} else {
								i = start;
								l = i + (end-start + 1);
							}
							
							for(; i < l; ++i) {
								// TODO(dario): don't exactly know what this does,
								// but we should just use zsSelectableList instead of zsMultiselect
								input = selectableList[i] ? selectableList[i].querySelector('input[type="checkbox"]') : null;
								if(input && !input.checked) {
									angular.element(input).triggerHandler('click');
								}
							}
						}
						
						function getSelectableList ( ) {
							if(list) {
								return list;
							}
							
							list = element[0].querySelectorAll('[data-zs-multiselectable]');
							
							return list;
						}
						
						attrs.$observe('zsMultiselect', function ( ) {
							var shouldBeEnabled = attrs.zsMultiselect === "false" ? false : !!attrs.zsMultiselect;
							if(shouldBeEnabled !== enabled) {
								if(shouldBeEnabled) {
									enable();
								} else {
									disable();
								}
							}
						});
						
						
						
					};
					
				}
			};
			
		} ]);
	
})();
/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsPlaceholder', [ '$document', function ( $document ) {
			
			var doc = $document[0],
				hasSupport = "placeholder" in $document[0].createElement('input');
			
			return {
				link: function ( scope, element, attrs ) {
					
					var clone,
						placeholder = scope.$eval(attrs.zsPlaceholder);
						
					
					if(hasSupport) {
						element.attr('placeholder', placeholder);
						return;
					}
					
					function setVisibility ( ) {
						var hasFocus = doc.activeElement === element[0] || doc.activeElement === clone[0],
							visibleElement;
						
						if(element.val()) {
							showSource();
							visibleElement = element;
						} else {
							showClone();
							visibleElement = clone;
						}
						
						if(hasFocus) {
							visibleElement[0].focus();
						}
					}
					
					function onCloneFocus ( ) {
						showSource();
						element[0].focus();
					}
					
					function showSource ( ) {
						var parent = clone.parent ? clone.parent() : null;
						if(parent && parent.length) {
							clone.after(element);
							clone[0].parentNode.removeChild(clone[0]);
						}
					}
					
					function showClone ( ) {
						var parent = element.parent ? element.parent() : null;
						if(parent && parent.length) {
							element.after(clone);
							element[0].parentNode.removeChild(element[0]);
						}
					}
					
					function onBlur ( ) {
						setVisibility();
					}
					
					function onModelChange ( ) {
						if(doc.activeElement !== element[0]) {
							setVisibility();
						}
					}
					
					
					clone = element.clone(true);
					
					clone.removeAttr('ng-model');
					clone.removeAttr('zs-placeholder');
					clone.addClass('zs-placeholder-clone');
					
					clone.bind('focus', onCloneFocus);
					element.bind('blur', onBlur);
					
					clone.val(placeholder);
					
					if(attrs.ngModel) {
						scope.$watch(attrs.ngModel, onModelChange);
					}
				}
			};
		}]);
	
})();
/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsPopup', [ '$timeout', '$document', 'templateCompiler', function ( $timeout, $document, templateCompiler ) {
			
			var fromGlobalToLocal = fetch('nl.mintlab.utils.dom.fromGlobalToLocal'),
				body = $document.find('body');
			
			return {
				scope: true,
				controller: [ '$scope', '$element', '$attrs', function ( $scope, $element, $attrs ) {
					
					var popup,
						isOpen,
						compiler;
					
					var options = this.options = {
						followMouse: false,
						registration: 'top'
					};
					
					function openPopup ( x, y ) {
						
						isOpen = true;
						x = x || 0;
						y = y || 0;
					
						templateCompiler.getCompiler($scope.$eval($attrs.zsPopup)).then(function ( cpl ) {
							compiler = cpl;
							
							$timeout(function ( ) {
								var point;
								
								compiler($scope, function ( clonedElement/*, scope*/ ) {
									
									popup = clonedElement;
									
									body.append(popup);
									
									if(!options.followMouse) {
										point = fromGlobalToLocal(popup[0].offsetParent, { x: x, y: y });
										popup.css('top', point.y + 'px');
										popup.css('left', point.x + 'px');
									} else {
										//TODO(dario): implement followMouse
									}
									
									$scope.$broadcast('popupopen', popup);
								});
							});
						});
					}
					
					function closePopup ( ) {
						$scope.$broadcast('popupclose', popup);
						isOpen = false;
						popup.remove();
						popup = null;
					}
					
					$scope.openPopup = function ( /*event or x, y*/ ) {
						var x,
							y,
							event;
						
						if(typeof arguments[0] === "number") {
							x = arguments[0],
							y = arguments[1];
						} else if(arguments[0]) {
							event = arguments[0];
							x = event.pageX;
							y = event.pageY;
							event.stopPropagation();
						}
						
						openPopup(x, y);
					};
					
					$scope.closePopup = function ( ) {
						closePopup();
					};
					
					$scope.isOpen = function ( ) {
						return isOpen;
					};
					
				}]
			};
			
		
		}]);
	
})();
/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsRestrict', [ function ( ) {
			
			return {
				compile: function ( /*tElement, tAttrs, transclude*/ ) {
					
					return function link ( scope, element, attrs ) {
						
						var restrict = scope.$eval(attrs.zsRestrict);
						console.log(restrict);
						
					};
					
				}
			};
			
		}]);
	
})();
/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('script', [ function ( ) {
			
			var safeApply = fetch('nl.mintlab.utils.safeApply');
			
			return {
				restrict: 'E',
				scope: true,
				terminal: 'true',
				compile: function ( /*tElement, tAttrs, transclude*/ ) {
					
					return function link ( scope, element, attrs ) {
						if(attrs.type === 'text/zs-scope-data') {
							var data = JSON.parse(element[0].innerHTML),
								// restrict: 'E' always creates new scope
								parentScope = scope.$parent;
														
							safeApply(parentScope, function ( ) {
								for(var key in data) {
									parentScope[key] = data[key];
								}
							});
							
						}
					};
					
				}
			};
			
		}]);
	
})();
/*global angular,$*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsSelect', [ function ( ) {
			
			return {
				link: function ( scope, element, attrs ) {
					
					var menu;
					
					function onChange ( event, object ) {
						var options = element.find('option'),
							val = object.value;
						
						angular.forEach(options, function ( opt ) {
							opt = angular.element(opt);
							if(opt.val() === val) {
								opt.attr('selected', 'selected');
							} else {
								opt.removeAttr('selected');
							}
						});
						
						element.triggerHandler('change');
					}
					
					scope.$watch('ngOptions', function ( /*old, nw, scope*/ ) {
						if(menu) {
							menu.$destroy();
						}
						menu = $(element[0]).selectmenu( {
							style: 'dropdown',
							//width: 100,
							//menuWidth: 150,
							change: onChange
						} );
						
					});
					
				}
			};
			
		}]);
	
})();
/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsSort', [ '$document', '$parse', function ( $document, $parse ) {
			
			var getMousePosition = fetch('nl.mintlab.utils.dom.getMousePosition'),
				indexOf = fetch('nl.mintlab.utils.shims.indexOf'),
				arrayMove = fetch('nl.mintlab.utils.collection.arrayMove'),
				cancelEvent = fetch('nl.mintlab.utils.events.cancelEvent');
			
			return {
				compile: function ( /*tElement, tAttrs, transclude*/ ) {
					
					return function link ( scope, element, attrs ) {
						
						var list,
							trackingEl;
						
						function track ( el ) {
							
							resetList();
							
							trackingEl = el;
							trackingEl.addClass('zs-sorting');
							
							$document.bind('dragover', onDragOver);
							$document.bind('drop', onDrop);
							$document.bind('mousemove', onMouseMove);
						}
						
						function untrack ( ) {
							
							resort();
							
							trackingEl.removeClass('zs-sorting');
							trackingEl = null;
							
							$document.unbind('dragover', onDragOver);
							$document.unbind('drop', onDrop);
							$document.unbind('mousemove', onMouseMove);	
						}
						
						function resort ( ) {
							var trackingScope = trackingEl.scope(),
								data = $parse(trackingEl.attr('data-ng-drag-data'))(trackingScope),
								array = $parse(attrs.zsSort)(scope),
								index,
								to,
								list = element[0].querySelectorAll('[data-zs-sortable]');
								
							index = indexOf(array, data);
							
							if(index !== -1) {
								to = indexOf(list, trackingEl[0]);
								arrayMove(array, data, to);
								scope.$emit('sort', data, index, to);
							}
							
						}
						
						function resetList ( ) {
							list = element[0].querySelectorAll('[data-zs-sortable]');
						}
						
						function onDragOver ( event ) {
							var pos = getMousePosition(event);
							positionTrackedElement(pos);
							cancelEvent(event);
						}
						
						function onDrop ( event ) {
							return cancelEvent(event);
						}
						
						function onMouseMove ( event ) {
							var pos = getMousePosition(event);
							positionTrackedElement(pos);
						}
						
						function positionTrackedElement ( pos ) {
							var rect,
								target,
								before,
								el,
								i,
								l;
								
							// TODO(dario): horizontal sort
							
							target = 0;
							
							for(i = 0, l = list.length; i < l; ++i) {
								el = list[i];
								rect = el.getBoundingClientRect();
								if(pos.y <= rect.top + rect.height/2) {
									target = i;
									break;
								}
							}
							
							if(pos.y > rect.top + rect.height/2) {
								before = null;
							} else {
								before = list[target];
							}
							
							
							if(before !== trackingEl[0]) {
								trackingEl[0].parentNode.insertBefore(trackingEl[0], before);
								resetList();
							}
							
							
						}
						
						scope.$on('startdrag', function ( event, el/*, mimetype*/ ) {
							if(el.attr('data-zs-sortable') !== undefined) {
								track(el);
							}
						});
						
						scope.$on('stopdrag', function ( event, el/*, mimetype*/ ) {
							if(trackingEl && trackingEl[0] === el[0]) {
								untrack();
							}
						});
						
					};
				}
			};
			
		}]);
	
})();
/*global angular,fetch*/
(function ( ) {
	
	var TEMPLATES = {
		'default': '/partials/directives/spot-enlighter/default.html',
		'contact': '/partials/directives/spot-enlighter/contact.html',
		'contact/medewerker': '/partials/directives/spot-enlighter/medewerker.html',
		'bag': '/partials/directives/spot-enlighter/address.html',
		'bag-street': '/partials/directives/spot-enlighter/street.html',
		'zaak': '/partials/directives/spot-enlighter/case.html'
	};
	
	var LABELS = {
		'default': undefined,
		'contact': 'voorletters + \' \' + geslachtsnaam',
		'contact/medewerker': 'naam'
	};
	
	angular.module('Zaaksysteem')
		.directive('zsSpotEnlighter', [ '$document', '$timeout', '$parse', 'templateCompiler', 'smartHttp', function ( $document, $timeout, $parse, templateCompiler, smartHttp) {
			
			var cancelEvent = fetch('nl.mintlab.utils.events.cancelEvent'),
				fromLocalToGlobal = fetch('nl.mintlab.utils.dom.fromLocalToGlobal'),
				getViewportSize = fetch('nl.mintlab.utils.dom.getViewportSize'),
				trim = fetch('nl.mintlab.utils.shims.trim'),
				isObject = angular.isObject,
				body = $document.find('body'),
				parent;
				
			function createMenu ( ) {
				
				parent = angular.element('<div></div>');
				parent.addClass('spot-enlighter-container');
				
				body.append(parent);
				
			}
			
			function moveToFront ( ) {
				body.append(parent);
			}
			
			createMenu();
			
			return {
				scope: false,
				require: 'ngModel',
				compile: function ( /*tElement, tAttrs, transclude*/) {
					
					return function link ( scope, element, attrs, ngModel ) {
						
						var menu,
							query,
							highlighted,
							loading,
							parent,
							loadingEl,
							timeoutCancel,
							expected,
							tagParent,
							tagEl,
							clearButton,
							interval = 125,
							isOpen = false;
							
							
						if(!element.parent().hasClass('spot-enlighter-wrapper')) {
							parent = angular.element('<div class="spot-enlighter-wrapper"></div>');
							element.after(parent);
							parent.append(element);
						} else {
							parent = element.parent();
						}
						
						loadingEl = angular.element('<div class="spot-enlighter-loading"></div>');
						element.after(loadingEl);
							
						tagParent = angular.element('<div class="spot-enlighter-tag-wrapper"></div>');
						tagParent.css('visibility', 'hidden');
						element.after(tagParent);
						
						tagEl = angular.element('<div class="spot-enlighter-tag" tabindex="0"></div>');
						tagParent.append(tagEl);
						
						clearButton = angular.element('<button type="button" class="spot-enlighter-clear">x</button>');
						tagParent.append(clearButton);
						
						function openMenu ( ) {
							
							if(isOpen) {
								return;
							}
							
							isOpen = true;
							
							if(!scope.$$phase && !scope.$root.$$phase) {
								scope.$apply(getTemplate);
							} else {
								getTemplate();
							}
							
						}
						
						function hideMenu ( ) {
							
							if(!isOpen) {
								return;
							}
							
							isOpen = false;
							
							unhighlight();
							setData([]);
							
							parent.removeClass('spot-enlighter-visible');
							if(menu) {
								menu.unbind('mousedown', onMenuMouseDown);
								menu.unbind('click', onMenuClick);
								menu.remove();
								menu = undefined;
							}
						}
						
						function getTemplate ( ) {
							
							var url = getTemplateUrl();
							
							templateCompiler.getCompiler(url).then(function ( compiler ) {
								
								if(element[0] !== $document[0].activeElement) {
									menu = undefined;
									isOpen = false;
									return;
								}
								
								moveToFront();
								
								compiler(scope, function ( clonedElement/*, scope*/ ) {
									menu = clonedElement;
									menu.bind('mousedown', onMenuMouseDown);
									menu.bind('click', onMenuClick);
									
									parent.append(menu);
									parent.addClass('spot-enlighter-visible');
									
									if(query) {
										getData();	
									}
									
									positionMenu();
								});
							});
						}
						
						function getTemplateUrl ( ) {
							var url;
							if(attrs.zsSpotEnlighterTemplate) {
								url = scope.$eval(attrs.zsSpotEnlighterTemplate);
							} else {
								url = TEMPLATES[scope.$eval(attrs.zsSpotEnlighterRestrict)] || TEMPLATES['default'];
							}
							return url;
						}
						
						function getObjectType ( ) {
							return scope.$eval(attrs.zsSpotEnlighterRestrict);
						}
						
						function getLabel ( obj ) {
							var toParse = attrs.zsSpotEnlighterLabel || LABELS[getObjectType()] || LABELS['default'],
								label = $parse(toParse)(obj);
							
							return label;
						}
						
						function positionMenu ( ) {
							var pos,
								viewportSize,
								elWidth,
								elHeight,
								menuWidth,
								menuHeight,
								vOrient,
								hOrient,
								x,
								y;
								
							if(!menu) {
								return;
							}
							
							viewportSize = getViewportSize();
							elWidth = element[0].clientWidth;
							elHeight = element[0].clientHeight;
							
							pos = fromLocalToGlobal(element[0], { x: 0, y: 0 });
							
							x = pos.x;
							
							menu.css('width', elWidth + 'px');
							
							menuWidth = menu[0].clientWidth;
							menuHeight = menu[0].clientHeight;
							
							if(pos.y + elHeight + menuHeight > viewportSize.height) {
								vOrient = 'top';
								y = pos.y - menuHeight;
							} else {
								vOrient = 'bottom';
								y = pos.y + elHeight;
							}
							
							if(pos.x + menuWidth > viewportSize.width) {
								x = Math.min(viewportSize.width, pos.x + elWidth) - menuWidth;
								hOrient = 'left';
							} else {
								x = pos.x;
								hOrient = 'right';
							}
							
							menu.css('top', y + 'px');
							menu.css('left', x + 'px');
							menu.attr('data-zs-spot-enlighter-horizontal-orientation', hOrient);
							menu.attr('data-zs-spot-enlighter-vertical-orientation', vOrient);
							
							scope.reversed = vOrient === 'top';
							
						}
						
						function invalidate ( ) {
							if(!loading) {
								if(timeoutCancel) {
									$timeout.cancel(timeoutCancel);
								}
								timeoutCancel = $timeout(function ( ) {
									timeoutCancel = null;
									getData();
								}, interval);
							}
						}
						
						function getData ( ) {
							var loadedQuery,
								params,
								objType = getObjectType(),
								postfix = '';
							
							if(!query) {				
								return;
							}
							
							loadedQuery = query;
							loading = true;
							
							params = {
								query: query
							};
							
							if(objType) {
								if(objType === 'contact' || objType === 'zaak') {
									params.object_type = objType;
								} else {
									postfix = '/' + objType;
								}
							}
							
							setLoader();
							
							smartHttp.connect( {
								url: 'objectsearch' + postfix,
								method: 'GET',
								params: params,
								// backend needs this to "know" it's a JSON request
								headers: {
									'X-Requested-With': 'XMLHttpRequest'
								}
							})
								.success(handleData)
								.error(handleError)
								.then(function ( ) {
									handleEnd(loadedQuery);
								}, function ( ) {
									handleEnd(loadedQuery);
								});
								
						}
						
						function handleData ( data, status, headers, config ) {
							var entries = data.json.entries,
								currentQuery = query,
								performedQuery = config.params.query;
								
							if(currentQuery === performedQuery) {
								setData(entries);
							}
						}
						
						function handleError ( ) {
							
						}
						
						function handleEnd ( loadedQuery ) {
							loading = false;
							if(query !== loadedQuery) {
								invalidate();
							}
							setLoader();
						}
						
						function setData ( entries ) {
							function updateEntries ( ) {
								scope.entries = entries;
								$timeout(function ( ) {
									positionMenu();
								});
							}
							
							if(!scope.$$phase && !scope.$root.$$phase) {
								scope.$apply(updateEntries);
							} else {
								updateEntries();
							}
						}
									
						function setQuery ( q ) {
							q = trim(q);
							if(query !== q) {
								if(!menu) {
									openMenu();
								}
								query = q;
								if(!query) {
									setData([]);
								}
								invalidate();
							}
						}
						
						function selectCurrent ( ) {
							var data;
							if(highlighted) {
								data = highlighted.scope().entry;
							}
							unhighlight();
							selectEntry(data);
							hideMenu();
						}
						
						function highlightNext ( ) {
							var next,
								nextSibling;
							
							if(highlighted) {
								nextSibling = angular.element(highlighted[0].nextSibling);
								while(nextSibling.length && (!nextSibling.scope() || nextSibling.scope().entry === undefined)) {
									nextSibling = angular.element(nextSibling[0].nextSibling);
								}
								next = nextSibling;
							}
							
							if(!next || !next.length) {
								next = menu.find('li').eq(0);
							}
							
							highlight(next);
						}
					
						function highlightPrev ( ) {
							var prev,
								prevSibling,
								list;
							
							if(highlighted) {
								prevSibling = angular.element(highlighted[0].previousSibling);
								while(prevSibling.length && (!prevSibling.scope() || prevSibling.scope().entry === undefined)) {
									prevSibling = angular.element(prevSibling[0].previousSibling);
								}
								prev = prevSibling;
							}
							
							if(!prev || !prev.length) {
								list = menu.find('li');
								prev = list.eq(list.length-1);
							}
							
							highlight(prev);
						}
						
						function highlight ( el ) {
				
							unhighlight();
							
							if(!el.length) {
								return;
							}
							
							el.addClass('spot-enlighter-entry-highlight');
							highlighted = el;
						}
						
						function unhighlight ( ) {
							if(highlighted) {
								highlighted.removeClass('spot-enlighter-entry-highlight');
								highlighted = null;
							}
						}
						
						function onChange ( event ) {
							
							var val;
							
							if(event.keyCode === 13 || event.keyCode === 9) {
								return cancelEvent(event, true);
							}
							
							val = element.val();
							
							setQuery(val);
							
							if(event.keyCode) {
								switch(event.keyCode) {
									
									case 27:
									if(menu) {
										hideMenu();
										return cancelEvent(event, true);
									}
									break;
									
									case 38:
									highlightPrev();
									break;
									
									case 40:
									highlightNext();
									break;
								}
							}
						}
						
						function onKeyDown ( event ) {
							switch(event.keyCode) {
								case 13: case 9:
								if(highlighted) {
									selectCurrent();
									tagEl[0].focus();
									return cancelEvent(event, true);
								}
								break;
								
								case 8:
								var getter = $parse(attrs.ngModel),
									val = getter(scope);
									
								if(val && (typeof val !== 'string' && typeof val !== 'number')) {
									selectEntry(null);
								}
								break;
							}
						}
						
						function onMenuMouseDown ( /*event*/ ) {
							element.unbind('blur', onBlur);
							menu.bind('mouseup', function onMenuMouseUp ( /*event*/ ) {
								menu.unbind('mouseup', onMenuMouseUp);
								// hideMenu();
								element.bind('blur', onBlur);
							});
						}
						
						function onMenuClick ( event ) {
							var el = angular.element(event.target),
								elScope = el.scope();
								
							if(elScope && elScope.entry) {
								selectEntry(elScope.entry);
								hideMenu();
							}
						}
						
						function onFocus ( /*event*/ ) {
							element.val('');
							selectEntry(null);
							openMenu();
						}
						
						function onBlur ( /*event*/ ) {
							hideMenu();
						}
						
						function fromUser ( val ) {
							// make sure user changes never result in model changes
							return val === '' ? ngModel.$modelValue : undefined;
						}
						
						function toUser ( obj ) {
							var output;
							
							setValidity();
							element.val(obj ? getLabel(obj) : '');
							
							output = obj ? getLabel(obj) : '';
							return output;
						}
						
						function selectEntry ( entry ) {
							
							var obj = entry ? entry.object : null;
							
							scope.$apply(function ( ) {
								var acc = $parse(attrs.ngModel);
								
								acc.assign(scope, obj);
								
								setQuery('');
								
								expected = obj ? getLabel(obj) : '';
								
								setValidity();
								
								// render isn't called in IE8/9 after enter/tab select & model change
								ngModel.$render();
								
							});
						}
						
						function getModelData ( ) {
							return $parse(attrs.ngModel)(scope);
						}
						
						function setValidity ( ) {
							var data = getModelData(),
								isRequired = attrs.zsObjectRequired && scope.$eval(attrs.zsObjectRequired);
								
							ngModel.$setValidity('zs-object', (data && isObject(data)) || !isRequired);
						}
						
						function onTagKeyDown ( event ) {
							if(event.keyCode === 8 || event.keyCode === 46) {
								selectEntry(null);
								element[0].focus();
								return cancelEvent(event, true);
							}
						}
						
						function onClearClick ( /*event*/ ) {
							selectEntry(null);
						}
						
						function setLoader ( ) {
							loadingEl.css('visibility', loading ? 'visible' : 'hidden');
						}
						
						setLoader();
						
						scope.$on('$destroy', function ( ) {
							hideMenu();
						});
						
						element.bind('focus', onFocus);
						element.bind('blur', onBlur);
						element.bind('change paste keyup input', onChange);
						element.bind('keydown', onKeyDown);
						tagEl.bind('keydown', onTagKeyDown);
						clearButton.bind('click', onClearClick);
						
						ngModel.$formatters.push(toUser);
						ngModel.$parsers.push(fromUser);
						
						ngModel.$render = function ( ) {
							var data = getModelData(),
								val = data ? getLabel(data) : '';
								
							tagEl.text(val);
							tagParent.css('visibility', val ? 'visible' : 'hidden');
							if(val) {
								element.attr('disabled', 'disabled');
							} else {
								element.removeAttr('disabled');
							}
						};
						
						attrs.$observe('zsObjectRequired', function ( ) {
							setValidity();
						});
						
						attrs.$observe('zsSpotEnlighterLabel', function ( ) {
							ngModel.$render();
						});
												
					};
					
				}
			};
			
		}]);

})();
/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsStyle', [ function ( ) {
			
			return {
				compile: function ( /*tElement, tAttrs, transclude*/ ) {
					
					return function link ( scope, element, attrs ) {
						
						attrs.$observe('zsStyle', function ( ) {
							setStyle(element);
						});
						
						function setStyle( ) {
							element.attr('style', attrs.zsStyle);
						}
						
						setStyle();
						
					};
				}
			};
			
		}]);
	
})();
/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsTemplate', [ '$parse', '$compile', function ( $parse, $compile ) {
			
			return {
				compile: function ( /*tElement, tAttrs, transclude*/ ) {
					
					return function link ( scope, element, attrs ) {
						
						var templateString = $parse(attrs.zsTemplate)(scope),
							holder = angular.element('<div></div>'),
							childNodes;
						
						holder[0].innerHTML = templateString;
						childNodes = holder[0].childNodes;
						
						element.append($compile(childNodes)(scope));
						
					};
				}
				
			};
			
		}]);
	
})();
/*global angular,window,fetch*/
(function ( ) {
	
	var DEFAULT_TIMEOUT = 10000,
		INTERVAL = 50;
	
	angular.module('Zaaksysteem')
		.directive('zsTimer', [ '$timeout', function ( $timeout ) {
			
			var setInterval = window.setInterval,
				clearInterval = window.clearInterval,
				safeApply = fetch('nl.mintlab.utils.safeApply'),
				getTime;
				
			if(typeof Date.now !== 'undefined') {
				getTime = function ( ){
					return Date.now();
				};
			} else {
				getTime = function ( ) {
					return +(new Date());
				};
			}
				
			
			return {
				scope: true,
				compile: function ( /*tElement, tAttrs, transclude*/ ) {
					
					return function link ( scope, element, attrs ) {
						
						var timeout = scope.$eval(attrs.zsTimer),
							timeoutCancel,
							intervalId,
							started,
							timeLeft;
							
						if(timeout === undefined) {
							timeout = DEFAULT_TIMEOUT;
						}
						
						timeout = parseInt(timeout, 10);
							
						if(timeout === 0) {
							return;
						}
						
						function clearOut ( ) {
							if(timeoutCancel) {
								$timeout.cancel(timeoutCancel);
								timeoutCancel = null;
							}
							if(intervalId) {
								clearInterval(intervalId);
								intervalId = null;
							}
						}
						
						function setTimeLeft ( ) {
							safeApply(scope, function ( ) {
								timeLeft = Math.max(0, timeout - (getTime() - started));
							});
						}
						
						scope.zsTimerLeft = function ( ) {
							return timeLeft;
						};
						
						scope.zsTimerStop = function ( ) {
							clearOut();
							timeLeft = 0;
						};
						
						intervalId = setInterval(function onInterval ( ) {
							setTimeLeft();
						}, INTERVAL);
						
						started = getTime();
						setTimeLeft();
						
						timeoutCancel = $timeout(function onTimeout ( ) {
							scope.$emit('zsTimerComplete');
							clearOut();
							timeLeft = 0;
						}, timeout, false);
						
						scope.$on('$destroy', function onDestroy ( ) {
							clearOut();
						});
						
					};
					
				}
			};
			
		}]);
	
})();
/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsTitle', [ '$document', '$timeout', 'templateCompiler', function ( $document, $timeout, templateCompiler ) {
		
		var setMouseEnabled = fetch('nl.mintlab.utils.dom.setMouseEnabled'),
			fromLocalToGlobal = fetch('nl.mintlab.utils.dom.fromLocalToGlobal'),
			getViewportSize = fetch('nl.mintlab.utils.dom.getViewportSize'),
			tpl = '/partials/directives/tooltip/tooltip.html',
			body = $document.find('body'),
			tooltip,
			span,
			arrow,
			origin;
			
		function createTooltip ( element ) {
			tooltip = angular.element(element);
			tooltip.css('position', 'fixed');
			span = tooltip.find('span').eq(0);
			arrow = angular.element(tooltip[0].querySelector('.title-tooltip-arrow')),
			setMouseEnabled(tooltip[0], false);
			body.append(element);
			hideTooltip();
		}
		
		function setPosition ( ) {
			var viewportWidth = getViewportSize().width,
				elementWidth = origin[0].clientWidth,
				elementHeight = origin[0].clientHeight,
				tooltipWidth = tooltip[0].clientWidth,
				tooltipHeight = tooltip[0].clientHeight,
				arrowWidth = arrow[0].clientWidth,
				topLeft = fromLocalToGlobal(origin[0], { x: 0, y: 0 }),
				rect = { x: topLeft.x, y: topLeft.y, width: elementWidth, height: elementHeight },
				registration,
				offsetY = 0,
				x = 0,
				y = 0,
				arrowX,
				d;
				
			if(rect.y - offsetY - tooltipHeight >= 0) {
				registration = 'top';
			} else {
				registration = 'bottom';	
			}
			
			tooltip.attr('data-zs-title-registration', registration);
			
			switch(registration) {
				case 'top':
				y = rect.y - tooltipHeight;
				break;
				
				case 'bottom':
				y = rect.y + rect.height/2 - tooltipHeight/2;
				break;
			}
			
			x = rect.x + rect.width/2 - tooltipWidth/2;
			arrowX = tooltipWidth/2 - arrowWidth/2;
			
			d = x - 0;
			if(d < 0 ) {
				x -= d;
				arrowX += d;
			}
			
			d = x + tooltipWidth - viewportWidth;
			if(d > 0) {
				x -= d;
				arrowX += d;
			}
			
			tooltip.css('left', x + 'px');
			tooltip.css('top', y + 'px');
			arrow.css('left', arrowX + 'px');
			
		}
				
		function showTooltip ( ) {
			tooltip.addClass('tooltip-visible');
			tooltip.removeClass('tooltip-hidden');
		}
		
		function hideTooltip ( ) {
			tooltip.removeClass('tooltip-visible');
			tooltip.addClass('tooltip-hidden');
		}
		
		function setOrigin ( element ) {
			origin = element;
			if(origin) {
				setPosition();
			}
		}
			
		templateCompiler.getElement(tpl).then(createTooltip);
		
		
		return {
			scope: true,
			link: function ( scope, element, attrs ) {
				
				var label;
				
				function onMouseOver ( ) {
					label = attrs.zsTitle;
					
					if(!label) {
						setOrigin(null);
						return;
					}
					
					span.text(label);
					
					setOrigin(element);
					showTooltip();
				}
				
				function onMouseOut ( ) {
					hideTooltip();
				}
				
				element.bind('mouseover', onMouseOver);
				element.bind('mouseout', onMouseOut);
				
				var destroyUnbind = scope.$on('$destroy', function ( ) {
					destroyUnbind();
					hideTooltip();
				});
				
			}
		};
		
	}]);
	
})();
/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.locale')
		.directive('script', [ 'translationService', function ( translationService ) {
			
			return {
				restrict: 'E',
				scope: true,
				terminal: true,
				compile: function ( /*tElement, tAttrs, transclude*/ ) {
					
					return function link ( scope, element, attrs ) {
						
						if(attrs.type === 'text/zs-translation-data') {
							var data;
						
							try {
								data = JSON.parse(element[0].innerHTML);
							} catch ( error ) {
								console.log('Error parsing translation data: ' + error);
							}
							
							for(var key in data) {
								translationService.set(key, data[key]);
							}
						}
					};
				}
			};
		}]);
})();
/*global define*/
(function ( ) {
	
	define('nl.mintlab.utils.dom.contains', function ( ) {
		
		return function ( parent, element ) {
			while(element) {
				element = element.parentNode;
				if(element === parent) {
					return true;
				}
			}
			return false;
		};
	});
	
})();
/*global fetch,define*/
(function ( ) {
	
	define('nl.mintlab.utils.dom.fromGlobalToLocal', function ( ) {
		
		var getViewportPosition = fetch('nl.mintlab.utils.dom.getViewportPosition');
		
		return function ( element, point ) {
			var docPos = getViewportPosition(element),
				x = point.x - docPos.x,
				y = point.y - docPos.y;
				
			return { x: x, y: y };
		};
	});
		
})();
/*global define,fetch*/
(function ( ) {
	
	define('nl.mintlab.utils.dom.fromLocalToGlobal', function ( ) {
		
		var getViewportPosition = fetch('nl.mintlab.utils.dom.getViewportPosition');
		
		return function ( element, point ) {
			var docPos = getViewportPosition(element),
				x = docPos.x + point.x,
				y = docPos.y + point.y;
				
			return { x: x, y: y };
		};
		
	});
	
})();
/*global define,fetch,window,document*/
(function ( ) {
	
	define('nl.mintlab.utils.dom.getDocumentPosition', function ( ) {
		
		var win = window,
			doc = document,
			docEl = document.documentElement,
			body = document.body,
			scrollX,
			scrollY;
		
		if("pageXOffset" in win) {
			scrollX = function ( ) {
				return win.pageXOffset;
			};
			scrollY = function ( ) {
				return win.pageYOffset;
			};
		} else if(doc.doctype ) {
			scrollX = function ( ) {
				return body.scrollLeft;
			};
			scrollY = function ( ) {
				return body.scrollTop;
			};
		} else {
			scrollX = function ( ) {
				return docEl.scrollLeft;
			};
			scrollY = function ( ) {
				return docEl.scrollTop;
			};
		}
		
		var getViewportPosition = fetch('nl.mintlab.utils.dom.getViewportPosition');
		
		return function ( element ) {
			var offset = getViewportPosition(element);
			return { x: offset.x + scrollX(), y: offset.y + scrollY() };
		};
	});
})();
/*global define,document,fetch*/
(function ( ) {
	
	define('nl.mintlab.utils.dom.getMousePosition', function ( ) {
		
		var body = document.body,
			fromLocalToGlobal = fetch('nl.mintlab.utils.dom.fromLocalToGlobal');
		
		return function ( event ) {
			var x,
				y,
				pos;
			
			if(event.pageX !== undefined) {
				pos = fromLocalToGlobal(body, { x: event.pageX, y: event.pageY } );
				x = pos.x;
				y = pos.y;
			} else if(event.clientX) {
				x = event.clientX;
				y = event.clientY;
			}
			
			return { x: x, y: y };
		};
	});
	
})();
/*global define*/
(function ( ) {
	
	define('nl.mintlab.utils.dom.getParents', function ( ) {
		
		return function ( element ) {
			var parents = [];
			while(element) {
				parents.push(element.parentElement);
				element = element.parentElement;
			}
			
			return parents;
		};
		
	});
	
})();
/*global define*/
(function ( ) {
	
	define('nl.mintlab.utils.dom.getViewportPosition', function ( ) {
		
		return function ( element ) {
			var rect;
			if(!element) {
				return { x: 0, y: 0 };
			}
			rect = element.getBoundingClientRect();
			return { x: rect.left, y: rect.top };
		};
	});
	
})();
/*global define,window,document*/
(function ( ) {
	
	define('nl.mintlab.utils.dom.getViewportSize', function ( ) {
		
		var win = window,
			docEl = document.documentElement;
		
		if('innerHeight' in win) {
			return function ( ) {
				return { width: win.innerWidth, height: win.innerHeight };
			};
		} else {
			return function ( ) {
				return { width: docEl.clientWidth, height: docEl.clientHeight };
			};
		}
		
	});
	
})();
/*global define,window,document*/
(function ( ) {
	
	define('nl.mintlab.utils.dom.getWindowHeight', function ( ) {
		
		var win = window,
			doc = document.documentElement,
			body = document.body,
			func;
		
		if("innerHeight" in win) {
			func = function ( ) {
				return win.innerHeight;
			};
		} else {
			func = function ( ) {
				return Math.min(doc.clientHeight, body.clientHeight);
			};
		}
		
		return func;
		
	});
	
})();
/*global define, fetch*/
(function ( ) {
	
	define('nl.mintlab.utils.dom.intersects', function ( ) {
		
		var getDocumentPosition = fetch('nl.mintlab.utils.dom.getDocumentPosition');
		
		return function ( element, point ) {
			
			var origin = getDocumentPosition(element),
				rect = { x: origin.x, y: origin.y, width: element.clientWidth, height: element.clientHeight };
			
			return point.x >= rect.x && point.x <= rect.x + rect.width && point.y >= rect.y && point.y <= rect.y + rect.height;
			
		};
	});
	
})();
/*global define,fetch,window,document*/
(function ( ) {
	
	define('nl.mintlab.utils.dom.setMouseEnabled', function ( ) {
		
		// from https://github.com/ausi/Feature-detection-technique-for-pointer-events/
		var	doc = document,
			body = doc.body,
			element = doc.createElement('x'),
			documentElement = doc.documentElement,
			getComputedStyle = window.getComputedStyle,
			supports;
			
		if(!('pointerEvents' in element.style)){
			supports = false;
		} else {
			element.style.pointerEvents = 'auto';
			element.style.pointerEvents = 'x';
			documentElement.appendChild(element);
			supports = getComputedStyle && getComputedStyle(element, '').pointerEvents === 'auto';
			documentElement.removeChild(element);
		}
		
		if(supports) {
			return function ( target, isEnabled ) {
				target.style['pointer-events'] = isEnabled ? 'auto' : 'none';
			};
		} else {
			
			return (function ( ) {
				
				var indexOf = fetch('nl.mintlab.utils.shims.indexOf'),
					addEventListener = fetch('nl.mintlab.utils.events.addEventListener'),
					removeEventListener = fetch('nl.mintlab.utils.events.removeEventListener'),
					dispatchEvent = fetch('nl.mintlab.utils.events.dispatchEvent'),
					fromGlobalToLocal = fetch('nl.mintlab.utils.dom.fromGlobalToLocal'),
					getMousePosition = fetch('nl.mintlab.utils.dom.getMousePosition'),
					getElementFromPoint = document.elementFromPoint,
					listeningTo = [];
				
				function listen ( target ) {
					var index = indexOf(listeningTo, target);
					if(index === -1) {
						listeningTo.push(target);
						addEventListener(target, 'click', onTargetMouseEvent);
						addEventListener(target, 'mouseover', onTargetMouseEvent);
						addEventListener(target, 'mouseout', onTargetMouseEvent);
					}
				}
				
				function unlisten ( target ) {
					var index = indexOf(listeningTo, target);
					if(index !== -1) {
						listeningTo.splice(index, 1);
						removeEventListener(target, 'click', onTargetMouseEvent);
						removeEventListener(target, 'mouseover', onTargetMouseEvent);
						removeEventListener(target, 'mouseout', onTargetMouseEvent);
					}
				}
				
				function onTargetMouseEvent ( event ) {
					
					var target = event.currentTarget,
						pos = fromGlobalToLocal(body, getMousePosition(event)),
						clonedEvent = doc.createEvent('MouseEvents'),
						hidden = [],
						el;
					
					/*TODO(dario): properly init mouse event*/
					clonedEvent.initMouseEvent(event.type);
					
					for(var key in event) {
						clonedEvent[key] = event[key];
					}
					
					while(indexOf(listeningTo, target) !== -1) {
						hidden.push( { element: target, display: target.style.display });
						target.style.display = 'none';
						target = getElementFromPoint(pos);
					}
					
					if(target) {
						dispatchEvent(target, clonedEvent);
					}
					
					for(var i = 0, l = hidden.length; i < l; ++i) {
						el = hidden[i].element;
						el.style.display = hidden[i].display;
					}
					
				}
				
				return function ( target, isEnabled ) {
					if(isEnabled) {
						listen(target);
					} else {
						unlisten(target);
					}
				};
				
			})();
		}
	});
	
})();
/*global define, window*/
(function ( ) {
	
	var win = window;
	
	define('nl.mintlab.utils.events.addEventListener', function ( ) {
		
		if(win.addEventListener) {
			return function ( dispatcher, type, listener, useCapture ) {
				dispatcher.addEventListener(type, listener, useCapture);
			};
		} else if(win.attachEvent) {
			return function ( dispatcher, type, listener, useCapture ) {
				if(useCapture) {
					//console.log('useCapture not supported in this browser');
				}
				dispatcher.attachEvent('on' + type, listener);
			};
		}
		
		throw new Error('events not supported in this browser');
		
	});
})();
/*global define, window*/
(function ( ) {
	
	define('nl.mintlab.utils.events.cancelEvent', function ( ) {
		
		var win = window,
			event = win.document.createEvent ? win.document.createEvent('Event') : win.document.createEventObject();
			
		if(event.stopPropagation) {
			return function ( event, preventDefault ) {
				if(preventDefault === undefined) {
					preventDefault = true;
				}
				event.stopPropagation();
				event.stopImmediatePropagation();
				if(preventDefault) {
					event.preventDefault();
				}
				return false;
			};
		} else {
			return function ( event, preventDefault) {
				if(preventDefault === undefined) {
					preventDefault = true;
				}
				event.returnValue = !preventDefault;
				event.cancelBubble = true;
				return false;
			};
		}
		
	});
	
})();
/*global define,document*/
(function ( ) {
	
	define('nl.mintlab.utils.events.dispatchEvent', function ( ) {
		var doc = document;
		
		if(doc.dispatchEvent) {
			return function ( element, event ) {
				return element.dispatchEvent(event);
			};
		} else if(doc.fireEvent) {
			return function ( element, event ) {
				return element.fireEvent(event);	
			};
		} else {
			console.log('dispatching events not supported in this browser');
		}
		
	});
	
})();
/*global define,window*/
(function ( ) {
	
	var win = window;
	
	define('nl.mintlab.utils.events.removeEventListener', function ( ) {
		
		if(win.removeEventListener) {
			return function ( dispatcher, type, listener, useCapture ) {
				dispatcher.removeEventListener(type, listener, useCapture);
			};
		} else if(win.attachEvent) {
			return function ( dispatcher, type, listener, useCapture ) {
				dispatcher.detachEvent(type, listener);
			};
		}
		
		throw new Error('Events not supported in this browser');
	});
})();
/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.filter('capitalize', function ( ) {
			return function ( from ) {
				return from ? from [0].toUpperCase() + from.substr(1) : '';
			};
		});
})();
/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.factory('sizeFilter', [ '$filter', function ( $filter ) {
			
			var numberFilter = $filter('number');
			
			return function ( from, fractionSize ) {
				var num, append;
				from = Number(from);
				if(from < 1024) {
					num = from;
					append = 'bytes';
				} else if(from < 1024 * 1024) {
					num = from/1024;
					append = 'KB';
				} else if(from < 1024 * 1024) {
					num = from/1024/1024;
					append = 'MB';
				}
				return numberFilter(num, fractionSize) + ' ' + append;
			};
			
			
		}]);
	
})();
/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.filter('snakeToCamelCase', function ( ) {
			
			function convert ( string ) {
				return string[1].toUpperCase();
			}
			
			return function ( source ) {
				return source.replace(/(_([a-z]))/g, convert);
			};
		});
	
	
})();
/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.locale')
		.filter('translate', [ 'translationService', function ( translationService ) {
			
			return function ( /*from*/  ) {
				return translationService.get.apply(translationService, arguments);
			};
			
		}]);
	
})();
/*global define*/
(function ( ) {
	
	define('nl.mintlab.utils.generateUid', function ( ) {
		
		function generateUid ( prefix ) {
			var uid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
				var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
				return v.toString(16);
			});
			if(prefix !== undefined) {
				uid = prefix + uid;
			}
			return uid;
		}
		
		return function ( prefix ) {
			return generateUid(prefix);
		};
	});
	
})();
/*global define*/
(function ( ) {
	
	define('nl.mintlab.utils.object.clone', function ( ) {
		
		return function clone ( obj, deep ) {
			
			var cl = new obj.constructor(),
				key,
				val;
			
			if(deep) {
				for(key in obj) {
					cl[key] = obj[key];
				}
			} else {
				for(key in obj) {
					val = obj[key];
					if(!(val === null || val === undefined || typeof(val) !== 'object')) {
						val = clone(obj[key]);
					}
					cl[key] = val;
				}
			}
			
			return cl;
			
		};
	});
	
})();
/*global define*/
(function ( ) {
	
	define('nl.mintlab.utils.object.extendProto', function ( ) {
		return function ( D, S ) {
			var protoSource = S.prototype,
				protoDest = D.prototype,
				key;
			
			for(key in protoSource) {
				protoDest[key]= protoSource[key];
			}
		};
	});
	
})();
(function ( ) {
	
	// Holy Grail inherit function, via Stoyan Stefanov's JavaScript Patterns
	
	define('nl.mintlab.utils.object.inherit', function ( ) {
		return function ( C, P ) {
			var F = function (){};
			F.prototype = P.prototype;
			C.prototype = new F();
			C.uber = P.prototype;
			C.prototype.constructor = C;
		};
	});
	
})();
/*global angular,define*/
(function ( ) {
	
	define('nl.mintlab.utils.safeApply', function ( ) {
			
			var noop = angular.noop;
			
			return function ( scope, func ) {
				if(!func) {
					func = noop;
				}
								
				if(scope.$$phase || scope.$root.$$phase) {
					func();
				} else {
					scope.$apply(func);
				}
				
			};
		});
		
})();
/*global define,angular*/
(function ( ) {
	
	define('nl.mintlab.utils.shims.indexOf', function ( ) {
		
		var func,
			isArray = angular.isArray;
		
		if(Array.prototype.indexOf !== undefined) {
			func = function ( array, searchElement, fromIndex ) {
				if(isArray(array)) {
					return array.indexOf(searchElement, fromIndex);
				} else {
					return Array.prototype.indexOf.call(array, searchElement, fromIndex);
				}
			};
		} else {
			func = function (array, searchElement /*, fromIndex */ ) {
				var t = array;
				var len = t.length >>> 0;
				if (len === 0) {
					return -1;
				}
				var n = 0;
				if (arguments.length > 1) {
					n = Number(arguments[1]);
					if (n != n) { // shortcut for verifying if it's NaN
						n = 0;
					} else if (n != 0 && n != Infinity && n != -Infinity) {
						n = (n > 0 || -1) * Math.floor(Math.abs(n));
					}
				}
				if (n >= len) {
					return -1;
				}
				var k = n >= 0 ? n : Math.max(len - Math.abs(n), 0);
				for (; k < len; k++) {
					if (k in t && t[k] === searchElement) {
						return k;
					}
				}
				return -1;
			};
		}
		
		return func;
		
	});
	
})();
/*global define*/
(function ( ) {
	
	define('nl.mintlab.utils.shims.trim', function ( ) {
		
		if(String.prototype.trim !== undefined) {
			return function ( string ) {
				return string ? string.trim() : null;
			};
		} else {
			return function ( string ) {
				return string ? string.replace(/^\s+|\s+$/g,'') : null;
			};
		}
		
	});
	
})();
/*global angular,fetch,_*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsSelectableList', [ '$document', '$parse', function ( $document, $parse ) {
			
			var indexOf = fetch('nl.mintlab.utils.shims.indexOf'),
				cancelEvent = fetch('nl.mintlab.utils.events.cancelEvent'),
				difference = _.difference,
				forEach = _.forEach,
				doc = $document[0];
			
			return {
				compile: function ( /*tElement, tAttrs, transclude*/ ) {
					
					return function link ( scope, element, attrs ) {
						
						var selection = [],
							selectables = [],
							firstSelected;
							
						if(scope.$eval(attrs.zsSelectableList) === false) {
							return;
						}
						
						function addSelectable ( el ) {
							selectables.push(el[0]);
							el.attr('tabindex', '0');
							el.bind('click', onClick);
							el.bind('contextmenu', onClick);
						}
						
						function removeSelectable ( el ) {
							selectables.splice(indexOf(selectables, el[0]), 1);
							el.removeAttr('tabindex');
							el.unbind('click', onClick);
							el.unbind('contextmenu', onClick);
						}
						
						function onKeyUp ( event ) {
							var keyCode = event.keyCode,
								shiftKey = event.shiftKey,
								ctrlKey = event.ctrlKey,
								list = element[0].querySelectorAll('[data-zs-selectable]'),
								index = indexOf(list, doc.activeElement);
								
							if(index === -1) {
								return;
							}
							
							switch(keyCode) {
								case 38:
								if(shiftKey) {
									index = Math.max(0, index-1);
									if(list[index]) {
										selectUpto(list[index]);
									}
								} else {
									index--;
									if(index < 0) {
										index = list.length-1;
									}
									if(ctrlKey) {
										list[index].focus();
									} else {
										selectSingle(list[index]);
									}
								}
								break;
								
								case 40:
								if(shiftKey) {
									index = Math.min(list.length-1, index + 1);
									if(list[index]) {
										selectUpto(list[index]);
									}
								} else {
									index++;
									if(index === list.length) {
										index = 0;
									}
									if(ctrlKey) {
										list[index].focus();
									} else {
										selectSingle(list[index]);
									}
								}
								break;
								
								case 32:
								toggle(doc.activeElement);
								break;
								
								case 27:
								selectNone();
								break;
							}
							
							
						}
						
						function onKeyDown ( event ) {
							var keyCode = event.keyCode,
								index = indexOf(selectables, doc.activeElement);
								
							if(index !== -1 && (keyCode === 38 || keyCode === 40 || keyCode === 32)) {
								return cancelEvent(event, true);
							}
						}
						
						function onClick ( event ) {
							var el = event.target,
								shiftKey = event.shiftKey,
								ctrlKey = event.ctrlKey;
								
							if(event.type === 'contextmenu') {
								if(!isSelected(event.currentTarget)) {
									selectSingle(event.currentTarget);
								}
							} else if(!shiftKey) {
								if(	ctrlKey || 
									(el.tagName.toLowerCase() === 'input' && el.getAttribute('type') === 'checkbox') ||
									(selection.length === 1 && selection[0] === getChildData(event.currentTarget))
								) {
									toggle(event.currentTarget);
								} else {
									selectSingle(event.currentTarget);
								}
							} else {
								selectUpto(event.currentTarget);
							}
						}
						
						function selectSingle ( selectable ) {
							firstSelected = selectable;
							selectable.focus();
							emitSelect([ getChildData(selectable) ]);
						}
						
						function selectUpto ( selectable ) {
							var list = element[0].querySelectorAll('[data-zs-selectable]'),
								indexFrom,
								indexTo,
								from,
								to,
								toEmit = [];
								
							if(!firstSelected || firstSelected === selectable) {
								selectSingle(selectable);
							} else {
								indexFrom = indexOf(list, firstSelected);
								indexTo = indexOf(list, selectable);
								
								if(indexFrom > indexTo) {
									from = indexTo;
									to = indexFrom;
								} else {
									from = indexFrom;
									to = indexTo;
								}
								
								selectable.focus();
								forEach(Array.prototype.slice.call(list, from,to+1), function ( value/*, key*/ ) {
									var data = getChildData(value);
									toEmit.push(data);
								});
								
								emitSelect(toEmit);
							}
							
						}
						
						function toggle ( selectable ) {
							var data = getChildData(selectable),
								index = indexOf(selection, data),
								toEmit = selection.concat();
							if(index === -1) {
								toEmit.push(data);
							} else {
								toEmit.splice(index, 1);
							}
							emitSelect(toEmit);
						}
						
						function selectNone ( ) {
							firstSelected = null;
							emitSelect([]);
						}
						
						function emitSelect ( list ) {
							var toAdd,
								toRemove;
							
							toAdd = difference(list, selection);
							toRemove = difference(selection, list);
							
							selection.length = 0;
							selection.push.apply(selection, list);
							
							scope.$emit('zsSelectableList:change', selection, toAdd, toRemove);
						}
						
						function getChildData ( child ) {
							child = angular.element(child);
							return $parse(child.attr('data-zs-select-data'))(child.scope());
						}
						
						function findSelectables ( ) {
							var children = element[0].querySelectorAll('[data-zs-selectable]'),
								el,
								i,
								l,
								toRemove = [],
								toAdd = [];
								
							for(i = 0, l = selectables.length; i < l; i++) {
								el = selectables[i];
								if(indexOf(selectables, children[i]) === -1) {
									toRemove.push(el);
								} 
							}
							
							for(i = 0, l = children.length; i < l; ++i) {
								el = children[i];
								if(el.nodeType === 1 && selectables.indexOf(el) === -1) {
									toAdd.push(el);
								}
							}
							
							forEach(toRemove, function ( value/*, key*/ ) {
								removeSelectable(angular.element(value));
							});
							
							forEach(toAdd, function ( value/*, key*/ ) {
								addSelectable(angular.element(value));
							});
							
						}
						
						function isSelected ( selectable ) {
							var data = getChildData(selectable),
								index = indexOf(selection, data);
								
							return index !== -1;
						}
						
						element.bind('keyup', onKeyUp);
						element.bind('keydown', onKeyDown);
						
						scope.$watch(function ( ) {
							findSelectables();
						});
						
					};
				}
			};
			
		}]);
	
})();