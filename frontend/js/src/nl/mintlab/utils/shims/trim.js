/*global define*/
(function ( ) {
	
	define('nl.mintlab.utils.shims.trim', function ( ) {
		
		if(String.prototype.trim !== undefined) {
			return function ( string ) {
				return string ? string.trim() : null;
			};
		} else {
			return function ( string ) {
				return string ? string.replace(/^\s+|\s+$/g,'') : null;
			};
		}
		
	});
	
})();