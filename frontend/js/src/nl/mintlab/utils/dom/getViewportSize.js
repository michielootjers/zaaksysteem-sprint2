/*global define,window,document*/
(function ( ) {
	
	define('nl.mintlab.utils.dom.getViewportSize', function ( ) {
		
		var win = window,
			docEl = document.documentElement;
		
		if('innerHeight' in win) {
			return function ( ) {
				return { width: win.innerWidth, height: win.innerHeight };
			};
		} else {
			return function ( ) {
				return { width: docEl.clientWidth, height: docEl.clientHeight };
			};
		}
		
	});
	
})();