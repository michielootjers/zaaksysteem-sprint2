/*global define*/
(function ( ) {
	
	define('nl.mintlab.utils.dom.getParents', function ( ) {
		
		return function ( element ) {
			var parents = [];
			while(element) {
				parents.push(element.parentElement);
				element = element.parentElement;
			}
			
			return parents;
		};
		
	});
	
})();