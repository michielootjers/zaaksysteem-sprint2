/*global angular,define*/
(function ( ) {
	
	define('nl.mintlab.utils.safeApply', function ( ) {
			
			var noop = angular.noop;
			
			return function ( scope, func ) {
				if(!func) {
					func = noop;
				}
								
				if(scope.$$phase || scope.$root.$$phase) {
					func();
				} else {
					scope.$apply(func);
				}
				
			};
		});
		
})();