/*global define*/
(function ( ) {
	
	define('nl.mintlab.utils.generateUid', function ( ) {
		
		function generateUid ( prefix ) {
			var uid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
				var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
				return v.toString(16);
			});
			if(prefix !== undefined) {
				uid = prefix + uid;
			}
			return uid;
		}
		
		return function ( prefix ) {
			return generateUid(prefix);
		};
	});
	
})();