/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsInfiniteScroll', [ '$timeout', '$document', '$window', function ( $timeout, $document, $window ) {
			
			var getViewportPosition = fetch('nl.mintlab.utils.dom.getViewportPosition'),
				getWindowHeight = fetch('nl.mintlab.utils.dom.getWindowHeight');
			
			return {
				scope: {
					
				},
				link: function ( scope, element, attrs ) {
					
					var _timeoutInterval = 1000,
						_invalidateInterval = 250,
						_contained = attrs.zsInfiniteScroll === 'contained',
						_dispatching = false,
						_dispatchedScrollHeight = NaN;
					
					if(_contained) {
						element.bind('scroll', onScroll);
					} else {
						$document.bind('scroll', onScroll);
					}
					
					$timeout(onInterval, _timeoutInterval);
					
					checkBoundaries();
					
					scope.$on('scrollinvalidate', function ( ) {
						_dispatchedScrollHeight = NaN;
						checkBoundaries();
					});
										
					function onScroll ( ) {
						checkBoundaries();
					}
					
					function onInterval ( ) {
						$timeout(onInterval, _timeoutInterval, false);
						checkBoundaries();
					}
					
					function invalidate ( from, upto ) {
						var scrollHeight = element[0].scrollHeight;
						if(!_dispatching && scrollHeight !== _dispatchedScrollHeight) {
							_dispatching = true;
							_dispatchedScrollHeight = scrollHeight;
							$timeout(function ( ) {
								_dispatching = false;
								scope.$emit('scrollend', from, upto);
							}, null, false);
						}
					}
					
					function checkBoundaries ( ) {
						//TODO(dario): implement contained
						var elementTop = getViewportPosition(element[0]).y,
							scrollHeight = element[0].scrollHeight,
							elementBottom = elementTop + scrollHeight,
							// IE8 does not support innerHeight
							viewportHeight = getWindowHeight(),
							visibleTop = Math.max(0, -elementTop),
							visibleBottom = scrollHeight - Math.max(0, elementBottom - viewportHeight),
							from = visibleTop / scrollHeight || 0,
							upto = visibleBottom/scrollHeight;
							
						if(upto === 1) {
							invalidate(from, upto);
						}
					}
					
					
				}
			};
			
		}]);
	
})();