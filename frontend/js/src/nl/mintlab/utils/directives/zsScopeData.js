/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('script', [ function ( ) {
			
			var safeApply = fetch('nl.mintlab.utils.safeApply');
			
			return {
				restrict: 'E',
				scope: true,
				terminal: 'true',
				compile: function ( /*tElement, tAttrs, transclude*/ ) {
					
					return function link ( scope, element, attrs ) {
						if(attrs.type === 'text/zs-scope-data') {
							var data = JSON.parse(element[0].innerHTML),
								// restrict: 'E' always creates new scope
								parentScope = scope.$parent;
														
							safeApply(parentScope, function ( ) {
								for(var key in data) {
									parentScope[key] = data[key];
								}
							});
							
						}
					};
					
				}
			};
			
		}]);
	
})();