/*global angular,fetch*/
(function ( ) {
	
	var OFFSET_X = 0;
	
	angular.module('Zaaksysteem')
		.directive('zsHoverMenu', [ function ( ) {
			
			var getViewportSize = fetch('nl.mintlab.utils.dom.getViewportSize'),
				fromLocalToGlobal = fetch('nl.mintlab.utils.dom.fromLocalToGlobal'),
				fromGlobalToLocal = fetch('nl.mintlab.utils.dom.fromGlobalToLocal');
		
			return {
				compile: function ( /*tElement, tAttrs, transclude*/ ) {
					
					return function ( scope, element, attrs ) {
						
						var menu = element.find('ul'),
							orientation = (attrs.zsHoverMenu === 'vertical' ? 'vertical' : 'horizontal');
						
						function onMouseOver ( ) {
							openMenu();
						}
						
						function onMouseOut ( ) {
							closeMenu();
						}
						
						function openMenu ( ) {
							menu.addClass('menu-open');
							element.addClass('element-menu-open');
							positionMenu();
						}
						
						function positionMenu ( ) {
							var width = menu[0].clientWidth,
								height = menu[0].clientHeight,
								viewportSize = getViewportSize(),
								x = 0,
								y = 0,
								parent = angular.element(menu[0].offsetParent),
								p = fromLocalToGlobal(parent[0], { x: 0, y: 0 }),
								parentBounds = { x: p.x, y: p.y, height: parent[0].clientHeight, width: parent[0].clientWidth };
								
							if(orientation === 'horizontal') {
								
								if(parentBounds.x + parentBounds.width + width > viewportSize.width) {
									x = parentBounds.x - width + OFFSET_X;
									parent.attr('data-zs-hover-menu-horizontal-orient', 'left');
								} else {
									x = parentBounds.x + parentBounds.width - OFFSET_X;
									parent.attr('data-zs-hover-menu-horizontal-orient', 'right');
								}
								
								if(parentBounds.y + parentBounds.height + height > viewportSize.height) {
									y = parentBounds.y + parentBounds.height - height;
									parent.attr('data-zs-hover-menu-vertical-orient', 'top');
								} else {
									y = parentBounds.y;
									parent.attr('data-zs-hover-menu-vertical-orient', 'bottom');
								}
							} else {
								throw new Error('vertical orientation not yet implemented in zsHoverMenu');
							}
							
							p = fromGlobalToLocal(parent[0], { x: x, y: y });
							
							menu.css('top', p.y + 'px');
							menu.css('left', p.x + 'px');
						}
						
						function closeMenu ( ) {
							menu.removeClass('menu-open');
							element.removeClass('element-menu-open');
						}
						
						function onMenuClick ( ) {
							closeMenu();
						}
						
						element.bind('mouseover', onMouseOver);
						element.bind('mouseout', onMouseOut);
						menu.bind('click', onMenuClick);
						
						closeMenu();
						
					};
					
				}
			};
		
	}]);
	
})();