/*global angular,window,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsConfirm', [ function ( ) {
			
			// TODO(dario): use template compiling from documentairly
			// to generate a decent dialog
			
			var addEventListener = fetch('nl.mintlab.utils.events.addEventListener'),
				cancelEvent = fetch('nl.mintlab.utils.events.cancelEvent'),
				win = window;
			
			return {
				// make sure directive is executed before ng-click
				priority: 101,
				compile: function ( /*tElement, tAttrs, transclude*/ ) {
					
					return function link ( scope, element, attrs ) {
						
						// FIXME(dario): does not work in IE8 as events
						// are executed in random order. this is in line
						// with the spec, so should be addressed anyway.
						
						function onClick ( event ) {
							var lbl = attrs.zsConfirm;
							if(lbl && !win.confirm(lbl)) {
								cancelEvent(event, true);
							}
						}
						
						addEventListener(element[0], 'click', onClick);
						
					};
					
				}
			};
		}]);
	
})();