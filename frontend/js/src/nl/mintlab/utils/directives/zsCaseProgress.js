/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.case')
		.directive('zsCaseProgress', [ function ( ) {
			
			return {
				replace: false,
				templateUrl: '/partials/directives/case/case-progress.html',
				scope: {
					progress: '@zsCaseProgress'
				},
				compile: function ( /*tElement, tAttrs, transclude*/ ) {
					
				}
			};
			
		}]);
	
})();