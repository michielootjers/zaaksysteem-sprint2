/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('ngUpload', [ '$parse', '$window', '$document', 'fileUploader', function ( $parse, $window, $document, fileUploader ) {
			
			var supportsFileApi = fileUploader.supports();
			
			return {
				link: function ( scope, element, attrs ) {
						
					var callback = $parse(attrs.ngUpload),
						button = element.find('button'),
						input = element.find('input'),
						form = element.find('form'),
						addEventListener = fetch('nl.mintlab.utils.events.addEventListener'),
						removeEventListener = fetch('nl.mintlab.utils.events.removeEventListener');
						
					
					if(supportsFileApi) {
						form.css('display', 'none');
						button.bind('click', function ( ) {
							// reset value to enable upload of same file
							input[0].value = '';
							input[0].click();
						});
						// input.bind('change') doesn't work for some reason
						addEventListener(input[0], 'change', function ( ) {
						
							var files = input[0].files,
								fileArray = [],
								i,
								l;
								
							if(files.length) {
								for(i = 0, l = files.length; i < l; ++i) {
									fileArray.push(files[i]);
								}
								callback(scope, { '$files': files } );
							}
						});
						element.attr('data-zs-file-api-supported', 'true');
					} else {
						
						(function ( ) {
							
							// replace input element because value is read only in IE
							
							function onChange ( ) {
								var files = [],
									fileName,
									file,
									val;
								
								val = input[0].value.split('/');
								fileName = val[val.length-1];
								
								file = {
									name: fileName,
									form: form[0]
								};
								
								files.push(file);
								
								callback(scope, { '$files': files } );
								
								reset();
							}
							
							function initialize ( ) {
								addEventListener(input[0], 'change', onChange);
							}
							
							function reset ( ) {
								
								var clone = input.clone(true),
									before = angular.element(input[0].previousSibling);
									
								if(!before.length) {
									input.parent().prepend(clone);
								} else {
									before.after(clone);
								}
								removeEventListener(input[0], 'change', onChange);
								input.remove();
								
								input = clone;
								
								initialize();
							}
							
							initialize();
							
							element.attr('data-zs-file-api-supported', 'false');
						})();
						
						
						
					}
					
					
					
					
				}
			};
		}]);
})();