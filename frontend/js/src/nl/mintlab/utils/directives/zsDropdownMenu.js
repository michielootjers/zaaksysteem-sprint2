/*global angular,fetch*/
(function ( ) {
	angular.module('Zaaksysteem')
		.directive('zsDropdownMenu', [ '$document', function ( $document ) {
			
			var contains = fetch('nl.mintlab.utils.dom.contains'),
				body = $document.find('body');
			
			return function ( scope, element/*, attrs*/ ) {
				
				var menu = element.find('ul'),
					button = element.find('button').eq(0);
				
				function toggleMenu ( ) {
					if(isOpen()) {
						closeMenu();
					} else {
						openMenu();
					}
				}
				
				function openMenu ( ) {
					menu.addClass('menu-open');
					body.bind('click', onBodyClick);
				}
				
				function closeMenu ( ) {
					menu.removeClass('menu-open');
					body.unbind('click', onBodyClick);
				}
				
				function onBodyClick ( event ) {
					var targetEl = event.target,
						menuContains = contains(menu[0], targetEl),
						elementContains = !menuContains && contains(element[0], targetEl);
						
					if(isOpen() && ((menuContains && angular.element(targetEl).parent('button').length) || !elementContains)) {
						closeMenu();
					}
					
				}
				
				function isOpen ( ) {
					return menu.hasClass('menu-open');
				}
				
				button.bind('click', function ( ) {
					toggleMenu();
				});
					
				closeMenu();
				
			};
			
		}]);
})();