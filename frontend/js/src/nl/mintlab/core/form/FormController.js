/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.form')
		.controller('nl.mintlab.core.form.FormController', [ '$scope', '$parse', '$interpolate', 'smartHttp', function ( $scope, $parse, $interpolate, smartHttp ) {
			
			var forEach = _.forEach,
				indexOf = _.indexOf,
				promises = [];
				
			function watch ( promise ) {
				var obj = {
					promise: promise
				};
				
				obj.unwatch = $scope.$watch(promise.watch, function ( nw/*, old*/ ) {
					if(nw===$parse(promise.when)($scope)) {
						$parse(promise.then)($scope);
					}
				});
				
				promises.push(obj);
				
			}
			
			function unwatch ( promise ) {			
				forEach(promises.concat(), function ( p ) {
					if(p.promise === promise) {
						p.unwatch();
						promises.splice(indexOf(p, 1));
					}
				});
			}
			
			function setFieldData ( ) {
				var fields = $scope.fields || [],
					field,
					i,
					l,
					val;
					
				for(i = 0, l = fields.length; i < l; ++i) {
					field = fields[i];
					val = undefined;
					if(field.value !== undefined) {
						val = field.value;
					} else {
						val = field['default'];
					}
					if(val !== undefined) {
						$scope[field.name] = $interpolate(val)($scope);
					}
				}
			}
			
			function resolve ( field ) {
				var value = $scope[field.name];
				if(field.data && field.data.resolve) {
					value = $parse(field.data.resolve)(value);
				}
				return value;
			}
			
			function getForm ( ) {
				return $scope[$scope.getName()];
			}
			
			function submitForm ( action ) {
				var url = action.data.url,
					data = {},
					fields = $scope.fields || [],
					field,
					i,
					l;
					
				$scope.submitting = true;
				
				for(i = 0, l = fields.length; i < l; ++i) {
					field = fields[i];
					if($scope.showField(field)) {
						data[field.name] = resolve(field);
					}
				}
				
				smartHttp.connect( {
					url: url,
					method: 'POST',
					data: data
				})
					.success(function onSuccess ( data ) {
						$scope.submitting = false;
					})
					.error(function onError ( data ) {
						$scope.submitting = false;
						validate(data);
					});
			}
			
			function validate ( data ) {
				var form = getForm(),
					fields = data.data || [],
					messages = data.messages || [],
					field,
					control,
					validityType,
					i,
					l;
					
				for(i = 0, l = fields.length; i < l; ++i) {
					field = fields[i];
					switch(field.result) {
						default:
						validityType = '';
						break;
						
						case 'missing':
						validityType = 'required';
						break;
						
						case 'invalid':
						validityType = 'pattern';
						break;
					}
					control = form[field.parameter];
					if(control) {
						control.$setValidity(validityType, false);
					}
				}
				
				for(i = 0, l = messages.length; i < l; ++i) {
					
				}
			}
			
			$scope.handleActionClick = function ( action/*, event*/ ) {
				
				switch(action.type) {
					case 'submit':
					submitForm(action);
					break;
				}
				
			};
			
			$scope.isActionDisabled = function ( action ) {
				var form = getForm(),
					isDisabled;
										
				switch(action.type) {
					case 'submit':
					isDisabled = !form.$valid;
					break;
					
					default:
					isDisabled = true;
					break;
				}
				
				return isDisabled;
			};
			
			$scope.showField = function ( field ) {
				var when = field.when;
				return when === undefined || $parse(when)($scope);
			};
			
			$scope.getRequired = function ( field ) {
				var required = field.required,
					isRequired;
					
				if(typeof required === 'boolean') {
					isRequired = required;
				} else {
					isRequired = $parse(required)($scope);
				}
				return isRequired;
			};
			
			$scope.getFieldId = function ( field ) {
				return field.name;
			};
			
			$scope.getPlaceholder = function ( field ) {
				var placeholder = field.data ? field.data.placeholder : undefined;
				return placeholder !== undefined ? $parse(placeholder)($scope) : '';
			};
			
			$scope.getName = function ( ) {
				return $scope.name;
			};
			
			// we can't use a dynamic ngModel value because
			// it's not interpolated (just parsed), so we
			// expose the scope object
			// see: https://github.com/angular/angular.js/issues/1404
			$scope.scope = $scope;
			
			$scope.$watch('promises', function ( nw, old ) {
				
				if(old && old.length) {
					forEach(old, function ( value ) {
						unwatch(value);
					});
				}
				
				if(nw && nw.length) {
					forEach(nw, function ( value ) {
						watch(value);
					});
				}
				
			});
			
			$scope.$watch('fields', function ( /*nw, old*/ ) {
				setFieldData();
			});
			
		}]);
	
})();