/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.form')
		.directive('zsFormField', [ function ( ) {
			
			return {
				priority: 100,
				require: [ 'ngModel' ],
				compile: function ( ) {
					
					return function link ( scope, element, attrs, controllers ) {
						
						var ngModel = controllers[0];
						
						attrs.$set('name', scope.field.name);
						element.attr('name', scope.field.name);
						ngModel.$name = scope.field.name;
						
					};
					
				}
			};
			
		}]);
	
})();