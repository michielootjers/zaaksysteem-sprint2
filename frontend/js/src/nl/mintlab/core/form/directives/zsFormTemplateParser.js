/*global angular*/
(function ( ) {
	
	var TEMPLATE_URL = '/html/core/form/form.html';
	
	angular.module('Zaaksysteem.form')
		.directive('zsFormTemplateParser', [ '$q', '$timeout', 'smartHttp', 'templateCompiler', '$compile', function ( $q, $timeout, smartHttp, templateCompiler, $compile ) {
			
			return {
				scope: true,
				compile: function ( /*tElement, tAttrs, transclude*/ ) {
					
					return function link ( scope, element, attrs ) {
						
						var configUrl = scope.$eval(attrs.zsFormTemplateParser),
							templateUrl = scope.$eval(attrs.zsFormTemplateUrl) || TEMPLATE_URL,
							compiler;
							
						function compileInterface ( ) {
							compiler(scope, function ( clonedElement ) {
								for(var i = 0, l = clonedElement.length; i < l; ++i) {
									element[0].appendChild(clonedElement[0]);
								}
							}); 
						}
						
						function setConfig ( config ) {
							var data = config.data;
							
							scope.name = config.name;
							scope.fields = config.fields;
							scope.actions = config.actions;
							scope.promises = config.promises;
							
							for(var key in data) {
								scope[key] = data[key];
							}
						}
												
						$q.all([
							smartHttp.connect({
								url: configUrl,
								method: 'GET'
							})
								.success(function ( data ) {
									setConfig(data);
								}),
							templateCompiler.getElement(templateUrl).then(function ( element ) {
								var clone = element.clone();
								for(var i = 0, l = clone.length; i < l; ++i) {
									if(clone[i].tagName.toLowerCase() === 'form') {
										clone.eq(i).attr('name', scope.name);
										break;
									}
								}
								compiler = $compile(clone);
							})
						])
						.then(function ( ) {
							$timeout(function ( ) {
								compileInterface();
							});
						});
						
					};
				}
				
			};
			
		}]);
	
})();