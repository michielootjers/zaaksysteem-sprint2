/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.controller('nl.mintlab.core.crud.CrudTableController', [ '$scope', '$parse', function ( $scope, $parse ) {
			
			$scope.sortedOn = null;
			$scope.reversed = false;
			
			$scope.sortOn = function ( column ) {
				if($scope.isSortedOn(column)) {
					$scope.reverse();
				}
				$scope.sortedOn = column;
			};
			
			$scope.isSortedOn = function ( column ) {
				return $scope.sortedOn === column;
			};
			
			$scope.reverse = function ( ) {
				$scope.reversed = !$scope.reversed;
			};
			
			$scope.getColumnValue = function ( column, item ) {
				var resolve = column.resolve + (column.filter ? '|' + column.filter : ''),
					getter = $parse(resolve);
				return getter(item);
			};
			
			$scope.getSortValue = function ( item ) {
				var col = $scope.sortedOn,
					getter;
					
				if(!col) {
					return $scope.items.indexOf(item);
				}
				
				if(!col.sort) {
					return $scope.getColumnValue(col, item);
				}
				
				if(typeof col.sort === 'string') {
					getter = $parse(col.sort);
				} else {
					getter = $parse(col.sort.resolve);
				}
				return getter(item);
			};
			
		}]);
	
})();