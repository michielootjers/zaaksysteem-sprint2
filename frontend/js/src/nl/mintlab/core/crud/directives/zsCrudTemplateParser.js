/*global angular*/
(function ( ) {
	
	var TEMPLATE_URL = '/html/core/crud/crud-interface.html';
	
	angular.module('Zaaksysteem')
		.directive('zsCrudTemplateParser', [ '$compile', '$parse', '$q', '$timeout', 'smartHttp', 'templateCompiler', function ( $compile, $parse, $q, $timeout, smartHttp, templateCompiler ) {
			
			return {
				compile: function ( ) {
					
					return function link ( scope, element, attrs ) {
						
						var configUrl = scope.$eval(attrs.zsCrudTemplateParser),
							templateUrl = scope.$eval(attrs.zsCrudTemplateUrl) || TEMPLATE_URL,
							baseUrl,
							templateElement;
							
						function compileInterface ( ) {
								
							templateCompiler.getCompiler(templateUrl).then(function ( compiler ) {
								compiler(scope, function ( clonedElement ) {
									for(var i = 0, l = clonedElement.length; i < l; ++i) {
										element[0].appendChild(clonedElement[0]);
									}
								}); 
							});
							
						}
						
						function setActions ( actions ) {
							scope.actions = actions;
						}
						
						function setColumns ( columns ) {
							scope.columns = columns;
						}
						
						function setUrl ( url ) {
							baseUrl = url;
						}
						
						function setTemplateElement ( el ) {
							templateElement = el;
						}
						
						function setItems ( items ) {
							scope.items = items;
						}
						
						function setOptions ( options ) {
							scope.options = options;
						}
						
						function loadData ( ) {
							smartHttp.connect({
								url: baseUrl,
								method: 'GET'
							})
								.success(function ( data ) {
									setItems(data.result);
								})
								.error(function ( data ) {
									console.log('Encountered error while collecting item data', data);
								});
						}
						
						function setConfig ( config ) {
							setActions(config.actions);
							setColumns(config.columns);
							setOptions(config.options);
							setUrl(config.url);
						}
												
						$q.all([
									smartHttp.connect({
										url: configUrl,
										method: 'GET'
									})
										.success(function ( data ) {
											setConfig(data);
										}),
									templateCompiler.getElement(templateUrl).then(function ( element ) {
										setTemplateElement(element);
									})
								]
						).then(function ( ) {
							$timeout(function ( ) {
								compileInterface();
								loadData();
							});
						});
						
					};
				}
				
			};
			
		}]);
	
})();