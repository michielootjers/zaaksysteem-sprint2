/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.controller('nl.mintlab.core.crud.CrudInterfaceController', [ '$scope', function ( $scope ) {
			
			var indexOf = _.indexOf,
				forEach = _.forEach;
			
			$scope.selectedItems = [];
			
			function applySelected ( selected ) {
				$scope.selectedItems = selected;
			}
			
			$scope.$on('zsSelectableList:change', function ( event, selected ) {
				if(!$scope.$$phase && !$scope.$root.$$phase) {
					$scope.$apply(function ( ) {
						applySelected(selected);
					});
				} else {
					applySelected(selected);
				}
			});
			
			$scope.isAllSelected = function ( ) {
				return $scope.selectedItems && $scope.items && $scope.selectedItems.length === $scope.items.length;
			};
			
			$scope.isSelected = function ( item ) {
				return indexOf($scope.selectedItems, item) !== -1;
			};
			
			$scope.handleSelectAllClick = function ( /*event*/ ) {
				if($scope.isAllSelected()) {
					$scope.deselectAll();
				} else {
					$scope.selectAll();
				}
			};
			
			$scope.selectAll = function ( ) {
				var items = $scope.items || [];
				
				$scope.selectedItems.length = 0;
				$scope.selectedItems.push.apply($scope.selectedItems, items);
			};
			
			$scope.deselectAll = function ( ) {
				$scope.selectedItems.length = 0;	
			};
			
			$scope.deselectItem = function ( item ) {
				var index = indexOf($scope.selectedItems, item);
				if(index !== -1) {
					$scope.selectedItems.splice(index, 1);
				}
			};
			
			$scope.deleteItems = function ( ) {
				var selected = $scope.selectedItems.concat();
				forEach(selected, function ( item/*, key*/ ) {
					$scope.deselectItem(item);
					$scope.items.splice(indexOf($scope.items, item));
				});
			};
			
		}]);
	
})();