/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem.dom')
		.service('templateCompiler', [ '$q', '$templateCache', '$compile', '$http', function ( $q, $templateCache, $compile, $http ) {
			
			var trim = fetch('nl.mintlab.utils.shims.trim'),
				templatePromises = {},
				templateData = {},
				compilerPromises = {},
				compilers = {};
			
			function TemplateCompiler ( ) {
				
			}
			
			function compileElement ( template ) {
				var deferred = $q.defer(),
					promise = deferred.promise;
					
				if(templatePromises[template]) {
					templatePromises[template].then(function ( ) {
						deferred.resolve(templateData[template]);
					});
					return promise;
				}
				
				templatePromises[template] = $q.when($templateCache.get(template) || $http.get(template, { cache: true })).then(function ( tpl ) {
									
					if(angular.isObject(tpl)) {
						tpl = tpl.data;
					}
					
					var element = angular.element(trim(tpl));
					
					
					templateData[template] = element;
					
					deferred.resolve(element);
					
				});
				
				return promise;
			}
			
			function getCompiler ( template ) {
				
				var deferred;
				
				if(!compilerPromises[template]) {
					deferred = $q.defer();
					compilerPromises[template] = deferred.promise;
					compileElement(template).then(function ( element ) {
						var compiler = $compile(element);
						compilers[template] = compiler;
						deferred.resolve(compiler);
					});
				}
				
				return compilerPromises[template];
			}
			
			TemplateCompiler.prototype.getCompiler = function ( template ) {
				return getCompiler(template);
			};
			
			TemplateCompiler.prototype.getElement = function ( template ) {
				return compileElement(template);
			};
			
			return new TemplateCompiler();
			
		}]);
	
})();

