/*global angular,fetch*/
(function () {
	
	angular.module('Zaaksysteem.kcc')
		.controller('nl.mintlab.kcc.CallController', [ '$scope', 'smartHttp', '$cookieStore', '$rootScope', function ( $scope, smartHttp, $cookieStore, $rootScope ) {
			
			var Call = fetch('nl.mintlab.kcc.Call');
			
			var _incomingCalls = [];
			
			$scope.activeSubject = null;
			$scope.initialized = false;
			$scope.tabData = null;
			
			function loadActiveSubject ( ) {
				var cookiedSubject = $cookieStore.get('activesubject');
				if(cookiedSubject) {
					setActiveSubject(cookiedSubject);
				}
				smartHttp.connect({
					method: 'GET',
					url: 'betrokkene/get_session'
				})
					.success(handleSuccess)
					.error(handleError);
			}
			
			function handleSuccess ( data ) {
				var subj;
				
				if(data && data.result && data.result.length) {
					subj = data.result[0].subject;
					setActiveSubject(subj);
					$scope.setTabData(subj);
				} else {
					setActiveSubject(null);
				}
			}
			
			function handleError ( ) {
				setActiveSubject(null);
			}
			
			function setActiveSubject ( subject ) {
				$scope.activeSubject = subject;
				setCookie();
			}
			
			function setCookie ( ) {
				if($scope.activeSubject) {
					$cookieStore.put('activesubject', $scope.activeSubject);
				} else {
					$cookieStore.remove('activesubject');
				}
			}
			
			$scope.enableSession = function ( contact ) {
				var betrokkeneId = contact.identifier;
				
				smartHttp.connect( {
					method: 'POST',
					url: 'betrokkene/enable_session/' + betrokkeneId
				})
					.success(function ( data ) {
						var subj = data && data.result && data.result.length ? data.result[0].subject : null;
						setActiveSubject(subj);
						$scope.setTabData($scope.activeSubject);
					});
			};
			
			$scope.disableSession = function ( ) {
				smartHttp.connect( {
					method: 'POST',
					url: 'betrokkene/disable_session'
				})
					.success(function ( /*data*/ ) {
						setActiveSubject(null);
						if(_incomingCalls.length) {
							$scope.setTabData(_incomingCalls[0]);
						} else {
							$scope.setTabData(null);
						}
					})
					.error(function ( ) {
						
					});
			};
			
			$scope.getSubjectByline = function ( ) {
				var byline = [],
					activeSubject = $scope.activeSubject;
					
				if(!activeSubject) {
					return '';
				}
				
				if(activeSubject.street) {
					byline.push(activeSubject.street);
				}
				if(activeSubject.postal_code) {
					byline.push(activeSubject.postal_code);
				}
				if(activeSubject.city) {
					byline.push(activeSubject.city);
				}
				if(activeSubject.telephone_numbers && activeSubject.telephone_numbers.length) {
					byline.push(activeSubject.telephone_numbers[0]);
				}
				if(activeSubject.email_addresses && activeSubject.email_addresses.length) {
					byline.push(activeSubject.email_addresses[0]);
				}
				return byline.join(', ');
			};
			
			$scope.getDisplayName = function ( obj ) {
				var displayName = '',
					contact;
				
				if(obj instanceof Call) {
					contact = obj.contact;
					if(contact && contact.natural_person) {
						displayName = contact.natural_person.given_names + ' ' + contact.natural_person.surname;
					} else if(contact && contact.non_natural_person) {
						displayName = contact.non_natural_person.name;
					} else {
						displayName = obj.phonenumber;
					}
				} else {
					contact = obj;
					if(contact) {
						displayName = contact.name;
					}
				}
				
				return displayName;
			};
			
			$scope.setTabData = function ( tabData ) {
				$scope.tabData = tabData;
			};
			
			$scope.acceptCall = function ( call ) {
				smartHttp.connect( {
					method: 'POST',
					url: 'kcc/call/accept',
					data: {
						event_id: call.id,
						accept: true
					}
				})
					.success(function ( /*data*/ ) {
						$scope.$broadcast('callaccept', call);
						if(call.contact) {
							$scope.enableSession(call.contact);
						}
					})
					.error(function ( /*data*/ ) {
						
					});
			};
			
			$scope.rejectCall = function ( call ) {
				smartHttp.connect( {
					method: 'POST',
					url: 'kcc/call/accept',
					data: {
						event_id: call.id,
						accept: false
					}
				})
					.success(function ( /*data*/ ) {
						$scope.$broadcast('callreject', call);
						if($scope.tabData === call) {
							$scope.setTabData($scope.activeSubject||_incomingCalls[0]);
						}
					})
					.error(function ( /*data*/ ) {
						
					});
			};

			$scope.$on('incomingcall', function ( event, calls) {
				
				_incomingCalls = calls;
				
				if(!$scope.tabData && calls[0]) {
					$scope.setTabData(calls[0]);
				}
			});
			
			// TODO(dario): find a cleaner way to set subject
			$rootScope.$on('legacy-activesubject', function ( /*event, result*/ ) {
				$scope.$apply(function ( ) {
					loadActiveSubject();
				});
			});
			
			loadActiveSubject();
			
		}]);
	
})();