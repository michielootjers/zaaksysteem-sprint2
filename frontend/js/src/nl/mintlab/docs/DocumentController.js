/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem.docs')
		.controller('nl.mintlab.docs.DocumentController', [ '$scope', 'smartHttp', '$filter', '$routeParams', 'fileUploader', '$timeout', function ( $scope, smartHttp, $filter, $routeParams, fileUploader, $timeout ) {
				
			var Folder = fetch('nl.mintlab.docs.Folder'),
				File = fetch('nl.mintlab.docs.File');
			
			$scope.trash = new Folder( { name: 'Prullenbak' });
			$scope.list = new Folder( { name: 'Map' });
			$scope.intake = new Folder( { name: 'Intake' });
			$scope.caseDocs = [];
			$scope.docCats = [];
			$scope.initialized = false;
			$scope.readOnly = false;
			$scope.loading = false;
			
			$scope.view = 'list';
			
			$scope.reloadData = function ( ) {
				
				var callsLeft = 4;
				
				function onComplete ( ) {
					callsLeft--;
					if(!callsLeft) {
						$scope.loading = false;
					}
				}
				
				$scope.caseDocs.length = 0;
				$scope.docCats.length = 0;
				
				$scope.trash.empty();
				$scope.list.empty();
				$scope.intake.empty();
				
				$scope.$broadcast('reload');
				
				$scope.loading = true;
				
				smartHttp.connect({
					url: ($scope.pip ? 'pip/' : '') + 'file/search/case_id/' + $scope.caseId,
					method: 'GET'
				})
					.success(processFileData)
					.then(onComplete, onComplete);
					
				if(!$scope.pip) {
					smartHttp.connect( {
						url: 'directory/search/case_id/' + $scope.caseId,
						method: 'GET'
					})
						.success(processFolderData)
						.then(onComplete, onComplete);
				}
				
				if(!$scope.pip) {	
					smartHttp.connect( {
						url: 'zaak/' + $scope.caseId + '/case_type_documents',
						method: 'GET'
					})
						.success(processCaseDocData)
						.then(onComplete, onComplete);
				}
					
				if(!$scope.pip) {
					smartHttp.connect( {
						url: 'file/document_categories',
						method: 'GET'
					})
						.success(processDocCatData)
						.then(onComplete, onComplete);
				}
			};
			
			$scope.setView = function ( view ) {
				$scope.view = view;
			};
			
			$scope.addFolder = function ( folder, folderName ) {
				var child = new Folder( { name: folderName });
				folder.add(child);
			};
			
			$scope.uploadFile = function ( files ) {
				
				var url = $scope.pip ? 'pip/file/create' : 'file/create';
				
				angular.forEach(files, function ( value ) {
					fileUploader.upload(value, smartHttp.getUrl(url), {
						'case_id': $scope.caseId
					}).then(function ( upload ) {
						var file = new File(),
							result = upload.getData().result,
							data = result ? result[0] : null;
							
						file.updating = true;
							
						file.updateWith(data);
						
						$timeout(function ( ) {
							file.updating = false;
						});
						
						if(!file.accepted) {
							$scope.intake.add(file);
						} else {
							$scope.list.add(file);
						}
						
					}, function ( /*upload*/ ) {
						
					});
				});
			};
			
			$scope.replaceFile = function ( file, replace ) {
				var replacement = replace[0];
				
				file.updating = true;
				
				if(replacement) {
					fileUploader.upload(replacement, smartHttp.getUrl('file/update_file'), {
						'file_id': file.id
					}).then(function ( upload ) {
						var result = upload.getData().result,
							data = result ? result[0] : null;
						file.updating = false;
						file.updateWith(data);
					}, function ( /*upload*/ ) {
						file.updating = false;
					});
				}
			};
			
			function processFileData ( data ) {
				var files = data.result || [],
					parsedFiles = [];
					
				angular.forEach(files, function ( file ) {
					parsedFiles.push(parseFileData(file));
				});
				
				$scope.initialized = true;
			}
			
			function processFolderData ( data ) {
				var folders = data.result || [];
				
				angular.forEach(folders, function ( folder ) {
					parseFolderData(folder);
				});
			}
			
			function processCaseDocData ( data ) {
				$scope.caseDocs = data.result || [];
			}
			
			function processDocCatData ( data ) {
				var docCats = data.result || [],
					result = [];
				
				for(var i = 0, l = docCats.length; i < l; ++i) {
					result.push({
						index: i,
						label: docCats[i],
						value: docCats[i]
					});
				}
				
				$scope.docCats = result;
			}
			
			function parseFileData ( fileData ) {
				var file = new File(),
					parent;
					
				file.updateWith(fileData);
				
				if(file.accepted && !file.date_deleted) {
					if(file.directory_id) {
						parent = $scope.list.getEntityByPath(file.directory_id.id) || parseFolderData(file.directory_id);
					}
					
					if(!parent) {
						parent = $scope.list;
					}
					
				} else if(!file.accepted) {
					parent = $scope.intake;
				} else {
					parent = $scope.trash;
				}
				parent.add(file);
			}
			
			function parseFolderData ( folderData ) {
				var folder = $scope.list.getEntityByPath( [ 'folder_' + folderData.id ]);
					
				if(!folder){
					folder = new Folder( { id: folderData.id, uuid: folderData.id } );
					$scope.list.add(folder);
				}
					
				folderData.uuid = folderData.id;
					
				folder.updateWith(folderData);
				
				return folder;
			}
			
			$scope.getCaseDocLabel = function ( caseDoc ) {
				var label = '';
				
				if(caseDoc) {
					label = caseDoc.label || caseDoc.bibliotheek_kenmerken_id.naam;
				}
				return label;
			};
			
			$scope.isSameCaseDoc = function ( caseDocA, caseDocB ) {
				if(caseDocA === caseDocB) {
					return true;
				}
				
				if(!caseDocA || !caseDocB) {
					return false;
				}
				
				return caseDocA.id === caseDocB.id;
			};
			
			$scope.isNotReferentialCaseDoc = function ( caseDoc ) {
				return !caseDoc || !caseDoc.referential;
			};
			
			$scope.init = function ( ) {
				
			};
			
			$scope.$watch('caseId', function ( nw/*, old*/ ) {
				if(nw) {
					$scope.reloadData();
				}
			});
			
			$scope.$on('drop', function ( event, data, mimeType ) {
				if(mimeType === 'Files') {
					$scope.uploadFile(data);
				}
			});
			
		}]);
})();