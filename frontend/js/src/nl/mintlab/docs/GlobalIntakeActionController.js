/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem.docs')
		.controller('nl.mintlab.docs.GlobalIntakeActionController', [ '$scope', 'smartHttp', function ( $scope, smartHttp ) {
			
			var forEach = angular.forEach,
				indexOf = fetch('nl.mintlab.utils.shims.indexOf');
			
			$scope.caseId = null;
			
			function processCaseDocData ( data ) {
				$scope.caseDocs = data.result || [];
			}
		
			$scope.assignFiles = function ( ) {
				
				var	caseId = $scope.caseId.id,
					files = $scope.selectedFiles.concat(),
					queue = [];
				
				$scope.loading = true;
				
				$scope.deselectAll();
				
				function onComplete ( file ) {
					var index = indexOf(files, file);
					files.splice(index, 1);
					if(!files.length) {
						$scope.loading = false;
						$scope.closePopup();
					}
				}
					
				forEach(files, function ( file ) {
					
					$scope.root.remove(file);
					
					file.case_id = caseId;
					
					queue.push(file);
					
					smartHttp.connect({
						url: 'file/update',
						method: 'GET',
						params: {
							file_id: file.id,
							case_id: caseId,
							case_type_document_id: file.case_type_document_id ? file.case_type_document_id.id : null,
							case_type_document_clear_old: false
						}
					})
						.success(function ( ) {
							onComplete(file);
							$scope.$broadcast('systemMessage', {
								type: 'info',
								code: 'assign-file-success',
								data: {
									files: files
								}
							});
						})
						.error(function ( ) {
							onComplete(file);
							$scope.root.add(file);
							file.case_id = null;
							file.case_type_document_id = null;
						});
				});
				
			};
			
			$scope.getFileNames = function ( ) {
				var fileNames = [];
				forEach($scope.selectedFiles, function ( file ) {
					fileNames.push(file.name + file.extension);
				});
				
				return fileNames.join(', ');
			};
			
			$scope.$watch('caseId', function ( ) {
				$scope.caseDocs = [];
				
				$scope.loading = true;
				
				if($scope.caseId) {
					smartHttp.connect({
						method: 'GET',
						url: 'zaak/' + $scope.caseId.id + '/case_type_documents'
					})
						.success(function ( data ) {
							$scope.loading = false;
							processCaseDocData(data);
						})
						.error(function ( ) {
							$scope.loading = false;
						});
				}
			});
		
	}]);
	
})();