/*global angular,fetch*/
(function ( ) {
	angular.module('Zaaksysteem.docs')
		.controller('nl.mintlab.docs.DocumentListController', [ '$scope', '$q', 'smartHttp', function ( $scope, $q, smartHttp ) {
			
			// TODO(dario): refactor to use different controllers for
			// list,intake,trash, using js inheritance
			
			var File = fetch('nl.mintlab.docs.File'),
				Folder = fetch('nl.mintlab.docs.Folder'),
				indexOf = fetch('nl.mintlab.utils.shims.indexOf'),
				words,
				cutFiles = [],
				_url = 'file/';
			
			$scope.root = null;
			$scope.viewType = 'listView';
			$scope.selectedFiles = [];
			
			$scope.maxDepth = 1;
			$scope.filterQuery = '';
			$scope.dragging = false;
			
			function clearData ( ) {
				$scope.deselectAll();
			}
			
			$scope.onToggleClick = function ( event, entity ) {
				if($scope.isSelected(entity)) {
					$scope.deselectEntity(entity);
				} else {
					$scope.selectEntity(entity);
				}
				
				if(event) {
					event.stopPropagation();
				}
			};
			
			$scope.onEntityClick = function ( event, entity ) {
				if($scope.isSelected(entity) && $scope.selectedFiles.length === 1) {
					$scope.deselectEntity(entity);
				} else {
					$scope.deselectAll();
					$scope.selectEntity(entity);
				}
			};
			
			$scope.onSelectAllClick = function ( /*event*/ ) {
				if($scope.isSelected($scope.root)) {
					$scope.deselectAll();
				} else {
					$scope.selectAll();
				}
			};
			
			$scope.selectEntity = function ( entity, recursive ) {
				if(!entity.getSelected()) {
					entity.setSelected(true);
					if(entity !== $scope.root) {
						$scope.selectedFiles.push(entity);
					}
				}
				
				if(recursive === true && entity instanceof Folder) {
					angular.forEach(entity.getFolders(), function ( value ) {
						$scope.selectEntity(value, true);
					});
					angular.forEach(entity.getFiles(), function ( value ) {
						$scope.selectEntity(value);
					});
				}
				
				setRootState();
			};
			
			$scope.deselectEntity = function ( entity, recursive ) {
				var selectedFiles = $scope.selectedFiles,
					index = indexOf(selectedFiles, entity);
				
				if(entity.getSelected()) {
					entity.setSelected(false);
					if(entity !== $scope.root) {
						selectedFiles.splice(index, 1);
					}
				}
				
				if(recursive === true && entity instanceof Folder) {
					angular.forEach(entity.getFolders(), function ( value ) {
						$scope.deselectEntity(value, true);
					});
					angular.forEach(entity.getFiles(), function ( value ) {
						$scope.deselectEntity(value);
					});
				}
					
				
				$scope.root.setSelected(false);
			};
			
			$scope.selectAll = function ( ) {
				$scope.selectEntity($scope.root, true);
			};
			
			$scope.deselectAll = function ( ) {
				$scope.deselectEntity($scope.root, true);
				while($scope.selectedFiles.length) {
					$scope.deselectEntity($scope.selectedFiles[0]);
				}
			};
			
			$scope.isSelected = function ( entity ) {
				if(!entity) {
					return null;
				}
				return entity.getSelected();
			};
			
			$scope.moveEntity = function ( entities, target ) {
				angular.forEach(entities, function ( entity ) {
					
					var currentParent = entity.getParent(),
						directoryId = target.getDepth() > 0 ? target.id : null;
					
					if(validateEntity(entity, target)) {
						target.add(entity);
					}
					
					entity.updating = true;
					
					smartHttp.connect({
						url: 'file/update/',
						method: 'POST',
						data: {
							file_id: entity.id,
							directory_id: directoryId
						}
					})
						.success(function ( ) {
							entity.directory_id = target.id;
							entity.updating = false;
						})
						.error(function ( ) {
							currentParent.add(entity);
							entity.updating = false;
						});
				});
			};
			
			$scope.removeEntity = function ( entities ) {
				var deferred = $q.defer(),
					queue;
				
				entities = entities.concat();
				queue = entities.concat();
				
				function complete ( entity ) {
					var index = indexOf(queue, entity);
					if(index !== -1) {
						queue.splice(index, 1);
						if(queue.length === 0) {
							deferred.resolve();
						}
					}
					entity.updating = false;
				}
				
				angular.forEach(entities, function ( entity ) {
					
					var parent = entity.getParent();
					
					entity.updating = true;
					
					if(!parent || parent === $scope.trash) {
						// file was already removed
						complete(entity);
						return;
					}
					
					if($scope.isSelected(entity)) {
						$scope.deselectEntity(entity);
					}
					
					if(entity.id === -1) {
						// file hasn't been saved
						parent.remove(entity);
						complete(entity);
						return;
					}
					
					if(entity instanceof File) {
						if($scope.caseId) {
							$scope.trash.add(entity);
						} else {
							parent.remove(entity);
						}
						smartHttp.connect({
							url: _url + 'update/',
							method: 'POST',
							data: {
								file_id: entity.id,
								deleted: true
							}
						})
							.success(function ( data ) {
								if(data.result && data.result[0]) {
									entity.updateWith(data.result[0]);
								}
								entity.deleted = true;
							})
							.error(function ( ) {
								parent.add(entity);
							})
							.then(function ( ) {
								complete(entity);
							});
							
					} else if(entity.getFiles().length === 0) {
						parent.remove(entity);
						smartHttp.connect({
							url: 'directory/delete/',
							method: 'POST',
							data: {
								directory_id: entity.id
							}
						})
							.success(function ( ) {
							
							})
							.error(function ( ) {
								parent.add(entity);
							})
							.then(function ( ) {
								complete(entity);
							});
					} else {
						var children = entity.getFiles();
						$scope.removeEntity(children).then(function ( ) {
							$scope.removeEntity([entity]);
						});
					}
					
				});
				
				if(!queue.length) {
					deferred.resolve();
				}
				
				return deferred.promise;
				
			};
			
			$scope.restoreEntity = function ( entities ) {
				entities = entities.concat();
				angular.forEach(entities, function ( entity ) {
					$scope.list.add(entity);
					if($scope.isSelected(entity)) {
						$scope.deselectEntity(entity);
					}
					
					smartHttp.connect({
						url: _url + 'update/',
						method: 'POST',
						data: {
							file_id: entity.id,
							deleted: false
						}
					})
						.success(function ( ) {
							entity.deleted = false;
						})
						.error(function ( ) {
							$scope.trash.add(entity);
						});
				});
			};
			
			$scope.destroyEntity = function ( entities ) {
				
				entities = entities.concat();
				angular.forEach(entities, function ( entity ) {
					
					entity.getParent().remove(entity);
					
					if($scope.isSelected(entity)) {
						$scope.deselectEntity(entity);
					}
					
					smartHttp.connect( {
						url: _url + 'update/',
						method: 'POST',
						data: {
							file_id: entity.id,
							destroyed: true
						}
					})
						.success(function ( ) {
							
						})
						.error(function ( ) {
							$scope.trash.add(entity);
						});
				});
				
			};
			
			$scope.acceptFile = function ( files ) {
				files = files.concat();
				angular.forEach(files, function ( file ) {
					
					file.updating = true;
					
					if($scope.isSelected(file)) {
						$scope.deselectEntity(file);
					}
					
					if(Number(file.is_revision) !== 1) {
						$scope.list.add(file);
						smartHttp.connect({
							url: _url + 'update/',
							method: 'POST',
							data: {
								file_id: file.id,
								accepted: true
							}
						})
							.success(function ( data ) {
								var fileData = data.result[0];
								file.updateWith(fileData);
								file.updating = false;
								if(file.destroyed) {
									file.getParent().remove(file);
								}
							})
							.error(function ( ) {
								$scope.intake.add(file);
								file.updating = false;
							});
					} else {
						
						var flatFileList = getFlatFileList(),
							i = 0,
							l = flatFileList.length,
							f,
							oldFile,
							parent = $scope.list;
							
						for(; i < l; ++i) {
							f = flatFileList[i];
							if(f.id === file.is_duplicate_of) {
								oldFile = f;
								break;
							}
						}
						
						if(oldFile) {
							parent = oldFile.getParent();
							parent.remove(oldFile);
						}
						
						parent.add(file);
						
						smartHttp.connect({
							url: 'file/update_file',
							method: 'POST',
							data: {
								file_id: file.is_duplicate_of,
								existing_file_id: file.id
							}
						})
							.success(function ( data ) {
								var fileData = data.result[0];
								file.updateWith(fileData);
								file.updating = false;
							})
							.error(function ( ) {
								$scope.intake.add(file);
								file.updating = false;
							});
					}
					
					
				});
			};
			
			$scope.rejectFile = function ( files ) {
				files = files.concat();
				angular.forEach(files, function ( file ) {
					if($scope.isSelected(file)) {
						$scope.deselectEntity(file);
					}
					$scope.intake.remove(file);
					smartHttp.connect({
						url: _url + 'update/',
						method: 'POST',
						data: {
							file_id: file.id,
							accepted: false,
							//TODO(dario): get this from ui
							rejection_reason: 'foo'
						}
					})
						.success(function ( ) {
						})
						.error(function ( ) {
							$scope.intake.add(file);
						});
				});
			};
			
			$scope.setCaseDoc = function ( files, caseDoc, clearPrevious ) {
				
				angular.forEach(files, function ( file ) {
					
					var prevCaseDocId = file.case_type_document_id,
						prevCaseDocOwner,
						hasChanged = !$scope.isSameCaseDoc(file.case_type_document_id, caseDoc),
						data;
						
					if(!hasChanged) {
						return;
					}
						
					file.updating = true;
					
					if(clearPrevious && caseDoc) {
						
						clearPrevious.case_type_document_id = null;
						prevCaseDocOwner = clearPrevious;
						
					}
					
					file.case_type_document_id = caseDoc;
					
					data = {
						file_id: file.id,
						case_type_document_id: caseDoc ? caseDoc.id : null
					};
					
					if(clearPrevious) {
						data.case_type_document_clear_old = prevCaseDocOwner.id;
					}
					
					smartHttp.connect({
						url: 'file/update',
						method: 'POST',
						data: data
					})
						.success(function ( data ) {
							file.updateWith(data.result[0]);
							$scope.$emit('casedocupdate');
						})
						.error(function ( ) {
							file.case_type_document_id = prevCaseDocId;
							if(prevCaseDocOwner) {
								prevCaseDocOwner.case_type_document_id = caseDoc;
							}
						})
						.then(function ( ) {
							file.updating = false;
						});
                });
			};
			
			$scope.getCaseDocAssignment = function ( caseDoc ) {
				
				var id = caseDoc ? caseDoc.id : null,
					flatFileList = getFlatFileList(),
					i,
					l,
					file;
					
					
				for(i = 0, l = flatFileList.length; i < l; ++i) {
					file = flatFileList[i];
					if(file.case_type_document_id && file.case_type_document_id.id === id) {
						return file;
					}
				}
				
				return null;
				
				
			};
			
			$scope.filterEntity = function ( entity ) {
				var name = entity.name,
					desc = entity.desc,
					type = entity.type ? entity.type.name : '',
					i,
					l,
					j,
					m,
					toTest = [],
					word,
					match;
				
				if($scope.filterQuery === '') {
					return true;
				}
					
				if(name) {
					toTest.push(name.toLowerCase());
				}
				if(desc) {
					toTest.push(desc.toLowerCase());
				}
				if(type) {
					toTest.push(type.toLowerCase());
				}
				
				for(i = 0, l = words.length, m = toTest.length; i < l; ++i) {
					word = words[i].toLowerCase();
					match = false;
					for(j = 0; j < m; ++j) {
						if(toTest[j].indexOf(word) !== -1) {
							match = true;
							break;
						}
					}
					if(!match) {
						return false;
					}
				}
				
				return true;
			};
			
			$scope.getFilteredChildren = function ( source ) {
				
				var filtered = [];
				
				function filterChildren ( parent, filterRoot ) {
					var i,
						l,
						children,
						entity;
						
						
					if(filterRoot && $scope.filterEntity(parent)) {
						filtered.push(parent);
					}
					
					children = parent.getFolders();
						
					for(i = 0, l = children.length; i < l; ++i) {
						entity = children[i];
						if($scope.filterEntity(entity)) {
							filtered.push(entity);
						}
						filterChildren(entity, true);
					}
					
					children = parent.getFiles();
					for(i = 0, l = children.length; i < l; ++i) {
						entity = children[i];
						if($scope.filterEntity(entity)) {
							filtered.push(entity);
						}
					}
				}
				
				if(!source) {
					source = $scope.root;
				}
			
				filterChildren(source, false);
				
				return filtered;
				
			};
			
			$scope.getAttachments = function ( ) {
				var attachments = [],
					selection = $scope.selectedFiles.concat(),
					file;
					
				for(var i = 0, l = selection.length; i < l; ++i) {
					file = selection[i];
					if(file.getEntityType() === 'file') {
						attachments.push(file);
					}
				}
				return attachments;
			};
			
			$scope.clearCutSelection = function ( ) {
				cutFiles.length = 0;
			};
			
			$scope.cutFiles = function ( files ) {
				var i,
					l,
					file;
					
				$scope.clearCutSelection();
				
				for(i = 0, l = files.length; i < l; ++i) {
					file = files[i];
					if(file.getEntityType() === 'file') {
						cutFiles.push(file);
					}
				}
			};
			
			$scope.pasteFiles = function ( ) {
				var target,
					files = $scope.selectedFiles;
				
				if(files.length && files[0].getEntityType() === 'folder') {
					target = files[0];
				} else if(files.length) {
					target = files[0].getParent();
				}
				
				$scope.moveEntity(cutFiles, target);
				$scope.clearCutSelection();
				
			};
			
			$scope.isCut = function ( file ) {
				return indexOf(cutFiles, file) !== -1;
			};
			
			$scope.hasCutFiles = function ( ) {
				return cutFiles && cutFiles.length;
			};
			
			$scope.isCuttable = function ( ) {
				var files = $scope.selectedFiles,
					i,
					l,
					file;
					
				for(i = 0, l = files.length; i < l; ++i) {
					file = files[i];
					if(file.getEntityType() === 'file') {
						return true;
					}
				}
				
				return false;
			};
			
			$scope.isPastable = function ( ) {
				return cutFiles.length;
			};
			
			$scope.hasFilesInSubdirectories = function ( ) {
				var selection = $scope.selectedFiles,
					file,
					root = $scope.root;
					
				for(var i = 0, l = selection.length; i < l; ++i) {
					file = selection[i];
					if(file.getParent() !== root) {
						return true;
					}
				}
				
				return false;
			};
			
			function validateEntity ( entity/*, target*/ ) {
				if(entity instanceof Folder) {
					return false;
				}
				
				return true;
			}
			
			function moveEntities ( targetFolder, data ) {
				var dropData = data,
					selectedFiles = $scope.selectedFiles;
					
				if(indexOf(selectedFiles, dropData) === -1) {
					$scope.moveEntity( [ dropData ], targetFolder);
				} else {
					$scope.moveEntity(selectedFiles, targetFolder);
				}
			}
			
			function setRootState ( ) {
				var files = $scope.root.getFiles(),
					folders = $scope.root.getFolders(),
					isSelected;
					
				if(files.length || folders.length) {
					isSelected = checkSelected($scope.root);
				} else {
					isSelected = $scope.root.getSelected();
				}
				
				$scope.root.setSelected(isSelected);
				
			}
			
			function checkSelected ( parent ) {
				var files = parent.getFiles(),
					folders = parent.getFolders(),
					i,
					l;
					
				for(i = 0, l = folders.length; i < l; ++i) {
					if(!folders[i].getSelected() || !checkSelected(folders[i])) {
						return false;
					}
				}
				
				for(i = 0, l = files.length; i < l; ++i) {
					if(!files[i].getSelected()) {
						return false;
					}
				}
				return true;
			}
			
			function getFlatFileList ( ) {
				var files = [];
					
				function getChildrenOf ( parent ) {
					var children = parent.getFolders(),
						i,
						l;
						
					files = files.concat(parent.getFiles());
					
					for(i = 0, l = children.length; i < l; ++i) {
						getChildrenOf(children[i]);
					}
				}
				
				getChildrenOf($scope.root);
				getChildrenOf($scope.list);
				
				return files;
			}
			
			$scope.$on('drop', function ( event, data, mimeType ) {
				var targetFolder = event.targetScope.entity;
				if(mimeType !== 'Files') {
					moveEntities(targetFolder, data);
				}
				$scope.dragging = false;
				$scope.$apply();
			});
			
			$scope.$on('reload', function ( ) {
				clearData();
			});
			
			$scope.$watch('filterQuery', function ( ) {
				words = $scope.filterQuery.split(' ');
			});
			
			$scope.$on('fileselect', function ( event, files ) {
				$scope.uploadFile(files);
			});
			
			$scope.$on('startdrag', function ( event, element, mimeType ) {
				
				var entity = event.targetScope.entity;
				if(entity && !$scope.isSelected(entity)) {
					$scope.deselectAll();
					$scope.selectEntity(entity);
				}
				
				$scope.dragging = mimeType;
				$scope.$apply();
			});
			
			$scope.$on('stopdrag', function ( /*event, element, mimeType*/ ) {
				$scope.dragging = '';
				$scope.$apply();
			});
			
			$scope.$on('contextmenuopen', function ( event ) {
				var entity = event.targetScope.entity;
				event.stopPropagation();
				if(!$scope.isSelected(entity)) {
					$scope.deselectAll();
					$scope.selectEntity(entity);
				}
			});
			
	}]);
})();
