/*global angular,fetch,$*/
(function ( ) {
	
	angular.module('Zaaksysteem.docs')
		.controller('nl.mintlab.docs.DocumentIntakeController', [ '$scope', 'smartHttp', function ( $scope, smartHttp ) {
			
			var File = fetch('nl.mintlab.docs.File'),
				Folder = fetch('nl.mintlab.docs.Folder');
			
			$scope.readOnly = false;
			$scope.docGlobal = new Folder( { name: 'global' } );
			$scope.loading = false;
			
			$scope.initialize = function ( ) {
				
				$scope.loading = true;
				
				smartHttp.connect({
					url: 'file/search_queue',
					method: 'GET'
				})
					.success(function ( data ) {
						var docs,
							file,
							i,
							l;
							
						docs = data.result || [];
						
						for(i = 0, l = docs.length; i < l; ++i) {
							file = new File();
							file.updateWith(docs[i]);
							$scope.docGlobal.add(file);
						}
						
					})
					.error(function ( ) {
						
					})
					.then(function ( ) {
						$scope.loading = false;
					}, function ( ) {
						$scope.loading = false;
					});
			};
			
			$scope.registerCase = function ( file ) {
				
				$('#ezra_nieuwe_zaak_tooltip').trigger({
					type: 'nieuweZaakTooltip',
					show: 1,
					popup: 1,
					action: '/zaak/create/?actie=doc_intake&amp;actie_value=' + file.id + '&amp;actie_description=Document%20aan%20zaak%20toevoegen'
				});
				
			};
			
			
		}]);
})();