/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.docs')
		.controller('nl.mintlab.docs.FileController', [ '$scope', '$window', 'smartHttp',  function ( $scope, $window, smartHttp ) {
			
			$scope.preview = false;
			$scope.collapsed = true;
			$scope.update = null;
			$scope.editMode = false;
			
			$scope.validTrustLevels = [
				'Openbaar',
				'Beperkt openbaar',
				'Intern',
				'Zaakvertrouwelijk',
				'Vertrouwelijk',
				'Confidentieel',
				'Geheim',
				'Zeer geheim'
			];
			
			$scope.validOrigins = [
				'Inkomend',
				'Uitgaand',
				'Intern'
			];
			
			
			$scope.toggleCollapse = function ( event ) {
				$scope.collapsed = !$scope.collapsed;
				if(event) {
					event.stopPropagation();
				}
			};
			
			$scope.onNameClick = function ( event ) {
				var isIntake = $scope.isIntake,
					url = isIntake ? ('zaak/intake/' + $scope.entity.id + '/download') : ('zaak/' + $scope.caseId + '/document/' + $scope.entity.id + '/download');
					
                if($scope.pip) {
                    url = 'pip/' + url;
                }

				$window.open(smartHttp.getUrl(url));
				event.stopPropagation();
			};
			
			$scope.onRemoveCaseDocClick = function ( event ) {
				$scope.setCaseDoc([ $scope.entity ], null);
				event.stopPropagation();
			};
			
			$scope.saveAttr = function ( key ) {
				var data = {
						file_id: $scope.entity.id
					};
								
				if(key.indexOf('metadata_id') === 0) {
					data.metadata = $scope.update.metadata_id;
				} else {
					data[key] = $scope.update[key];
				}
				
				$scope.entity.updating = true;
				
				smartHttp.connect({
					url: 'file/update',
					method: 'POST',
					data: data
				})
					.success(function ( data ) {
						var fileData = data.result ? data.result[0] : null;
						$scope.entity.updateWith(fileData);
						if(key === 'is_revision') {
							$scope.entity.is_revision = $scope.update.is_revision;
						}
					})
					.error(function ( /*data*/ ) {
						$scope.update = $scope.entity.clone();
					})
					.then(function ( ) {
						$scope.entity.updating = false;
					});
				
			};
			
			$scope.setEditMode = function ( editMode ) {
				$scope.editMode = editMode;
			};
			
			$scope.onPropertyFormButtonClick = function ( event ) {
				event.stopPropagation();
			};
			
			// TODO(dario): refactor common methods in StoredEntityController
			$scope.$on('editsave', function ( event/*, key, value*/ ) {
				event.stopPropagation();
			});
			
			$scope.$on('editcancel', function ( event/*, key, value*/ ) {
				event.stopPropagation();
			});
			
			$scope.$on('drop', function ( event, data/*, mimetype*/ ) {
				var caseDoc = data.caseDoc,
					clearPrevious = data.clearPrevious;
				
				$scope.setCaseDoc( [ $scope.entity ] , caseDoc, clearPrevious);
				event.stopPropagation();
			});
			
			$scope.$watch('entity', function ( ) {
				if($scope.entity && $scope.entity.is_revision === undefined) {
					$scope.entity.is_revision = $scope.entity.is_duplicate_of ? 1 : 0;
				}
				$scope.update = $scope.entity ? $scope.entity.clone() : null;
			});
			
		}]);
	
})();
