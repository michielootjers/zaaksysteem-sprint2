/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem.net')
		.service('fileUploader', [ '$q', '$window', function ( $q, $window ) {
			
			var EventDispatcher = fetch('nl.mintlab.events.EventDispatcher'),
				inherit = fetch('nl.mintlab.utils.object.inherit'),
				FileUpload = fetch('nl.mintlab.docs.FileUpload'),
				IFrameUpload = fetch('nl.mintlab.docs.IFrameUpload'),
				indexOf = fetch('nl.mintlab.utils.shims.indexOf'),
				supports = !!$window.File;
				
			function FileUploader ( ) {
				this._uploadList = [];
			}
			
			inherit(FileUploader, EventDispatcher);
			
			FileUploader.prototype.upload = function ( file, url, params ) {
				var upload = supports ? new FileUpload(file) : new IFrameUpload(file),
					deferred = $q.defer(),
					promise = deferred.promise,
					uploadList = this._uploadList;
					
				if(!supports) {
					params.return_content_type = 'text/plain';
				}
				
				uploadList.push(upload);
				
				upload.subscribe('complete', function ( ) {
					deferred.resolve(upload);
				});
				upload.subscribe('error', function ( ) {
					deferred.reject(upload);
				});
				upload.subscribe('end', function ( ) {
					uploadList.splice(indexOf(uploadList, upload), 1);
				});
				
				this.publish('fileadd', upload);
				upload.send(url, params);
				
				return promise;
			};
			
			FileUploader.prototype.getUploadList = function ( ) {
				return this._uploadList;
			};
			
			FileUploader.prototype.supports = function ( ) {
				return supports;	
			};
			
			
			return new FileUploader();
			
			
		}]);
	
})();