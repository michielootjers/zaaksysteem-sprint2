/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.docs')
		.controller('nl.mintlab.docs.VersionController', [ '$scope', 'smartHttp', function ( $scope, smartHttp ) {
			
			$scope.revisions = [];
			
			$scope.revertTo = function ( revision ) {
				
				$scope.entity.updating = true;
				
				smartHttp.connect( {
					url: 'file/revert_to',
					method: 'POST',
					data: {
						file_id: revision.file_id
					}
				})
					.success(function ( data ) {
						var fileData = data.result ? data.result[0] : null;
						
						$scope.entity.updateWith(fileData);
						
						reloadData();
					})
					.error(function ( ) {
						
					})
					.then(function ( ) {
						$scope.entity.updating = false;
					})
			};
			
			function reloadData ( ) {
			
				smartHttp.connect( {
					url: 'file/version_info/file_id/' + $scope.entity.id
				})
					.success(function ( data ) {
						var revs = data.result || [];
						$scope.revisions = revs;
					})
					.error(function ( error ) {
						
					});
					
			}
			
			reloadData();
			
		}]);
	
})();