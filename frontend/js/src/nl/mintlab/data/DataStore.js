/*global angular,fetch,window,setTimeout*/
(function ( ) {
	
	angular.module('Zaaksysteem.data')
		.provider('dataStore', [ function ( ) {
			
			var	win = window,
				isArray = angular.isArray,
				EventDispatcher = fetch('nl.mintlab.events.EventDispatcher'),
				data = {},
				listeners = {};
			
			function set ( id, d ) {
				data[id] = d;
				getListener(id).trigger();
			}
			
			function get ( id ) {
				return data[id];
			}
			
			function unset ( id ) {
				set(id, undefined);
				delete data[id];
			}
			
			function push ( id, d ) {
				var arr = data[id];
				if(!isArray(arr)) {
					arr = [];
				}
				arr.push(d);
				set(id, arr);
			}
			
			function observe ( id, callback ) {
				setTimeout(function ( ) {
					getListener(id).trigger();
				},0);
				return getListener(id).observe(callback);
			}
			
			function getListener ( id ) {
				var listener = listeners[id];
				if(!listener) {
					listener = listeners[id] = createListener(id);
				}
				return listener;
			}
			
			function createListener ( id ) {
				var dispatcher = new EventDispatcher();
				return {
					observe: function ( callback ) {
						dispatcher.subscribe('dataChange', callback);
						return function ( ) {
							dispatcher.unsubscribe('*', callback);
						};
					},
					trigger: function ( ) {
						dispatcher.publish('dataChange', get(id));
					}
				};
			}
			
			
			var dataStore = {
				get: get,
				set: set,
				unset: unset,
				push: push,
				observe: observe
			};
			
			win.dataStore = dataStore;
			
			return {
				$get: function ( ) {
					return dataStore;
				}
			};
			
			
		}]);
	
})();