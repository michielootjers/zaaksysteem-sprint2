/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.case', [ 'Zaaksysteem.net', 'Zaaksysteem.locale' ]);
	
})();