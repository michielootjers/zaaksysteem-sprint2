/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.case')
		.controller('nl.mintlab.case.SidebarController', [ '$scope', 'smartHttp', function ( $scope, smartHttp ) {
			
			$scope.caseId = null;
			$scope.actions = [];
			$scope.checklistItems = [];
			$scope.caseDocs = [];
			
			function getData ( ) {
				
				smartHttp.connect({
					method: 'GET',
					url: 'zaak/' + $scope.caseId + '/actions',
					params: {
						milestone: $scope.phaseId
					}
				})
					.success(function ( data) {
						$scope.actions = data.result || [];
					})
					.error(function ( /*data*/ ) {
						
					});
					
				if($scope.showChecklist) {
					smartHttp.connect({
						method: 'GET',
						url: 'zaak/' + $scope.caseId + '/checklist/view',
						params: {
							milestone: $scope.phaseId
						}
					})
						.success(function ( data ) {
							$scope.checklistItems = data.result || [];
						})
						.error(function ( /*data*/ ) {
							
						});
				}
				
				smartHttp.connect( {
					url: 'zaak/' + $scope.caseId + '/case_type_documents',
					method: 'GET'
				})
					.success(function ( data ) {
						$scope.caseDocs = data.result;
					})
					.error(function ( /*data*/ ) {
						
					});
			}
			
			$scope.reloadData = function ( ) {
				getData();
			};
			
			$scope.init = function ( caseId, phaseId, closed, showChecklist ) {
				$scope.caseId = caseId;
				$scope.phaseId = phaseId;
				$scope.closed = closed;
				$scope.showChecklist = showChecklist;
				// getData();
			};
			
		}]);
	
})();