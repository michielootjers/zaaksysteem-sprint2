/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.case')
		.controller('nl.mintlab.case.CaseController', [ '$scope', 'smartHttp', 'translationService', function ( $scope, smartHttp, translationService ) {
			
			$scope.templates = [];
			
			function loadTemplates ( ) {
				var caseId = $scope.caseId;
				
				smartHttp.connect({
					url: 'zaak/' + caseId + '/get_sjablonen',
					method: 'GET'
				})
					.success(function onSuccess ( data ) {
						$scope.templates = data.result;
					})
					.error(function onError ( /*data*/ ) {
						$scope.$emit('systemMessage', {
							content: translationService.get('Fout bij het laden van de sjablonen'),
							type: 'error'
						});
					});
			}
			
			$scope.init = function ( caseId ) {
				$scope.caseId = caseId;
				loadTemplates();
			};
			
		}]);
	
})();