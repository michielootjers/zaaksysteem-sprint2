/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem.case')
		.controller('nl.mintlab.case.CaseRelationController', [ '$scope', 'smartHttp', function ( $scope, smartHttp ) {
			
			var indexOf = fetch('nl.mintlab.utils.shims.indexOf'),
				arrayMove = fetch('nl.mintlab.utils.collection.arrayMove');
			
			$scope.relations = [];
			
			function parseData ( data ) {
				var relations = data.result || [],
					i,
					l,
					rel;
					
				for(i = 0, l = relations.length; i < l; ++i) {
					rel = relations[i];
					rel.order = i;
				}
				
				$scope.relations = relations;
				
			}
			
			$scope.reloadData = function ( ) {
				smartHttp.connect( {
					method: 'GET',
					url: 'zaak/' + $scope.caseId + '/relations'
				})
					.success(function onSuccess ( data ) {
						parseData(data);
					})
					.error(function onError ( ) {
					});
			};
			
			$scope.init = function ( ) {
				$scope.reloadData();	
			};
			
			$scope.relate = function ( caseObj ) {
				
				var caseId = caseObj.id,
					relation;
					
					
				relation = {
					'case': caseObj
				};
				
				$scope.relations.push(relation);
				
				smartHttp.connect({
					method: 'POST',
					url: 'zaak/' + $scope.caseId + '/relations/add',
					data: {
						case_id: caseId
					}
				})
					.success(function onSuccess ( data ) {
						var result = data.result[0];
						for(var key in result) {
							relation[key] = result[key];			
						}
					})
					.error(function onError ( /*data*/ ) {
						var index = indexOf($scope.relations, relation);
						if(index !== -1) {
							$scope.relations.splice(index, 1);
						}
					});
				
			};
			
			$scope.unrelate = function ( relation ) {
				var relationId = relation.id,
					index = indexOf($scope.relations, relation);
					
				if(index !== -1) {
					$scope.relations.splice(index, 1);
				}
				
				smartHttp.connect({
					method: 'POST',
					url: 'zaak/' + $scope.caseId + '/relations/remove',
					data: {
						relation_id: relationId
					}
				})
					.success(function onSuccess ( /*data*/ ) {
					})
					.error(function onError ( /*data*/ ) {
						$scope.relations.push(relation);
					});
			};
			
			$scope.$on('sort', function ( event, data, from, to ) {
				
				var after = $scope.relations[to-1],
					afterId = after ? after.id : null;
				
				smartHttp.connect({
					url: 'zaak/' + $scope.caseId + '/relations/move',
					method: 'POST',
					data: {
						relation_id: data.id,
						after: afterId
					}
				})
					.success(function onSuccess ( /*data*/ ) {
						
					})
					.error(function onError ( data ) {
						arrayMove($scope.relations, data, from);
					});
				
			});
			
			
			
		}]);
})();