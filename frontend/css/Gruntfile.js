/*global module*/

module.exports = function ( grunt ) {
	
	grunt.initConfig({
		loc: {
			css: '../../root/css'
		},
		watch: {
			compass: {
				files: [ 'src/**/*.*' ],
				tasks: [ 'compass' ],
				options: {
					livereload: false,
					nospawn: true
				}
			},
			css: {
				files: [ '../../root/**/*.css' ],
				tasks: [ 'noop' ],
				options: {
					livereload: true,
					nospawn: true
				}
			},
			options: {
				nospawn: true
			}
		},
		noop: {
			
		},
		compass: {
			build: {
				options: {
					sassDir: 'src/',
					cssDir: '../../root/tpl/zaak_v1/nl_NL/css/',
					outputStyle:'expanded',
					noLineComments: true
				}
			}
		}
		
	});
	
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-compass');
	
	grunt.registerTask('default', [ 'watch' ] );
	grunt.registerTask('noop', function ( ) {
		
	});
	
};