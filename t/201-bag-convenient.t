#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;

my $zs = bless {schema => connect_test_db_ok()}, 'Zaaksysteem::TestUtils';
my $schema = $zs->schema;
### Test header end

use File::Basename;

sub _create_bag_records {
    my $schema      = shift;

    $schema->resultset('BagNummeraanduiding')->create(
        {
            identificatie   => '9876543218375842',
            begindatum      => '201301010000',
            huisnummer      => 23,
            officieel       => 'N',
            huisletter      => 'A',
            huisnummertoevoeging => '1rec',
            postcode        => '1051JL',
            woonplaats      => '9001',
            inonderzoek     => 'N',
            openbareruimte  => '1234567890123456',
            type            => 'verblijfsobject',
            documentdatum   => '',
            documentnummer  => '',
            status          => 'Naamgeving uitgegeven',
            correctie       => 'N'
        }
    );

    $schema->resultset('BagOpenbareruimte')->create(
        {
            identificatie   => '1234567890123456',
            begindatum      => '201301010000',
            naam            => 'Donker Curtiusstraat',
            officieel       => 'N',
            woonplaats      => '9001',
            inonderzoek     => 'N',
            type            => 'Weg',
            documentdatum   => '',
            documentnummer  => '',
            status          => 'Naamgeving uitgegeven',
            correctie       => 'N'
        }
    );

    $schema->resultset('BagWoonplaats')->create(
        {
            identificatie   => '9001',
            begindatum      => '201301010000',
            naam            => 'Amsterdam',
            officieel       => 'N',
            inonderzoek     => 'N',
            documentdatum   => '',
            documentnummer  => '',
            correctie       => 'N',
            status          => 'Naamgeving uitgegeven',
        }
    );
}


$zs->zs_transaction_ok(sub {
    _create_bag_records($schema);

    my $record = $schema->resultset('BagNummeraanduiding')->get_record_by_source_identifier(
        'nummeraanduiding-9876543218375842'
    );

    is ($record->identificatie, '9876543218375842',
        'found belonging record nummeraanduiding'
    );

    $record = $schema->resultset('BagNummeraanduiding')->get_record_by_source_identifier(
        'openbareruimte-1234567890123456'
    );

    is ($record->identificatie, '1234567890123456',
        'found belonging record openbareruimte'
    );

    $record = $schema->resultset('BagNummeraanduiding')->get_record_by_source_identifier(
        'woonplaats-9001'
    );

    is ($record->identificatie, '9001',
        'found belonging record woonplaats'
    );
}, 'bag: get_record_by_source_identifier find objects');


$zs->zs_transaction_ok(sub {
    _create_bag_records($schema);

    my $record  = $schema
                ->resultset('BagNummeraanduiding')
                ->get_address_data_by_source_identifier(
                    'nummeraanduiding-9876543218375842'
                );

    is (
        grep (
            { defined($record->{ $_ }) && $record->{ $_ } }
            qw/
                huisletter
                huisnummer
                huisnummertoevoeging
                postcode
                straat
                woonplaats
            /
        ),
        6,
        'Got complete nummeraanduiding'
    );
}, 'bag: get_address_data_by_source_identifier by nummeraanduiding');


$zs->zs_transaction_ok(sub {
    _create_bag_records($schema);

    my $record  = $schema
                ->resultset('BagNummeraanduiding')
                ->get_address_data_by_source_identifier(
                    'openbareruimte-1234567890123456'
                );

    is (
        grep (
            { defined($record->{ $_ }) && $record->{ $_ } }
            qw/
                huisletter
                huisnummer
                huisnummertoevoeging
                postcode
                straat
                woonplaats
            /
        ),
        2,
        'Got complete openbareruimte'
    );
}, 'bag: get_address_data_by_source_identifier by openbareruimte');


$zs->zs_transaction_ok(sub {
    _create_bag_records($schema);

    my $record  = $schema
                ->resultset('BagNummeraanduiding')
                ->get_human_identifier_by_source_identifier(
                    'nummeraanduiding-9876543218375842'
                );

    is (
        $record,
        'Donker Curtiusstraat 23A-1rec',
        'Got human identifier'
    );
}, 'bag: get_human_identifier_by_source_identifier by nummeraanduiding');


$zs->zs_transaction_ok(sub {
    _create_bag_records($schema);

    my $record  = $schema
                ->resultset('BagNummeraanduiding')
                ->get_human_identifier_by_source_identifier(
                    'nummeraanduiding-9876543218375842',
                    {
                        prefix_with_city => 1
                    }
                );

    is (
        $record,
        'Amsterdam - Donker Curtiusstraat 23A-1rec',
        'Got human identifier'
    );
}, 'bag: get_human_identifier_by_source_identifier by nummeraanduiding with city');


zs_done_testing();
