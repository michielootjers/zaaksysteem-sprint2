#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;

my $zs = bless {schema => connect_test_db_ok()}, 'Zaaksysteem::TestUtils';
my $schema = $zs->schema;
### Test header end


$zs->zs_transaction_ok(sub {
    my $file = $zs->create_file_ok;

    is $file->get_root_file->id, $file->id, 'Got self as root';
}, 'get_root_file on first version');


$zs->zs_transaction_ok(sub {
    my $file = $zs->create_file_ok;

    my $result = $file->update_file({
        subject       => $zs->get_subject_ok,
        original_name => 'Original Name.jpg',
        new_file_path => $zs->config->{filestore_test_file_path}
    });
    is $file->get_root_file->id, $file->id, 'Got self as root';
    is $result->get_root_file->id, $file->id, 'Got first as root from second';
}, 'get_root_file with second version');


$zs->zs_transaction_ok(sub {
    my $file = $zs->create_file_ok;
    my $case = $zs->get_case_ok;

    my $result = $file->update_file({
        subject       => $zs->get_subject_ok,
        original_name => 'Original Name.jpg',
        new_file_path => $zs->config->{filestore_test_file_path}
    });
    is $result->id, $file->get_last_version->id, 'Got last version';
}, 'get_last_version');


$zs->zs_transaction_ok(sub {
    my $file = $zs->create_file_ok;
    my $case = $zs->get_case_ok;

    my $update = $file->update_file({
        subject       => $zs->get_subject_ok,
        original_name => 'Original Name.jpg',
        new_file_path => $zs->config->{filestore_test_file_path}
    });
    ok $update, 'Update to new file';

    my $result = $file->make_leading($zs->get_subject_ok);
    is $result->id, $file->get_last_version->id, 'File is now the last version';
}, 'make_leading');


zs_done_testing();