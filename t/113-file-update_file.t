#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;

my $zs = bless {schema => connect_test_db_ok()}, 'Zaaksysteem::TestUtils';
my $schema = $zs->schema;
### Test header end

$zs->zs_transaction_ok(sub {
    my $file = $zs->create_file_ok;
    my $case = $zs->get_case_ok;
    my $subject  = $zs->get_subject_ok;
    my $new_name = 'I am Batman.doc.docx';

    # Add some extra properties
    $file->update_properties({
        subject      => $subject,
        directory_id => $zs->create_directory_ok,
        metadata     => { description => 'Yayayayaya' },
    });
    ok $file->metadata->id, 'File has metadata set';
    ok $file->directory->id, 'File has directory set';

    my $directory_id = $file->directory->id;

    my $result = $file->update_file({
        subject       => $subject,
        original_name => $new_name,
        new_file_path => $zs->config->{filestore_test_file_path}
    });

    ok $result, 'Updated file';
    is $result->name.$result->extension, $new_name, 'Name + extension matches new name';
    is $result->metadata->description, $file->metadata->description, 'Description copied';
    is $result->directory->id, $directory_id, 'Directory copied';
    ok !$file->directory, 'Cleared directory on old file';
    is $result->filestore->original_name, $new_name, 'original_name set to new file name';
    isnt $result->filestore->id, $file->filestore->id, 'Filestore row is a new entry';
    is $result->date_created, DateTime->now, 'Date created is now';
    is $result->created_by, $subject, 'Given subject is set in created_by';
    is $result->date_modified, DateTime->now, 'Date modified is now';
    is $result->modified_by, $subject, 'Given subject is set in modified_by';
    is $result->root_file->id, $file->id, 'File points to the root file';
    is $result->version, 2, 'Version was raised';
    like $result->creation_reason,
        qr/Document.*FilestoreTest.*versie\ 1.*Batman.*versie\ 2/,
        'Correct creation reason';
}, 'update_file');


$zs->zs_transaction_ok(sub {
    my $file = $zs->create_file_ok;
    my $case = $zs->get_case_ok;

    my $result = $file->update_file({
        subject       => $zs->get_subject_ok,
        original_name => 'Original Name.jpg',
        new_file_path => $zs->config->{filestore_test_file_path}
    });
    ok $result, 'Updated file';
    is $result->root_file_id->id, $file->id, 'File points to the root file';
    is $result->version, 2, 'Version was raised';
    isnt $result->filestore->id, $file->filestore->id, 'Has new filestore entry';

    throws_ok sub{
        $file->update_file({
            subject       => $zs->get_subject_ok,
            original_name => 'Original Name.jpg',
            new_file_path => $zs->config->{filestore_test_file},
        })
    }, qr/is at version 2, can't modify/, 'Updating an old file fails';
}, 'update_file on old version');


$zs->zs_transaction_ok(sub {
    my $file = $zs->create_file_ok;
    my $case = $zs->get_case_ok;
    my $subject  = $zs->get_subject_ok;

    # Add some extra properties
    $file->update_properties({
        subject      => $subject,
        directory_id => $zs->create_directory_ok,
        metadata     => { description => 'Yayayayaya' },
    });
    ok $file->metadata->id,  'File has metadata set';
    ok $file->directory->id, 'File has directory set';

    my $directory_id = $file->directory->id;

    my $first_update = $file->update_file({
        subject       => $subject,
        original_name => 'Clever Girl.docx',
        new_file_path => $zs->config->{filestore_test_file_path}
    });
    ok $first_update, 'First update done';
    my $second_update = $first_update->update_file({
        subject       => $subject,
        original_name => 'Gordon Freeman.jpg',
        new_file_path => $zs->config->{filestore_test_file_path}
    });
    ok $second_update, 'Second update done';

    $second_update->update({directory_id => $zs->create_directory_ok});
    ok $second_update->directory, 'Set new directory';

    my $result = $file->make_leading($subject);
    ok $result, 'Made the initial file leading';
    is $result->name, $file->name, 'Name copied';
    is $result->metadata->description, $file->metadata->description, 'Description copied';
    is $result->directory->id, $second_update->directory->id, 'Last used directory copied';
    ok !$second_update->discard_changes->directory, 'Cleared directory on second update (used to be last)';
    is $result->filestore->original_name, $file->name.$file->extension, 'original_name set to file name';
    isnt $result->filestore->id, $file->filestore->id, 'Filestore row is a new entry';
    is $result->date_created, DateTime->now, 'Date created is now';
    is $result->created_by, $subject, 'Given subject is set in created_by';
    is $result->date_modified, DateTime->now, 'Date modified is now';
    is $result->modified_by, $subject, 'Given subject is set in modified_by';
    is $result->root_file->id, $file->id, 'File points to the root file';
    is $result->version, 4, 'Version was raised';

    like $result->creation_reason,
        qr/Gordon.*versie 3.*vervangen/, 'Correct creation reason';
}, 'update_file with is_restore (make_leading)');


$zs->zs_transaction_ok(sub {
    my $subject = $zs->get_subject_ok;
    my $file    = $zs->create_file_ok;
    $file->update_properties({
        accepted => 1,
        subject  => $subject,
        case_type_document_id => $zs->get_case_type_document_ok->id,
    });
    my $second_file = $zs->create_file_ok;
    ok !$second_file->accepted, 'Second file is not accepted';

    ok $file->update_existing({
        subject => $subject,
        existing_file_id => $second_file->id,
    }), 'Replaced file';
    $second_file->discard_changes;
    is $second_file->root_file->id, $file->id, 'Files are now related';
    is $file->name, $second_file->name, 'Names match';
    ok $second_file->accepted, 'File got accepted';
    ok $second_file->case_type_document, 'CTD copied';
}, 'update_existing should not rename');


$zs->zs_transaction_ok(sub {
    my $subject   = $zs->get_subject_ok;
    my $file      = $zs->create_file_ok;
    my $directory = $zs->create_directory_ok;
    $file->update_properties({
        accepted     => 1,
        subject      => $subject,
        directory_id => $directory->id,
        case_type_document_id => $zs->get_case_type_document_ok->id,
    });
    my $second_file = $zs->create_file_ok;
    ok !$second_file->accepted, 'Second file is not accepted';

    ok $file->update_existing({
        subject => $subject,
        existing_file_id => $second_file->id,
    }), 'Replaced file';
    $second_file->discard_changes;
    ok !$file->directory_id, 'Old file directory removed';
    is $second_file->directory->id, $directory->id, 'Second file has correct directory set';
}, 'update_existing should set deleted on old');


$zs->zs_transaction_ok(sub {
    my $subject   = $zs->get_subject_ok;
    my $file      = $zs->create_file_ok;
    
    ok $file->update_properties({
        subject => $subject,
        publish_pip => 1,
        publish_website => 1,
    }), 'Set PIP publish false, website publish true';

    my $second_file = $zs->create_file_ok;
    ok !$second_file->publish_pip, 'PIP publish is false';
    ok !$second_file->publish_website, 'Website publish is false';

    ok $file->update_existing({
        subject => $subject,
        existing_file_id => $second_file->id,
    }), 'Replaced file';
    $second_file->discard_changes;
    ok $second_file->publish_pip, 'PIP publish is true';
    ok $second_file->publish_website, 'Website publish is true';
}, 'update_existing should copy publish values');


zs_done_testing();