#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;

my $zs = bless {schema => connect_test_db_ok()}, 'Zaaksysteem::TestUtils';
my $schema = $zs->schema;
### Test header end


$zs->zs_transaction_ok(sub {
    my $case    = $zs->get_case_ok;
    my $subject = $zs->get_subject_ok;

	my $result = $schema->resultset('File')->file_create({
        file_path => $zs->config->{filestore_test_file_path},
        name      => 'Ze_Fallen_Madonna.jpg',
        db_params => {
            publish_website => 1,
            publish_pip     => 1,
            created_by      => $subject,
            case_id         => $case->id,
        },
        metadata => {
            description => 'With the large.. yes.',
        },
    });

    ok $result, 'create succeeds';
    ok $result->filestore, 'Has filestore set';
    ok !$result->reject_to_queue, 'reject_to_queue is false';
    is $result->created_by, $subject, 'Correct created subject';
    ### Date_created is in TODO test below, place here when fixed
    is $result->modified_by, $subject, 'Correct modified subject';
    is $result->date_modified, DateTime->now, 'Correct modified date';
    is $result->case->id, $case->id, 'Case is set properly';
    ok $result->publish_pip, 'PIP publishing set to true';
    ok $result->publish_website, 'Website publishing set to true';
    like $result->creation_reason, qr/Ze_Fallen.*toegevoegd/, 'Correct creation reason';
    is $result->name, 'Ze_Fallen_Madonna';
    is $result->extension, '.jpg';
    like $result->metadata->description, qr/With the large/, 'Correct metadata description';
    is $result->search_term, $result->name, 'Search term has name set';
    
    TODO: {
        local $TODO = 'This returns in the wrong time zone for some odd reason. Probable related to the creative handling of time zones in various bits of code.';
        is $result->date_created, DateTime->now, 'Correct created date';
    }
}, 'file_create');


$zs->zs_transaction_ok(sub {
    my $case    = $zs->get_case_ok;
    my $subject = $zs->get_subject_ok;

    throws_ok sub {
        $schema->resultset('File')->file_create({
            file_path => $zs->config->{filestore_test_file_path},
            name      => 'What',
            db_params => {
                created_by      => $subject,
                case_id         => $case->id,
            },
        });
    }, qr/File does not have an extension/, 'No extension dies';
}, 'file_create file without extension');


$zs->zs_transaction_ok(sub {
    my $case    = $zs->get_case_ok;
    my $subject = $zs->get_subject_ok;

    ok $schema->resultset('File')->file_create({
        file_path        => $zs->config->{filestore_test_file_path},
        name             => '!@&#-%^$_.doc',
        ignore_extension => 1,
        db_params => {
            created_by      => $subject,
            case_id         => $case->id,
        },
     }), 'Created file with special characters';
}, 'file_create file with special characters');


$zs->zs_transaction_ok(sub {
    my $case    = $zs->get_case_ok;
    my $subject = $zs->get_subject_ok;

    throws_ok sub {
        $schema->resultset('File')->file_create({
            file_path        => $zs->config->{filestore_test_file_path},
            name             => 'What',
            ignore_extension => 1,
            db_params => {
                created_by      => $subject,
                case_id         => $case->id,
            },
        });
    }, qr/ignore_extension was set.*no extension at all/, 'No extension dies';
}, 'file_create file without extension and ignore_extension true');


$zs->zs_transaction_ok(sub {
    my $case    = $zs->get_case_ok;
    my $subject = $zs->get_subject_ok;

    throws_ok sub {
        $schema->resultset('File')->file_create({
            file_path => $zs->config->{filestore_test_file_path},
            name      => 'What.cod',
            db_params => {
                created_by      => $subject,
                case_id         => $case->id,
            },
        });
    }, qr/Bestandstype niet toegestaan: .cod/, 'No extension dies';
}, 'file_create file with invalid extension');


$zs->zs_transaction_ok(sub {
    my $case    = $zs->get_case_ok;
    my $subject = $zs->get_subject_ok;

    my $result = $schema->resultset('File')->file_create({
        file_path         => $zs->config->{filestore_test_file_path},
        name              => 'What.cod',
        ignore_extension  => 1,
        db_params => {
            created_by      => $subject,
            case_id         => $case->id,
        },
    });
    ok $result, 'File created';
}, 'file_create file with invalid extension and ignore_extension true');


$zs->zs_transaction_ok(sub {
    my $subject = $zs->get_subject_ok;

    my $result = $schema->resultset('File')->file_create({
        file_path         => $zs->config->{filestore_test_file_path},
        name              => 'What.doc',
        db_params => {
            created_by      => $subject,
        },
    });
    ok $result, 'File created';
    ok $result->reject_to_queue, 'reject_to_queue is true';
}, 'file_create file without case_id or subject_id has reject_to_queue set');


$zs->zs_transaction_ok(sub {
    my $file = $zs->create_file_ok;
    ok $file->update({accepted => 1}), 'Accepted first file';
    my $second_file = $zs->create_file_ok;

    ok !$file->is_duplicate_name, 'First file does not have duplicate name set';
    ok  $second_file->is_duplicate_name, 'Second file has duplicate name set';
}, 'file_create existing filename');


$zs->zs_transaction_ok(sub {
    my $file = $zs->create_file_ok;
    my $second_file = $zs->create_file_ok;

    ok !$file->is_duplicate_name, 'First file does not have duplicate name set';
    ok  !$second_file->is_duplicate_name, 'Second file does not have duplicate name set';
}, 'file_create existing filename not set for unaccepted');

$zs->zs_transaction_ok(sub {
    my $file = $zs->create_file_ok;
    ok $file, 'Created first file';
    ok $file->update_properties({deleted => 1, subject => $zs->get_subject_ok});
    ok $file->date_deleted, 'Set deleted on first file';
    my $second_file = $zs->create_file_ok;
    ok $second_file, 'Created second file';
    ok !$second_file->is_duplicate_name, 'Second file does not have is_duplicate_name set';
}, 'file_create existing filename with deleted file');

$zs->zs_transaction_ok(sub {
    my $filestore = $zs->create_file_ok->filestore;
    
    my $result = $schema->resultset('File')->file_create({
        db_params => {
            created_by    => $zs->get_subject_ok,
            filestore_id  => $filestore->id,
            publish_pip   => 1,
        },
        file_path          => $zs->config->{filestore_test_file_path},
        name               => 'ADRIAAAAAAAAN.jpg',
    });

    ok $result, 'Created file';
    is $result->filestore->id, $filestore->id, 'File has correct filestore';
}, 'file_create existing filestore');

$zs->zs_transaction_ok(sub {
    my $result = $schema->resultset('File')->file_create({
        db_params => {
            created_by    => $zs->get_subject_ok,
        },
        file_path => $zs->config->{filestore_test_file_path},
        name      => 'Publish Types 4 LIFE YO.jpg',
    });
    ok !$result->publish_pip, 'Correct default publish_pip';
    ok !$result->publish_website, 'Correct default publish_website';
}, 'file_create default publish_pip and publish_website');

$zs->zs_transaction_ok(sub {
    my $result = $schema->resultset('File')->file_create({
        db_params => {
            created_by    => $zs->get_subject_ok,
        },
        file_path => $zs->config->{filestore_test_file_path},
        name      => 'pip-doc.docx',
    });
    is $result->name, 'pip-doc', 'Name got parsed properly';
}, 'file_create with double extension in filename');

$zs->zs_transaction_ok(sub {
    my $result = $schema->resultset('File')->file_create({
        db_params => {
            created_by    => $zs->get_subject_ok,
        },
        file_path => $zs->config->{filestore_test_file_path},
        name      => 'pip.doc.docx',
    });
    is $result->name, 'pip.doc', 'Name got parsed properly';
}, 'file_create with double dot in filename');

$zs->zs_transaction_ok(sub {
    my $result = $schema->resultset('File')->file_create({
        db_params => {
            created_by    => $zs->get_subject_ok,
        },
        file_path => $zs->config->{filestore_test_file_path},
        name      => '!@#$%^&.docx',
    });
    is $result->name, '!@#$%^&', 'Name got parsed properly';
}, 'file_create with unusual characters in filename');

$zs->zs_transaction_ok(sub {
    throws_ok sub {
        $schema->resultset('File')->file_create()
    }, qr/invalid input/, 'file_create() noargs';

    throws_ok sub {
        $zs->create_file_ok->update_properties()
    }, qr/invalid input/, 'update_properties() noargs';

    throws_ok sub {
        $zs->create_file_ok->update_file()
    }, qr/invalid input/, 'update_file() noargs';
}, 'noargs tests');

$zs->zs_transaction_ok(sub {
    my $case_type_document = $zs->get_case_type_document_ok;
    ok $case_type_document->update({
        pip => 1,
        publish_public => 0,
    }), 'PIP publish true, public publish false';

    my $result = $schema->resultset('File')->file_create({
        db_params => {
            created_by              => $zs->get_subject_ok,
            case_type_document_id   => $case_type_document->id,
        },
        file_path               => $zs->config->{filestore_test_file_path},
        name                    => 'case_type_document.txt',
    });
    is $result->case_type_document->id, $case_type_document->id, 'case_type_document set';
    ok $result->publish_pip, 'Publish PIP is true';
    ok !$result->publish_website, 'Publish website is false';
}, 'file_create with case_type_document_id');

$zs->zs_transaction_ok(sub {

    my $case_type_document = $zs->get_case_type_document_ok->id;

    my $result = $schema->resultset('File')->file_create({
        db_params => {
            created_by              => $zs->get_subject_ok,
            case_type_document_id   => $case_type_document,
        },
        file_path               => $zs->config->{filestore_test_file_path},
        name                    => 'case_type_document.txt',
    });

    my $second_file = $schema->resultset('File')->file_create({
        db_params => {
            created_by              => $zs->get_subject_ok,
            case_type_document_id   => $case_type_document,
        },
        file_path               => $zs->config->{filestore_test_file_path},
        name                    => 'case_type_document.txt',
    });
    is $second_file->case_type_document->id, $result->case_type_document->id,
        'Second file has the same CTD set as the first file';
}, 'file_create with case_type_document_id when other exists');

$zs->zs_transaction_ok(sub {
    my $case_type_document = $zs->get_case_type_document_ok->id;

    my $result = $schema->resultset('File')->file_create({
        db_params => {
            created_by              => $zs->get_subject_ok,
            case_type_document_id   => $case_type_document,
        },
        file_path               => $zs->config->{filestore_test_file_path},
        name                    => 'case_type_document.txt',
    });

    $result = $schema->resultset('File')->file_create({
        db_params => {
            created_by                      => $zs->get_subject_ok,
            case_type_document_id           => $case_type_document,
        },
        case_type_document_clear_old    => 1,
        file_path               => $zs->config->{filestore_test_file_path},
        name                    => 'case_type_document.txt',
    });

    is (
        $schema->resultset('File')->search(
            {
                case_type_document_id   => $case_type_document,
            }
        )->count, 1, 'Found exactly one document with case_type_document'
    );
}, 'file_create overwrite case_type_document_id when other exists');

zs_done_testing();
