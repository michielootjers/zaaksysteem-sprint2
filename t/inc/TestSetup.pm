package TestSetup;

use warnings;
use strict;

use Config::Auto;
use DBI;
use POSIX qw(strftime);
use Params::Check qw(check);
use Test::Builder::Module;
use Test::Exception;
use Test::More;
use Test::NoWarnings ();
use Zaaksysteem::Schema;
use Zaaksysteem::TestUtils;

use base 'Test::Builder::Module';
my $tb = __PACKAGE__->builder;

our @EXPORT = (
    qw(
        connect_test_db_ok
        remove_test_db_ok
        zs_done_testing
        zs_transaction_ok

        read_test_config
    ),
    @Test::More::EXPORT,
    @Test::Exception::EXPORT,
    @Test::NoWarnings::EXPORT,
    @Zaaksysteem::TestUtils::EXPORT,
);

# Connect to the test database, create one if no mytest is found.
sub connect_test_db_ok {
    my @credentials;
    if (-e '.mytest') {
        @credentials = _read_mytest();
    }
    else {
        @credentials = _create_mytest();
    }

    my $schema = Zaaksysteem::Schema->connect(
        @credentials,
        {AutoCommit => 1},
    );

    $tb->ok($schema, "Connected to database $credentials[0]");

    my $req_config = $schema->resultset('Config')->search({
        -or => [
            parameter => 'filestore_location',
            parameter => 'tmp_location',
        ],
    });

    if (!$req_config->count) {
        my $filestore_location = $schema->resultset('Config')->create({
            parameter => 'filestore_location',
            value     => 't/inc/Teststore',
        });
        $tb->ok($filestore_location, 'Added filestore_location to config');
        my $tmp_location = $schema->resultset('Config')->create({
            parameter => 'tmp_location',
            value     => 't/inc/tmp',
        });
        $tb->ok($tmp_location, 'Added tmp_location to config');
    }

    return $schema;
}

sub _read_mytest {
    my $config = Config::Auto::parse('./.mytest');

    my $args = check(
        {
            dsn         => {required => 1},
            db_user     => {required => 1},
            db_password => {required => 1},
        },
        $config,
    );


    return ($args->{dsn}, $args->{db_user}, $args->{db_password});
}

sub _create_mytest {
    my $args = read_test_config() or die('Could not read test_config');
    my $dbh = _connect_template($args);

    my $unix_username = getpwuid($<);
    my $dbname = strftime("zaaksysteem_test_${unix_username}_%Y%m%d_%H%M%S", localtime);

    $dbh->do("CREATE DATABASE $dbname ENCODING 'UTF-8'");
    $dbh->disconnect();

    {
        local $ENV{PGOPTIONS} = "--client-min-messages=warning";
        local $ENV{PGUSER} = $args->{db_user};
        local $ENV{PGPASSWORD} = $args->{db_password};
        local $ENV{PGDATABASE} = $dbname;
        local $ENV{PGHOST};

        if ($args->{db_host}) {
            $ENV{PGHOST} = $args->{db_host};
        }

        if (
            system(qw(
                psql
                    -X
                    -q 
                    --single-transaction
                    --pset pager=off
                    --set ON_ERROR_STOP=1
                    -f db/test-template.sql
            )) != 0
        ) {
            $tb->BAIL_OUT("Could not load database schema: $@");
        }
    }

    open my $mytest, '>', '.mytest';

    my $real_dsn = "dbi:Pg:dbname=$dbname";
    if ($args->{db_host}) {
        $real_dsn .= ";host=$args->{db_host}";
    }
    print $mytest "dsn = $real_dsn\n";
    print $mytest "db_user = $args->{db_user}\n";
    print $mytest "db_password = $args->{db_password}\n";
    close $mytest;

    return _read_mytest();
}

# Set the test-plan automatically and include test-lib(s)
sub import {
    require lib;
    lib->import('t/inc');

    Test::NoWarnings->import();
    goto &Test::Builder::Module::import;
}



# Clean up the test database. Should really only be called by 999-cleanup.t
sub remove_test_db_ok {
    ### TODO: maybe let it figure out the handle by itself somehow.
    my ($handle) = @_;

    # Destroy the DB-handle to prevent db-in-use-errors.
    $handle->storage->disconnect();
    $tb->ok(1, "Destroyed database-handle");

    my $test_config = read_test_config();
    my $dbh = _connect_template($test_config);

    my @mytest = _read_mytest();

    my ($dbname) = $mytest[0] =~ /dbname=([^;]+)/;

    unlink('./.mytest');

    $dbh->do("SELECT pg_terminate_backend(procpid) FROM pg_stat_activity WHERE procpid <> pg_backend_pid() AND datname = \'$dbname\'");
    $dbh->do("DROP DATABASE $dbname");

    $tb->ok(1, "Removed database $mytest[0] and .mytest");

    return;
}

=head2

Expand the default done_testing a bit with a static no warnings check instead
of relying on do_end_test (which only runs in some cases).

=cut

sub zs_done_testing {
    Test::NoWarnings::had_no_warnings();
    $Test::NoWarnings::do_end_test = 0;
    $tb->done_testing(@_);
}

sub read_test_config {
    my $config = eval {
        Config::Auto::parse('zaaksysteem-test.conf', path => ['./etc', '/etc/zaaksysteem/']);
    };
    if($@) {
        $tb->BAIL_OUT("zaaksysteem-test.conf not found");
    }

    return check(
        {
            db_host              => {required => 0},
            db_user              => {required => 1},
            db_password          => {required => 1},
            create_db_user       => {required => 1},
            create_db_password   => {required => 1},
            filestore_location   => {required => 1},
            filestore_test_file_path => {required => 1},
            filestore_test_file_name => {required => 1},
        },
        $config,
    );
}

sub _connect_template {
    my $args = shift;

    # Assumptions :)
    my $dsn = "dbi:Pg:dbname=template1";
    $dsn .= ";host=$args->{db_host}" if defined $args->{db_host};

    return DBI->connect(
        $dsn,
        $args->{create_db_user},
        $args->{create_db_password},
        {
            RaiseError => 1,
            PrintError => 0,
            AutoCommit => 1,
        },
    );
}

1;
