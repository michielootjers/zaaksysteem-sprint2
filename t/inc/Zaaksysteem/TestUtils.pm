package Zaaksysteem::TestUtils;

use warnings;
use strict;

use Moose;
use Params::Check qw(check);
use Zaaksysteem::Schema;

use base 'Test::Builder::Module';
my $tb = __PACKAGE__->builder;

has schema => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Schema',
    required => 1,
);

has config => (
    is       => 'ro',
    lazy     => 1,
    default  => sub { return TestSetup::read_test_config() },
);

=head2

Wrapper for a database-transaction so you can add a description and
don't have to type rollback every time.

=cut

sub zs_transaction_ok ($;&;$) {
    my ($self, $coderef, $description) = @_;

    use Try::Tiny;
    try {
        my $t = $self->schema->storage->txn_do(
            sub {
                $coderef->();
                $self->schema->txn_rollback();
                return 1;
            }
        );
        return $tb->ok($t, $description);
    } catch {
        my $error = shift;
        die $error;
    }
    
}

=head2 get_case_ok

Get a static case from the database.

=cut

sub get_case_ok {
    my $self = shift;
    my $case = $self->schema->resultset('Zaak')->single;
    $tb->ok($case, "Found case: $case");
    return $case;
}

=head2 get_case_type_ok

Gets a static database case type. If no case type is found, create one.

=cut

sub get_case_type_ok {
    my $self = shift;
    my $zt = $self->schema->resultset('Zaaktype')->single;
    $tb->ok($zt, "Found zaaktype: $zt");
    return $zt;
}

=head2 $zs->get_subject_ok

Generates a random subject string.

=cut

sub get_subject_ok {
    my @types = ('medewerker', 'natuurlijk_persoon');
    my $type  = $types[ rand @types ];
    my $id    = int(rand(10000));
    my $subject = "betrokkene-$type-$id";
    $tb->ok($subject, "Generated subject: $subject");
    return $subject;
}

=head2 empty_filestore_ok

Clean up the file store, deleting all files within it.

=cut

sub empty_filestore_ok {
    my $self = shift;
    require File::Path;
    my $rm = File::Path::rmtree($self->config->{'filestore_location'});
    $tb->ok($rm, "Emptied ".$self->config->{'filestore_location'});
    return 1;
}

=head2 create_file_ok

Create a file entry.

=cut

sub create_file_ok {
    my $self = shift;

    my $result = $self->schema->resultset('File')->file_create({
        db_params => {
            created_by    => $self->get_subject_ok,
            case_id       => $self->get_case_ok->id,    
        },
        name      => $self->config->{'filestore_test_file_name'},
        file_path => $self->config->{'filestore_test_file_path'},
    });
    $tb->ok($result, "Created file: $result");
    return $result;
}

=head2 create_filestore_ok

Create a filestore entry.

=cut

sub create_filestore_ok {
    my $self = shift;

    my $result = $self->schema->resultset('Filestore')->filestore_create({
        file_path     => $self->config->{filestore_test_file_path},
        original_name => $self->config->{filestore_test_file_name},
    });
    $tb->ok($result, "Created filestore: $result");
    return $result;
}

=head2 create_directory_ok

Create a directory entry.

=cut

sub create_directory_ok {
    my $self = shift;
    my $case        = $self->get_case_ok;
    my @valid_chars = ('A'..'z', 'a'..'z', 0..9);
    my $rand_string;
    $rand_string .= $valid_chars[rand @valid_chars] for 8..30;

    my $result = $self->schema->resultset('Directory')->directory_create({
        case_id => $case->id,
        name    => $rand_string,
    });
    $tb->ok($result, "Created directory: $result");
    return $result;
}

=head2 get_zaaktype_kenmerk_ok($schema)

Get a zaaktype kenmerk.

=cut

sub get_zaaktype_kenmerk_ok {
    my $self = shift;
    my $result = $self->schema->resultset('ZaaktypeKenmerken')->search(
        {}, {rows => 1}
    )->single;
    $tb->ok($result, "Found zaaktype kenmerk: $result");
    return $result;
}

=head2 get_ustore_ok

Get a Ustore object for testing.

=cut

sub get_ustore_ok {
    my $self = shift;
    my $store = File::UStore->new(
        path     =>  $self->config->{filestore_location},
        prefix   => 'zs_',
        depth    => 5,
    );
    $tb->ok($store, "Created store: $store");
    return $store;
}

=head2 get_case_type_document_ok($schema)

Look up a case type document and return it.

=cut

sub get_case_type_document_ok {
    my $self = shift;
    my $ctd = $self->schema->resultset('ZaaktypeKenmerken')->search(
        {
            value_type => 'file',
        },
        {
            join => 'bibliotheek_kenmerken_id'
        }
    );
    $tb->ok($ctd, "Found case type document: $ctd");
    return $ctd->first;
}

=head2 create_contactmoment_note_ok($schema)

Create a contactmoment entry of type note.

=cut

sub create_contactmoment_note_ok {
    my $self = shift;

    my @valid_chars = ('A'..'z', 'a'..'z', 0..9, ' ');
    my $message;
    $message .= $valid_chars[rand @valid_chars] for 20..60;

    my $result = $self->schema->resultset('Contactmoment')->contactmoment_create({
        type       => 'note',
        subject_id => $self->get_subject_ok,
        created_by => $self->get_subject_ok,
        medium     => 'balie',
        case_id    => $self->get_case_ok,
        message    => $message,
    });
    $tb->ok($result, "Created note contactmoment: $result");
    return $result;
}


1;

__END__
