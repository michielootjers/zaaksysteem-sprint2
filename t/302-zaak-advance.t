#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;

my $zs = bless {schema => connect_test_db_ok()}, 'Zaaksysteem::TestUtils';
my $schema = $zs->schema;
### Test header end


$zs->zs_transaction_ok(sub {
    my $case    = $zs->get_case_ok;

    ok (
        $case->is_document_queue_empty,
        'is_document_queue_empty: can advance, no documents are in queue'
    );

    my $result = $schema->resultset('File')->file_create({
        db_params => {
            created_by              => $zs->get_subject_ok,
            case_id                 => $case->id,
        },
        file_path               => $zs->config->{filestore_test_file_path},
        name                    => 'case_type_document.txt',
    });

    ok (
        !$case->is_document_queue_empty,
        'is_document_queue_empty: cannot advance, documents are in queue'
    );
}, 'Document advance checking');


TODO: {
    local $TODO = 'Because there is no betrokkene in the test suite, this test cannot be done yet.';
    note 'is_document_complete';
}


$zs->zs_transaction_ok(sub {
    my $case    = $zs->get_case_ok;

    ok (
        !(scalar keys %{ $case->required_subcases_unfinished }),
        'required_subcases_unfinished: can advance, no related cases started'
    );
}, 'Childcase advance checking');


zs_done_testing();
