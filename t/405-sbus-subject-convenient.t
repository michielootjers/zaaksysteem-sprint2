#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;

my $zs = bless {schema => connect_test_db_ok()}, 'Zaaksysteem::TestUtils';
my $schema = $zs->schema;
### Test header end

use Cwd;
use File::Basename;
use Zaaksysteem::Log::CallingLogger;

BEGIN { use_ok('Zaaksysteem::SBUS'); }

my $log         = Zaaksysteem::Log::CallingLogger->new;
my $sbus        = Zaaksysteem::SBUS->new(
    config  => {
        home        => getcwd,
        default_ontvanger => "CMG",
        default_zender    => "ZSNL",
        test_transport    => 1,
    },
    log             => $log,
    schema          => $schema,
    die_on_error    => 1,
);
my $bsn = '987654321';


$zs->zs_transaction_ok(sub {
    throws_ok(
        sub {
            $sbus->subject_search();
        },
        qr/requires a Parameter HASHREF/,
        'subject_search validation error: required parameter'
    );

    my $search_params = $sbus->_subject_search_proper_params(
        {
            burgerservicenummer => '123456',
            'a_nummer'          => '55',
            voorletters         => 'A',
        }
    );

    ok(defined($search_params->{ $_ }), 'Found search key: ' . $_)
        for qw/bsn-nummer a-nummer voorletters/;

    is($search_params->{voorletters}, 'A', 'Proper voorletter found');
    is($search_params->{'a-nummer'}, '55', 'Proper a-nummer found');
}, 'SBUS: Subject retrieval checks');


$zs->zs_transaction_ok(sub {
    my $subjects    = $sbus->subject_search(
        {
            system_of_record_id => '28237237283',
        }
    );

    ok(
        (
            UNIVERSAL::isa($subjects, 'ARRAY') &&
            scalar(@{ $subjects }) == 1
        ),
        , 'Found 1 subject'
    );

    $subjects    = $sbus->subject_search(
        {
            geslachtsnaam   => 'Jansen',
        }
    );

    ok(
        (
            UNIVERSAL::isa($subjects, 'ARRAY') &&
            scalar(@{ $subjects }) == 3
        ),
        , 'Found 3 subjects'
    );
}, 'SBUS: Subject retrieval');


$zs->zs_transaction_ok(sub {
    my $is_activated    = $sbus->subject_activate(
        {
            system_of_record_id => '28237237283',
            #burgerservicenummer => $bsn,
        }
    );

    ok($is_activated, 'Activated subject');
}, 'SBUS: Subject activation (indicatorAfnemerIndicatie)');

SKIP: {
    skip 'Subject-related tests currently do not function.', 17;
    $zs->zs_transaction_ok(sub {
        throws_ok(sub {
               $sbus->subject_import_and_activate(
                    {
                        geslachtsnaam => 'Jansen',
                    }
                );
            }, qr/Missing parameters/,
            'Check validation'
        );

        my $rv;
        ok($rv = $sbus->subject_import_and_activate(
            {
                system_of_record_id => '28237237283',
            }
        ), 'Succesfully imported subject');

        ok($schema->resultset('NatuurlijkPersoon')->search(
            {
                system_of_record_id => $rv->system_of_record_id,
            }
        )->count, 'Found subject by system_of_record_id');

        my $log = $schema->resultset('Logging')->search(
            {},
            {
                order_by    => { -desc  => 'id' }
            }
        )->first;

        my $sorid = $rv->system_of_record_id;
        like($log->onderwerp, qr/$sorid/, 'Corect logline set');
    }, 'SBUS: Subject import AND activation (indicatorAfnemerIndicatie)');


    $zs->zs_transaction_ok(sub {
        my $subjects    = $sbus->subject_search(
            {
                burgerservicenummer => $bsn,
            }
        );

        my $subject = shift( @{ $subjects });
        ok(
            $sbus->subject_import($subject),
            'Import subject'
        );

        my $np = $schema->resultset('NatuurlijkPersoon')->search(
            {
                burgerservicenummer => $bsn
            }
        )->first;

        ok($np, 'Found added subject');

        for my $col (
            qw/
                a_nummer
                burgerservicenummer
                voorletters
                voornamen
                voorvoegsel
                geslachtsnaam
                geslachtsaanduiding
                datum_overlijden
                indicatie_geheim
                burgerlijke_staat
                aanduiding_naamgebruik
            /
        ) {
            $subject->{ $col } = undef unless $subject->{ $col };
            is($np->$col, $subject->{ $col }, 'Found identical col: ' . $col);
        }


        throws_ok(sub {
               $sbus->subject_import($subject)
            }, qr/Subject already imported/,
            'Subject already imported'
        );
    }, 'SBUS: Subject import AND activation (indicatorAfnemerIndicatie), validation');


    $zs->zs_transaction_ok(sub {
        my $is_deactivated    = $sbus->subject_deactivate(
            {
                burgerservicenummer => $bsn,
            }
        );

        ok($is_deactivated, 'Deactivated subject');
    }, 'SBUS: Subject de-activation (indicatorAfnemerIndicatie)');


    $zs->zs_transaction_ok(sub {
        throws_ok(sub {
                my $is_deactivated    = $sbus->subject_remove_and_deactivate(
                    {
                        burgerservicenummer => $bsn,
                    }
                );
            },
            qr/Record not found in database/,
            'Do not remove indicatorAfnemerIndicatie when row doesn\'t exist'
        );

        my $subjects    = $sbus->subject_search(
            {
                burgerservicenummer => $bsn,
            }
        );

        my $subject = shift( @{ $subjects });

        ok(
            $sbus->subject_import($subject),
            'Import subject'
        );

        my $is_deactivated    = $sbus->subject_remove_and_deactivate(
            {
                burgerservicenummer => $bsn,
            }
        );

        ok($is_deactivated, 'Removed and deactivated subject');
    }, 'SBUS: Subject remove AND de-activation (indicatorAfnemerIndicatie)');
}

zs_done_testing();
