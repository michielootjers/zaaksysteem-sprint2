#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;

my $zs = bless {schema => connect_test_db_ok()}, 'Zaaksysteem::TestUtils';
my $schema = $zs->schema;
### Test header end


$zs->zs_transaction_ok(sub {
    my $file = $zs->create_file_ok;
    my $name      = '!@&#-%^$_';
    my $subject   = $zs->get_subject_ok;

    my ($result) = $file->update_properties({
        name     => $name,
        accepted => 1,
        subject  => $subject,
    });
    ok $result, "Updated $result";
    is $result->name, $name, 'Name as expected';
    is $result->accepted, 1, 'Accepted is true';
    is $result->modified_by, $subject, 'Correct modified subject';
    is $result->date_modified, DateTime->now, 'Correct modified date';
}, 'update_properties');


$zs->zs_transaction_ok(sub {
    my $file = $zs->create_file_ok;

    throws_ok sub {
        $file->update_properties({
            subject  => $zs->get_subject_ok,
            accepted => 'DIT MAG NIET',
            case_id  => undef,
        });
    }, qr/Invalid options given: accepted/, 'invalid options fail';
}, 'update_properties with invalid options');


$zs->zs_transaction_ok(sub {
    my $file = $zs->create_file_ok;

    ok $file->update({date_deleted => DateTime->now}), 'Set deleted on file';

    throws_ok sub {
        $file->update_properties({
            subject  => $zs->get_subject_ok,
            case_id => undef,
        });
    }, qr/Cannot alter a deleted/, 'updating deleted fails';
}, 'update_properties on deleted file');


$zs->zs_transaction_ok(sub {
    my $file      = $zs->create_file_ok;
    my $directory = $zs->create_directory_ok;

    $file->update({directory => $directory});
    $file->update({case_type_document_id => $zs->get_case_type_document_ok->id});
    
    ok $file->directory, 'File has directory set';
    ok $file->case_type_document_id, 'case_type_document_id is set';

    my $subject = $zs->get_subject_ok;
    my $result  = $file->update_properties({
            subject  => $subject,
            deleted  => 1,
    });
    ok  $result, 'Updated file';
    is  $result->date_deleted, DateTime->now, 'Deleted date set';
    is  $result->deleted_by, $subject, 'Correct subject';
    ok !$result->directory_id, 'Directory unset';
    ok !$result->case_type_document_id, 'Case type document ID unset';
}, 'update_properties delete file');


$zs->zs_transaction_ok(sub {
    my $file    = $zs->create_file_ok;
    my $subject = $zs->get_subject_ok;
    my $ctd     = $zs->get_case_type_document_ok;

    ok $ctd->update({
        pip => 0,
        publish_public => 1,
    }), 'PIP publish false, public publish true';

    ok $file->update({
        publish_pip => 1,
        publish_website => 0,
    }), 'Made sure PIP and website are opposite of CTD default';

    $file->update_properties({
        subject               => $subject,
        case_type_document_id => $ctd->id
    });
    is $file->case_type_document->id, $ctd->id, 'Case type document set';
    ok $file->publish_website, 'Publish website true';
    ok !$file->publish_pip, 'Publish PIP false';
}, 'update_properties set case_type_document');


$zs->zs_transaction_ok(sub {
    my $file    = $zs->create_file_ok;
    my $subject = $zs->get_subject_ok;
    my $ctd     = $zs->get_case_type_document_ok;

    ok $ctd->update({
        pip => 0,
        publish_public => 1,
    }), 'PIP publish false, public publish true';

    ok $file->update({
        publish_pip => 1,
        publish_website => 0,
    }), 'Made sure PIP and website are opposite of CTD default';

    $file->update_properties({
        subject     => $subject,
        publish_pip => 1,
        case_type_document_id => $ctd->id
    });

    ok $file->publish_website, 'Publish website true';
    ok $file->publish_pip, 'Publish PIP true';
}, 'update_properties set case_type_document - respect overrides for defaults');


$zs->zs_transaction_ok(sub {
    my $file    = $zs->create_file_ok;
    my $subject = $zs->get_subject_ok;
    my $ctd     = $zs->get_case_type_document_ok;

    $file->update_properties({
        subject               => $subject,
        case_type_document_id => $ctd->id
    });
    is $file->case_type_document->id, $ctd->id, 'Case type document set';

    my $second_file = $zs->create_file_ok;

    $second_file->update_properties({
        subject               => $subject,
        case_type_document_id => $ctd->id,
    });
    is $second_file->case_type_document->id, $file->case_type_document->id,
        'Assiging same CTD to other file works';
}, 'update_properties set case_type_document on existing');


$zs->zs_transaction_ok(sub {
    my $file    = $zs->create_file_ok;
    my $subject = $zs->get_subject_ok;
    my $ctd     = $zs->get_case_type_document_ok;

    $file->update_properties({
        subject               => $subject,
        case_type_document_id => $ctd->id
    });
    is $file->case_type_document->id, $ctd->id, 'Case type document set';

    my $second_file = $zs->create_file_ok;

    $second_file->update_properties({
        subject               => $subject,
        case_type_document_id => $ctd->id,
        case_type_document_clear_old => $file->id,
    });
    ok !$file->discard_changes->case_type_document, 'Cleared CTD on first file';
    ok $second_file->case_type_document, 'Second file has CTD set';
}, 'update_properties set case_type_document on existing');


$zs->zs_transaction_ok(sub {
    my $file = $zs->create_file_ok;

    $file->update({case_type_document_id => $zs->get_case_type_document_ok->id});
    ok $file->case_type_document_id, 'case_type_document_id is set';

    my $subject = $zs->get_subject_ok;
    my $result  = $file->update_properties({
        subject               => $subject,
        case_type_document_id => undef,
    });
    ok  $result, 'Updated file';
    ok !$result->case_type_document_id, 'Case type ID unset';
}, 'update_properties unset case_type_document');


$zs->zs_transaction_ok(sub {
    my $file      = $zs->create_file_ok;
    my $directory = $zs->create_directory_ok;

    $file->update({directory => $directory});
    ok $file->directory, 'File has directory set';

    my $subject = $zs->get_subject_ok;
    my $delete  = $file->update_properties({
            subject  => $subject,
            deleted  => 1,
    });
    ok  $delete, 'Updated file to deleted';

    # Let a different subject restore
    my $second_subject = $zs->get_subject_ok;
    my $undelete  = $file->update_properties({
            subject  => $second_subject,
            deleted  => 0,
    });
    ok  $undelete, 'Updated file to not deleted';
    is  $undelete->date_deleted, undef, 'Deleted date removed';
    is  $undelete->deleted_by, undef, 'Deleted by undefined';
    ok !$undelete->directory_id, 'Directory still unset';
    is  $undelete->modified_by, $second_subject, 'Correct subject in modified_by';
}, 'update_properties restore file');


$zs->zs_transaction_ok(sub {
    my $file      = $zs->create_file_ok;
    my $directory = $zs->create_directory_ok;

    my $result = $file->update_properties({
        subject      => $zs->get_subject_ok,
        directory_id => $directory->id,
    });
    ok $result, 'Updated file';
    is $result->directory->id, $directory->id, 'Set proper directory';
}, 'update_properties add directory');


$zs->zs_transaction_ok(sub {
    my $file      = $zs->create_file_ok;
    my $directory = $zs->create_directory_ok;

    $file->update({directory_id => $directory});

    ok $file->directory, 'File has directory set';
    my $result = $file->update_properties({
        subject      => $zs->get_subject_ok,
        directory_id => undef,
    });
    ok $result, 'Updated file';
    is $result->directory_id, undef, 'No directory defined';
}, 'update_properties remove directory');


$zs->zs_transaction_ok(sub {
    my $file   = $zs->create_file_ok;
    my $result = $file->update_properties({
        subject   => $zs->get_subject_ok,
        accepted  => 1,
    });
    ok $result, 'Updated file';
    is $result->accepted, 1, 'Accepted file';
}, 'update_properties accept file');


$zs->zs_transaction_ok(sub {
    my $file   = $zs->create_file_ok;
    ok $file->update({publish_pip => 1}), 'Set publish_pip to true';
    my $result = $file->update_properties({
        subject   => $zs->get_subject_ok,
        accepted  => 1,
    });
    ok $result, 'Updated file';
    is $result->accepted, 1, 'Accepted file';
}, 'update_properties accept file from PIP');


$zs->zs_transaction_ok(sub {
    my $file   = $zs->create_file_ok;
    my $reason = "BECAUSE IT'S WRONG";

    ok $file->case_id, 'Has case ID set';

    my $result = $file->update_properties({
        subject   => $zs->get_subject_ok,
        accepted  => 0,
        rejection_reason => "BECAUSE IT'S WRONG",
    });
    ok $result, 'Updated file';
    is $result->rejection_reason, $reason, 'Rejection reason set';
    ok $result->case_id, 'Case ID not removed';
    ok $result->date_deleted, 'Deleted set';
    is $result->accepted, 0, 'Accepted set to false';
}, 'update_properties reject file');


$zs->zs_transaction_ok(sub {
    my $file   = $zs->create_file_ok;
    my $reason = 'PIP reject';
    my $prep = $file->update({
        publish_pip           => 1,
        case_type_document_id => $zs->get_case_type_document_ok,
        directory_id          => $zs->create_directory_ok,
    });
    ok $prep->publish_pip, 'publish_pip is true';
    ok $prep->case_type_document, 'Case type document is set';
    ok $prep->directory, 'Directory is set';

    my $result = $file->update_properties({
        subject   => $zs->get_subject_ok,
        accepted  => 0,
        rejection_reason => $reason,
    });
    ok $result, 'Updated file';
    is $result->rejection_reason, $reason, 'Reason matches';
    is $result->case_id->id, $file->case->id, 'Case ID not removed';
    is $result->date_deleted, DateTime->now, 'File has deleted set';
    ok !$result->case_type_document, 'Case type document removed';
    ok !$result->directory, 'Directory removed';
}, 'update_properties reject file from PIP');


$zs->zs_transaction_ok(sub {
    my $file   = $zs->create_file_ok;

    # By default a file doesn't get rejected to queue
    ok $file->update({reject_to_queue => 1}), 'Set reject_to_queue true';
    throws_ok sub {
        $file->discard_changes->update_properties({
            subject  => $zs->get_subject_ok,
            accepted => 0,
        })
    }, qr/Rejection reason required/, 'Declined file without reason';
}, 'update_properties reject file without reason');


$zs->zs_transaction_ok(sub {
    my $file = $zs->create_file_ok;
    my $case = $zs->get_case_ok;
    ok $file->update({case_id => undef}), 'Case ID unset';

    my $update = $file->update_properties({
        subject  => $zs->get_subject_ok,
        case_id  => $case->id,
    });
    ok $update, 'Updated file';
    is $update->case->id, $case->id, 'Has case set';
}, 'update_properties assign case');


$zs->zs_transaction_ok(sub {
    my $file = $zs->create_file_ok;
    my $case = $zs->get_case_ok;

    ok !$file->metadata, 'File does not have metadata yet';

    my $update = $file->update_properties({
        subject  => $zs->get_subject_ok,
        metadata => {
            description => 'Arrrr',
            document_category => 'Spluf',
        },
    });
    ok $update, 'Updated file';
    is $update->metadata->description, 'Arrrr', 'Description matches';
    is $update->metadata->document_category, 'Spluf', 'Category matches';

    # Update with existing metadata entry
    my $existingupdate = $file->update_properties({
        subject  => $zs->get_subject_ok,
        metadata => {
            description => 'Barrrr',
        },
    });
    is $update->metadata->description, 'Barrrr', 'Metadata got updated';
}, 'update_properties set metadata');


$zs->zs_transaction_ok(sub {
    my $file = $zs->create_file_ok;
    my $case = $zs->get_case_ok;

    ok $file->case_id, 'Has case ID set';

    throws_ok sub {
        $file->update_properties({
            subject  => $zs->get_subject_ok,
            case_id  => $case->id,
        })
    }, qr/Cannot assign a new case/, 'Unable to assign new case';
}, 'update_properties assign case with existing case');


$zs->zs_transaction_ok(sub {
    my $file = $zs->create_file_ok;
    throws_ok sub {
        $file->update_properties({case_id => $zs->get_case_ok->id})
    }, qr/Missing options: subject/, 'Got missing subject error';
}, 'update_properties without subject parameter');

zs_done_testing();
