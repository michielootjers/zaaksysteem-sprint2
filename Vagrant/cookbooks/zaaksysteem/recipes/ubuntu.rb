#
# Cookbook Name:: zaaksysteem
# Recipe:: ubuntu
#
# Copyright 2013, Example Com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

### Remove stupid GRUB error recordfail
bash "grub_recordfail_turnoff" do
    user "root"
    cwd "/etc/default"
    code <<-EOT
    echo "GRUB_RECORDFAIL_TIMEOUT=0" >> /etc/default/grub
    EOT
    not_if 'grep GRUB_RECORDFAIL_TIMEOUT /etc/default/grub'
end

