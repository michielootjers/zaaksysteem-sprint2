#!/bin/bash

if [ $# -lt "1" ]; then
    echo "USAGE: $0 databasename"
    exit;
fi

DATABASE=$1;

DBFILLED=`echo "SELECT table_name FROM information_schema.tables WHERE table_schema = 'public'; " | psql $DATABASE| grep zaaktype_node`

if [ "$DBFILLED" != "" ]; then
    echo "DATABASE $DATABASE ALREADY LOADED";
    exit 1;
fi
