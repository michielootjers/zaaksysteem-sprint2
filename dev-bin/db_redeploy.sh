#!/bin/sh

DB_NAME=${ZS_DB_NAME}
ROOT_DIR=$(dirname $0)'/../'

# Check DBIx::Class version
perl -e 'use DBIx::Class 0.08196'

echo "Assuming path to root of Zaaksysteem is '${ROOT_DIR}'"

if [ -z ${IS_DEV} ]; then
    echo "This script can only run on a development box. Set IS_DEV=1 if you are."
    exit;
fi

if [ -z ${DB_NAME} ]; then
    echo "This script needs a database name. Either set ZS_DB_NAME in your environment, or modify this script."
    exit;
fi

echo "Using database '${DB_NAME}'"

${ROOT_DIR}/script/zaaksysteem_create.pl model DB DBIC::Schema Zaaksysteem::Schema \
    create=static \
    components=InflateColumn::DateTime,TimeStamp \
    skip_load_external=1 \
    dbi:Pg:dbname=${DB_NAME}

rm ${ROOT_DIR}/lib/Zaaksysteem/Model/DB.pm.new
rm ${ROOT_DIR}/t/model_DB.t
