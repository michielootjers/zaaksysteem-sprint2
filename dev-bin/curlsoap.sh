#!/bin/sh

if [ "$1" = "" ]; then
    echo "USAGE $0 xmlfile.xml";
    exit
fi
curl -kvv --data-binary @$1  -H "SOAPAction: {http://www.egem.nl/StUF/StUF0204}ontvangKennisgeving" -H "Content-Type: text/xml;charset=UTF-8" http://10.44.0.11/api/stuf/bg0204
